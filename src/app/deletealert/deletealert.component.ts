import { Component, OnInit } from '@angular/core';
import { DeletealertService } from '../service/deletealert.service';

@Component({
  selector: 'app-deletealert',
  templateUrl: './deletealert.component.html',
  styleUrls: ['./deletealert.component.scss']
})
export class DeletealertComponent implements OnInit {

  message: any;

  constructor(
    private deletealertService: DeletealertService
  ) { }

  ngOnInit() {
    this.deletealertService.getMessage().subscribe(message => {
      this.message = message;
    });
  }

}
