import { Component, OnInit, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Options, LabelType, ChangeContext } from 'ng5-slider';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';
import { DashboardService } from '../service/dashboard.service';
import { CropmasterService } from '../service/cropmaster.service';
import { LocalstorageService } from '../service/localstorage.service';
import { CommonService } from '../service/common.service';
import { AlertService } from '../service/alert.service';
import { TransferserviceService } from '../service/transferservice.service';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { DatePipe } from '@angular/common';
import { AuthService } from '../service/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { MastersService } from '../service/masters.service';
import { min } from 'moment';
import { DebugHelper } from 'protractor/built/debugger';
import { ExportFieldData } from '../model/export_field_data';
import { DashboardData } from '../model/dashboard_data';

declare var $: any;
declare function setModelValue(crop_name, culture_size, duration,
  start_date, end_date, harvest_date, before, beforeCultureJson, swapped_with_text, swapped_with_name, crop_rotation_rule_violated, is_rule_breaked, editDetails): any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  @Input() value = '';
  @Output() dateChange = new EventEmitter();

  dashboardData$: any;

  dashboardTopBoxesData$: any;
  dashboardTopBoxesSizePerCategoryData$: any;
  dashboardFooterTextData$: [];
  fieldCultures$: any;
  fieldByIndex$: any;
  fieldDetailsByIdIndex$: any;
  serverError$: any;
  dashboardDataError = false;
  fullView = true;
  singleView = false;
  cultureMinDateObj: Date;
  cultureMaxDateObj: Date;
  cultureSizeIssue = false;

  addFieldForm: FormGroup;
  fieldErrors$: any;
  invalidRegisterField = false;
  addCultureForm: FormGroup;
  fieldCultureErrors$: any;
  fieldCultureWarning$: any;
  invalidRegisterFieldCulture = false;
  allCrops$: any;
  mostPlantedCrops$: any;
  MostPlantedCropIds$: any;
  LessPlantedCrops$: any;
  CultureYears$: any;
  fieldPlannedCrops$: any;
  fieldModal = 'none';
  cultureModal = 'none';
  deleteCulture = 'none';
  exportModal = 'none';
  userFields: any;
  userExports: any;
  exportForm: FormGroup;
  slideTimer;
  rangeInput: object;
  filtered = false;
  dataDuration = 366;
  tableMonths: any;
  emptyDatesArr: any;
  emptyDatesArrLength = 0;
  fullViewSlidervalue: number;
  fullViewSlidermaxValue: number;
  fullViewSlideroptions: Options;
  singleViewSlidervalue: number;
  singleViewSlidermaxValue: number;

  singleViewMinDate: any;
  singleViewMaxDate: any;

  masterFertilizeDate: any;
  masterPlantProDate: any;
  masterSoilDate: any;

  soilSampleDate: any;

  singleViewSlideroptions: Options;
  singleViewYearShown: number;
  math: any;
  dateRangeInputErrorMsg = '';
  dateRangeToMin: Date;
  dateRangeToMax: Date;
  showArchieve = false;
  showSwappedWith = false;
  showHideEchoSize = false;
  fullViewMonthShow = '';
  dateRangeFromErrorMsg = '';
  dateRangeToErrorMsg = '';
  ExportReportingDateErrorMsg = '';
  fullViewMonthNo: number;
  showHideSwappedWith = false;
  name: string;
  addEditText = 'Create New';
  addEditTextCultre = "";
  currentDate: string;
  showRemainingSpace = false;
  remainingSpace = 0;
  cropSizePerCategoryYear: any;
  sizePerCategoryMoreLessText = 'More';
  sizePerCategoryMoreLessShowHide = false;
  singleViewFieldIndex = 0;
  suggestionModal = 'none';
  mobileDetailedRow: any;

  isError = false;
  serverError: any;
  isErrorfname = false;
  isErrorfsize = false;
  errorMessage = '';
  ecoSizeIssue = false;
  isSuccess = false;
  successMessage = '';

  isSubmitted = false;
  isLoading = false;
  isLoadingDashboard = false;
  subscriptionId: any;
  weeks: any;
  warningPerioEndModel = 'none';
  endDays = 0;
  alertPerioEndModel = 'none';
  UserFullName: '';
  StreetName: '';
  PostalCode: '';
  City: '';
  PhoneNumber: '';
  Country: '';
  isExpired = false;
  cropRotationTitle = "Crop Rotation Rules are Breaked!";
  EditField = "Edit Field";
  FieldInformation = "Field Information"

  mastersModal = 'none';
  plantActivityForm: FormGroup;
  allPesticide$: any;
  allMachine$: any;
  allPests$: any;
  cultureEndDate: '';
  cultureSizeMaster: any;
  allPlantActivity$: any;
  soilActivityForm: FormGroup;
  allSoil$: any;
  allSoilActivity$: any;
  comment: '';
  cultureIdMaster: any;
  fieldIdMaster: any;
  userIdMaster: any;
  hactareConversion = 100;
  exampleModal = 'none';

  fertiActivityForm: FormGroup;
  allFertilizer$: any;
  disabledField = '';
  allFertilizerActivity$: any;
  corr_factor: any;
  archieveDeleteShow = false;
  showYearList = false;
  yearList$: any;
  showOtherCrops = true;
  fieldSaveShow = true;
  soilShowOtherCrops = true;
  plantShowOtherCrops = true;
  //  Archiveornotarchive="fields.archieve == 0 ? 'Archive' : 'Unarchive'"
  monthArr = [];
  jan = "";
  feb = "";
  mar = "";
  apr = "";
  may = "";
  jun = "";
  july = "";
  aug = "";
  sep = "";
  oct = "";
  nov = "";
  dec = "";
  DatePickerOptions: DatepickerOptions;
  IsMobile: boolean;
  monthList: any;
  weekList: any;
  cropMultiList$: any;
  cropMultiListSoil$: any;
  cropMultiListPlant$:any;
  groupFields: Object = { groupBy: 'feild_name', text: 'crop_detail', value: 'ac_cultureId' };

  currentPage: number = 1;
  chartLimit: number = 3;
  IsAllLoaded: boolean = false;
  IsLoading: boolean = false;
  IsSelection: boolean = false;
 
  removedFieldId = '';
  archivedFieldId = '';
  unArchivedFieldId = '';
 
  plantMultiple: any[] = [{
    pesticide_id: '',
    amount: '',
    unit: '',
    total_amount: '',
    waiting_period: '',
    pests_id: '',
    end_date: ''
  }];

  constructor(
    private dashboard: DashboardService,
    private router: Router,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private cropmaster: CropmasterService,
    private storage: LocalstorageService,
    private alertService: AlertService,
    private common: CommonService,
    private transfereService: TransferserviceService,
    private translate: TranslateService,
    private deviceDetector: DeviceDetectorService,
    private master: MastersService,

  ) {
    this.IsMobile = this.deviceDetector.isMobile();
    //console.log("IsMobile - ", this.IsMobile);

    this.DatePickerOptions = {
      displayFormat: 'DD.MM.YYYY',
    };

    this.math = Math;
    this.translate.get("show_more").subscribe((result: string) => {
      this.sizePerCategoryMoreLessText = result;
    });
    this.translate.get("jan").subscribe((result: string) => {
      this.jan = result;
      this.setMonths();
    });
    this.translate.get("feb").subscribe((result: string) => {
      this.feb = result;
      this.setMonths();
    });
    this.translate.get("mar").subscribe((result: string) => {
      this.mar = result;
      this.setMonths();
    });
    this.translate.get("apr").subscribe((result: string) => {
      this.apr = result;
      this.setMonths();
    });
    this.translate.get("may").subscribe((result: string) => {
      this.may = result;
      this.setMonths();
    });
    this.translate.get("jun").subscribe((result: string) => {
      this.jun = result;
      this.setMonths();
    });
    this.translate.get("july").subscribe((result: string) => {
      this.july = result;
      this.setMonths();
    });
    this.translate.get("aug").subscribe((result: string) => {
      this.aug = result;
      this.setMonths();
    });
    this.translate.get("sep").subscribe((result: string) => {
      this.sep = result;
      this.setMonths();
    });
    this.translate.get("oct").subscribe((result: string) => {
      this.oct = result;
      this.setMonths();
    });
    this.translate.get("nov").subscribe((result: string) => {
      this.nov = result;
      this.setMonths();
    });
    this.translate.get("dec").subscribe((result: string) => {
      this.dec = result;
      this.setMonths();
    });

    this.translate.get("create_new_field").subscribe((result: string) => {
      this.addEditText = result;
    });
    this.translate.get("add_new_culture").subscribe((result: string) => {
      this.addEditTextCultre = result;
    });

  }

  ngOnInit() {
    this.isLoading = true;
    this.addFieldForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      size: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      echo: [''],
      echoSize: [''],
      address: ['', Validators.maxLength(300)],
      city: ['', [Validators.maxLength(100), Validators.pattern('[A-Za-zÀ-ÿ]+')]],
      zipcode: ['', [Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      notes: ['', Validators.maxLength(500)],
      fieldId: [''],

      nitrogen: ['', [Validators.maxLength(10), Validators.pattern('[0-9 .]*')]],
      phosphorus: ['', [Validators.maxLength(10), Validators.pattern('[0-9 .]*')]],
      potassium_oxide: ['', [Validators.maxLength(10), Validators.pattern('[0-9 .]*')]],
      magnesium: ['', [Validators.maxLength(10), Validators.pattern('[0-9 . ]*')]],
      soil_sample_date: [''],
      ph_value: [''],
      size_change_year: [''],
      old_field_size: [''],
      old_echo_size: ['']
    }, { validator: this.fieldCustomValidation() });

    this.addCultureForm = this.formBuilder.group({
      delicate: [''],
      dateFrom: ['', [Validators.required]],
      dateTo: [''],
      harvest_date: ['', [Validators.required]],
      cultivatedArea: ['', [Validators.required, Validators.maxLength(10)]],
      fieldId: [''],
      cultureId: [''],
      isSwapped: [''],
      swappedWith: [''],
      plantVariety: ['', Validators.maxLength(500)],
      notes: ['', Validators.maxLength(500)],
      seed_volume: [''],
      harvest_volume: [''],

    }, { validator: this.customValidation() });

    this.exportForm = this.formBuilder.group({
      fields: new FormArray([], this.minSelectedCheckboxes(1)),
      pdfType: ['standard', Validators.required],
      startDate: [''],
      endDate: [''],
      fieldIds: [''],
      language: [''],
    });

    this.plantActivityForm = this.formBuilder.group({
      plant_date: ['', [Validators.required]],

      pesticide_id_0: ['', [Validators.required]],
      amount_0: ['', [Validators.required]],
      unit_0: ['', [Validators.required]],
      waiting_period_0: ['', [Validators.required]],
      total_amount_0: ['', [Validators.required]],
      pests_id_0: ['', [Validators.required]],
      end_date_0: [''],
      waiting_time_0: [''],
      all_total_amount_0: [''],
      unit_with_ha_0: [''],

      machine_id: ['', [Validators.required]],
      person_name: ['', [Validators.required]],

      comment: [''],
      plant_protection_id: [''],
      field_id: [''],
      culture_id: [''],
      other_crop_ids: ['']

    });

    this.soilActivityForm = this.formBuilder.group({
      soil_date: ['', [Validators.required]],
      soil_id: ['', [Validators.required]],
      machine_id: ['', [Validators.required]],
      person_name: ['', [Validators.required]],
      comment: [''],
      field_id: [''],
      culture_id: [''],
      soil_activity_id: [''],
      other_crop_ids: ['']
    });

    this.fertiActivityForm = this.formBuilder.group({

      fertilizer_date: ['', [Validators.required]],
      fertilizer_id: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      unit: ['', [Validators.required]],
      total_amount: ['', [Validators.required]],

      basic_nitrogen: ['', [Validators.required]],
      basic_phosphorus: ['', [Validators.required]],
      basic_potassium: ['', [Validators.required]],
      basic_magnesium: ['', [Validators.required]],

      corrected_nitrogen: ['', [Validators.required]],
      corrected_phosphorus: ['', [Validators.required]],
      corrected_potassium: ['', [Validators.required]],
      corrected_magnesium: ['', [Validators.required]],

      new_nitrogen: ['', [Validators.required]],
      new_phosphorus: ['', [Validators.required]],
      new_potassium: ['', [Validators.required]],
      new_magnesium: ['', [Validators.required]],

      balance_nitrogen: ['', [Validators.required]],
      balance_phosphorus: ['', [Validators.required]],
      balance_potassium: ['', [Validators.required]],
      balance_magnesium: ['', [Validators.required]],

      machine_id: ['', [Validators.required]],
      person_name: ['', [Validators.required]],
      comment: [''],
      field_id: [''],
      culture_id: [''],
      factor_nitrogen: [''],
      factor_phosphorus: [''],
      factor_potassium: [''],
      factor_magnesium: [''],
      pre_nitrogen: [''],
      pre_phosphorus: [''],
      pre_potassium: [''],
      pre_magnesium: [''],
      old_nitrogen: [''],
      old_phosphorus: [''],
      old_potassium: [''],
      old_magnesium: [''],
      fertilizer_activity_id: [''],
      total_amount_unit: [''],
      other_crop_ids: [''],
      all_crop_amount: [''],
      unit_with_ha: ['']
    });

    this.getServerDate();
    this.getAllCrops();
    this.formControlValueChanged();
    this.exportformControlValueChanged();
    this.plantMultipleField();

    var isFirstLogin = this.storage.get('isFirstLogin');
    if (isFirstLogin == true) {
      localStorage.removeItem('isFirstLogin');
      this.exampleModal = 'block';
      $("#exampleModalId").modal('show');
    }

  }
  plantMultipleField() {

  }

  checkInputValue(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 65 && charCode <= 90) || charCode > 57) {
      return false;
    }
    return true;
  }

  setMonths() {
    this.monthArr = [
      {
        'month': 'January',
        'symbol': this.jan,
      },
      {
        'month': 'Febuary',
        'symbol': this.feb,
      },
      {
        'month': 'March',
        'symbol': this.mar,
      },
      {
        'month': 'April',
        'symbol': this.apr,
      },
      {
        'month': 'May',
        'symbol': this.may,
      },
      {
        'month': 'June',
        'symbol': this.jun,
      },
      {
        'month': 'July',
        'symbol': this.july,
      },
      {
        'month': 'August',
        'symbol': this.aug,
      },
      {
        'month': 'September',
        'symbol': this.sep,
      },
      {
        'month': 'October',
        'symbol': this.oct,
      },
      {
        'month': 'November',
        'symbol': this.nov,
      },
      {
        'month': 'December',
        'symbol': this.dec,
      }
    ];
  }

  Validationfieldname() {
    this.isErrorfname = true;
  }

  ValidationErrorfieldname() {
    this.isErrorfname = false;
  }

  editCulturefromModel() {
    $("#hoverInfoModel").modal('hide');

    this.editCulture(this.mobileDetailedRow.field_id, this.mobileDetailedRow.user_id, this.mobileDetailedRow);
  }

  opemMasterfromModel() {
    $("#hoverInfoModel").modal('hide');
    this.openmasterModal(this.mobileDetailedRow.end_date, this.mobileDetailedRow.harvest_date, this.mobileDetailedRow.field_name, this.mobileDetailedRow.crop_name, this.mobileDetailedRow.culture_size, this.mobileDetailedRow.field_id, this.mobileDetailedRow.culture_id, this.mobileDetailedRow.user_id);
  }

  setTSModelValue(crop_name, culture_size, start_date, end_date, harvest_date, beforeCulture, swappedWith, rule_breaked) {
    if (crop_name == undefined) {
      return;
    }
    
    this.mobileDetailedRow = beforeCulture;
    var duration, before, swapped_with_text, crop_rotation_rule_violated;
    this.translate.get("duration").subscribe((result: string) => {
      duration = result;
    });
    this.translate.get("before").subscribe((result: string) => {
      before = result;
    });
    this.translate.get("swapped_with").subscribe((result: string) => {
      swapped_with_text = result;
    });
    this.translate.get("crop_rotation_rule_violated").subscribe((result: string) => {
      crop_rotation_rule_violated = result;
    });
    var swappedWithreplace = '', beforeCultureJson = [];
    var obj;
    
    for (let index = 0; index < beforeCulture.before_culture_crop_name.length; index++) {
      var  index_end_date;
      var  index_harvest_date;

        if(beforeCulture.before_culture_end_date[index] != null){
          var  index_end_date = beforeCulture.before_culture_end_date[index].replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1");
        }else if(beforeCulture.before_culture_harvest_date[index] != null){
          var  index_end_date =  beforeCulture.before_culture_harvest_date[index].replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1")
        }
        if(beforeCulture.before_culture_harvest_date[index] != null){
          var index_harvest_date = beforeCulture.before_culture_harvest_date[index].replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1");
        }
      obj = {
        size: beforeCulture.before_culture_size[index],
        start_date: beforeCulture.before_culture_start_date[index].replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"),
        end_date:index_end_date,
        harvest_date:index_harvest_date
      };
      beforeCultureJson.push(obj);
    }
    if (swappedWith) {
      swappedWithreplace = swappedWith;
    }
    if (end_date != null) {
      end_date = end_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1");
    }else{
      end_date = harvest_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1");
    }
    setModelValue(crop_name, culture_size, duration,
      start_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"),
      end_date,
      harvest_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"),
      before, beforeCultureJson, swapped_with_text, swappedWithreplace,
      crop_rotation_rule_violated, rule_breaked, beforeCulture
    );
    
  }

  renderDatePicker() {
    this.renderDatepicker('dateFrom', null);
    this.renderDatepicker('dateTo', this.currentDate);
    this.renderDatepicker('harvestDateTo', this.currentDate);

  }

  updatedateTo() {
    const from = this.addCultureForm.controls.dateFrom.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3");
    this.renderDatepicker("dateTo", from);
    this.renderDatepicker("harvestDateTo", from);
  }
  updateendDate() {
    const from = this.addCultureForm.controls.dateFrom.value;
    this.renderDatepicker("endDate", from);
  }

  renderDatepicker(id, minDate) {
    $("#" + id).datepicker("destroy");

    var closeText;
    var prevText;
    var nextText;
    var currentText;
    var monthNames;
    var monthNamesShort;
    var dayNames;
    var dayNamesShort;
    var dayNamesMin;

    var monthKeyArr = ['january', 'feburary', 'march', 'april', 'may', 'june', 'july_full', 'august', 'september', 'october', 'november', 'december'];
    this.monthList = [];
    for (let monthname of monthKeyArr) {
      this.translate.get(monthname).subscribe((result: string) => {
        this.monthList.push(result);
      });
    }

    var weekKeyArr = ['sunday_min', 'monday_min', 'tuesday_min', 'wednesday_min', 'thursday_min', 'friday_min', 'saturday_min'];
    this.weekList = [];
    for (let weekname of weekKeyArr) {
      this.translate.get(weekname).subscribe((result: string) => {
        this.weekList.push(result);
      });
    }

    closeText = "Erledigt";
    prevText = "früher";
    nextText = "Nächster";
    currentText = "heute";
    monthNames = this.monthList;
    monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
    dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    dayNamesShort = ["Son", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    dayNamesMin = this.weekList;

    $("#" + id).datepicker({
      firstDay: 1,
      dateFormat: 'dd.mm.yy',
      closeText: closeText,
      prevText: prevText,
      nextText: nextText,
      currentText: currentText,
      monthNames: monthNames,
      monthNamesShort: monthNamesShort,
      dayNames: dayNames,
      dayNamesShort: dayNamesShort,
      dayNamesMin: dayNamesMin,
      weekHeader: "Sem.",
      minDate: new Date(minDate),
      onSelect: (value) => {
        this.value = value;
        if (id == 'dateFrom') {
          this.addCultureForm.controls['dateFrom'].setValue(this.value);
          this.updatedateTo();
        } else if (id == "startDate") {
          this.addCultureForm.controls['startDate'].setValue(this.value);
          this.updateendDate();
        } else if (id == "endDate") {
          this.addCultureForm.controls['endDate'].setValue(this.value);
        } else if (id == "dateTo") {
          this.addCultureForm.controls['dateTo'].setValue(this.value);
        } else if (id == "harvestDateTo") {
          this.addCultureForm.controls['harvest_date'].setValue(this.value);
        }
        this.getFieldRemainingSpaceForDuration();
      },
      beforeShow: function () {
        if (id == "dateFrom") {
          $("#modalBodyCulture").scrollTop(10);
        } else {
          $("#modalBodyCulture").scrollTop(95);
        }
      }
    });

    $("#" + id).datepicker("refresh");
  }

  renderDetailsDatepicker(id, minDate) {
    $("#" + id).datepicker("destroy");

    var closeText;
    var prevText;
    var nextText;
    var currentText;
    var monthNames;
    var monthNamesShort;
    var dayNames;
    var dayNamesShort;
    var dayNamesMin;

    var monthKeyArr = ['january', 'feburary', 'march', 'april', 'may', 'june', 'july_full', 'august', 'september', 'october', 'november', 'december'];
    this.monthList = [];
    for (let monthname of monthKeyArr) {
      this.translate.get(monthname).subscribe((result: string) => {
        this.monthList.push(result);
      });
    }

    var weekKeyArr = ['sunday_min', 'monday_min', 'tuesday_min', 'wednesday_min', 'thursday_min', 'friday_min', 'saturday_min'];
    this.weekList = [];
    for (let weekname of weekKeyArr) {
      this.translate.get(weekname).subscribe((result: string) => {
        this.weekList.push(result);
      });
    }

    closeText = "Erledigt";
    prevText = "früher";
    nextText = "Nächster";
    currentText = "heute";
    monthNames = this.monthList;
    monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
    dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    dayNamesShort = ["Son", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    dayNamesMin = this.weekList;

    $("#" + id).datepicker({
      firstDay: 1,
      dateFormat: 'dd.mm.yy',
      closeText: closeText,
      prevText: prevText,
      nextText: nextText,
      currentText: currentText,
      monthNames: monthNames,
      monthNamesShort: monthNamesShort,
      dayNames: dayNames,
      dayNamesShort: dayNamesShort,
      dayNamesMin: dayNamesMin,
      weekHeader: "Sem.",
      minDate: new Date(minDate),
      onSelect: (value) => {
        this.value = value;
        var selectedDate = new Date(value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
        const selected_date = selectedDate.getFullYear().toString() + "-" + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" + selectedDate.getDate().toString();
        if (id == 'singleViewfromDate') {
          this.singleViewSlidervalue = selectedDate.getTime();
          this.fromDateChange();
        } else if (id == 'singleViewtoDate') {
          this.singleViewSlidermaxValue = selectedDate.getTime();
          this.toDateChange();
        } else if (id == 'masterFertzrfromDate') {
          this.masterFertilizeDate = selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString();
          this.getCropListMultiFertilizer(this.cultureIdMaster, selected_date);
        } else if (id == 'masterPlantProfromDate') {
          this.masterPlantProDate = selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString();
          this.plantActivityForm.controls['plant_date'].setValue(selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString());
          this.getProductDetail(0);
          this.getCropListMultiFertilizer(this.cultureIdMaster, selected_date);
        } else if (id == 'masterSoilfromDate') {
          this.masterSoilDate = selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString();
          this.soilActivityForm.controls['soil_date'].setValue(selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString());
          this.getCropListMultiSoil(this.cultureIdMaster, selected_date);
        } else if (id == 'soilSamplefromDate') {
          this.soilSampleDate = selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString();
          this.addFieldForm.controls['soil_sample_date'].setValue(selectedDate.getDate().toString() + "." + ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "." + selectedDate.getFullYear().toString());
        }
      }
    });
    $("#" + id).datepicker("refresh");

  }

  /* Added by Deepika *///
  changeDate(id) {
    if (id == 'singleViewfromDate') {
      var selectedDate = new Date(this.singleViewMinDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
      this.singleViewSlidervalue = selectedDate.getTime();
      this.fromDateChange();
    } else {
      var selectedDate = new Date(this.singleViewMaxDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
      this.singleViewSlidermaxValue = selectedDate.getTime();
      this.toDateChange();
    }

  }
  /* End */

  Validationfieldsize() {
    this.isErrorfsize = true;
  }

  ValidationErrorfieldsize() {
    this.isErrorfsize = false;
    var old_field_size = this.addFieldForm.value.old_field_size;
    var new_field_size = this.addFieldForm.value.size;

    var new_eco = this.addFieldForm.value.echoSize;
    var old_echo = this.addFieldForm.value.old_echo_size;

    var field_id = this.addFieldForm.value.fieldId;

    if (field_id > 0) {
      if (old_field_size != new_field_size || old_echo != new_eco) {

        this.showYearList = true;
      } else {
        //this.showYearList = false;
      }
    }

  }

  addCheckboxes() {
    this.userFields.map((o, i) => {
      const control = new FormControl(i >= 0);
      (this.exportForm.controls.fields as FormArray).push(control);
    });
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  closeSuggestionModal(types) {
    if (types == 'danger') {
      this.alertPerioEndModel = 'none';
    } else {
      this.suggestionModal = 'none'
      this.warningPerioEndModel = 'none';
      var array = { weeks: this.weeks, subscriptionId: this.subscriptionId, type: types };
      this.common.updateSuggestionStatus(array).pipe().subscribe((data: any) => {
        this.getUserData();
      });
    }
  }

  getUserData() {
    this.common.getUserData().pipe(first()).subscribe((data: any) => {
      if (data.status) {

        const userData = data.data;
        var oneDay = 24 * 60 * 60 * 1000;
        let current = new Date(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2") + "T00:00:00");
        if (userData.plan == "Trial") {
          let startDate = new Date(userData.subscription_start.replace(" ", "T"));
          let endDate = new Date(userData.subscription_end.replace(" ", "T"));
          var diffDays = Math.round(Math.abs((startDate.getTime() - current.getTime()) / (oneDay)));
          this.subscriptionId = userData.ac_subscriptionId;
          if (diffDays >= 14 && userData.suggestion_twoweeks == 0) {
            this.suggestionModal = 'block';
            this.weeks = 2;
          }
          if (diffDays >= 28 && userData.suggestion_fourweeks == 0) {
            this.suggestionModal = 'block';
            this.weeks = 4;
          }
          var trialPending = Math.round(Math.abs((current.getTime() - endDate.getTime()) / (oneDay)));
          if (trialPending <= 14 && userData.trial_alert == 0) {
            this.warningPerioEndModel = 'block';
            this.endDays = trialPending;
          }
          if (current > endDate) {
            this.alertPerioEndModel = 'block';
          }
          endDate.setDate(endDate.getDate() + 7);
          if (current > endDate) {
            this.storage.set('isExpired', true);
          } else {
            this.storage.remove('isExpired');
          }
        } else {
          let startDate = new Date(userData.subscription_start.replace(" ", "T"));
          let endDate = new Date(userData.subscription_end.replace(" ", "T"));
          if (current > endDate) {
            this.alertPerioEndModel = 'block';
            this.storage.set('isExpired', true);
            this.isExpired = true;
          } else {
            this.storage.remove('isExpired');
            this.isExpired = false;
          }
        }
        this.UserFullName = userData.name;
        this.StreetName = userData.street_name;
        this.PostalCode = userData.postal_code;
        this.City = userData.city;
        this.Country = userData.country;
        this.PhoneNumber = userData.phone;
      }
    });
  }

  logOut() {
    const currentUser = this.storage.get('loginSession');
    this.auth.logOut(currentUser)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          this.storage.remove('loginSession');
          this.storage.remove('currentLanguage');
          this.router.navigate(['login']);
        }
      });
  }

  exportformControlValueChanged() {
    const startDateControl = this.exportForm.get('startDate');
    const endDateControl = this.exportForm.get('endDate');
    this.exportForm.get('pdfType').valueChanges.subscribe(
      (mode: string) => {
        if (mode === 'individual') {
          startDateControl.setValidators([Validators.required, Validators.maxLength(100)]);
          endDateControl.setValidators([Validators.required]);
        } else if (mode === 'standard') {
          startDateControl.clearValidators();
          endDateControl.clearValidators();
        }
        startDateControl.updateValueAndValidity();
        endDateControl.updateValueAndValidity();
      });
  }

  formControlValueChanged() {
    const echoSizeControl = this.addFieldForm.get('echoSize');
    this.addFieldForm.get('echo').valueChanges.subscribe(
      (mode: string) => {
        if (mode === '1') {
          echoSizeControl.setValidators([Validators.maxLength(10), Validators.pattern('[0-9 ]*')]);
        } else if (mode === '0') {
          echoSizeControl.clearValidators();
        }
        echoSizeControl.updateValueAndValidity();
      });
  }

  customValidation() {
    return (group: FormGroup): { [key: string]: any } => {
      const from = group.controls['dateFrom'];
      const to = group.controls['dateTo'];
      const harvest_to = group.controls['harvest_date'];
      const isSwapped = group.controls['isSwapped'];
      const swappedWith = group.controls['swappedWith'];

      if (from.value && harvest_to.value) {

        var dateFrom = new Date(from.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
        var harvestDateTo = new Date(harvest_to.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
        if (dateFrom > harvestDateTo) {
          var message = "";
          this.translate.get("date_from_greater").subscribe((result: string) => {
            message = result;
          });
          return {
            dates: message
          };
        }
      }
      if (from.value && to.value) {

        var dateFrom = new Date(from.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
        var dateTo = new Date(to.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1") + "T00:00:00");
        if (dateFrom > dateTo) {
          var message = "";
          this.translate.get("date_from_greater").subscribe((result: string) => {
            message = result;
          });
          return {
            dates: message
          };
        }
      }
      if (isSwapped.value === true && swappedWith.value === '') {
        var messageSwapp = "";
        this.translate.get("swapped_with_required").subscribe((result: string) => {
          messageSwapp = result;
        });
        return {
          swappedWithRequired: messageSwapp
        };
      }
      return {};
    };
  }

  fieldCustomValidation() {
    return (group: FormGroup): { [key: string]: any } => {
      const isecho = group.controls['echo'];
      const echoSize = group.controls['echoSize'];
      if (isecho.value === true && echoSize.value === '') {
        var messageEcho = "";
        this.translate.get("echo_size_required").subscribe((result: string) => {
          messageEcho = result;
        });
        return {
          echoSizeRequired: messageEcho
        };
      }
      return {};
    };
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  getServerDate() {
    this.common.getServerDate().pipe(first()).subscribe((data: any) => {

      if (data.status) {
        this.currentDate = data.data;
        this.renderDatePicker();
        const todayDateObj = new Date(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2") + "T00:00:00");
        this.cropSizePerCategoryYear = todayDateObj.getFullYear();
        todayDateObj.setMonth(todayDateObj.getMonth() - 2);
        this.cultureMinDateObj = JSON.parse(JSON.stringify(todayDateObj));
        todayDateObj.setMonth(todayDateObj.getMonth() + 12);
        todayDateObj.setDate(todayDateObj.getDate() - 1);
        this.cultureMaxDateObj = JSON.parse(JSON.stringify(todayDateObj));
        this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
        this.getUserData();
        this.getDashboardChartData(this.rangeInput);
        this.fullViewtimescroller();
      }
    });
  }

  get formData() { return <FormArray>this.exportForm.get('fields'); }

  openexportModal() {
    this.exportModal = 'block';
    this.getuserAllFields();
    this.getusersAllExports();
  }

  getuserAllFields() {
    this.dashboard.getuserAllFields().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.userFields = data.data;
        this.exportForm.controls['pdfType'].setValue('standard');
        this.exportForm.controls['startDate'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
        this.exportForm.controls['endDate'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
        this.addCheckboxes();
        this.serverError = false;
      } else {
        this.serverError = true;
      }
    });
  }

  checkObject(obj) {
    return true;
  }

  getusersAllExports() {
    this.dashboard.getusersAllExports().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.userExports = data.data;
        this.serverError = false;
      } else {
        this.serverError = true;
      }
    });
  }

  closeexportModal() {
    this.exportForm.reset();
    this.exportForm.setControl('fields', new FormArray([], this.minSelectedCheckboxes(1)));
    this.exportModal = 'none';
  }

  getDashboardChartData(range) {
    const currentUser: any = this.storage.get('loginSession');
    range.language = currentUser.language;
    range.page_no = this.currentPage;
    //range.page_no = 1;
    range.limit = 3;
    
    this.dashboard.getDashboardChartData(range).pipe(first()).subscribe((data: any) => {
      
      if (data.status) {
        if(this.currentPage == 1){
          this.dashboardData$ = [];
        }
        if(data.data.is_slider){
          this.dashboardData$ = data.data.dashboard_data;
        }else{
          if (this.dashboardData$ != undefined && this.dashboardData$.length != 0 && data.data.dashboard_data.length > 0) {
            /// Append data to array ///
            let allLoadedField = [];
            for (let j = 0; j < this.dashboardData$.length; j++) {
              allLoadedField.push(this.dashboardData$[j]);
            }
            if (!this.IsAllLoaded && !this.unArchivedFieldId) {
              data.data.dashboard_data.forEach(element => {
                let checkAlready = true;
               
                  if(allLoadedField.indexOf(element.field_id) !== -1) {
                    
                    checkAlready = false;
                  }
                  
                if(checkAlready == true){
                  
                  this.dashboardData$.push(element);
                }
              });
            } else {
              this.dashboardData$;
            }
          } else if (data.data.dashboard_data.length > 0) {
            ///first page data///
            
            this.dashboardData$ = data.data.dashboard_data;
          }
        }
        if (this.fieldCultures$ != undefined && this.fieldCultures$.length != 0) {
          Object.assign(this.fieldCultures$, data.data.field_cultures);
        } else {
          this.fieldCultures$ = data.data.field_cultures;
        }
        ///this.dashboardData$ = data.data.dashboard_data;
        //this.fieldCultures$ = data.data.field_cultures;

        this.dashboardFooterTextData$ = data.data.footerTextData;
        this.getDashboardTopBoxesData();
        this.getUserMostPlantedCrops(range);

        if (this.removedFieldId != '' || this.archivedFieldId != '' || this.unArchivedFieldId) {
          for (let i = 0; i < this.dashboardData$.length; i++) {
            if (this.dashboardData$[i].field_id == this.removedFieldId) {
              this.dashboardData$.splice(i, 1);
              this.removedFieldId = '';
            }
            if (this.dashboardData$[i].field_id == this.archivedFieldId) {
              this.dashboardData$[i].archieve = 1;
            }
            if(this.dashboardData$[i].field_id == this.unArchivedFieldId){
              this.dashboardData$[i].archieve = 0;
            }
          }
        }

        if (data.data.total_page > 0) {
          this.IsAllLoaded = (data.data.total_page <= this.currentPage);

        } else {
          this.IsAllLoaded = true;
        }
        this.isLoading = false;
        this.isLoadingDashboard = false;

      } else {
        this.translate.get(data.message).subscribe((result: string) => {
          this.serverError$ = result;
        });
        this.dashboardDataError = true;
        this.isLoading = false;
      }
    });
  }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    if ((scrollHeight - scrollPosition) < 950) {
      if (!this.IsAllLoaded && !this.isLoadingDashboard) {
        this.isLoadingDashboard = true;
        if(this.dashboardData$ != undefined && this.dashboardData$.length > 0){
          this.currentPage += 1;   
        }
        this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
        this.getDashboardChartData(this.rangeInput);
      }
    }
  }

  getDashboardTopBoxesData() {
    const currentUser: any = this.storage.get('loginSession');
    const Input = { 'language': currentUser.language };
    this.dashboard.getDashboardTopBoxesData(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.dashboardTopBoxesData$ = data.data.field_data;
        this.dashboardTopBoxesSizePerCategoryData$ = data.data.SizePerData;
        this.CultureYears$ = data.data.CultureYears;
      }
    });
  }

  getsizePerCategoryByYear(year) {
    this.cropSizePerCategoryYear = year;
    const currentUser: any = this.storage.get('loginSession');
    const Input = { 'language': currentUser.language, 'year': year };
    this.dashboard.getsizePerCategoryByYear(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.dashboardTopBoxesSizePerCategoryData$ = data.data;
      }
    });
  }

  getAllCrops() {
    let defaultLanguage;
    const currentUser: any = this.storage.get('loginSession');
    defaultLanguage = { 'language': currentUser.language };
    this.cropmaster.getAllCrops(defaultLanguage).subscribe(
      data => this.allCrops$ = data['data']
    );

  }

  getUserMostPlantedCrops(range) {
    const currentUser: any = this.storage.get('loginSession');
    range.language = currentUser.language;
    this.dashboard.getUserMostPlantedCrops(range).pipe(first()).subscribe((data: any) => {
      this.mostPlantedCrops$ = data.data.MostPlantedCrops;
      this.mostPlantedCrops$.forEach(element => {
        element.isFiltered = false;
      });
      this.LessPlantedCrops$ = data.data.LessPlantedCrops;
    });
  }

  closeCultureModal() {
    this.addCultureForm.reset();
    this.showRemainingSpace = false;
    this.isError = false;
    this.remainingSpace = 0;
    this.cultureModal = 'none';
    this.deleteCulture = 'none';
    $("#culturemodalId").modal('hide');
    this.showHideSwappedWith = false;
    this.invalidRegisterFieldCulture = false;
    this.renderDatePicker();
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    //this.cultureModal = 'none';
    //this.deleteCulture = 'none';
    //this.fieldModal = 'none';
  }

  addNewCulture(fieldId: string, index: any) {
    this.isSubmitted = false;
    this.cultureSizeIssue = false;
    const fieldIdByIndex = this.dashboardData$[index].field_id;
    if (fieldIdByIndex === fieldId) {
      this.isError = false;
      this.cultureModal = 'block';
      $("#culturemodalId").modal('show');
      $("#culturemodalId").modal({ backdrop: "static" });
      this.translate.get("add_new_culture").subscribe((result: string) => {
        this.addEditTextCultre = result;
      });
      this.addCultureForm.reset();
      this.addCultureForm.controls['fieldId'].setValue(fieldId);
      if (this.IsMobile) {
        this.addCultureForm.controls['dateFrom'].setValue(this.currentDate.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
        ///this.addCultureForm.controls['dateTo'].setValue(this.currentDate.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
        this.addCultureForm.controls['harvest_date'].setValue(this.currentDate.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      } else {
        this.addCultureForm.controls['dateFrom'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
        ///this.addCultureForm.controls['dateTo'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
        this.addCultureForm.controls['harvest_date'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
      }
      this.isLoading = false;
      this.renderDatePicker();
      this.getFieldRemainingSpaceForDuration();
    }
  }

  getFieldRemainingSpaceForDuration() {

    const from = this.addCultureForm.controls.dateFrom.value;
    const to = this.addCultureForm.controls.dateTo.value;
    const harvest_to = this.addCultureForm.controls.harvest_date.value;
    const fieldId = this.addCultureForm.controls.fieldId.value;

    if (!this.isSubmitted && from !== null && harvest_to !== null && fieldId !== '' && from <= harvest_to) {
      this.getFieldRemainingSpace(from, harvest_to, fieldId);
    }
    if (!this.isSubmitted && from !== null && to !== null && fieldId !== '' && from <= to) {
      this.getFieldRemainingSpace(from, to, fieldId);
    }
  }

  getFieldRemainingSpace(from, to, fieldId) {
    this.showRemainingSpace = false;
    const InputData = { 'from': from, 'to': to, 'fieldId': fieldId };
    this.dashboard.getFieldRemainingSpace(InputData)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          this.showRemainingSpace = true;
          this.remainingSpace = data.data;
        } else {
          this.showRemainingSpace = false;
          this.remainingSpace = 0;
        }
      });
  }

  addCulture() {
    this.cultureSizeIssue = false;
    const from = this.addCultureForm.controls.dateFrom.value;
    const to = this.addCultureForm.controls.dateTo.value;
    const harvest_to = this.addCultureForm.controls.harvest_date.value;
    if (this.addCultureForm.invalid) {
      this.addCultureForm.controls['dateFrom'].setValue(from);
      this.addCultureForm.controls['dateTo'].setValue(to);
      this.addCultureForm.controls['harvest_date'].setValue(harvest_to);
      return;
    }
    this.isSubmitted = true;
    this.isLoading = true;
    this.addCultureForm.controls['dateFrom'].setValue(from.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    if (to != null) {
      this.addCultureForm.controls['dateTo'].setValue(to.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    }
    this.addCultureForm.controls['harvest_date'].setValue(harvest_to.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    this.dashboard.addFieldCulture(this.addCultureForm.value).pipe(first()).subscribe((data: any) => {

      if (data.status) {
        this.cultureSizeIssue = false;
        this.isError = false;
        this.addCultureForm.reset();
        this.invalidRegisterFieldCulture = false;
        this.showRemainingSpace = false;
        this.remainingSpace = 0;
        this.cultureModal = 'none';
        $("#culturemodalId").modal('hide');
        this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
        this.getDashboardChartData(this.rangeInput);

      } else {
        if (data.cropStatus) {
          this.cultureSizeIssue = false;
          this.isError = false;
          this.cultureModal = 'block';
          $("#culturemodalId").modal('show');
          this.invalidRegisterFieldCulture = false;
          const that = this;
          var cropRotationBreakMessage = "";
          this.translate.get("crop_rotation_break_message").subscribe((result: string) => {
            cropRotationBreakMessage = result;
          });
          that.alertService.confirmThis('Warning', cropRotationBreakMessage, function () {
            that.isLoading = true;
            that.addCultureWithCropRotationWarning();
          }, function () {
            ///console.log('No');
          });

          this.translate.get(data.message).subscribe((result: string) => {
            this.fieldCultureWarning$ = result;
          });
          this.isLoading = false;
        } else {
          if (data.sizeIssue) {
            this.cultureSizeIssue = true;
          } else {
            this.cultureSizeIssue = false;
            this.isError = true;
            this.cultureModal = 'block';
            $("#culturemodalId").modal('show');
            this.invalidRegisterFieldCulture = true;
            this.translate.get(data.message).subscribe((result: string) => {
              this.fieldCultureErrors$ = result;
              this.errorMessage = this.fieldCultureErrors$;
            });
          }
          this.isLoading = false;
        }
        this.addCultureForm.controls['dateFrom'].setValue(from);
        this.addCultureForm.controls['dateTo'].setValue(to);
        this.addCultureForm.controls['harvest_date'].setValue(harvest_to);
      }
    });
  }

  addCultureWithCropRotationWarning() {
    if (this.addCultureForm.invalid) {
      return;
    }
    const from = this.addCultureForm.controls.dateFrom.value;
    const to = this.addCultureForm.controls.dateTo.value;
    const harvest_to = this.addCultureForm.controls.harvest_date.value;
    this.addCultureForm.controls['dateFrom'].setValue(from.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    if (to != null) {
      this.addCultureForm.controls['dateTo'].setValue(to.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    }
    this.addCultureForm.controls['harvest_date'].setValue(harvest_to.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    this.dashboard.addCultureWithCropRotationWarning(this.addCultureForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.addCultureForm.reset();
        this.invalidRegisterFieldCulture = false;
        this.showRemainingSpace = false;
        this.remainingSpace = 0;
        this.cultureModal = 'none';
        $("#culturemodalId").modal('hide');
        this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
        this.getDashboardChartData(this.rangeInput);
      } else {
        this.cultureModal = 'block';
        $("#culturemodalId").modal('show');
        this.invalidRegisterFieldCulture = true;
        this.translate.get(data.message).subscribe((result: string) => {
          this.fieldCultureErrors$ = result;
        });
      }
      this.addCultureForm.controls['dateFrom'].setValue(from);
      this.addCultureForm.controls['dateTo'].setValue(to);
      this.addCultureForm.controls['harvest_date'].setValue(harvest_to);
    });
  }

  filterbycrop(cropId: string) {
    this.mostPlantedCrops$.forEach(element => {
      if (cropId) {
        if (element.crop_id == cropId) {
          element.isFiltered = !element.isFiltered;
        }
      } else {
        element.isFiltered = false;
      }
    });

    this.LessPlantedCrops$.forEach(element => {
      if (cropId) {
        if (element.crop_id == cropId) {
          element.isFiltered = !element.isFiltered;
        }
      } else {
        element.isFiltered = false;
      }
    });
    this.isLoading = true;

    this.filtered = false;
    if (this.fullView) {
      for (let i = 0; i < this.dashboardData$.length; i++) {
        const fields = this.dashboardData$[i];
        for (const key in fields) {
          if (key === 'field_id') {
            const fieldId = fields[key];
            const fieldsArr = this.fieldCultures$[fieldId];
            for (let j = 0; j < fieldsArr.length; j++) {
              const cultures = fieldsArr[j];
              for (let k = 0; k < cultures.length; k++) {
                if (cultures[k]) {
                  const cultureDetail = cultures[k];
                  cultureDetail.is_filter = '0';
                }
              }
            }
          }
        }
      }

      this.mostPlantedCrops$.forEach(element => {
        for (let i = 0; i < this.dashboardData$.length; i++) {
          const fields = this.dashboardData$[i];
          for (const key in fields) {
            if (key === 'field_id') {
              const fieldId = fields[key];
              const fieldsArr = this.fieldCultures$[fieldId];
              for (let j = 0; j < fieldsArr.length; j++) {
                const cultures = fieldsArr[j];
                for (let k = 0; k < cultures.length; k++) {
                  if (cultures[k]) {
                    const cultureDetail = cultures[k];
                    if (cropId) {
                      if (cultureDetail.crop_id === element.crop_id && element.isFiltered) {
                        cultureDetail.is_filter = '1';
                      }
                      this.filtered = true;
                    }
                  }
                }
              }
            }
          }
        }
      });

      this.LessPlantedCrops$.forEach(element => {
        for (let i = 0; i < this.dashboardData$.length; i++) {
          const fields = this.dashboardData$[i];
          for (const key in fields) {
            if (key === 'field_id') {
              const fieldId = fields[key];
              const fieldsArr = this.fieldCultures$[fieldId];
              for (let j = 0; j < fieldsArr.length; j++) {
                const cultures = fieldsArr[j];
                for (let k = 0; k < cultures.length; k++) {
                  if (cultures[k]) {
                    const cultureDetail = cultures[k];
                    if (cropId) {
                      if (cultureDetail.crop_id === element.crop_id && element.isFiltered) {
                        cultureDetail.is_filter = '1';
                      }
                      this.filtered = true;
                    }
                  }
                }
              }
            }
          }
        }
      });

      //this.spinner.hide();
      this.isLoading = false;
    }
    if (this.singleView) {
      for (let j = 0; j < this.fieldDetailsByIdIndex$.length; j++) {
        const cultures = this.fieldDetailsByIdIndex$[j];
        for (let k = 0; k < cultures.length; k++) {
          if (cultures[k]) {
            const cultureDetail = cultures[k];
            cultureDetail.is_filter = '0';
          }
        }
      }
      this.mostPlantedCrops$.forEach(element => {
        for (let j = 0; j < this.fieldDetailsByIdIndex$.length; j++) {
          const cultures = this.fieldDetailsByIdIndex$[j];
          for (let k = 0; k < cultures.length; k++) {
            if (cultures[k]) {
              const cultureDetail = cultures[k];
              if (cropId) {
                if (cultureDetail.crop_id === element.crop_id && element.isFiltered) {
                  cultureDetail.is_filter = '1';
                }
                this.filtered = true;
              }
            }
          }
        }
      });

      this.LessPlantedCrops$.forEach(element => {
        for (let j = 0; j < this.fieldDetailsByIdIndex$.length; j++) {
          const cultures = this.fieldDetailsByIdIndex$[j];
          for (let k = 0; k < cultures.length; k++) {
            if (cultures[k]) {
              const cultureDetail = cultures[k];
              if (cropId) {
                if (cultureDetail.crop_id === element.crop_id && element.isFiltered) {
                  cultureDetail.is_filter = '1';
                }
                this.filtered = true;
              }
            }
          }
        }
      });
      //this.spinner.hide();
      this.isLoading = false;
    }

    let isFilterAll = true;

    this.mostPlantedCrops$.forEach(element => {
      if (!element.isFiltered) {
        isFilterAll = false;
      }
    });

    if (isFilterAll) {
      this.filtered = false;
    }
  }

  addNewField() {

    const newField = new Object();
    newField['field_id'] = '';
    newField['field_name'] = '';
    newField['field_size'] = '';
    newField['cultures'] = [{}];
    newField['archieve'] = '0';
    this.dashboardData$.push(newField);
    this.isError = false;
    this.fieldModal = 'block';
    $("#fieldmodalId").modal('show');
    $("#fieldmodalId").modal({ backdrop: "static" });
    this.translate.get("create_new_field").subscribe((result: string) => {
      this.addEditText = result;
    });
    this.addFieldForm.reset();
    const todaydate = new Date();
    if (this.IsMobile) {
      this.soilSampleDate = todaydate.getFullYear().toString() + "-" + ("0" + (todaydate.getMonth() + 1)).slice(-2) + "-" + todaydate.getDate().toString();
    } else {
      this.soilSampleDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    if (this.IsMobile == false) {
      this.renderDetailsDatepicker('soilSamplefromDate', null);
    }
  }

  closeFieldModal() {
    
    if (!this.addFieldForm.controls.fieldId.value) {
      this.dashboardData$.pop();
    }
    this.isError = false;
    this.addFieldForm.reset();
    this.showHideEchoSize = false;
    this.fieldModal = 'none';
    this.showYearList = false;
    $("#fieldmodalId").modal('hide');
  }

  addField() {
    this.showYearList = false;
    if (this.addFieldForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.dashboard.addField(this.addFieldForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.ecoSizeIssue = false;
        this.isError = false;
        this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
        this.fieldModal = 'none';
        $("#fieldmodalId").modal('hide');
        //this.getDashboardChartData(this.rangeInput);
        window.location.reload();
      } else {
        if (data.sizeIssue) {
          this.ecoSizeIssue = true;
        } else {
          this.isError = true;
          this.ecoSizeIssue = false;
          this.fieldModal = 'block';
          $("#fieldmodalId").modal('show');
          this.cultureModal = 'none';
          $("#culturemodalId").modal('hide');
          this.invalidRegisterField = true;
          this.translate.get(data.message).subscribe((result: string) => {
            this.errorMessage = result;
          });
        }
        this.isLoading = false;
      }
    });
  }

  editCulture(fieldId, user_id, data) {

    if (user_id == 1) {
      this.fieldSaveShow = false;
    } else {
      this.fieldSaveShow = true;
    }

    this.cultureModal = 'block';
    $("#culturemodalId").modal('show');
    this.deleteCulture = 'block';
    this.translate.get("edit_culture").subscribe((result: string) => {
      this.addEditTextCultre = result;
    });

    this.addCultureForm.reset();
    this.showHideSwappedWith = false;
    this.invalidRegisterFieldCulture = false;
    this.addCultureForm.controls['delicate'].setValue(data.crop_id);
    this.addCultureForm.controls['fieldId'].setValue(fieldId);
    this.addCultureForm.controls['cultureId'].setValue(data.culture_id);
    if (this.IsMobile) {
      this.addCultureForm.controls['dateFrom'].setValue(data.start_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      if (data.end_date != null) {
        this.addCultureForm.controls['dateTo'].setValue(data.end_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      }
      this.addCultureForm.controls['harvest_date'].setValue(data.harvest_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
    } else {
      this.addCultureForm.controls['dateFrom'].setValue(data.start_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      if (data.end_date != null) {
        this.addCultureForm.controls['dateTo'].setValue(data.end_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      }
      this.addCultureForm.controls['harvest_date'].setValue(data.harvest_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
    }

    this.updatedateTo();
    this.addCultureForm.controls['cultivatedArea'].setValue(data.culture_size);
    if (data.is_swapped === '1') {
      this.addCultureForm.controls['isSwapped'].setValue(data.is_swapped);
      this.showHideSwappedWith = true;
    }
    this.addCultureForm.controls['swappedWith'].setValue(data.swapped_with);
    this.addCultureForm.controls['plantVariety'].setValue(data.plantVariety);
    this.addCultureForm.controls['notes'].setValue(data.culture_notes);
    this.addCultureForm.controls['seed_volume'].setValue(data.seed_volume);
    this.addCultureForm.controls['harvest_volume'].setValue(data.harvest_volume);

    this.isLoading = false;
  }

  editField(data) {

    this.showYearList = false;
    const todaydate = new Date();
    this.fieldModal = 'block';
    $("#fieldmodalId").modal('show');
    this.translate.get("edit_field").subscribe((result: string) => {
      this.addEditText = result;
    });

    this.addFieldForm.reset();
    this.invalidRegisterField = false;
    this.showHideEchoSize = false;
    this.addFieldForm.controls['fieldId'].setValue(data.field_id);
    this.addFieldForm.controls['name'].setValue(data.field_name);
    this.addFieldForm.controls['size'].setValue(data.field_size);
    this.addFieldForm.controls['address'].setValue(data.address);
    this.addFieldForm.controls['city'].setValue(data.city);
    this.addFieldForm.controls['zipcode'].setValue(data.zipcode);
    this.addFieldForm.controls['notes'].setValue(data.notes);
    this.addFieldForm.controls['nitrogen'].setValue(data.nitrogen);
    this.addFieldForm.controls['phosphorus'].setValue(data.phosphorus);
    this.addFieldForm.controls['potassium_oxide'].setValue(data.potassium_oxide);
    this.addFieldForm.controls['magnesium'].setValue(data.magnesium);
    this.addFieldForm.controls['ph_value'].setValue(data.ph_value);
    this.addFieldForm.controls['old_field_size'].setValue(data.field_size);

    if (data.archieve == 1) {
      this.archieveDeleteShow = true;
    }
    if (data.user_id == 1) {
      this.fieldSaveShow = false;
    } else {
      this.fieldSaveShow = true;
    }
    if (data.echo === '1') {
      this.addFieldForm.controls['echo'].setValue(data.echo);
      this.addFieldForm.controls['old_echo_size'].setValue(data.echo);
      this.showHideEchoSize = true;
    }
    this.addFieldForm.controls['echoSize'].setValue(data.echo_size);
    if (data.soil_sample_date != null) {
      if (this.IsMobile) {
        this.addFieldForm.controls['soil_sample_date'].setValue(data.soil_sample_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      } else {
        this.addFieldForm.controls['soil_sample_date'].setValue(data.soil_sample_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      }
    }
    var dt = new Date();
    var current_year = dt.getFullYear();
    this.yearList$ = [(dt.getFullYear() - 1), dt.getFullYear(), (dt.getFullYear() + 1)];

    this.addFieldForm.controls['size_change_year'].setValue(current_year);
  }

  archiveField(fieldId, archiveStatus) {

    const that = this;
    let confirmMsg;
    let notifymMsg;
    if (archiveStatus === '0') {
      this.translate.get("archived_message").subscribe((result: string) => {
        confirmMsg = result;
      });
      this.translate.get("archived_success").subscribe((result: string) => {
        notifymMsg = result;
      });
    } else {
      this.translate.get("unarchived_message").subscribe((result: string) => {
        confirmMsg = result;
      });
      this.translate.get("unarchived_success").subscribe((result: string) => {
        notifymMsg = result;
      });
    }

    that.alertService.confirmThis('Confirmation', confirmMsg, function () {
      that.isLoading = true;
      const archieveInput = { 'field_id': fieldId, 'archiveStatus': archiveStatus };
      that.dashboard.archiveField(archieveInput)
        .pipe(first())
        .subscribe((data: any) => {
          if (data.status) {
            if (archiveStatus === '0') {
              that.archivedFieldId = fieldId;
            } else {
              that.unArchivedFieldId = fieldId;
            }
            
            that.rangeInput = { 'min': that.cultureMinDateObj, 'max': that.cultureMaxDateObj };
            that.getDashboardChartData(that.rangeInput);
            //window.location.reload();
          } else {
            that.translate.get(data.message).subscribe((result: string) => {
              that.serverError$ = result;
            });
            that.dashboardDataError = true;
            that.isLoading = false;
          }
        });
    }, function () {
      ///console.log('No');
    });
  }

  deleteField() {
    const fieldId = this.addFieldForm.value.fieldId;
    let confirmMsg;
    this.translate.get("delete_field_confirmation_text").subscribe((result: string) => {
      confirmMsg = result;
    });
    let that = this;
    this.alertService.confirmThis('Confirmation', confirmMsg, function () {
      if (fieldId !== '') {
        const input = { 'fieldId': fieldId };
        that.isLoading = true;
        that.dashboard.deleteField(input)
          .pipe(first())
          .subscribe((data: any) => {
            if (data.status) {
              that.removedFieldId = fieldId;
              that.isError = false;
              that.addFieldForm.reset();
              that.fieldModal = 'none';
              $("#fieldmodalId").modal('hide');
              that.rangeInput = { 'min': that.cultureMinDateObj, 'max': that.cultureMaxDateObj };
              that.getDashboardChartData(that.rangeInput);
              that.isLoading = false;
            } else {
              that.isError = true;
              that.translate.get(data.message).subscribe((result: string) => {
                that.errorMessage = result;
              });
              that.isLoading = false;
            }
          });
      } else {
        //that.errorMessage = 'Something went wrong! Please try again later.';
      }
    }, function () {
      ///console.log('No');
    });
  }

  fullViewtimescroller() {
    const dateRange: string[] = this.createFullViewDateRange();
    const mindate = new Date(this.cultureMinDateObj);
    const maxdate = new Date(this.cultureMaxDateObj);
    const MinDateIndex = dateRange.indexOf(mindate.toDateString());
    const MaxDataeIndex = dateRange.indexOf(maxdate.toDateString());
    this.fullViewSlidervalue = new Date(dateRange[MinDateIndex]).getTime();
    this.fullViewSlidermaxValue = new Date(dateRange[MaxDataeIndex]).getTime();
    this.fullViewSlideroptions = {
      stepsArray: dateRange.map((date: string) => {
        return { value: new Date(date).getTime() };
      }),
      translate: (value: number, label: LabelType): string => {
        return this.monthArr[new Date(value).getMonth()].symbol + '. ' + new Date(value).getFullYear();
      },
      noSwitching: true,
      minRange: 365,
      maxRange: 365,
      pushRange: true,
      showTicksValues: true,
      tickStep: 60,
      tickValueStep: 120,
      draggableRange: true,
      disabled: this.isExpired
    };
  }

  fullViewtimeScrollChangeEnd(changeContext: ChangeContext): void {

    this.isLoading = true;
    const minDateObj = new Date(+changeContext.value);
    const maxDateObj = new Date(+changeContext.highValue);

    const dates: Date[] = [];
    const year = minDateObj.getFullYear();
    for (let i = 1; i <= 1095; i++) {
      dates.push(new Date(year, 0, i));
    }

    const MinDateIndex = dates.map(Number).indexOf(+minDateObj);
    const MaxDataeIndex = dates.map(Number).indexOf(+maxDateObj);
    if (MinDateIndex === 0) {
      this.showPreviousYearSlider(minDateObj);
    }

    if (MaxDataeIndex === 364 || MaxDataeIndex === 729 || MaxDataeIndex === 728) {
      this.showNextYearSlider(maxDateObj);
    }

    this.cultureMinDateObj = JSON.parse(JSON.stringify(minDateObj));
    this.cultureMaxDateObj = JSON.parse(JSON.stringify(maxDateObj));
    this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj,'service_type':'slider' };

    this.getDashboardChartData(this.rangeInput);
    this.getUserMostPlantedCrops(this.rangeInput);
  }

  createFullViewDateRange(): string[] {
    const dates: string[] = [];
    const todayDateObj = new Date(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2") + "T00:00:00");
    const currentYear = todayDateObj.getFullYear();
    const lastYear = currentYear - 1;
    for (let i = 1; i <= 1095; i++) {
      var date = new Date(lastYear, 0, i).toDateString();
      dates.push(date);
    }
    return dates;
  }

  createSingleViewDateRange(): Date[] {

    const dates: Date[] = [];
    const todayDateObj = new Date(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2") + "T00:00:00");
    const currentYear = todayDateObj.getFullYear();
    const startYear = currentYear - 7;
    for (let i = 1; i <= 5479; i++) {
      dates.push(new Date(startYear, 0, i));
    }
    return dates;
  }

  showDetails(fieldId, index) {
    this.isLoading = true;
    this.fieldByIndex$ = this.dashboardData$[index];
    this.fieldDetailsByIdIndex$ = this.fieldCultures$[fieldId];
    this.singleViewFieldIndex = index;
    this.fullView = false;
    this.filtered = false;
    this.singleView = true;
    this.SingleViewtimescroller(fieldId);
  }

  showAllFields() {

    this.isLoading = true;
    const todayDateObj = new Date(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2") + "T00:00:00");
    this.cropSizePerCategoryYear = todayDateObj.getFullYear();
    todayDateObj.setMonth(todayDateObj.getMonth() - 2);
    this.cultureMinDateObj = JSON.parse(JSON.stringify(todayDateObj));
    todayDateObj.setMonth(todayDateObj.getMonth() + 12);
    todayDateObj.setDate(todayDateObj.getDate() - 1);
    this.cultureMaxDateObj = JSON.parse(JSON.stringify(todayDateObj));
    this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
    this.getDashboardChartData(this.rangeInput);
    this.fullViewtimescroller();
    this.fullView = true;
    this.singleView = false;
    //this.isLoading = false;
    this.filtered = false;
  }

  SingleViewtimescroller(fieldId) {

    const mindate = new Date(this.cultureMinDateObj);
    const maxdate = new Date(this.cultureMaxDateObj);
    if (this.IsMobile) {
      this.singleViewMinDate = mindate.getFullYear().toString() + "-" + ("0" + (mindate.getMonth() + 1)).slice(-2) + "-" + mindate.getDate().toString();
      this.singleViewMaxDate = maxdate.getFullYear().toString() + "-" + ("0" + (maxdate.getMonth() + 1)).slice(-2) + "-" + maxdate.getDate().toString();
    } else {
      this.singleViewMinDate = mindate.getDate().toString() + "." + (mindate.getMonth() + 1).toString() + "." + mindate.getFullYear().toString();
      this.singleViewMaxDate = maxdate.getDate().toString() + "." + (mindate.getMonth() + 1).toString() + "." + maxdate.getFullYear().toString();
    }

    this.singleViewSlidervalue = mindate.getTime();
    this.singleViewSlidermaxValue = maxdate.getTime();

    this.getSingleFieldByScroll(mindate, maxdate);
    this.userPlannedFieldCrops(fieldId, mindate, maxdate);

    if (this.IsMobile == false) {
      setTimeout(function () {
        this.renderDetailsDatepicker('singleViewfromDate', null);
        this.renderDetailsDatepicker('singleViewtoDate', null);
      }.bind(this), 2000);
    }
  }

  userPlannedFieldCrops(fieldId, minDateObj, maxDateObj) {
    this.cultureMinDateObj = JSON.parse(JSON.stringify(minDateObj));
    this.cultureMaxDateObj = JSON.parse(JSON.stringify(maxDateObj));

    this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
    let fieldIdArr;
    const currentUser: any = this.storage.get('loginSession');
    fieldIdArr = { 'field_id': fieldId, 'range': this.rangeInput, 'language': currentUser.language };
    this.dashboard.getUserPlannedFieldCrops(fieldIdArr).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.fieldPlannedCrops$ = data.data;
      } else {
        this.fieldPlannedCrops$ = [];
      }
    });
  }

  singleViewtimeScrollChangeEnd(changeContext: ChangeContext): void {

    this.isLoading = true;
    const minDateObj = new Date(+changeContext.value);
    const maxDateObj = new Date(+changeContext.highValue);
    this.getSingleFieldByScroll(minDateObj, maxDateObj);
  }

  getSingleFieldByScroll(minDateObj, maxDateObj) {
    this.cultureMinDateObj = JSON.parse(JSON.stringify(minDateObj));
    this.cultureMaxDateObj = JSON.parse(JSON.stringify(maxDateObj));

    this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj, 'fieldId': this.fieldByIndex$.field_id };

    this.getSingleFieldChartData(this.rangeInput);
    this.getUserMostPlantedCrops(this.rangeInput);
    this.userPlannedFieldCrops(this.fieldByIndex$.field_id, minDateObj, maxDateObj);
    this.makeTableMonths(this.cultureMinDateObj, this.cultureMaxDateObj);
  }

  getSingleFieldChartData(range) {
    const currentUser: any = this.storage.get('loginSession');
    range.language = currentUser.language;
    this.dashboard.getSingleFieldChartData(range).pipe(first()).subscribe((data: any) => {
      
      this.isLoading = false;
      if (data.status) {
        this.fieldDetailsByIdIndex$ = data.data.field_cultures;
        this.dataDuration = data.data.dataDuration;
        this.createEmptyDateRange();
      } else {
        this.fieldDetailsByIdIndex$ = data.data.field_cultures;
      }
    });
  }

  makeTableMonths(startDate, endDate) {

    const startDateObj = new Date(startDate);
    const endDateObj = new Date(endDate);

    const startDateStr = (startDateObj.getMonth() + 1) + '/' + startDateObj.getDate() + '/' + startDateObj.getFullYear();
    const start = startDateStr.split('/');
    const dates = [];

    const timeDiff = Math.abs(endDateObj.getTime() - startDateObj.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    const diffMnth = Math.abs(diffDays / 365.25).toString().split('.')[0];
    const diffYear = this.singleViewYearShown = parseInt(diffMnth, 10) + 1;

    const startMonth = parseInt(start[0], 10) - 1;
    let startMonthCpy = parseInt(start[0], 10) - 1;
    let year = parseInt(start[2], 10) - 2000;
    let yearCpy = parseInt(start[2], 10) - 2000;
    for (let j = 0; j < diffYear * 12; j = j + diffYear) {
      let month = startMonth + j;
      startMonthCpy = startMonthCpy + diffYear;
      if (month >= 12) {
        month = month % 12;
      }
      if (startMonthCpy > 12) {
        yearCpy = year;
        year = year + 1;
        startMonthCpy = startMonthCpy % 12;
      } else {
        yearCpy = year;
      }
      const displayMonthCpy = this.monthArr[month].symbol;
      if (diffYear > 1) {
        const displayMonth = this.monthArr[(month + diffYear - 1) % 12].symbol;
        dates.push([displayMonthCpy + '' + yearCpy + '-' + displayMonth + '' + year]);
      } else {
        dates.push([displayMonthCpy + ' ' + year]);
      }
    }
    this.tableMonths = dates;
  }

  createEmptyDateRange() {

    const startDate = new Date(this.cultureMinDateObj);
    const dates: Date[] = [];
    for (let i = 1; i <= (startDate.getDate() - 1); i++) {
      dates.push(new Date(startDate.getFullYear(), startDate.getMonth(), i));
    }
    this.emptyDatesArr = dates;
    this.emptyDatesArrLength = dates.length;
  }

  fromDateChange() {
    this.dateRangeFromErrorMsg = '';
    this.dateRangeToErrorMsg = '';
    this.isLoading = true;
    const maxTimestamp = this.singleViewSlidermaxValue;

    const minDateObj = new Date(this.singleViewSlidervalue);
    const maxDateObj = new Date(this.singleViewSlidermaxValue);

    const minTimestamp = minDateObj.getTime();
    if ((maxTimestamp < minTimestamp) || (maxTimestamp - minTimestamp < 31536000000)) {

      minDateObj.setFullYear(maxDateObj.getFullYear() - 1);
      minDateObj.setMonth(maxDateObj.getMonth());
      minDateObj.setDate(maxDateObj.getDate() + 1);

      if (this.IsMobile) {
        this.singleViewMinDate = minDateObj.getFullYear().toString() + "-" + ("0" + (minDateObj.getMonth() + 1)).slice(-2) + "-" + minDateObj.getDate().toString();
      } else {
        this.singleViewMinDate = minDateObj.getDate().toString() + "." + (minDateObj.getMonth() + 1).toString() + "." + minDateObj.getFullYear().toString();
      }

      this.singleViewSlidervalue = minDateObj.getTime();
      if (maxTimestamp < minTimestamp) {
        this.translate.get("date_from_greater").subscribe((result: string) => {
          this.dateRangeFromErrorMsg = result;
        });
      } else if (maxTimestamp - minTimestamp < 31536000000) {
        this.translate.get("minimum_one_year_data").subscribe((result: string) => {
          this.dateRangeFromErrorMsg = result;
        });
      }
    }

    if (maxTimestamp - minTimestamp > 220924800000) {
      minDateObj.setFullYear(maxDateObj.getFullYear() - 7);
      minDateObj.setMonth(maxDateObj.getMonth());
      minDateObj.setDate(maxDateObj.getDate() + 1);

      if (this.IsMobile) {
        this.singleViewMinDate = minDateObj.getFullYear().toString() + "-" + ("0" + (minDateObj.getMonth() + 1)).slice(-2) + "-" + minDateObj.getDate().toString();
      } else {
        this.singleViewMinDate = minDateObj.getDate().toString() + "." + (minDateObj.getMonth() + 1).toString() + "." + minDateObj.getFullYear().toString();
      }

      this.singleViewSlidervalue = minDateObj.getTime();
      this.translate.get("maximum_seven_year").subscribe((result: string) => {
        this.dateRangeFromErrorMsg = result;
      });
    }

    this.getSingleFieldByScroll(minDateObj, maxDateObj);
  }

  toDateChange() {
    this.dateRangeFromErrorMsg = '';
    this.dateRangeToErrorMsg = '';
    this.isLoading = true;

    const minTimestamp = this.singleViewSlidervalue;
    const minDateObj = new Date(this.singleViewSlidervalue);
    const maxDateObj = new Date(this.singleViewSlidermaxValue);

    const maxTimestamp = maxDateObj.getTime();
    if ((maxTimestamp < minTimestamp) || (maxTimestamp - minTimestamp < 31536000000)) {
      maxDateObj.setFullYear(minDateObj.getFullYear() + 1);
      maxDateObj.setMonth(minDateObj.getMonth());
      maxDateObj.setDate(minDateObj.getDate() - 1);

      if (this.IsMobile) {
        this.singleViewMaxDate = maxDateObj.getFullYear().toString() + "-" + ("0" + (maxDateObj.getMonth() + 1)).slice(-2) + "-" + maxDateObj.getDate().toString();
      } else {
        this.singleViewMaxDate = maxDateObj.getDate().toString() + "." + (maxDateObj.getMonth() + 1).toString() + "." + maxDateObj.getFullYear().toString();
      }

      this.singleViewSlidermaxValue = maxDateObj.getTime();
      if (maxTimestamp < minTimestamp) {
        this.translate.get("to_date_greater").subscribe((result: string) => {
          this.dateRangeToErrorMsg = result;
        });
      } else if (maxTimestamp - minTimestamp < 31536000000) {
        this.translate.get("minimum_one_year_data").subscribe((result: string) => {
          this.dateRangeToErrorMsg = result;
        });
      }
    }

    if (maxTimestamp - minTimestamp > 220924800000) {
      maxDateObj.setFullYear(minDateObj.getFullYear() + 7);
      maxDateObj.setMonth(minDateObj.getMonth());
      maxDateObj.setDate(minDateObj.getDate() - 1);

      if (this.IsMobile) {
        this.singleViewMaxDate = maxDateObj.getFullYear().toString() + "-" + ("0" + (maxDateObj.getMonth() + 1)).slice(-2) + "-" + maxDateObj.getDate().toString();
      } else {
        this.singleViewMaxDate = maxDateObj.getDate().toString() + "." + (maxDateObj.getMonth() + 1).toString() + "." + maxDateObj.getFullYear().toString();
      }

      this.singleViewSlidermaxValue = maxDateObj.getTime();
      this.translate.get("maximum_seven_year").subscribe((result: string) => {
        this.dateRangeToErrorMsg = result;
      });
    }
    this.getSingleFieldByScroll(minDateObj, maxDateObj);
  }

  setFullMonth(columnindex, obj) {
    const start = new Date(this.cultureMinDateObj);
    const startMonth = start.getMonth();
    const startDate = start.getDate();
    if (columnindex === 0) {
      obj.fullViewMonthShow = this.monthArr[startMonth].symbol;
      obj.fullViewMonthNo = startMonth;
    }
    start.setDate(start.getDate() + columnindex);
    const newMonth = start.getMonth();
    if (obj.fullViewMonthNo !== newMonth || (columnindex === 0 && startDate <= 15)) {
      obj.fullViewMonthShow = this.monthArr[newMonth].symbol;
      obj.fullViewMonthNo = newMonth;
      return true;
    } else {
      return false;
    }
  }

  checkShowMonthOnField(columnindex) {
    const start = new Date(this.cultureMinDateObj);
    const startMonth = start.getMonth();
    const startDate = start.getDate();
    if (columnindex === 0) {
      this.fullViewMonthShow = this.monthArr[startMonth].symbol;
      this.fullViewMonthNo = startMonth;
    }
    start.setDate(start.getDate() + columnindex);
    const newMonth = start.getMonth();
    if (this.fullViewMonthNo !== newMonth || (columnindex === 0 && startDate <= 15)) {
      this.fullViewMonthShow = this.monthArr[newMonth].symbol;
      this.fullViewMonthNo = newMonth;
      return true;
    } else {
      return false;
    }
  }

  checkUncheckSwapping(event) {
    this.addCultureForm.controls['swappedWith'].setValue('');
    if (event.target.checked) {
      this.showHideSwappedWith = true;
    } else {
      this.showHideSwappedWith = false;
    }
  }

  checkUncheckEcho(event) {
    this.addFieldForm.get('echoSize').reset();
    if (event.target.checked) {
      this.showHideEchoSize = true;
    } else {
      this.showHideEchoSize = false;
    }
  }

  addEditCulturePopup(fieldId, user_id, rows, index, columnindex) {

    this.isSubmitted = false;
    this.cultureSizeIssue = false;
    this.isLoading = true;
    if (fieldId !== undefined && rows.culture_id !== undefined && rows.crop_id !== undefined && rows.culture_size !== undefined && rows.end_date !== undefined && rows.start_date !== undefined && rows.is_swapped !== undefined && rows.swapped_with !== undefined && rows.plantVariety !== undefined && rows.culture_notes !== undefined && index !== undefined) {
      this.editCulture(fieldId, user_id, rows);
    } else {
      if (user_id == 1) {
        this.fieldSaveShow = false;
      } else {
        this.fieldSaveShow = true;
      }
      var newdate = new Date(this.cultureMinDateObj);
      newdate.setDate(newdate.getDate() + columnindex);
      var selectedDate = ("0" + (newdate.getDate())).slice(-2) + '.' + ("0" + (newdate.getMonth() + 1)).slice(-2) + '.' + newdate.getFullYear();
      this.fieldDetailsByIdIndex$;
      const fieldIdByIndex = this.dashboardData$[index].field_id;
      if (fieldIdByIndex === fieldId) {
        this.isError = false;
        this.cultureModal = 'block';
        $("#culturemodalId").modal('show');
        $("#culturemodalId").modal({ backdrop: "static" });
        this.translate.get("add_new_culture").subscribe((result: string) => {
          this.addEditTextCultre = result;
        });

        this.addCultureForm.reset();
        this.addCultureForm.controls['fieldId'].setValue(fieldId);
        this.addCultureForm.controls['dateFrom'].setValue(selectedDate);
        this.addCultureForm.controls['harvest_date'].setValue(selectedDate);
        this.isLoading = false;
      }
      this.renderDatepicker('dateFrom', null);
      this.renderDatepicker('dateTo', newdate);
      this.renderDatepicker('harvestDateTo', newdate);
      this.getFieldRemainingSpaceForDuration();
      this.deleteCulture = 'none';
    }
  }

  showHideSizePerCategoryCrops() {

    var show_more = "";
    var show_less = "";

    this.translate.get("show_more").subscribe((result: string) => {
      show_more = result;
    });
    this.translate.get("show_less").subscribe((result: string) => {
      show_less = result;
    });
    if (this.sizePerCategoryMoreLessText == show_more) {
      this.sizePerCategoryMoreLessText = show_less;
    } else {
      this.sizePerCategoryMoreLessText = show_more;
    }
    this.sizePerCategoryMoreLessShowHide = (this.sizePerCategoryMoreLessShowHide === false) ? true : false;
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'field') {
          that.closeFieldModal();
        }
        if (modalName === 'culture') {
          that.closeCultureModal();
        }
        if (modalName === 'export') {
          that.closeexportModal();
        }
        modal.style.display = 'none';
      }
    };
  }

  deleteCulturefunc() {
    const cultureId = this.addCultureForm.value.cultureId;
    const fieldId = this.addCultureForm.value.fieldId;
    if (cultureId !== '') {
      const input = { 'cultureId': cultureId, 'fieldId': fieldId };
      this.isLoading = true;
      this.dashboard.deleteCulture(input)
        .pipe(first())
        .subscribe((data: any) => {
          if (data.status) {
            this.isError = false;
            this.addCultureForm.reset();
            this.cultureModal = 'none';
            $("#culturemodalId").modal('hide');
            this.rangeInput = { 'min': this.cultureMinDateObj, 'max': this.cultureMaxDateObj };
            this.getDashboardChartData(this.rangeInput);
            this.isLoading = false;
          } else {
            this.isError = true;
            this.cultureModal = 'block';
            $("#culturemodalId").modal('show');
            this.translate.get(data.message).subscribe((result: string) => {
              this.errorMessage = result;
            });
            this.isLoading = false;
          }
        });
    } else {
      this.isError = true;
      this.cultureModal = 'block';
      $("#culturemodalId").modal('show');
      this.translate.get("error_something_wrong").subscribe((result: string) => {
        this.errorMessage = result;
      });
    }
  }

  showPreviousYearSlider(minDateObj) {
    const dates: Date[] = [];
    const currentYear = minDateObj.getFullYear();
    const lastYear = currentYear - 1;
    for (let i = 1; i <= 1095; i++) {
      dates.push(new Date(lastYear, 0, i));
    }
    const newOptions: Options = Object.assign({}, this.fullViewSlideroptions);
    newOptions.stepsArray = dates.map((date: Date) => {
      return { value: date.getTime() };
    });
    this.fullViewSlideroptions = newOptions;
  }

  showNextYearSlider(maxDateObj) {
    const dates: Date[] = [];
    const currentYear = maxDateObj.getFullYear();
    const lastYear = currentYear - 1;
    for (let i = 1; i <= 1095; i++) {
      dates.push(new Date(lastYear, 0, i));
    }

    const newOptions: Options = Object.assign({}, this.fullViewSlideroptions);
    newOptions.stepsArray = dates.map((date: Date) => {
      return { value: date.getTime() };
    });
    this.fullViewSlideroptions = newOptions;
  }

  tabularExport() {
    if (this.exportForm.invalid) {
      return;
    }

    const currentUser: any = this.storage.get('loginSession');
    this.exportForm.controls['language'].setValue(currentUser.language);

    const selectedOrderIds = this.exportForm.value.fields
      .map((v, i) => v ? this.userFields[i].field_id : null)
      .filter(v => v !== null);

    if (selectedOrderIds.length > 0) {
      this.exportForm.controls['fieldIds'].setValue(selectedOrderIds);
    } else {
      this.exportForm.controls['fieldIds'].setValue([]);
    }
    this.isLoading = true;
    const formvalues = this.exportForm.value;
    this.dashboard.tabularExport(formvalues)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          this.isError = false;
          this.errorMessage = '';
          const file_path = data.data;
          const a = document.createElement('a');
          a.href = file_path;
          a.target = '_blank';
          a.download = '';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          this.isLoading = false;
          this.getusersAllExports();
        } else {
          this.isError = true;
          this.translate.get(data.message).subscribe((result: string) => {
            this.errorMessage = result;
          });
          this.isLoading = false;
        }
      });
  }

  graphicalExport() {
    if (this.exportForm.invalid) {
      return;
    }

    const currentUser: any = this.storage.get('loginSession');
    this.exportForm.controls['language'].setValue(currentUser.language);

    const selectedOrderIds = this.exportForm.value.fields
      .map((v, i) => v ? this.userFields[i].field_id : null)
      .filter(v => v !== null);

    if (selectedOrderIds.length > 0) {
      this.exportForm.controls['fieldIds'].setValue(selectedOrderIds);
    } else {
      this.exportForm.controls['fieldIds'].setValue([]);
    }
    this.isLoading = true;
    const formvalues = this.exportForm.value;
    this.dashboard.chartExportCheck(formvalues)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          this.isError = false;
          this.errorMessage = '';
          this.transfereService.setinputData(formvalues);
          this.isLoading = false;
          this.router.navigate(['graphicalExport']);
        } else {
          this.isError = true;
          this.translate.get(data.message).subscribe((result: string) => {
            this.errorMessage = result;
          });
          this.isLoading = false;
        }
      });
  }

  openmasterModal(enddate, harvest_date, field_name, crop_name, culture_size, field_id, culture_id, user_id) {

    this.resetPlantActivityForm();
    if (user_id == 1) {
      this.fieldSaveShow = false;
    } else {
      this.fieldSaveShow = true;
    }

    this.showOtherCrops = true;
    this.fertiActivityForm.reset();

    this.disabledField = '';
    if (enddate == null) {
      this.cultureEndDate = harvest_date;
    } else {
      this.cultureEndDate = enddate;
    }
    this.cultureSizeMaster = culture_size;
    this.cultureIdMaster = culture_id;
    this.fieldIdMaster = field_id;
    this.userIdMaster = user_id;

    this.fertiActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.fertiActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);

    const todaydate = new Date();
    const current_date = todaydate.getFullYear().toString() + "-" + ("0" + (todaydate.getMonth() + 1)).slice(-2) + "-" + todaydate.getDate().toString();
    if (this.IsMobile == true) {
      var new_date = ("0" + (todaydate.getMonth() + 1)).slice(-2) + "/" + todaydate.getDate().toString() + "/" + todaydate.getFullYear().toString();
    } else {
      var new_date = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }

    this.masterFertilizeDate = new_date;
    this.masterPlantProDate = new_date;
    this.fertiActivityForm.controls['fertilizer_date'].setValue(new_date);
    this.plantActivityForm.controls['plant_date'].setValue(new_date);
    this.soilActivityForm.controls['soil_date'].setValue(new_date);

    this.isError = false;
    this.errorMessage = '';

    if (this.IsMobile == false) {
      this.renderDetailsDatepicker('masterFertzrfromDate', null);
    }

    this.getAllMachine();
    this.getAllFertilizer();
    this.FertilizerCultureDetail(culture_id);
    this.getAllFertilizerActivity(culture_id);
    this.getCropListMultiFertilizer(culture_id, current_date);
    this.getCropListMultiSoil(culture_id, current_date);
    this.getCropListMultiPlant(culture_id, current_date);

    this.getAllSoil();
    this.getAllPesticide();
    this.getAllPests();

    this.mastersModal = 'block';
    $("#mastersModalId").modal('show');
    $("#masterCrop").html('<span>' + crop_name + ' </span> (' + culture_size + 'a) ');
    $('#masterField').html(' ' + field_name);

    this.fertiActivityForm.controls['fertilizer_id'].setValue('');
    this.fertiActivityForm.controls['machine_id'].setValue('');
    this.fertiActivityForm.controls['unit'].setValue('');

  }

  getCropMultiListByDate() {
    this.getCropListMultiFertilizer(this.cultureIdMaster, this.masterFertilizeDate);
  }

  PlantTabOpen() {

    this.plantActivityForm.reset();
    this.plantActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.plantActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);
    this.getAllPlantActivity(this.cultureIdMaster);
    const todaydate = new Date();
    if (this.IsMobile) {
      this.masterPlantProDate = ("0" + (todaydate.getMonth() + 1)).slice(-2) + "/" + todaydate.getDate().toString() + "/" + todaydate.getFullYear().toString();
    } else {
      this.masterPlantProDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    this.plantActivityForm.controls['plant_date'].setValue(this.masterPlantProDate);
    if (this.IsMobile == false) {
      this.renderDetailsDatepicker('masterPlantProfromDate', null);
    }

    for (let i = 0; i < this.plantMultiple.length; i++) {
      this.plantActivityForm.controls['pests_id_' + i].setValue('');
      this.plantActivityForm.controls['pesticide_id_' + i].setValue('');
      this.plantActivityForm.controls['unit_' + i].setValue('');
    }
    this.plantActivityForm.controls['machine_id'].setValue('');
    this.removePlantField(1);
  }

  SoilTabOpen() {

    this.soilActivityForm.reset();
    this.soilActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.soilActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);

    const todaydate = new Date();
    if (this.IsMobile) {
      this.masterSoilDate = ("0" + (todaydate.getMonth() + 1)).slice(-2) + "/" + todaydate.getDate().toString() + "/" + todaydate.getFullYear().toString();
    } else {
      this.masterSoilDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    this.soilActivityForm.controls['soil_date'].setValue(this.masterSoilDate);

    if (this.IsMobile == false) {
      this.renderDetailsDatepicker('masterSoilfromDate', null);
    }
    this.getAllSoilActivity(this.cultureIdMaster);

    this.soilActivityForm.controls['soil_id'].setValue('');
    this.soilActivityForm.controls['machine_id'].setValue('');
  }

  closemasterModal() {
    this.mastersModal = 'none';
    $("#mastersModalId").modal('hide');
    this.plantActivityForm.reset();
    this.soilActivityForm.reset();
    this.fertiActivityForm.reset();
  }

  getAllPesticide() {
    this.master.getAllPesticide().pipe(first()).subscribe((data: any) => {
      var dataList = [];
      for (let detail of data['data']) {
        if (this.userIdMaster == 1) {
          if (detail.is_example == 1) {
            this.translate.get("pesticide_example").subscribe((result: string) => {
              detail.pesticide_name = result;
            });
            dataList.push(detail);
          }
        } else {
          if (detail.is_example == 0) {
            dataList.push(detail);
          }
        }
      }
      this.allPesticide$ = dataList;
    });
  }

  getAllMachine() {
    this.master.getAllMachine().pipe(first()).subscribe((data: any) => {
      var dataList = [];
      for (let detail of data['data']) {
        if (this.userIdMaster == 1) {
          if (detail.is_example == 1) {
            dataList.push(detail);
          }
        } else {
          if (detail.is_example == 0) {
            dataList.push(detail);
          }
        }
      }
      this.allMachine$ = dataList;
    });
  }

  getAllPests() {
    this.master.getAllPests().pipe(first()).subscribe((data: any) => {
      var dataList = [];
      for (let detail of data['data']) {
        if (this.userIdMaster == 1) {
          if (detail.is_example == 1) {
            this.translate.get("pest_example").subscribe((result: string) => {
              detail.pests_name = result;
            });
            dataList.push(detail);
          }
        } else {
          if (detail.is_example == 0) {
            dataList.push(detail);
          }
        }
      }
      this.allPests$ = dataList;
    });
  }

  getAllSoil() {
    this.master.getAllSoil().pipe(first()).subscribe((data: any) => {

      var dataList = [];
      for (let detail of data['data']) {
        if (this.userIdMaster == 1) {
          if (detail.is_example == 1) {
            this.translate.get("soil_example").subscribe((result: string) => {
              detail.soil_name = result;
            });
            dataList.push(detail);
          }
        } else {
          if (detail.is_example == 0) {
            dataList.push(detail);
          }
        }
      }
      this.allSoil$ = dataList;
    });
  }

  getAllFertilizer() {
    this.master.getAllFertilizer().pipe(first()).subscribe((data: any) => {
      var dataList = [];
      for (let detail of data['data']) {
        if (this.userIdMaster == 1) {
          if (detail.is_example == 1) {
            this.translate.get("Fertilizer_example").subscribe((result: string) => {
              detail.fertilizer_name = result;
            });
            dataList.push(detail);
          }
        } else {
          if (detail.is_example == 0) {
            dataList.push(detail);
          }
        }
      }
      this.allFertilizer$ = dataList;
    });
  }

  getProductDetail(i) {

    var plant_date = '';
    const id = this.plantActivityForm.value['pesticide_id_' + i];
    plant_date = this.plantActivityForm.value.plant_date;
    var amount = this.plantActivityForm.get('amount_' + i).value;

    const other_crop_ids = this.plantActivityForm.value.other_crop_ids;

    if (id != '') {
      for (let x of this.allPesticide$) {
        if (x.pesticide_id == id) {

          const unit = x.unit;
          const waitingDays = +x.waiting_days;
          const dateArr = plant_date.split(".");
          const plantDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
          var dte = new Date(plantDate);
          dte.setDate(dte.getDate() + waitingDays);
          dte.setHours(0); dte.setMinutes(0);
          const newtDate = dte.getFullYear().toString() + "-" + ("0" + (dte.getMonth() + 1)).slice(-2) + "-" + dte.getDate().toString();
          var cltrDate = new Date(this.cultureEndDate);
          cltrDate.setHours(0); cltrDate.setMinutes(0);

          const waitingDateTime = dte.getTime();
          const cultureDateTime = cltrDate.getTime();
          this.plantActivityForm.controls['waiting_time_' + i].setValue(waitingDays);
          this.plantActivityForm.controls['waiting_period_' + i].setValue(waitingDays + ' days');
          this.plantActivityForm.controls['end_date_' + i].setValue(newtDate);
          this.plantActivityForm.controls['unit_' + i].setValue(unit);
          this.plantActivityForm.controls['unit_with_ha_' + i].setValue(unit + '/ha');
          if (waitingDateTime > cultureDateTime) {
            //console.log('waiting error');
            //var err = 'waiting date (' + newtDate + ') is greater than culture end date (' + this.cultureEndDate + ')';
            var err = '';
            this.translate.get('waiting_warning_single_crop', { 'watiting_date': newtDate, 'end_date': this.cultureEndDate }).subscribe((result: string) => {
              err = result;
            });

            this.isError = true;
            this.errorMessage = err;
          } else {
            this.isError = false;
            this.errorMessage = '';
          }

          //// get total for all other crops ///
          var waiting_period = this.plantActivityForm.get('waiting_time_' + i).value;
          var all_total_amount = amount * (this.cultureSizeMaster / this.hactareConversion);

          var errCropName = '';
          if (other_crop_ids != null) {
            if (other_crop_ids.length > 0) {

              for (var k = 0; k < other_crop_ids.length; k++) {
                var ac_cultureId = other_crop_ids[k];
                for (var j = 0; j < this.cropMultiListPlant$.length; j++) {
                  if (this.cropMultiListPlant$[j]['ac_cultureId'] == ac_cultureId) {

                    var end_date = this.cropMultiListPlant$[j]['end_date'];
                    var harvest_date = this.cropMultiListPlant$[j]['harvest_date'];

                    if (end_date == null) {
                      var cultureEndDate = harvest_date;
                    } else {
                      var cultureEndDate = end_date;
                    }
                    const waitingDays = waiting_period;

                    plant_date = this.plantActivityForm.value.plant_date;
                    const dateArr = plant_date.split(".");
                    const plantDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
                    var dte = new Date(plantDate);
                    dte.setDate(dte.getDate() + waitingDays);
                    dte.setHours(0); dte.setMinutes(0);
                    const newtDate = dte.getFullYear().toString() + "-" + ("0" + (dte.getMonth() + 1)).slice(-2) + "-" + dte.getDate().toString();
                    var cltrDate = new Date(cultureEndDate);
                    cltrDate.setHours(0); cltrDate.setMinutes(0);
                    const waitingDateTime = dte.getTime();
                    const cultureDateTime = cltrDate.getTime();
                    if (waitingDateTime > cultureDateTime) {
                      if (errCropName == '') {
                        var errCropName = ' '+this.cropMultiListPlant$[j]['crop_name'] + '.';
                      } else {
                        var errCropName = this.cropMultiListPlant$[j]['crop_name'] + ',' + errCropName;
                      }
                    }
                    if (this.cropMultiListPlant$[j]['size'] != '') {
                      const sizeinhectare = (this.cropMultiListPlant$[j]['size'] / this.hactareConversion);
                      var all_total_amount = (amount * sizeinhectare) + all_total_amount;
                      if (unit) {
                        this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount + ' ' + unit);
                      } else {
                        this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount);
                      }
                    }
                  }
                }
              }

            } else {
              this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount);
            }
            if (errCropName != '') {
              var warning_text = "";
              this.translate.get('waiting_period_warning_other').subscribe((result: string) => {
                warning_text = result;
              });
              var err = warning_text + errCropName;
              this.isError = true;
              this.errorMessage = err;
            } else {
              this.isError = false;
              this.errorMessage = '';
            }
          }//// End get total for all other crops ///

        }
      }
    } else {
      this.plantActivityForm.controls['waiting_period_' + i].setValue('');
      this.plantActivityForm.controls['waiting_time_' + i].setValue('');
    }

  }

  plantProtectionAmount(i) {

    const amount = this.plantActivityForm.value['amount_' + i];
    const unit = this.plantActivityForm.value['unit_' + i];
    const other_crop_ids = this.plantActivityForm.value.other_crop_ids;

    if (amount != '' && amount > 0) {
      this.cultureSizeMaster;
      const sizeinhectare = (this.cultureSizeMaster / this.hactareConversion);
      var total_amount = amount * sizeinhectare;
      if (unit) {
        this.plantActivityForm.controls['total_amount_' + i].setValue(total_amount + ' ' + unit);
      } else {
        this.plantActivityForm.controls['total_amount_' + i].setValue(total_amount);
      }

      //// get total for all other crops ///
      var all_total_amount = total_amount;
      if (other_crop_ids != null) {
        if (other_crop_ids.length > 0) {
          for (var k = 0; k < other_crop_ids.length; k++) {
            var ac_cultureId = other_crop_ids[k];
            for (var j = 0; j < this.cropMultiListPlant$.length; j++) {
              if (this.cropMultiListPlant$[j]['ac_cultureId'] == ac_cultureId) {

                if (this.cropMultiListPlant$[j]['size'] != '') {
                  const sizeinhectare = (this.cropMultiListPlant$[j]['size'] / this.hactareConversion);
                  var all_total_amount = (amount * sizeinhectare) + all_total_amount;
                  if (unit) {
                    this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount + ' ' + unit);
                  } else {
                    this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount);
                  }
                }
              }
            }
          }
        } else {
          this.plantActivityForm.controls['all_total_amount_' + i].setValue(all_total_amount + ' ' + unit);
        }
      }//// End get total for all other crops ///

    } else {
      this.plantActivityForm.controls['total_amount_' + i].setValue('');
    }

  }

  addPlantField(i) {

    if (this.plantMultiple.length < 10) {
      this.plantMultiple.push({
        pesticide_id: '',
        amount: '',
        unit: '',
        total_amount: '',
        waiting_period: '',
        pests_id: '',
        end_date: '',
        waiting_time: '',
        all_total_amount: '',
        unit_with_ha: ''
      });

      var i = i + 1;
      var pesticide_id = 'pesticide_id_' + i;
      var amount = 'amount_' + i;
      var unit = 'unit_' + i;
      var waiting_period = 'waiting_period_' + i;
      var total_amount = 'total_amount_' + i;
      var pests_id = 'pests_id_' + i;
      var end_date = 'end_date_' + i;
      var waiting_time = 'waiting_time_' + i;
      var all_total_amount = 'all_total_amount_' + i;
      var unit_with_ha = 'unit_with_ha_' + i;

      this.plantActivityForm.addControl(pesticide_id, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(amount, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(unit, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(waiting_period, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(total_amount, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(pests_id, new FormControl('', Validators.required));
      this.plantActivityForm.addControl(end_date, new FormControl(''));
      this.plantActivityForm.addControl(waiting_time, new FormControl(''));
      this.plantActivityForm.addControl(all_total_amount, new FormControl(''));
      this.plantActivityForm.addControl(unit_with_ha, new FormControl(''));
    }
  }

  removePlantField(i: number) {
    this.plantMultiple.splice(i, 1);

    this.plantActivityForm.controls['pests_id_' + i].setValue('');
    this.plantActivityForm.controls['pesticide_id_' + i].setValue('');
    this.plantActivityForm.controls['unit_' + i].setValue('');
    this.plantActivityForm.controls['amount_' + i].setValue('');
    this.plantActivityForm.controls['total_amount_' + i].setValue('');
    this.plantActivityForm.controls['waiting_period_' + i].setValue('');
    this.plantActivityForm.controls['end_date_' + i].setValue('');
    this.plantActivityForm.controls['waiting_time_' + i].setValue('');
    this.plantActivityForm.controls['all_total_amount_' + i].setValue('');
    this.plantActivityForm.controls['unit_with_ha_' + i].setValue('');

    var pesticide_id = 'pesticide_id_' + i;
    var amount = 'amount_' + i;
    var unit = 'unit_' + i;
    var waiting_period = 'waiting_period_' + i;
    var total_amount = 'total_amount_' + i;
    var pests_id = 'pests_id_' + i;
    var end_date = 'end_date_' + i;
    var waiting_time = 'waiting_time_' + i;
    var all_total_amount = 'all_total_amount_' + i;
    var unit_with_ha = 'unit_with_ha_' + i;

    this.plantActivityForm.removeControl(pesticide_id);
    this.plantActivityForm.removeControl(amount);
    this.plantActivityForm.removeControl(unit);
    this.plantActivityForm.removeControl(waiting_period);
    this.plantActivityForm.removeControl(total_amount);
    this.plantActivityForm.removeControl(pests_id);
    this.plantActivityForm.removeControl(end_date);
    this.plantActivityForm.removeControl(waiting_time);
    this.plantActivityForm.removeControl(all_total_amount);
    this.plantActivityForm.removeControl(unit_with_ha);
  }

  multiCropSelectPlant(event) {

    var plant_date = this.plantActivityForm.value.plant_date;
    //const plant_date = this.plantActivityForm.value.plant_date;
    if(!this.IsMobile){
      const dateArr = plant_date.split(".");
      var plantDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
    }else{
      const dateArr = plant_date.split("/");
      var plantDate = dateArr[2] + "-" + dateArr[0] + "-" + dateArr[1];
    }
    
    var dte = new Date(plantDate);
    var errCropName = '';
    for (let k = 0; k < this.plantMultiple.length; k++) {

      var amount = this.plantActivityForm.get('amount_' + k).value;
      var unit = this.plantActivityForm.get('unit_' + k).value;
      var waiting_period = this.plantActivityForm.get('waiting_time_' + k).value;

      var all_total_amount = amount * (this.cultureSizeMaster / this.hactareConversion);
      if (event.value) {
        if (event.value.length > 0) {
          for (var i = 0; i < event.value.length; i++) {
            var ac_cultureId = event.value[i];
            for (var j = 0; j < this.cropMultiListPlant$.length; j++) {
              if (this.cropMultiListPlant$[j]['ac_cultureId'] == ac_cultureId) {

                var end_date = this.cropMultiListPlant$[j]['end_date'];
                var harvest_date = this.cropMultiListPlant$[j]['harvest_date'];

                if (end_date == null) {
                  var cultureEndDate = harvest_date;
                } else {
                  var cultureEndDate = end_date;
                }
                const waitingDays = waiting_period;
                plant_date = this.plantActivityForm.value.plant_date;
                const dateArr = plant_date.split(".");
                const plantDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
                var dte = new Date(plantDate);
                dte.setDate(dte.getDate() + waitingDays);
                dte.setHours(0); dte.setMinutes(0);
                const newtDate = dte.getFullYear().toString() + "-" + ("0" + (dte.getMonth() + 1)).slice(-2) + "-" + dte.getDate().toString();
                var cltrDate = new Date(cultureEndDate);
                cltrDate.setHours(0); cltrDate.setMinutes(0);

                const waitingDateTime = dte.getTime();
                const cultureDateTime = cltrDate.getTime();

                if (waitingDateTime > cultureDateTime) {
                  if (errCropName == '') {
                    var errCropName = ' '+this.cropMultiListPlant$[j]['crop_name'] + '.';
                  } else {
                    var errCropName = this.cropMultiListPlant$[j]['crop_name'] + ',' + errCropName;
                  }
                }

                if (this.cropMultiListPlant$[j]['size'] != '') {
                  const sizeinhectare = (this.cropMultiListPlant$[j]['size'] / this.hactareConversion);
                  var all_total_amount = (amount * sizeinhectare) + all_total_amount;
                  if (unit) {
                    this.plantActivityForm.controls['all_total_amount_' + k].setValue(all_total_amount + ' ' + unit);
                  } else {
                    this.plantActivityForm.controls['all_total_amount_' + k].setValue(all_total_amount);
                  }
                }

              }
            }
          }
        } else {
          this.plantActivityForm.controls['all_total_amount_' + k].setValue(all_total_amount + ' ' + unit);
        }
      }
      if (errCropName != '') {
        var warning_text = "";
        this.translate.get('waiting_period_warning_other').subscribe((result: string) => {
          warning_text = result;
        });
        var err = warning_text + errCropName;
        this.isError = true;
        this.errorMessage = err;
      } else {
        this.isError = false;
        this.errorMessage = '';
      }
    }

  }

  PlantActivity() {
    if (this.plantActivityForm.invalid) {
      return;
    }

    const plant_date = this.plantActivityForm.value.plant_date;
    if(!this.IsMobile){
      const dateArr = plant_date.split(".");
      const plantDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
      this.plantActivityForm.controls['plant_date'].setValue(plantDate);
    }
    
    this.master.plantActivity(this.plantActivityForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isError = false;
        this.isSuccess = true;
        this.resetPlantActivityForm();
        this.getAllPlantActivity(this.cultureIdMaster);
        this.plantShowOtherCrops = true;
        this.PlantTabOpen();
      } else {
        this.isError = true;
        this.isSuccess = false;
        this.plantActivityForm.controls['plant_date'].setValue(plant_date);
      }
      if (data.message && data.message != '') {
        this.translate.get(data.message).subscribe((result: string) => {
          if (this.isSuccess == true) {
            this.successMessage = result;
          } else {
            this.errorMessage = result;
          }
        });
      }
    });

  }

  getAllPlantActivity(culture_id) {
    const input = { 'culture_id': culture_id };
    this.master.getAllPlantActivity(input).pipe(first()).subscribe((data: any) => {
      this.allPlantActivity$ = data['data'];
    });
  }

  editPlantActivity(data) {

    this.plantShowOtherCrops = false;

    this.plantActivityForm.controls['plant_protection_id'].setValue(data.plant_protection_id);
    this.plantActivityForm.controls['pesticide_id_0'].setValue(data.pesticide_id);
    //this.plantActivityForm.controls['plant_date'].setValue(data.plant_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
    this.plantActivityForm.controls['amount_0'].setValue(data.amount);
    this.plantActivityForm.controls['unit_0'].setValue(data.unit);
    this.plantActivityForm.controls['unit_with_ha_0'].setValue(data.unit+'/ha');
    this.plantActivityForm.controls['waiting_period_0'].setValue(data.waiting_period);
    this.plantActivityForm.controls['total_amount_0'].setValue(data.total_amount);
    this.plantActivityForm.controls['machine_id'].setValue(data.machine_id);
    this.plantActivityForm.controls['pests_id_0'].setValue(data.pests_id);
    this.plantActivityForm.controls['field_id'].setValue(data.field_id);
    this.plantActivityForm.controls['culture_id'].setValue(data.culture_id);
    this.plantActivityForm.controls['person_name'].setValue(data.person_name);
    this.plantActivityForm.controls['end_date_0'].setValue(data.end_date);
    this.plantActivityForm.controls['comment'].setValue(data.comment);
    if (data.sub_activity != '') {
      for (var i = 0; i < data.sub_activity.length; i++) {

        this.addPlantField(i);

        var subdata = data.sub_activity[i];
        var sr = i + 1;
        this.plantActivityForm.controls['pesticide_id_' + sr].setValue(subdata.pesticide_id);
        this.plantActivityForm.controls['amount_' + sr].setValue(subdata.amount);
        this.plantActivityForm.controls['unit_' + sr].setValue(subdata.unit);
        this.plantActivityForm.controls['waiting_period_' + sr].setValue(subdata.waiting_period);
        this.plantActivityForm.controls['total_amount_' + sr].setValue(subdata.total_amount);
        this.plantActivityForm.controls['pests_id_' + sr].setValue(subdata.pests_id);
        this.plantActivityForm.controls['end_date_' + sr].setValue(subdata.end_date);

      }
    }

    if (data.plant_date != null) {
      if (this.IsMobile) {
        this.plantActivityForm.controls['plant_date'].setValue(data.plant_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      } else {
        this.plantActivityForm.controls['plant_date'].setValue(data.plant_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      }
    }

  }

  deletePlantActivity(id) {
    this.resetPlantActivityForm()
    let that = this;
    let msg = "Are you sure you want to delete this ?"
    that.alertService.confirmThis('Confirmation', msg, function () {
      const input = { 'plant_protection_id': id };
      that.master.deletePlantActivty(input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isError = false;
          that.isSuccess = true;
          that.getAllPlantActivity(that.cultureIdMaster);
        } else {
          that.isError = true;
          that.isSuccess = false;
        }
        if (data.message && data.message != '') {
          that.translate.get(data.message).subscribe((result: string) => {
            if (that.isSuccess == true) {
              that.successMessage = result;
            } else {
              that.errorMessage = result;
            }
          });
        }
      });

    }, function () {
      ////console.log('No');
    });
  }

  resetPlantActivityForm() {

    this.plantActivityForm.reset();

    const todaydate = new Date();
    if (this.IsMobile) {
      this.masterPlantProDate = todaydate.getFullYear().toString() + "-" + ("0" + (todaydate.getMonth() + 1)).slice(-2) + "-" + todaydate.getDate().toString();
    } else {
      this.masterPlantProDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    this.plantActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.plantActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);

    this.plantMultiple = [];
    this.addPlantField(0);

  }

  soilActivity() {
    if (this.soilActivityForm.invalid) {
      return;
    }
    const soil_date = this.soilActivityForm.value.soil_date;
    if(!this.IsMobile){
      const dateArr = soil_date.split(".");
      const soilDate = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
      this.soilActivityForm.controls['soil_date'].setValue(soilDate);
    }
    this.master.soilActivity(this.soilActivityForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isError = false;
        this.isSuccess = true;
        this.resetSoilActivityForm();
        this.getAllSoilActivity(this.cultureIdMaster);
        this.soilShowOtherCrops = true;
      } else {
        this.isError = true;
        this.isSuccess = false;
        this.soilActivityForm.controls['soil_date'].setValue(soil_date);
      }
      if (data.message && data.message != '') {
        this.translate.get(data.message).subscribe((result: string) => {
          if (this.isSuccess == true) {
            this.successMessage = result;
          } else {
            this.errorMessage = result;
          }
        });
      }
    });
  }

  getAllSoilActivity(culture_id) {
    const input = { 'culture_id': culture_id };
    this.master.getAllSoilActivity(input).pipe(first()).subscribe((data: any) => {
      this.allSoilActivity$ = data['data'];
    });
  }

  editSoilActivity(data) {
    this.soilShowOtherCrops = false;
    this.soilActivityForm.controls['soil_activity_id'].setValue(data.soil_activity_id);
    //this.soilActivityForm.controls['soil_date'].setValue(data.soil_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
    this.soilActivityForm.controls['machine_id'].setValue(data.machine_id);
    this.soilActivityForm.controls['soil_id'].setValue(data.soil_id);
    this.soilActivityForm.controls['person_name'].setValue(data.person_name);
    this.soilActivityForm.controls['comment'].setValue(data.comment);
    this.soilActivityForm.controls['field_id'].setValue(data.field_id);
    this.soilActivityForm.controls['culture_id'].setValue(data.culture_id);

    if (data.soil_date != null) {
      if (this.IsMobile) {
        this.soilActivityForm.controls['soil_date'].setValue(data.soil_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      } else {
        this.soilActivityForm.controls['soil_date'].setValue(data.soil_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      }
    }
  }

  deleteSoilActivty(id) {
    this.resetSoilActivityForm();
    let that = this;
    let msg = "Are you sure you want to delete this ?"
    that.alertService.confirmThis('Confirmation', msg, function () {
      const input = { 'soil_activity_id': id };
      that.master.deleteSoilActivty(input).pipe(first()).subscribe((data: any) => {

        if (data.status) {
          that.isError = false;
          that.isSuccess = true;
          that.getAllSoilActivity(that.cultureIdMaster);
        } else {
          that.isError = true;
          that.isSuccess = false;
        }
        if (data.message && data.message != '') {
          that.translate.get(data.message).subscribe((result: string) => {
            if (that.isSuccess == true) {
              that.successMessage = result;
            } else {
              that.errorMessage = result;
            }
          });
        }
      });

    }, function () {
      ///console.log('No');
    });
  }

  resetSoilActivityForm() {

    this.soilActivityForm.reset();

    const todaydate = new Date();
    if (this.IsMobile) {
      this.masterSoilDate = todaydate.getFullYear().toString() + "-" + ("0" + (todaydate.getMonth() + 1)).slice(-2) + "-" + todaydate.getDate().toString();
    } else {
      this.masterSoilDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    this.soilActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.soilActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);

  }

  FertilizerActivity() {
    if (this.fertiActivityForm.invalid) {
      return;
    }
    const fertilizer_date = this.fertiActivityForm.value.fertilizer_date;
    this.master.fertilizerActivity(this.fertiActivityForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isError = false;
        this.isSuccess = true;
        this.resetFertiActivityForm();
        this.getAllFertilizerActivity(this.cultureIdMaster);
        this.showOtherCrops = true;
      } else {
        this.isError = true;
        this.isSuccess = false;
        this.fertiActivityForm.controls['fertilizer_date'].setValue(fertilizer_date);
      }
      if (data.message && data.message != '') {
        this.translate.get(data.message).subscribe((result: string) => {
          if (this.isSuccess == true) {
            this.successMessage = result;
          } else {
            this.errorMessage = result;
          }
        });
      }
    });
  }

  getCropListMultiFertilizer(culture_id, fertilizer_date) {
    const currentUser: any = this.storage.get('loginSession');
    const input = { 'culture_id': culture_id, 'fertilizer_date': fertilizer_date, 'language': currentUser.language };
    this.master.getCropMultiFertilizer(input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.cropMultiList$ = data.data;
      }
    });
  }

  getCropListMultiSoil(culture_id, soil_date) {
    const currentUser: any = this.storage.get('loginSession');
    const input = { 'culture_id': culture_id, 'soil_date': soil_date, 'language': currentUser.language };
    this.master.getCropMultiSoil(input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.cropMultiListSoil$ = data.data;
      }
    });
  }

  getCropListMultiPlant(culture_id, fertilizer_date) {
    const currentUser: any = this.storage.get('loginSession');
    const input = { 'culture_id': culture_id, 'fertilizer_date': fertilizer_date, 'language': currentUser.language };
    this.master.getCropMultiFertilizer(input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.cropMultiListPlant$ = data.data;
      }
    });
  }

  multiCropSelect(event) {

    const amount = this.fertiActivityForm.value.amount;
    const unit = this.fertiActivityForm.value.unit;
    const total_amount_unit = this.fertiActivityForm.value.total_amount;
    var total_amount = total_amount_unit;
    if (event.value.length > 0) {
      for (var i = 0; i < event.value.length; i++) {
        var ac_cultureId = event.value[i];
        for (var j = 0; j < this.cropMultiList$.length; j++) {
          if (this.cropMultiList$[j]['ac_cultureId'] == ac_cultureId) {
            if (this.cropMultiList$[j]['size'] != '') {
              const sizeinhectare = (this.cropMultiList$[j]['size'] / this.hactareConversion);
              var total_amount = (amount * sizeinhectare) + total_amount;
              this.fertiActivityForm.controls['all_crop_amount'].setValue(total_amount + unit);
            }
          }
        }
      }
    } else {
      this.fertiActivityForm.controls['all_crop_amount'].setValue('');
    }

  }

  FertilizerTotalAmount() {

    const other_crop_ids = this.fertiActivityForm.value.other_crop_ids;
    const fertilizer_id = this.fertiActivityForm.value.fertilizer_id;
    for (var i = 0; i < this.allFertilizer$.length; i++) {
      if (this.allFertilizer$[i]['fertilizer_id'] == fertilizer_id) {
        const unit = this.allFertilizer$[i]['unit'];
        this.fertiActivityForm.controls['unit'].setValue(unit);
        this.fertiActivityForm.controls['unit_with_ha'].setValue(unit + '/ha');
      }
    }

    const amount = this.fertiActivityForm.value.amount;
    const unit = this.fertiActivityForm.value.unit;

    var factor_nitrogen = 1;
    var factor_phosphorus = this.fertiActivityForm.value.factor_phosphorus;
    factor_phosphorus = (factor_phosphorus != null) ? factor_phosphorus : 1;

    var factor_potassium = this.fertiActivityForm.value.factor_potassium;
    factor_potassium = (factor_potassium != null) ? factor_potassium : 1;

    var factor_magnesium = this.fertiActivityForm.value.factor_magnesium;
    factor_magnesium = (factor_magnesium != null) ? factor_magnesium : 1;

    var basic_nitrogen = this.fertiActivityForm.value.basic_nitrogen;
    var basic_phosphorus = this.fertiActivityForm.value.basic_phosphorus;
    var basic_potassium = this.fertiActivityForm.value.basic_potassium;
    var basic_magnesium = this.fertiActivityForm.value.basic_magnesium;

    if (basic_nitrogen > 0) {
      var corr_nitrogen = factor_nitrogen * basic_nitrogen;
      this.fertiActivityForm.controls['corrected_nitrogen'].setValue(corr_nitrogen);
    }
    if (basic_phosphorus > 0) {
      var corr_phosphorus = factor_phosphorus * basic_phosphorus;
      this.fertiActivityForm.controls['corrected_phosphorus'].setValue(corr_phosphorus);
    }
    if (basic_potassium > 0) {
      var corr_potassium = factor_potassium * basic_potassium;
      this.fertiActivityForm.controls['corrected_potassium'].setValue(corr_potassium);
    }
    if (basic_magnesium > 0) {
      var corr_magnesium = factor_magnesium * basic_magnesium;
      this.fertiActivityForm.controls['corrected_magnesium'].setValue(corr_magnesium);
    }

    if (amount != '' && amount > 0) {
      this.cultureSizeMaster;
      const sizeinhectare = (this.cultureSizeMaster / this.hactareConversion);
      const total_amount = amount * sizeinhectare;
      /// corrected demand ///
      /// cd = cf*bd
      if (unit) {
        var cal_amount = amount;
        if (unit == 'g') {
          /// 1kg=1000g ///
          cal_amount = amount / 1000;
        } else if (unit == 'dl') {
          /// 1l=10dl ///
          cal_amount = amount / 10;
        }

        if (fertilizer_id != '') {

          //// get total for all other crops ///
          if (other_crop_ids != null) {
            if (other_crop_ids.length > 0) {
              var crop_total_amount = total_amount;
              for (var i = 0; i < other_crop_ids.length; i++) {
                var ac_cultureId = other_crop_ids[i];
                for (var j = 0; j < this.cropMultiList$.length; j++) {
                  if (this.cropMultiList$[j]['ac_cultureId'] == ac_cultureId) {
                    if (this.cropMultiList$[j]['size'] != '') {
                      const cropsizeinhectare = (this.cropMultiList$[j]['size'] / this.hactareConversion);
                      var crop_total_amount = (amount * cropsizeinhectare) + crop_total_amount;
                      this.fertiActivityForm.controls['all_crop_amount'].setValue(crop_total_amount + unit);
                    }
                  }
                }
              }
            } else {
              this.fertiActivityForm.controls['all_crop_amount'].setValue('');
            }
          }//// End get total for all other crops ///

          const input = { 'fertilizer_id': fertilizer_id }
          this.master.getFertilizerDetail(input).pipe(first()).subscribe((result: any) => {
            if (result.status) {
              const new_nitrogen = result.data.nitrogen * cal_amount;
              const new_phosphorus = result.data.phosphorus * cal_amount;
              const new_potassium = result.data.potassium_oxide * cal_amount;
              const new_magnesium = result.data.magnesium * cal_amount;

              this.fertiActivityForm.controls['new_nitrogen'].setValue(new_nitrogen);
              this.fertiActivityForm.controls['new_phosphorus'].setValue(new_phosphorus);
              this.fertiActivityForm.controls['new_potassium'].setValue(new_potassium);
              this.fertiActivityForm.controls['new_magnesium'].setValue(new_magnesium);

              var old_nitrogen = this.fertiActivityForm.value.old_nitrogen;
              var pre_nitrogen = this.fertiActivityForm.value.pre_nitrogen;
              if (old_nitrogen != null) {
                pre_nitrogen = (old_nitrogen != '') ? old_nitrogen : 0;
              } else {
                pre_nitrogen = (pre_nitrogen != '') ? pre_nitrogen : 0;
              }

              var old_phosphorus = this.fertiActivityForm.value.old_phosphorus;
              var pre_phosphorus = this.fertiActivityForm.value.pre_phosphorus;
              if (old_phosphorus != null) {
                pre_phosphorus = (old_phosphorus != '') ? old_phosphorus : 0;
              } else {
                pre_phosphorus = (pre_phosphorus != '') ? pre_phosphorus : 0;
              }

              var old_potassium = this.fertiActivityForm.value.old_potassium;
              var pre_potassium = this.fertiActivityForm.value.pre_potassium;
              if (old_potassium != null) {
                pre_potassium = (old_potassium != '') ? old_potassium : 0;
              } else {
                pre_potassium = (pre_potassium != '') ? pre_potassium : 0;
              }

              var old_magnesium = this.fertiActivityForm.value.old_magnesium;
              var pre_magnesium = this.fertiActivityForm.value.pre_magnesium;
              if (old_magnesium != null) {
                pre_magnesium = (old_magnesium != '') ? old_magnesium : 0;
              } else {
                pre_magnesium = (pre_magnesium != '') ? pre_magnesium : 0;
              }

              var corrected_nitrogen = this.fertiActivityForm.value.corrected_nitrogen;
              var corrected_phosphorus = this.fertiActivityForm.value.corrected_phosphorus;
              var corrected_potassium = this.fertiActivityForm.value.corrected_potassium;
              var corrected_magnesium = this.fertiActivityForm.value.corrected_magnesium;

              const balance_nitrogen = corrected_nitrogen - (Number(pre_nitrogen) + Number(new_nitrogen));
              const balance_phosphorus = corrected_phosphorus - (Number(pre_phosphorus) + Number(new_phosphorus));
              const balance_potassium = corrected_potassium - (Number(pre_potassium) + Number(new_potassium));
              const balance_magnesium = corrected_magnesium - (Number(pre_magnesium) + Number(new_magnesium));

              if (balance_nitrogen > 0) {
                var new_balance_nitrogen = '-' + balance_nitrogen;
              } else if (balance_nitrogen < 0) {
                var new_balance_nitrogen = '+' + Math.abs(balance_nitrogen);
              } else {
                var new_balance_nitrogen = balance_nitrogen.toString();
              }

              if (balance_phosphorus > 0) {
                var new_balance_phosphorus = '-' + balance_phosphorus;
              } else if (balance_phosphorus < 0) {
                var new_balance_phosphorus = '+' + Math.abs(balance_phosphorus);
              } else {
                var new_balance_phosphorus = balance_phosphorus.toString();
              }

              if (balance_potassium > 0) {
                var new_balance_potassium = '-' + balance_potassium;
              } else if (balance_potassium < 0) {
                var new_balance_potassium = '+' + Math.abs(balance_potassium);
              } else {
                var new_balance_potassium = balance_potassium.toString();
              }

              if (balance_magnesium > 0) {
                var new_balance_magnesium = '-' + balance_magnesium;
              } else if (balance_magnesium < 0) {
                var new_balance_magnesium = '+' + Math.abs(balance_magnesium);
              } else {
                var new_balance_magnesium = balance_magnesium.toString();
              }

              this.fertiActivityForm.controls['balance_nitrogen'].setValue(new_balance_nitrogen);
              this.fertiActivityForm.controls['balance_phosphorus'].setValue(new_balance_phosphorus);
              this.fertiActivityForm.controls['balance_potassium'].setValue(new_balance_potassium);
              this.fertiActivityForm.controls['balance_magnesium'].setValue(new_balance_magnesium);
            }
          });
        }
        this.fertiActivityForm.controls['total_amount'].setValue(total_amount);
        this.fertiActivityForm.controls['total_amount_unit'].setValue(total_amount + ' ' + unit);
      } else {
        this.fertiActivityForm.controls['total_amount'].setValue(total_amount);
      }
    } else {
      this.fertiActivityForm.controls['total_amount'].setValue('');
    }

  }

  FertilizerBasicEdit() {
    this.disabledField = null;
  }

  FertilizerCultureDetail(culture_id) {

    const input = { 'culture_id': culture_id };
    this.master.fertilizerCultureDetail(input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        var previous = data['previous'];
        var balance = data['balance'];
        data = data['data'];
        this.fertiActivityForm.controls['basic_nitrogen'].setValue(data.c_nitrogen);
        this.fertiActivityForm.controls['basic_phosphorus'].setValue(data.c_phosphorus);
        this.fertiActivityForm.controls['basic_potassium'].setValue(data.c_potassium_oxide);
        this.fertiActivityForm.controls['basic_magnesium'].setValue(data.c_magnesium);

        this.fertiActivityForm.controls['factor_nitrogen'].setValue(data.nitrogen);
        this.fertiActivityForm.controls['factor_phosphorus'].setValue(data.phosphorus);
        this.fertiActivityForm.controls['factor_potassium'].setValue(data.potassium_oxide);
        this.fertiActivityForm.controls['factor_magnesium'].setValue(data.magnesium);

        var factor_nitrogen = 1;
        var factor_phosphorus = (data.phosphorus != null) ? data.phosphorus : 1;
        var factor_potassium = (data.potassium_oxide != null) ? data.potassium_oxide : 1;
        var factor_magnesium = (data.magnesium != null) ? data.magnesium : 1;

        var basic_nitrogen = (data.c_nitrogen != null) ? data.c_nitrogen : 1;
        var basic_phosphorus = (data.c_phosphorus != null) ? data.c_phosphorus : 1;
        var basic_potassium = (data.c_potassium_oxide != null) ? data.c_potassium_oxide : 1;
        var basic_magnesium = (data.c_magnesium != null) ? data.c_magnesium : 1;

        var corrected_nitrogen = factor_nitrogen * basic_nitrogen;
        var corrected_phosphorus = factor_phosphorus * basic_phosphorus;
        var corrected_potassium = factor_potassium * basic_potassium;
        var corrected_magnesium = factor_magnesium * basic_magnesium;

        this.fertiActivityForm.controls['corrected_nitrogen'].setValue(corrected_nitrogen);
        this.fertiActivityForm.controls['corrected_phosphorus'].setValue(corrected_phosphorus);
        this.fertiActivityForm.controls['corrected_potassium'].setValue(corrected_potassium);
        this.fertiActivityForm.controls['corrected_magnesium'].setValue(corrected_magnesium);

        if (previous != '') {
          this.fertiActivityForm.controls['pre_nitrogen'].setValue(previous.total_nitrigen);
          this.fertiActivityForm.controls['pre_phosphorus'].setValue(previous.total_phosphorus);
          this.fertiActivityForm.controls['pre_potassium'].setValue(previous.total_potassium);
          this.fertiActivityForm.controls['pre_magnesium'].setValue(previous.total_magnesium);

          const balance_nitrogen = corrected_nitrogen - (Number(previous.total_nitrigen));
          const balance_phosphorus = corrected_phosphorus - (Number(previous.total_phosphorus));
          const balance_potassium = corrected_potassium - (Number(previous.total_potassium));
          const balance_magnesium = corrected_magnesium - (Number(previous.total_magnesium));

          if (balance_nitrogen > 0) {
            var new_balance_nitrogen = '-' + balance_nitrogen;
          } else if (balance_nitrogen < 0) {
            var new_balance_nitrogen = '+' + Math.abs(balance_nitrogen);
          } else {
            var new_balance_nitrogen = balance_nitrogen.toString();
          }

          if (balance_phosphorus > 0) {
            var new_balance_phosphorus = '-' + balance_phosphorus;
          } else if (balance_phosphorus < 0) {
            var new_balance_phosphorus = '+' + Math.abs(balance_phosphorus);
          } else {
            var new_balance_phosphorus = balance_phosphorus.toString();
          }

          if (balance_potassium > 0) {
            var new_balance_potassium = '-' + balance_potassium;
          } else if (balance_potassium < 0) {
            var new_balance_potassium = '+' + Math.abs(balance_potassium);
          } else {
            var new_balance_potassium = balance_potassium.toString();
          }

          if (balance_magnesium > 0) {
            var new_balance_magnesium = '-' + balance_magnesium;
          } else if (balance_magnesium < 0) {
            var new_balance_magnesium = '+' + Math.abs(balance_magnesium);
          } else {
            var new_balance_magnesium = balance_magnesium.toString();
          }

          if (new_balance_nitrogen.split('.')[1]) {
            new_balance_nitrogen = new_balance_nitrogen.split('.')[0] + parseInt(new_balance_nitrogen.split('.')[1]).toFixed(2);
          }
          if (new_balance_phosphorus.split('.')[1]) {
            new_balance_phosphorus = new_balance_phosphorus.split('.')[0] + parseInt(new_balance_phosphorus.split('.')[1]).toFixed(2);
          }
          if (new_balance_potassium.split('.')[1]) {
            new_balance_potassium = new_balance_potassium.split('.')[0] + parseInt(new_balance_potassium.split('.')[1]).toFixed(2);
          }
          if (new_balance_magnesium.split('.')[1]) {
            new_balance_magnesium = new_balance_magnesium.split('.')[0] + parseInt(new_balance_magnesium.split('.')[1]).toFixed(2);
          }

          this.fertiActivityForm.controls['balance_nitrogen'].setValue(new_balance_nitrogen);
          this.fertiActivityForm.controls['balance_phosphorus'].setValue(new_balance_phosphorus);
          this.fertiActivityForm.controls['balance_potassium'].setValue(new_balance_potassium);
          this.fertiActivityForm.controls['balance_magnesium'].setValue(new_balance_magnesium);

        }
        this.corr_factor = '(-, ' + factor_phosphorus + ', ' + factor_potassium + ', ' + factor_magnesium + ')';
      } else {

      }

    });
  }

  getAllFertilizerActivity(culture_id) {
    const input = { 'culture_id': culture_id };
    this.master.getAllFertilizerActivity(input).pipe(first()).subscribe((data: any) => {
      this.allFertilizerActivity$ = data['data'];
    });
  }

  resetFertiActivityForm() {

    this.fertiActivityForm.reset();
    const todaydate = new Date();
    if (this.IsMobile) {
      this.masterFertilizeDate = todaydate.getFullYear().toString() + "-" + ("0" + (todaydate.getMonth() + 1)).slice(-2) + "-" + todaydate.getDate().toString();
    } else {
      this.masterFertilizeDate = todaydate.getDate().toString() + "." + (todaydate.getMonth() + 1).toString() + "." + todaydate.getFullYear().toString();
    }
    this.fertiActivityForm.controls['field_id'].setValue(this.fieldIdMaster);
    this.fertiActivityForm.controls['culture_id'].setValue(this.cultureIdMaster);
    this.FertilizerCultureDetail(this.cultureIdMaster);
  }

  editFertilizerActivity(data) {
    this.showOtherCrops = false;
    const activity_id = data.fertilizer_activity_id;
    this.fertiActivityForm.controls['fertilizer_activity_id'].setValue(data.fertilizer_activity_id);
    //this.fertiActivityForm.controls['fertilizer_date'].setValue(data.fertilizer_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
    this.fertiActivityForm.controls['fertilizer_id'].setValue(data.fertilizer_id);
    this.fertiActivityForm.controls['amount'].setValue(data.amount);
    this.fertiActivityForm.controls['unit'].setValue(data.unit);
    this.fertiActivityForm.controls['unit_with_ha'].setValue(data.unit + '/ha');
    this.fertiActivityForm.controls['total_amount'].setValue(data.total_amount);
    this.fertiActivityForm.controls['total_amount_unit'].setValue(data.total_amount + ' ' + data.unit);
    this.fertiActivityForm.controls['machine_id'].setValue(data.machine_id);
    this.fertiActivityForm.controls['person_name'].setValue(data.person_name);
    this.fertiActivityForm.controls['comment'].setValue(data.comment);
    this.fertiActivityForm.controls['field_id'].setValue(data.field_id);
    this.fertiActivityForm.controls['culture_id'].setValue(data.culture_id);

    this.fertiActivityForm.controls['new_nitrogen'].setValue(data.new_nitrogen);
    this.fertiActivityForm.controls['new_phosphorus'].setValue(data.new_phosphorus);
    this.fertiActivityForm.controls['new_potassium'].setValue(data.new_potassium);
    this.fertiActivityForm.controls['new_magnesium'].setValue(data.new_magnesium);

    var pre_nitrogen = this.fertiActivityForm.value.pre_nitrogen;
    var pre_phosphorus = this.fertiActivityForm.value.pre_phosphorus;
    var pre_potassium = this.fertiActivityForm.value.pre_potassium;
    var pre_magnesium = this.fertiActivityForm.value.pre_magnesium;

    this.fertiActivityForm.controls['old_nitrogen'].setValue(pre_nitrogen - data.new_nitrogen);
    this.fertiActivityForm.controls['old_phosphorus'].setValue(pre_phosphorus - data.new_phosphorus);
    this.fertiActivityForm.controls['old_potassium'].setValue(pre_potassium - data.new_potassium);
    this.fertiActivityForm.controls['old_magnesium'].setValue(pre_magnesium - data.new_magnesium);

    if (data.fertilizer_date != null) {
      if (this.IsMobile) {
        this.fertiActivityForm.controls['fertilizer_date'].setValue(data.fertilizer_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$1-$2-$3"));
      } else {
        this.fertiActivityForm.controls['fertilizer_date'].setValue(data.fertilizer_date.replace(/(\d{4})-(\d{2})-(\d{2})/, "$3.$2.$1"));
      }
    }

  }

  deleteFertilizerActivty(id) {
    this.resetFertiActivityForm();
    let that = this;
    let msg = "Are you sure you want to delete this ?"
    that.alertService.confirmThis('Confirmation', msg, function () {
      const input = { 'fertilizer_activity_id': id };
      that.master.deleteFertilizerActivty(input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isError = false;
          that.isSuccess = true;
          that.getAllFertilizerActivity(that.cultureIdMaster);
          that.FertilizerCultureDetail(that.cultureIdMaster);
        } else {
          that.isError = true;
          that.isSuccess = false;
        }
        if (data.message && data.message != '') {
          that.translate.get(data.message).subscribe((result: string) => {
            if (that.isSuccess == true) {
              that.successMessage = result;
            } else {
              that.errorMessage = result;
            }
          });
        }
      });

    }, function () {
      ///console.log('No');
    });
  }

  //// 
  FadeOutLink() {
    setTimeout(() => {
      this.isSuccess = false;
    }, 3000);
  }

}
