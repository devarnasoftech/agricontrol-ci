import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit {
  @Input() heading: string;
  @Input() paragraph: string;
  @Input() primaryActionText: string;
  @Input() primaryActionHref: string;

  constructor() {}

  ngOnInit() {}
}
