import { Component, OnInit, HostListener } from '@angular/core';
import { CommonService } from '../service/common.service';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../service/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { LocalstorageService } from '../service/localstorage.service';

declare var jQuery: any;

@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

	sideBar: any;
	contents: any;
	searchText: any;
	isSearch: boolean = false;
	filterContents: any;
	filtersideBar: any;
	isHeader = false;
	language = "";
	selectedLanguage = "de";

	constructor(private common: CommonService, private auth: AuthService, private localstorage: LocalstorageService, private translate: TranslateService, private router: Router, private appComponent: AppComponent) {
		let session = this.localstorage.get("loginSession");
		if (session && session.languageKey) {
			translate.setDefaultLang(session.languageKey);
		} else {
			this.selectedLanguage = translate.getDefaultLang();
			if (this.selectedLanguage === undefined) {
				if (this.localstorage.get("defaultLanguage") === false) {
					this.localstorage.set("defaultLanguage", "en");
					this.selectedLanguage = "en";
				} else {
					this.selectedLanguage = this.localstorage.get("defaultLanguage");
				}
				translate.setDefaultLang(this.selectedLanguage);
			}
		}
	}

	ngOnInit() {
		if (!this.auth.isLoggednIn()) {
			this.isHeader = true;
			debugger;
			this.appComponent.currentURL = this.router.url;
		}
		this.language = this.translate.defaultLang;
		this.getAllFaq();
	}

	changedLanguage() {
		this.translate.setDefaultLang(this.selectedLanguage);
		this.localstorage.set("defaultLanguage", this.selectedLanguage);
		window.location.reload();
	}

	getAllFaq() {
		var array = { lang: this.language };
		this.common.getAllFaq(array).subscribe((data: any) => {
			if (data.status) {
				this.contents = data.data;
				this.sideBar = data.data.filter(x => x.parent_id === "0");
				this.sideBar.forEach(side => {
					side.childMenu = data.data.filter(x => x.parent_id == side.ac_faq_id);
				});
				this.filterContents = this.contents;
				this.filtersideBar = this.sideBar;
			}
		});
	}

	filterContent() {
		if (this.searchText.length > 0) {
			this.isSearch = true;
			this.filterContents = this.contents.filter(x => x.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
				x.content.toLowerCase().includes(this.searchText.toLowerCase()));
			if (this.filterContents.length > 0) {
				this.gotoDiv(this.filterContents[0].route);
			}
		} else {
			this.isSearch = false;
			this.filterContents = this.contents;
			this.filtersideBar = this.sideBar;
		}
	}
	onSearchChange() {
		this.filterContents = this.contents.filter(x => x.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
			x.content.toLowerCase().includes(this.searchText.toLowerCase()));
		if (this.filterContents.length > 0) {
			let id = this.filterContents[0].route;
			this.gotoDiv(id);
		}

	}

	clearSearch() {
		this.isSearch = false;
		this.searchText = "";
		this.filterContents = this.contents;
		this.filtersideBar = this.sideBar;
	}

	gotoDiv(id) {
		if (jQuery("#" + id).offset() !== undefined) {
			jQuery('html,body').animate({
				scrollTop: jQuery("#" + id).offset().top - 150
			}, 'slow');
		}
	}

	@HostListener('window:scroll', ['$event'])
	scrollHandler(event) {
		var scrollPosition = jQuery(document).scrollTop();
		jQuery('#faqContent div').each(function () {
			var refElement = jQuery(this);
			var id = refElement.attr("id");
			if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
				jQuery('#mainNav ol li img').removeClass('right_img_active');
				jQuery('#mainNav ol li ol li img').removeClass('right_img_active');
				jQuery('#' + id + '_active').addClass('right_img_active');
			}
		});
	}

	ngAfterViewInit(): void {
		const content = document.querySelector('#faqContent');
		const scroll$ = fromEvent(content, 'scroll').pipe(map(() => content));
		scroll$.subscribe(element => {
			var scrollPosition = jQuery(document).scrollTop();
			jQuery('#faqContent div').each(function () {
				var refElement = jQuery(this);
				var id = refElement.attr("id");
				if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
					jQuery('#mainNav ol li img').removeClass('right_img_active');
					jQuery('#mainNav ol li ol li img').removeClass('right_img_active');
					jQuery('#' + id + '_active').addClass('right_img_active');
				}
			});
		});
	}
}
