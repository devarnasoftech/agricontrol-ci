import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminauthService } from '../service/adminauth.service';
import { LocalstorageService } from '../service/localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AdminauthGuard implements CanActivate {

  constructor(
    private router: Router,
    private storage: LocalstorageService,
    private adminservice: AdminauthService
    ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.adminservice.isLoggednIn()) {
        return true;
      } else {
        this.storage.remove('adminloginSession');
        this.storage.remove('currentLanguage');
        this.router.navigate(['/admin/login']);
        return false;
      }
  }
}
