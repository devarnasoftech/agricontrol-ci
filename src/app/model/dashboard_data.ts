import { DashboardFieldData } from "./dashboard_field_data";

export class DashboardData {
        
    dashboard_data : DashboardFieldData[];
    field_cultures : [];
    footerTextData : [];
}