export class Culture {
    id: number;
    fieldId: number;
    delicate: number;
    dateFrom: Date;
    dateTo: Date;
    cultivatedArea: number;
    isSwapped: boolean;
    swappedWith: string;
    plantVariety: string;
    notes: string;
}
