
export class ExportFieldData{

    field_id : number;
    field_name : string;
    field_size : number;
    is_swapped : number;
}