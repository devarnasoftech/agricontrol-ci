import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmincropsComponent } from './admincrops.component';

describe('AdmincropsComponent', () => {
  let component: AdmincropsComponent;
  let fixture: ComponentFixture<AdmincropsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmincropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmincropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
