import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminlanguageComponent } from './adminlanguage.component';

describe('AdminlanguageComponent', () => {
  let component: AdminlanguageComponent;
  let fixture: ComponentFixture<AdminlanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminlanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminlanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
