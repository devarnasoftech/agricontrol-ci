import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransferserviceService {

  constructor() { }

  data: any;

  setinputData(data) {
    this.data = data;
  }

  getinputData() {
    const temp = JSON.parse(JSON.stringify(this.data));
    this.clearData();
    return temp;
  }

  clearData() {
    this.data = undefined;
  }
}
