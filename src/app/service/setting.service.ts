import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';
import { Profile } from '../model/profile';
import { User } from '../model/user';
import { environment } from '../../environments/environment';
 

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  apiBaseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  updateProfile(profile: Profile) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/updateProfile', profile, authHeader);
  }

  changePassword(passwords: User) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/changePassword', passwords, authHeader);
  }

  getUserData() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'auth/getUserData', authHeader);
  }
  getAllteam_member() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'auth/getAllteam_member', authHeader);
  }
  deleteUser(input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/deleteUser', input, authHeader);
  }
  addTeam_memberData(data) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/addTeam_memberData', data, authHeader); 
  }

  addImage(uploadData) {
    const authHeader = this.storage.getImageAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/addImage', uploadData, authHeader);
  }
  deleteUserAccount(input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/deleteUserAccount', input, authHeader);
  }

}
