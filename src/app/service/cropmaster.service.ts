import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CropmasterService {

  apiBaseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  getAllCrops(defaultLanguage) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/getAllCrops', defaultLanguage, authHeader);
  }

  getAllUserCrops(defaultLanguage) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/getAllUserCrops', defaultLanguage, authHeader);
  }

  changeCropColor(colorInput) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'Cropmaster/changeCropColor', colorInput, authHeader);
  }
}
