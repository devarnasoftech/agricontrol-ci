import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';

import { User } from '../model/user';
import { Profile } from '../model/profile';
import { Crop } from '../model/crop';
import { Language } from '../model/language';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminauthService {

  apiBaseUrl = environment.apiBaseUrl;

  _userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> { return this._userActionOccured.asObservable(); }

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  login(user: User) {
    return this.http.post(this.apiBaseUrl + 'admin/login', user);
  }

  isLoggednIn() {
    return this.storage.getAdminAuthToken();
  }

  notifyUserAction() {
    this._userActionOccured.next();
  }

  logOut(user: User) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/logout', user, authHeader);
  }

  getAllUsers() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'admin/getAllUsers', authHeader);
  }

  getAllUsersCsv() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'admin/getAllUsersCsv', authHeader);
  }

  getUserHistory(user_id) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getUserHistory', user_id, authHeader);
  }

  getAllLanguages() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'common/getAllLanguage', authHeader);
  }

  getLanguageBySymbol(language) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/getLanguageBySymbol', language, authHeader);
  }

  deleteUser(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/deleteUser', input, authHeader);
  }

  updateActivation(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/updateActivation', input, authHeader);
  }

  getUserDataById(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getUserDataById', input, authHeader);
  }

  updateUseData(profile: Profile) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/updateUseData', profile, authHeader);
  }

  upgradeUserPlan(planDetails: Profile) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/upgradeUserPlan', planDetails, authHeader);
  }

  getUserData() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'admin/getAdminUserData', authHeader);
  }

  updateProfile(profile: Profile) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/updateProfile', profile, authHeader);
  }

  addImage(uploadData) {
    const authHeader = this.storage.getAdminImageAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/addImage', uploadData, authHeader);
  }

  changePassword(passwords: User) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/changePassword', passwords, authHeader);
  }

  getAllCrops(defaultLanguage) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getAllCrops', defaultLanguage, authHeader);
  }

  getAllCropFamily(defaultLanguage) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getAllCropFamily', defaultLanguage, authHeader);
  }

  getCropDataById(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getCropDataById', input, authHeader);
  }

  updateCropData(crop: Crop) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/updateCropData', crop, authHeader);
  }

  deleteCrop(crop: any) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/deleteCrop', crop, authHeader);
  }

  addCropData(crop: Crop) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/addCropData', crop, authHeader);
  }

  exportUserList() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'admin/exportUserList', authHeader);
  }
  exportcsvUserList() {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'admin/exportcsvUserList', authHeader);
  }

  getLanguageById(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/getLanguageById', input, authHeader);
  }

  updateLanguageData(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/updateLanguageData', input, authHeader);
  }

  addLanguageData(language: Language) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/addLanguageData', language, authHeader);
  }
  
  getAllLanguageKeyValue(){
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.get(this.apiBaseUrl + 'common/getAllLanguageKeyValue', authHeader);
  }

  updateJsonFile(input){
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/updateJsonFile', input, authHeader);
  }

  publishLanguage(input) {
    const authHeader = this.storage.getAdminAuthHeader();
    return this.http.post(this.apiBaseUrl + 'admin/publishLanguage', input, authHeader);
  }

}
