<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Auth extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('auths');
        $this->load->model('commons');
    }

    public function deleteData_get()
    {
        echo 'Tested';
    }
    /**
     * Get all Crops
     */
    public function getAllteam_member_get()
    {
        try {
            $usersData = $this->auths->getAllteam_member();
            if ($usersData && count($usersData) > 0) {
                $post = array('status' => TRUE, 'data' => $usersData);
            } else {
                $post = array('status' => FALSE, 'data' => array(), 'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    public function addTeam_memberData_post()
    {
        try {
            $formData = array(
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'userID' => $this->api_token->ac_userId,
                'language' => $this->post('language'),
                'company' => $this->post('company')
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[40]|is_unique[ac_user.email]');
            $this->form_validation->set_rules('userID', 'userID', 'required|trim');
            $this->form_validation->set_rules('language', 'Language', 'required|trim');
            $this->form_validation->set_rules('company', 'Company', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');
            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE, 'message' => "email_already");
            } else {
                $responseData = $this->auths->add_team_member_data($formData);
                if ($responseData) {
                    $sent = $this->auths->send_team_member_password_email($formData['email'], $formData['language']);
                    if ($sent) {
                        $post = array('status' => TRUE, 'message' => 'mail_sent', 'url' => $sent);
                    } else {
                        $post = array('status' => FALSE, 'message' => 'error_something_wrong');
                    }
                } else {
                    $post = array('status' => FALSE, 'message' => 'data_not_added');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    public function sendAdminNotification_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            if (empty($user_id)) {
                $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            } else {
                $data = $this->auths->adminSubscriptionNotification($user_id);
                if ($data) {
                    $post = array('status' => TRUE, 'message' => 'admin_notify_success');
                } else {
                    $post = array('status' => FALSE, 'message' => 'admin_notify_fail');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function deleteUser_post()
    {
        try {
            $user_id = $this->post('user_id');
            if (empty($user_id)) {
                $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            } else {
                $delete = $this->auths->delete_user($user_id);
                if ($delete) {
                    $post = array('status' => TRUE, 'message' => 'user_delete');
                } else {
                    $post = array('status' => FALSE, 'message' => 'user_delete_fail');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }
    /**
     * Register function for the app 
     */
    public function register_post()
    {
        try {
            $formData = array(
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'password' => $this->post('password'),
                'confirmPassword' => $this->post('confirmPassword'),
                'subscriptionPlan' => $this->post('subscriptionPlan'),
                'company' => $this->post('company'),
                'languageKey' => $this->post('languageKey')
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[25]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[40]|is_unique[ac_user.email]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|trim|min_length[6]|matches[password]');
            $this->form_validation->set_rules('company', 'Company', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('subscriptionPlan', 'subscription Plan', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE, 'message' => validation_errors());
            } else {
                $response = $this->auths->add_user($formData);
                if ($response['userID'] && $response['userID'] > 0) {
                    if ($response['mail_sent']) {
                        $post = array('status' => TRUE, 'message' => 'registration_success');
                    } else {
                        $post = array('status' => TRUE, 'message' => 'registration_mail_fail');
                    }
                } else {
                    $post = array('status' => FALSE, 'message' => 'error_something_wrong');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function login_post()
    {
        try {
            $formData = array(
                'email' => $this->post('email'),
                'password' => $this->post('password'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[50]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE, 'message' => validation_errors());
            } else {
                $userInfo = $this->auths->get_login($formData);
                if ($userInfo && !empty($userInfo)) {
                    if($userInfo->is_deleted == 0){
                        if ($userInfo->is_verified == 1) {
                            $subscription = $this->auths->checkSubscription($userInfo->ac_userId);
                            if ($subscription) {
                                $isExpired = false;
                            } else {
                                $isExpired = true;
                            }

                            $token = $this->auths->generate_user_token($userInfo->ac_userId);
                            if ($isExpired == false) {
                                unset($userInfo->ac_userId);
                            }
                            if ($token) {
                                $userInfo->token = $token;
                                $post = array('status' => TRUE, 'data' => $userInfo, 'isExpired' => $isExpired, 'deleted_user_msg' => FALSE);
                            } else {
                                $post = array('status' => FALSE, 'message' => 'error_something_wrong');
                            }
                        } else {
                            $post = array('status' => FALSE, 'message' => 'user_notverified');
                        }
                    }else{
                        if($userInfo->delete_date != ''){
                            $deleteDate = strtotime("+7 day", strtotime($userInfo->delete_date));
                            $deleteDate = date('Y-m-d', $deleteDate);
                            $today_date = date('Y-m-d');
                            if($today_date <= $deleteDate){
                                $post = array('status' => TRUE,'user_id'=>$userInfo->ac_userId, 'deleted_user_msg' => TRUE);
                            }else{
                                $post = array('status' => FALSE, 'message' => 'user_notexists');
                            }
                        }else{
                            $post = array('status' => FALSE, 'message' => 'user_notexists');
                        }
                    }
                } else {
                    $post = array('status' => FALSE, 'message' => 'user_notexists');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {

            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function resendConfirm_post()
    {
        try {
            $formData = array(
                'email' => $this->post('email'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[40]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE, 'message' => validation_errors());
            } else {
                $is_sent = $this->auths->sendConfirmationEmail($formData['email'], null);
                if ($is_sent) {
                    $post = array('status' => TRUE, 'message' => 'mail_sent');
                } else {
                    $post = array('status' => FALSE, 'message' => 'error_something_wrong');
                }
            }
            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function checkEmailUnique_post()
    {
        try {
            $formData = array('email' => $this->post('email'));
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[40]|is_unique[ac_user.email]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE, 'message' => validation_errors());
            } else {
                $post = array('status' => True, 'message' => 'email_available');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Confirm Email Address function for the app 
     */
    public function confirmEmailId_post()
    {
        try {
            $formData = array('emailToken' => $this->post('emailToken') );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('emailToken', 'Email Token', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'message' => validation_errors());
            } else {
                $decodedString = base64_decode($formData['emailToken']);
                $decodedArr = explode('##', $decodedString);
                $email = (isset($decodedArr[1])) ? $decodedArr[1] : '';

                if (!empty($email)) {
                    $user = $this->auths->get_user_by_email($email);

                    if (isset($user) && !empty($user) && $user->is_verified == 1) {
                        $post = array('status' => FALSE,'message' => 'invalid_url');
                    } else {
                        $verify = $this->auths->verify_user($email, $token='');
                        if ($verify) {
                            $user->token = $this->auths->generate_user_token($user->ac_userId);
                            unset($user->ac_userId);
                            $post = array('status' => TRUE,'data' => $user);
                        } else {
                            $post = array('status' => FALSE,'message' => 'error_something_wrong');
                        }
                    }
                } else {
                    $post = array('status' => FALSE,'message' => 'invalid_url');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function logout_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $token = $this->api_token->token;

            if ($user_id != '' && $token != '') {
                $update = $this->auths->delete_user_token($user_id, $token);
                if ($update) {
                    $post = array('status' => TRUE,'message' => 'logout_msg');
                } else {
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * User Profile function for the app 
     */
    public function updateProfile_post()
    {
        try {
            $formData = array(
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'company' => $this->post('company'),
                'language' => $this->post('language'),
                'phone' => $this->post('phone'),
                'streetName' => $this->post('streetName'),
                'postalCode' => $this->post('postalCode'),
                'city' => $this->post('city'),
                'country' => $this->post('country'),
            );

            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[50]');
            $this->form_validation->set_rules('company', 'Company', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('language', 'Language', 'required|trim');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[15]');
            $this->form_validation->set_rules('streetName', 'Street Name', 'required|trim|max_length[150]');
            $this->form_validation->set_rules('postalCode', 'Postal Code', 'required|trim|numeric|max_length[10]');
            $this->form_validation->set_rules('city', 'City', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('country', 'Country', 'required|trim|max_length[100]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'message' => validation_errors());
            }else{
                ///$formData['phone'] = $this->post('phone');
                if ($this->api_token->is_parent != 1) {
                    $user_id = $this->api_token->ac_currentUserId;
                } else {
                    $user_id = $this->api_token->ac_userId;
                }
                $check_email = $this->auths->check_email_unique($formData['email'], $user_id);
                if ($check_email) {
                    $post = array('status' => FALSE,'message' => 'email_already');
                } else {
                    $response = $this->auths->update_user_profile($formData, $user_id);
                    if ($response) {
                        $post = array('status' => TRUE,'message' => 'profile_update_success');
                    } else {
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }
                }
            }    
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Reset Password function for the app 
     */
    public function changePassword_post()
    {
        try {
            $formData = array(
                'oldPassword' => $this->post('oldPassword'),
                'password' => $this->post('password'),
                'confirmPassword' => $this->post('confirmPassword'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('oldPassword', 'Old Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|trim|min_length[6]|matches[password]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'message' => validation_errors());
            } else {
                if ($this->api_token->is_parent != 1) {
                    $user_id = $this->api_token->ac_currentUserId;
                } else {
                    $user_id = $this->api_token->ac_userId;
                }
                $check_old_password = $this->auths->check_old_password($user_id, $formData['oldPassword']);
                if ($check_old_password) {
                    $updated = $this->auths->update_password_by_id($user_id, $formData['password']);
                    if ($updated) {
                        $post = array('status' => TRUE,'message' => 'reset_password_success');
                    } else {
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }
                } else {
                    $post = array('status' => FALSE,'message' => 'old_password_wrong');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    function alpha_dash_space($str)
    {
        if (!preg_match('/^[a-zA-Z\s]+$/', $str)) {
            $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha characters & White spaces');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get User Data
     */
    public function getUserData_get()
    {
        try {
            $user_data = array();
            if ($this->api_token->is_parent != 1) {
                $user_id = $this->api_token->ac_currentUserId;
            } else {
                $user_id = $this->api_token->ac_userId;
            }
            if($user_id) {
                $data = $this->auths->get_user($user_id);
                if (!empty($data)) {
                    $user_data = $data;
                }
                $post = array('status' => TRUE,'data' => $user_data,'message' => 'Dashboard Data');
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status'=>FALSE,'message'=>'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Forgot Password function for the app 
     */
    public function forgotPassword_post()
    {
        try {
            $formData = array(
                'email' => $this->post('email'),
                'languageKey' => $this->post('languageKey')
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[40]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'message' => validation_errors());
            } else {
                $count = $this->auths->check_user_by_eamilId($formData['email']);
                if ($count && $count > 0) {
                    $sent = $this->auths->send_reset_password_email($formData['email'], $formData['languageKey']);

                    if ($sent) {
                        $post = array('status' => TRUE,'message' => 'forgot_success_message');
                    } else {
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }
                } else {
                    $post = array('status' => FALSE,'message' => 'email_not_exists');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Reset Password function for the app 
     */
    public function checkEmailToken_post()
    {
        try {
            $formData = array(
                'resetPasswordToken' => $this->post('resetPasswordToken'),
            );
            if (!empty($formData['resetPasswordToken'])) {
                $decodedString = base64_decode($formData['resetPasswordToken']);
                $decodedArr = explode('##', $decodedString);
                $email = (isset($decodedArr[1])) ? $decodedArr[1] : '';

                $userExist = $this->auths->check_user_by_eamilId($email);
                if ($userExist && $userExist > 0) {
                    $post = array('status' => TRUE,'message' => 'valid_token');
                } else {
                    $post = array('status' => FALSE,'message' => 'contact_to_admin');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'contact_to_admin');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Reset Password function for the app 
     */
    public function setPassword_post()
    {
        try {
            $formData = array(
                'password' => $this->post('password'),
                'confirmPassword' => $this->post('confirmPassword'),
                'resetPasswordToken' => $this->post('resetPasswordToken'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('confirmPassword', 'confirm Password', 'required|trim|min_length[6]|matches[password]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'message' => validation_errors());
            } else {
                if (!empty($formData['resetPasswordToken'])) {
                    $decodedString = base64_decode($formData['resetPasswordToken']);
                    $decodedArr = explode('##', $decodedString);
                    $email = (isset($decodedArr[1])) ? $decodedArr[1] : '';

                    $userExist = $this->auths->check_user_by_eamilId($email);

                    if ($userExist && $userExist > 0) {
                        $updated = $this->auths->update_password($email, $formData['password']);
                        if ($updated) {
                            $post = array('status' => TRUE,'message' => 'reset_password_success');
                        } else {
                            $post = array('status' => FALSE,'message' => 'error_something_wrong');
                        }
                    } else {
                        $post = array('status' => FALSE,'message' => 'invalid_url');
                    }
                } else {
                    $post = array('status' => FALSE,'message' => 'invalid_url');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function checkAuthToken_post()
    {
        try {
            $token = $this->post('token');

            if (isset($token) && !empty($token)) {
                $count = $this->auths->check_user_by_token($token);

                if ($count && $count > 0) {
                    $post = array('status' => True,'message' => 'ok');
                    
                } else {
                    $post = array('status' => FALSE,'message' => 'unauthenticated');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'unauthenticated');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function updatePopupStatus_post()
    {
        try {
            $weeks =  $this->post('weeks');
            $subscriptionId =  $this->post('subscriptionId');
            $type =  $this->post('type');
            $languageData = $this->commons->updateSuggestionStatus($subscriptionId, $weeks, $type);
            $this->response(['status' => TRUE, 'message' => 'update_success'], REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register function for the app 
     */
    public function addImage_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            if ($user_id) {
                $this->form_validation->set_rules('image', '', 'callback_file_check');

                if ($this->form_validation->run() == true) {
                    $config['upload_path']   = 'uploads/profile_picture/';
                    $config['allowed_types'] = '*';
                    $config['max_size']      = 3072;
                    $config['max_width']     = 2000;
                    $config['max_height']    = 2000;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('image')) {
                        $uploadData = $this->upload->data();
                        $uploadedFile = $uploadData['file_name'];

                        $res = $this->auths->update_image_name($uploadedFile, $user_id);
                        if ($res) {
                            $post = array('status' => TRUE,'data' => $uploadedFile,'message' => 'profile_update_success');
                        } else {
                            $post = array('status' => FALSE,'message' => 'error_something_wrong');
                        }
                    } else {
                        $data['error_msg'] = $this->upload->display_errors();
                        $post = array('status' => FALSE,'message' => $data['error_msg']);
                    }
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /*
     * file value and type check during validation
     */
    public function file_check($str)
    {
        $allowed_mime_type_arr = array('image/gif', 'image/jpeg', 'image/png');
        $mime = get_mime_by_extension($_FILES['image']['name']);
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
            } else {
                $this->form_validation->set_message('file_check', 'Please select only gif/jpg/png file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

     /**
     * User account delete
     */
    public function deleteUserAccount_post()
    {
        try {
            $user_id = $this->post('user_id');
            $user_plan = $this->post('user_plan');

            if (empty($user_id)) {
                $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            } else {
                
                $delete = $this->auths->delete_user_account($user_id,$user_plan);
                if ($delete) {
                    $post = array('status' => TRUE, 'message' => 'user_delete');
                } else {
                    $post = array('status' => FALSE, 'message' => 'user_delete_fail');
                }
                $post = array('status' => TRUE, 'message' => 'data_delete_success');
                
                /*$delete = $this->auths->delete_user($user_id);
                if ($delete) {
                    $post = array('status' => TRUE, 'message' => 'user_delete');
                } else {
                    $post = array('status' => FALSE, 'message' => 'user_delete_fail');
                }*/
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

    public function restoreUserAccount_post()
    {
        try {
            $user_id = $this->post('user_id');
            if (empty($user_id)) {
                $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            } else {
                $update = $this->auths->restore_account($user_id);
                if ($update) {
                    $post = array('status' => TRUE);
                } else {
                    $post = array('status' => FALSE);
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $post = array('status' => FALSE, 'message' => 'error_something_wrong');
            $this->response($post, REST_Controller::HTTP_OK);
        }
    }

}
