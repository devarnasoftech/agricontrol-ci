<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Cropmaster extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('cropmasters');
    }

    /**
     * Register field 
     */
    public function changeCropColor_post()
    {
        try {     
            $formData = array(
                'cropId' => $this->post('cropId'),
                'color' => $this->post('color')
            );

            if(!empty($formData['cropId'])){
                $cropId = $formData['cropId'];
                $color = $formData['color'];
                $user_id = $this->api_token->ac_userId;

				$this->db->select('ac_user_crop_color_id');
				$this->db->where('user_id',$user_id);
				$this->db->where('crop_id',$cropId);
				$query= $this->db->get('ac_user_crop_color');
				if($query->num_rows() == 0){
					$this->db->select('color');
					$this->db->where('ac_cropId', $cropId);
					$query2= $this->db->get('ac_crop');
					$crop = $query2->row();
					if(isset($crop) && !empty($crop)){
						$data = array(
							'crop_id' => $cropId,
							'user_id' => $user_id,
							'color'   => $crop->color
						);
						$this->db->insert('ac_user_crop_color', $data);
					}
				}
                $update = $this->cropmasters->change_crop_color($user_id, $color, $cropId); 
                if($update)
                {
                    $post = array('status' => TRUE,'message' => 'color_change_success');
                }
                else
                {
                    $post = array('status' => FALSE,'message' => 'color_change_fail');
                }
            }else{
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);    

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
}
