<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Admin extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('admins');
        $this->load->model('commons');
        $this->load->model('user_model');
        $this->load->library('Pdf');
    }

	public function getUserHistory_post()
    {
        try {   
            $formData = array(
                'user_id' => $this->post('user_id'),
            );
			$usersData = $this->admins->getUserHistory($formData);
            if($usersData && count($usersData) > 0)
            {
                $this->set_response(['status' => TRUE,'data' => $usersData], REST_Controller::HTTP_OK);
            }
            else
            {
                 $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found' ], REST_Controller::HTTP_OK);
            }
		} catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
	}

    /**
     * Get all Crops
     */
    public function getAllUsers_get()
    {
        try {
            $usersData = $this->admins->getAllUsers();
            if($usersData && count($usersData) > 0)
            {
                $this->set_response(['status' => TRUE,'data' => $usersData], REST_Controller::HTTP_OK);
            }
            else
            {
                 $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

	/**
     * Get all Crops
     */
    public function getAllUsersCsv_get()
    {
        try {
            $usersData = $this->admins->getAllUsersCSV();
            if($usersData && count($usersData) > 0)
            {
                $this->set_response(['status' => TRUE,'data' => $usersData], REST_Controller::HTTP_OK);
            }
            else
            {
                 $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the admin 
     */
    public function login_post()
    {
        try {
            $formData = array(
                'email' => $this->post('email'),
                'password' => $this->post('password'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[50]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                $user = $this->admins->get_login($formData);
                if($user)
                {
                    $token = $this->admins->generate_user_token($user->ac_userId);
                    if ($token) {
                        unset($user->ac_userId);
                        $user->token = $token;
                        $post = array('status' => TRUE,'data' => $user);
                    } else{
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }                            
                }
                else
                {
                    $post = array('status' => FALSE,'message' => 'user_notexists');
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function logout_post()
    {
        try {
            $user_id = $this->api_token->user_id;
            $token = $this->api_token->token;

            if($user_id != '' && $token != ''){
                $update = $this->admins->delete_user_token($user_id, $token);
                if($update){
                    $post = array('status' => TRUE,'message' => 'logout_msg');
                } else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
            }
            else
            {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }             
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function deleteUser_post()
    {
        try {
            $user_id = $this->post('user_id');
          
            if(empty($user_id))
            {
                 $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);  
            }
            else
            {
                $delete = $this->admins->delete_user($user_id);
                if($delete)
                {                        
                    $this->set_response(['status' => TRUE,'message' => 'delete_success'], REST_Controller::HTTP_OK);                         
                }
                else
                {
                    $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
                }
            }              
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

	public function updateActivation_post()
    {
        try {

			$status = $this->post('status');
			$subscriptionId = $this->post('subscriptionId');

            if(empty($subscriptionId))
            {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            else
            {
                $activation = $this->admins->userActivation($status, $subscriptionId);
                if($activation)
                {            
                    $post = array('status' => TRUE,'message' => 'update_success');            
                }
                else
                {
                    $post = array('status' => FALSE,'message' => 'update_fail');
                }
            }   
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function getUserDataById_post()
    {
        try {
            $user_data = array();
            $user_id = $this->post('user_id');

            if(!empty($user_id))
            {
                $data = $this->admins->get_user($user_id);
                if(!empty($data)){
                    $user_data = $data;
                }
                $this->response(['status' => TRUE,'data' => $user_data,'message' => 'user Data'], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * User Profile function for the app 
     */

    public function upgradeUserPlan_post()
    {
        try {
                $formData = array(
                    'method' => $this->post('method'),
                    'comment' => $this->post('comment'),
                    'userId' => $this->post('userId'),
                    'planId' => $this->post('planId')
                );

                $this->form_validation->set_data($formData);
                $this->form_validation->set_rules('userId', 'User Id', 'required|trim');
                $this->form_validation->set_rules('method', 'method', 'trim');
                $this->form_validation->set_rules('comment', 'comment', 'trim');
                $this->form_validation->set_rules('planId', 'plan Id', 'required|trim');
                $this->form_validation->set_error_delimiters('', '<br>');

                if ($this->form_validation->run($this) === FALSE)
                {
                    $post = array('status' => FALSE,'message' => validation_errors()); 
                }else{
                
                    $response = $this->admins->upgrade_user_plan($formData);
                    if($response)
                    {
                        $post = array('status' => TRUE,'message' => 'plan_upgrade_success');
                    }
                    else
                    {
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }
                }
                $this->set_response($post, REST_Controller::HTTP_OK);   

            } catch(Exception $e) {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
    }

    public function updateUseData_post()
    {
        try {
                $formData = array(
                    'name' => $this->post('name'),
                    'company' => $this->post('company'),
                    'email' => $this->post('email'),
                    'phone' => $this->post('phone'),
                    'streetName' => $this->post('streetName'),
                    'postalCode' => $this->post('postalCode'),
                    'city' => $this->post('city'),
                    'country' => $this->post('country'),
                    'userId' => $this->post('userId')
                );

                $this->form_validation->set_data($formData);
                $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
                $this->form_validation->set_rules('company', 'Company', 'required|trim|max_length[100]');
                $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[50]');
                $this->form_validation->set_rules('phone', 'Phone', 'required|trim|max_length[15]');
                $this->form_validation->set_rules('streetName', 'Street Name', 'required|trim|max_length[150]');
                $this->form_validation->set_rules('postalCode', 'Postal Code', 'required|trim|numeric|max_length[10]');
                $this->form_validation->set_rules('city', 'City', 'required|trim|max_length[100]');
                $this->form_validation->set_rules('country', 'Country', 'required|trim|max_length[100]');
                $this->form_validation->set_rules('userId', 'user Id', 'required|trim');
                $this->form_validation->set_error_delimiters('', '<br>');

                if ($this->form_validation->run($this) === FALSE)
                {
                    $post = array('status' => FALSE,'message' => validation_errors());
                }else{

                    $check_email = $this->admins->check_email_unique($formData['email'], $formData['userId']);
                    if($check_email){
                        $post = array('status' => FALSE,'message' => 'email_already');
                    }else{
                        $response = $this->admins->update_user_data($formData);
                        if($response)
                        {
                            $post = array('status' => TRUE,'message' => 'update_success');
                        }
                        else
                        {
                            $post = array('status' => FALSE,'message' => 'update_fail');
                        }
                    }  
                }
                $this->response($post, REST_Controller::HTTP_OK);    

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get User Data
     */
    public function getAdminUserData_get()
    {
        try {
            $user_data = array();
            $user_id = $this->api_token->ac_userId;
            if($user_id)
            {
                $data = $this->admins->get_user($user_id);
                if(!empty($data)){
                    $user_data = $data;
                }
                $this->response(['status' => TRUE,'data' => $user_data,'message' => 'User Data'], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * User Profile function for the app 
     */
    public function updateProfile_post()
    {
        try {
            $formData = array(
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'company' => $this->post('company'),
                'language' => $this->post('language'),
                'phone' => $this->post('phone'),
                'streetName' => $this->post('streetName'),
                'postalCode' => $this->post('postalCode'),
                'city' => $this->post('city'),
                'country' => $this->post('country'),
            );

            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[50]');
            $this->form_validation->set_rules('company', 'Company', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('language', 'Language', 'required|trim');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[15]');
            $this->form_validation->set_rules('streetName', 'Street Name', 'required|trim|max_length[150]');
            $this->form_validation->set_rules('postalCode', 'Postal Code', 'required|trim|numeric|max_length[10]');
            $this->form_validation->set_rules('city', 'City', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('country', 'Country', 'required|trim|max_length[100]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                 $this->response(['status' => FALSE,'message' => validation_errors()], REST_Controller::HTTP_OK); 
            }
            $user_id = $this->api_token->ac_userId;
            $check_email = $this->admins->check_email_unique($formData['email'], $user_id);
            if($check_email){
                $this->response(['status' => FALSE,'message' => 'email_already'], REST_Controller::HTTP_OK);
            }else{
                $response = $this->admins->update_user_profile($formData, $user_id);
                if($response)
                {
                    $this->set_response(['status' => TRUE,'message' => 'profile_update_success'], REST_Controller::HTTP_OK);                         
                }
                else
                {
                    $this->response(['status' => FALSE,'message' => 'update_fail'], REST_Controller::HTTP_OK);
                }
            }            
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register function for the app 
     */
    public function addImage_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;

            if($user_id){
                $this->form_validation->set_rules('image', '', 'callback_file_check');

                if($this->form_validation->run() == true){
                    $config['upload_path']   = 'uploads/profile_picture/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size']      = 3072;
                    $config['max_width']     = 2000;
                    $config['max_height']    = 2000;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if($this->upload->do_upload('image')){
                        $uploadData = $this->upload->data();
                        $uploadedFile = $uploadData['file_name'];
                        
                        $res = $this->admins->update_image_name($uploadedFile, $user_id);
                        if($res)
                        {      
                            $post = array('status' => TRUE,'data' => $uploadedFile,'message' => 'profile_update_success');
                        }
                        else
                        {
                            $post = array('status' => FALSE,'message' => 'error_something_wrong');
                        }
                    }else{
                        $data['error_msg'] = $this->upload->display_errors();
                        $post = array('status' => FALSE,'message' => $data['error_msg']);
                    }
                }
            }else{
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /*
     * file value and type check during validation
     */
    public function file_check($str){
        $allowed_mime_type_arr = array('image/gif','image/jpeg','image/png');
        $mime = get_mime_by_extension($_FILES['image']['name']);
        if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'valid_file_extenstion');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'no_file');
            return false;
        }
    }

    /**
     * Reset Password function for the app 
     */
    public function changePassword_post()
    {
        try {         
            $formData = array(
                'oldPassword' => $this->post('oldPassword'),
                'password' => $this->post('password'),
                'confirmPassword' => $this->post('confirmPassword'),
            );
            $this->form_validation->set_data($formData);

            $this->form_validation->set_rules('oldPassword', 'Old Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|trim|min_length[6]|matches[password]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $this->response(['status' => FALSE,'message' => validation_errors()], REST_Controller::HTTP_OK); 
            }
            else
            {
                $user_id = $this->api_token->ac_userId;
                $check_old_password = $this->admins->check_old_password($user_id, $formData['oldPassword']);
                if($check_old_password){
                    $updated = $this->admins->update_password_by_id($user_id, $formData['password']);
                    
                    if($updated)
                    {
                        $this->set_response(['status' => TRUE,'message' => 'update_success'], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
                    } 
                }
                else
                {
                    $this->response(['status' => FALSE,'message' => 'old_password_wrong'], REST_Controller::HTTP_OK);
                }  
            }

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops
     */
    public function getAllCrops_post()
    {
        try {   
            $formData = array(
                'languageId' => $this->post('language'),
            );
            if(!empty($formData['languageId'])){
                $languageId = $formData['languageId'];                
            }else{
                $languageId = 1;
            }

            $languageData = $this->commons->getLanguageById($languageId);
            $cropsData = $this->commons->getAllCrops($languageData->symbol);
            if($cropsData && count($cropsData) > 0)
            {
                $this->set_response(['status' => TRUE,'data' => $cropsData], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops
     */
    public function getAllCropFamily_post()
    {
        try {   
            $formData = array(
                'languageId' => $this->post('language'),
            );
            if(!empty($formData['languageId'])){
                $languageId = $formData['languageId'];                
            }else{
                $languageId = 1;
            }

            $languageData = $this->commons->getLanguageById($languageId);
            $cropFamilyData = $this->commons->getAllCropFamily($languageData->symbol);
            if($cropFamilyData && count($cropFamilyData) > 0)
            {
                $this->set_response(['status' => TRUE,'data' => $cropFamilyData], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * login function for the app 
     */
    public function getCropDataById_post()
    {
        try {
            $crop_data = array();
            $crop_id = $this->post('crop_id');
            $languageId = $this->post('language');

            if(!empty($languageId)){
                $languageId = $languageId;                
            }else{
                $languageId = 1;
            }
            $languageData = $this->commons->getLanguageById($languageId);
            if(!empty($crop_id))
            {
                $data = $this->admins->get_crop($crop_id, $languageData->symbol);
                if(!empty($data)){
                    $crop_data = $data;
                }
                $this->response(['status' => TRUE,'data' => $crop_data,'message' => 'crop Data'], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

	public function addCropData_post()
    {
        try {
            $languagesList = $this->commons->getAllLanguage();

            $formData = array(
                /*'name_de' => $this->post('name_de'),
				'name_fr' => $this->post('name_fr'),
				'name_en' => $this->post('name_en'),
                'name_it' => $this->post('name_it'),
                */
                'family' => $this->post('family'),
                'color' => $this->post('color'),
                'cropId' => $this->post('cropId'),
                'language' => $this->post('language')
            );

            foreach ($languagesList as $lang) {
                $name = 'name_'.$lang->symbol;
                $formData[$name] = $this->post($name); 
            }

            $this->form_validation->set_data($formData);
            // $this->form_validation->set_rules('name_de', 'Name', 'required|trim|max_length[100]');
			// $this->form_validation->set_rules('name_fr', 'Name', 'required|trim|max_length[100]');
			// $this->form_validation->set_rules('name_en', 'Name', 'required|trim|max_length[100]');
            // $this->form_validation->set_rules('name_it', 'Name', 'required|trim|max_length[100]');
            foreach($languagesList as $lang){
                $name = 'name_'.$lang->symbol;
                $this->form_validation->set_rules($name, 'required|trim|max_length[100]');
            }
            $this->form_validation->set_rules('family', 'Crop Family', 'required|trim');
            $this->form_validation->set_rules('color', 'Color', 'required|trim');
            $this->form_validation->set_rules('cropId', 'crop Id', 'required|trim');
            $this->form_validation->set_rules('language', 'Language', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }else{
                unset($formData['language']);
                $formData['crop_family_id'] = $formData['family'];
                unset($formData['family']);

                if($formData['cropId'] > 0 && $formData['cropId'] != ''){
                    //// update ////
                    $formData['ac_cropId'] = $formData['cropId'];
                    unset($formData['cropId']);

                    $response = $this->admins->update_crop_data($formData);
                    if($response)
                    {
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }
                    else
                    {
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    } 
                }else{
                    /// save ///
                    unset($formData['cropId']);
                    $response = $this->admins->add_crop_data($formData);
                    if($response)
                    {
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }
                    else
                    {
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    } 

                }

                        
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * User Profile function for the app 
     */
    public function updateCropData_post()
    {
        try {

            $formData = array(
                'name_de' => $this->post('name_de'),
				'name_fr' => $this->post('name_fr'),
				'name_en' => $this->post('name_en'),
				'name_it' => $this->post('name_it'),
                'family' => $this->post('family'),
                'color' => $this->post('color'),
                'cropId' => $this->post('cropId'),
                'language' => $this->post('language')
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name_de', 'Name', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('name_fr', 'Name', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('name_en', 'Name', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('name_it', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('family', 'Crop Family', 'required|trim');
            $this->form_validation->set_rules('color', 'Color', 'required|trim');
            $this->form_validation->set_rules('cropId', 'crop Id', 'required|trim');
            $this->form_validation->set_rules('language', 'Language', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }else{
                $response = $this->admins->update_crop_data($formData);
                if($response)
                {
                    $post = array('status' => TRUE,'message' => 'update_success');
                }
                else
                {
                    $post = array('status' => FALSE,'message' => 'update_fail');
                }  
            }
            $this->response($post , REST_Controller::HTTP_OK);     

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

	public function deleteCrop_post()
    {
        try {
            $formData = array(
                'cropId' => $this->post('crop_id')
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('cropId', 'crop Id', 'required|trim');
            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }else{
                $response = $this->admins->delete_crop($formData);
                if($response)
                {
                    $post = array('status' => TRUE,'message' => 'update_success');
                }
                else
                {
                    $post = array('status' => FALSE,'message' => 'update_fail');
                } 
            }   
            $this->response($post , REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops
     */
    public function exportUserList_get()
    {
        try {
            $usersData = $this->admins->getAllUsers();

            if($usersData && count($usersData) > 0)
            {
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('Agricontrol');
                $pdf->SetTitle('Export User List');
                $pdf->SetSubject('Agricontrol');
                $pdf->SetKeywords('Agricontrol');

                $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0,0,0), array(255,255,255));
                $pdf->setFooterData(array(0,0,0), array(255,255,255));

                // set header and footer fonts
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                $pdf->SetMargins(10, 36, 18, true);
                $pdf->SetHeaderMargin(12);
                $pdf->SetFooterMargin(18);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
                
                $pdf->setFontSubsetting(true);
                $pdf->SetFont('dejavusans', '', 10, '', true);
                $pdf->AddPage('L');
                $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
                
                $html = '<style>
                            table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                              border: 1px solid #666666;
                              font-size: 12px;
                            }
                            tr.noBorder td {
                              border: 0;
                              font-size: 12px;
                            }
                            .footerText{
                                position: absolute;
                                bottom: 0;
                                left: 0;
                                right: 0;
                                margin: 0 auto;
                            }
                        </style>
                        <h3 style="font-size: 15px;font-weight:bold;text-align:center;">Users List</h3>&nbsp;<br/>';

                $html .= '<table class="fieldTable" cellpadding="7" cellspacing="0" style="font-size: 9px;">
                        <tr class="noBorder">
                            <th style="background-color: #66bd66; color: #fff; width: 15%; font-size: 9px;text-align:center;"><b>Name</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 15%; font-size: 9px;text-align:center;"><b>Company</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 15%; font-size: 9px;text-align:center;"><b>Email</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 20%; font-size: 9px;text-align:center;"><b>Address</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 10%; font-size: 9px;text-align:center;"><b>Subscription Start</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 10%; font-size: 9px;text-align:center;"><b>Subscription End</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 10%; font-size: 9px;text-align:center;"><b>Pay Method</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 5%; font-size: 9px;text-align:center;"><b>Status</b></th>
							<th style="background-color: #66bd66; color: #fff; width: 5%; font-size: 9px;text-align:center;"><b>Language</b></th>
                        </tr>';

                foreach ($usersData as $key => $value) {                    
                    $html .= '<tr style="font-color: #343030;">
                                <td style="width: 15%; font-size: 9px;text-align:center;"> '.ucfirst($value->name).'</td>
                                <td style="width: 15%; font-size: 9px;text-align:center;">'.ucfirst($value->company).' a</td>
                                <td style="width: 15%; font-size: 9px;text-align:center;"> '.$value->email.'</td>
                                <td style="width: 20%; font-size: 9px;text-align:center;"> '.ucfirst($value->street_name).', '.$value->postal_code.', '.ucfirst($value->city).'</td>
                                <td style="width: 10%; font-size: 9px;text-align:center;"> '.$value->subscription_start.'</td>
                                <td style="width: 10%; font-size: 9px;text-align:center;"> '.$value->subscription_end.'</td>
                                <td style="width: 10%;font-size: 9px;text-align:center;">'.$value->method.' </td>';
								
                    if($value->is_verified == '1'){
                        $html .= '<td style="width: 5%;font-size: 9px;text-align:center;">Active</td>';
                    }else{
                        $html .= '<td style="width: 5%;font-size: 9px;text-align:center;">Deactive</td>';
                    }
					$html .= '<td style="width: 5%;font-size: 9px;text-align:center;">'.$value->LanguageName.' </td>';
                    $html .= '</tr>';
                }

                $html .= '</table>';       
                $headers = array('Content-Type: text/html; charset=UTF-8');
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 0, 0, true, '', true);
                ob_end_clean();
                $current_time_stamp = time();
                $file_name = 'userlist_'.$current_time_stamp.'.pdf';
                $pdf->Output(FCPATH .'uploads/exports/'.$file_name, 'F');

                ///$file_path = 'https://agricontrol.app/dashboard/uploads/exports/'.$file_name;
                $file_path = EXPORT_PATH.$file_name;

                $this->set_response(['status' => TRUE,'data' => $file_path], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(['status' => FALSE,'data' => array(),'message' => 'no_data_found'], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops in CSV format
     */
    public function exportcsvUserList_get()
    {
			// file name 
		   $filename = 'users_'.date('Ymd').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
   
		   // get data 
		   
		   $usersData = $this->admins->getCSVAllUsers();

		   // file creation 
		   $file = fopen('php://output', 'w');
 
		   $header = array("Name","Company","Email","Address", "Subscription Start", "Subscription End", "Pay Method", "Status", "Language"); 
		   fputcsv($file, $header);
		   foreach ($usersData as $key=>$line){ 
			 fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
			
        // try {
        //     $usersData = $this->admins->getAllUsers();

        //     if($usersData && count($usersData) > 0)
        //     {
        //         // file name 
        //             $filename = 'users_'.date('Ymd').'.csv'; 
        //             header("Content-Description: File Transfer"); 
        //             header("Content-Disposition: attachment; filename=$filename"); 
        //             header("Content-Type: application/csv; ");

        //             // get data 
        //             $usersData = $this->Main_model->getAllUsers();

        //             // file creation 
        //             $file = fopen('php://output', 'w');

        //             $header = array("Name","Company","Email","Address"); 
        //             fputcsv($file, $header);
        //             foreach ($usersData as $key=>$line){ 
        //             fputcsv($file,$line); 
                    
        //             }
        //             fclose($file); 
        //             exit; 
        //     }

                 

        //         $headers = array('Content-Type: text/html; charset=UTF-8');
        //         $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        //         ob_end_clean();
        //         $current_time_stamp = time();
        //         $file_name = 'userlist_'.$current_time_stamp.'.pdf';
        //         $pdf->Output(FCPATH .'uploads/exports/'.$file_name, 'F');

        //         $file_path = 'http://localhost/agricontrol/uploads/exports/'.$file_name;

        //         $this->set_response([
        //             'status' => TRUE,
        //             'data' => $file_path
        //         ], REST_Controller::HTTP_OK);
        //     }
        //     else
        //     {
        //          $this->response([
        //             'status' => FALSE,
        //             'data' => array(),
        //             'message' => 'no_data_found'
        //         ], REST_Controller::HTTP_OK);
        //     }
        // } catch(Exception $e) {
        //     $this->response([
        //                 'status' => FALSE,
        //                 'message' => 'error_something_wrong'
        //             ], REST_Controller::HTTP_OK);
        // }
    }

    /**
     * Get language by Id
     */
    public function getLanguageById_post()
    {
        try {
            $result_data = array();
            $lang_id = $this->post('lang_id');

            if(!empty($lang_id))
            {
                $data = $this->admins->get_language($lang_id);

                if(!empty($data)){
                    $result_data = $data;
                }
             
                $this->response([
                    'status' => TRUE,
                    'data' => $result_data,
                    'message' => 'Language Data'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Something went wrong! Please try again later 123.'
                ], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response([
                'status' => FALSE,
                'message' => 'Something went wrong! Please try again later.'
            ], REST_Controller::HTTP_OK);
        }
    }
   
    /**
     * User language File upload function for the app 
     */
    public function languageFile_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;

            if($user_id){
                $this->form_validation->set_rules('image', '', 'callback_file_check');
                //$server_url = 'http://localhost:4200/';
                if($this->form_validation->run() == true){
                    $config['upload_path']   = 'uploads/profile_picture/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size']      = 3072;
                    $config['max_width']     = 2000;
                    $config['max_height']    = 2000;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if($this->upload->do_upload('image')){
                        $uploadData = $this->upload->data();
                        $uploadedFile = $uploadData['file_name'];
                        
                        $res = $this->admins->update_image_name($uploadedFile, $user_id);
            
                        if($res)
                        {      
                            $this->set_response([
                                'status' => TRUE,
                                'data' => $uploadedFile,
                                'message' => 'User Profile updated successfully.'
                            ], REST_Controller::HTTP_OK); 
                        }
                        else
                        {
                            $this->response([
                                'status' => FALSE,
                                'message' => 'Something went wrong! Please try again later 33.'
                            ], REST_Controller::HTTP_OK);
                        }
                    }else{
                        $data['error_msg'] = $this->upload->display_errors();
                        $this->response([
                            'status' => FALSE,
                            'message' => $data['error_msg']
                        ], REST_Controller::HTTP_OK);
                    }
                }
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Something went wrong! Please try again later 222.'
                ], REST_Controller::HTTP_OK);
            }
        } catch(Exception $e) {
            $this->response([
                'status' => FALSE,
                'message' => 'Something went wrong! Please try again later 11.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Update language name function for the app 
     */
    public function updateLanguageData_post()
    {
        try {
            $formData = array(
                'name' => $this->post('name'),
                'langId' => $this->post('langId'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('langId', 'Language Id', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                 $this->response([
                    'status' => FALSE,
                    'message' => validation_errors()
                ], REST_Controller::HTTP_OK); 
            }

            $response = $this->admins->update_language_data($formData);
            if($response)
            {
                $this->set_response([
                    'status' => TRUE,
                    'message' => 'Data Updated Successfully.'
                ], REST_Controller::HTTP_OK);                         
            }
            else
            {
                $this->response([
                        'status' => FALSE,
                        'message' => 'Data Not Updated.'
                    ], REST_Controller::HTTP_OK);
            }         
        } catch(Exception $e) {
            $this->response([
                        'status' => FALSE,
                        'message' => 'Something went wrong! Please try again later.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function addLanguageData_post()
    {
        try {
            $symbol = strtolower($this->post('symbol'));
            $name = $this->post('name');
            $formData = array(
                'name' => $this->post('name'),
				'symbol' => $symbol,
                'langId' => $this->post('langId'),
            );
            $this->form_validation->set_data($formData);

            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('symbol', 'Symbole', 'required|trim|max_length[3]');
            $this->form_validation->set_rules('langId', 'Language Id', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                 $this->response([
                    'status' => FALSE,
                    'message' => validation_errors()
                ], REST_Controller::HTTP_OK); 
            }
            //print_r($formData);
            $check = $this->admins->check_language_unique($name,$symbol);
            if(!$check)
            {
                $response = $this->admins->add_language_data($formData);
                if($response)
                {
                    //// add column in table ///
                    //-- crop --//
                    $feild_name = 'name_'.$symbol;
                    $cropAddSql = "ALTER TABLE `ac_crop` ADD COLUMN `".$feild_name."` VARCHAR(250) CHARACTER SET `utf8` COLLATE `utf8_general_ci` AFTER `name_de`";
                    $this->user_model->addColumnSql($cropAddSql);
                    $cropCopySql = "UPDATE `ac_crop` SET `".$feild_name."`= `name_en` ";
                    $update = $this->user_model->updateRecordSql($cropCopySql);

                    //-- crop family --//
                    $familySql = "ALTER TABLE `ac_crop_family` ADD COLUMN `".$feild_name."` VARCHAR(250) CHARACTER SET `utf8` COLLATE `utf8_general_ci` AFTER `name_de` ";
                    $this->user_model->addColumnSql($familySql);
                    $familyCopySql = "UPDATE `ac_crop_family` SET `".$feild_name."`= `name_en` ";
                    $this->user_model->updateRecordSql($familyCopySql);

                    //-- Faq detail --//
                    $titleName = "title_".$symbol;
                    $contentName = "content_".$symbol;
                    $faqSql = "ALTER TABLE `ac_faq`
                    ADD COLUMN `".$titleName."` VARCHAR(250) CHARACTER SET `utf8` COLLATE `utf8_general_ci` AFTER `title_fr`,
                    ADD COLUMN `".$contentName."` TEXT CHARACTER SET `utf8` COLLATE `utf8_general_ci` AFTER `content_fr` ";
                    $this->user_model->addColumnSql($faqSql);

                    $faqCopySql= "UPDATE `ac_faq` SET `".$titleName."` =`title_en` , `".$contentName."` = `content_en`";
                    $this->user_model->updateRecordSql($faqCopySql);
                    
                    //// add language file ///
                    $filename = $symbol.'.json';
                    ///$filename = 'file123.json';
                    $oldfile = LOCAL_JSON_FILE_PATH.'en.json';
                    $newFile = LOCAL_JSON_FILE_PATH.$filename;
                    
                    //// reate new json file ///
                    $error = '';
                    //$handle = fopen($newFile, 'w'); //implicitly creates file
                    if(copy($oldfile, $newFile)){
                    //if(fopen($newFile, 'w')){
                        //$str = file_get_contents($oldfile);//get contents of your json file and store it in a string
                        //$arr = json_decode($str, true);
                        //print_r($arr);
                        /// get all key from enlish json file //
                        /*if(!empty($arr)){
                            foreach($arr as $key=>$value){
                                /*$newArr[]= array(
                                    $key =>'',
                                );*/
                                /*$newArr[$key] = '';
                            }
                        }*/
                        //// copy all key in new json file ///
                        //$json = json_encode($arr,JSON_PRETTY_PRINT);
                        //file_put_contents($newFile, $json);
                    }else{
                        // or die('Cannot open file: '.$filename);
                        $error = "Not created file :-".$filename;
                    }
                        
                        
                    //// --- End create json file --- ////

                    $this->set_response([
                        'status' => TRUE,
                        'test' => $update,
                        'message' => 'Data Added Successfully.'
                    ], REST_Controller::HTTP_OK);                         
                }
                else
                {
                    $this->response([
                            'status' => FALSE,
                            'message' => 'Data Not Added.'
                        ], REST_Controller::HTTP_OK);
                }  
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Already Added.'
                ], REST_Controller::HTTP_OK);
            }   
        } catch(Exception $e) {
            $this->response([
                        'status' => FALSE,
                        'message' => 'Something went wrong! Please try again later.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function publishLanguage_post()
    {
        try {
            $formData = array(
                'status' => $this->post('status'),
                'ac_languageId' => $this->post('ac_languageId'),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('status', 'Status', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('ac_languageId', 'Language Id', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                 $this->response([
                    'status' => FALSE,
                    'message' => validation_errors()
                ], REST_Controller::HTTP_OK); 
            }

            $querystring = "SELECT * FROM `ac_language` WHERE `ac_languageId` = '".$formData['ac_languageId']."' ";

            $langData = $this->db->query($querystring);
            if($langData->num_rows() > 0)
            {
                foreach ($langData->result() as $key => $val) {
                    $is_publish = $val->is_publish;
                }
            }
            $formData['is_publish'] = ($is_publish == 0)?1:0;
            $response = $this->admins->publish_language($formData);
            if($response)
            {
                $this->set_response([
                    'status' => TRUE,
                    'message' => 'update_success'
                ], REST_Controller::HTTP_OK);                         
            }
            else
            {
                $this->response([
                        'status' => FALSE,
                        'message' => 'update_fail'
                    ], REST_Controller::HTTP_OK);
            }         
        } catch(Exception $e) {
            $this->response([
                        'status' => FALSE,
                        'message' => 'error_something_wrong'
                    ], REST_Controller::HTTP_OK);
        }
    }

}
