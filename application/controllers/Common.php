<?php
use Restserver\Libraries\REST_Controller;
///use function GuzzleHttp\json_decode;

defined('BASEPATH') OR exit('No direct script access allowed');

/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Common extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('commons');
        $this->load->model('user_model');
    }

    public function cleanDatabase_get()
    {
        try {           
            $cleaned = $this->commons->cleanDatabase();

            if($cleaned)
            {
                $post = array('status' => TRUE,'data' => 'delete_success');
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Subscription Plans
     */
    public function getAllSubscriptions_get()
    {
        try {           
            $subscriptions = $this->commons->getAllPlans();

            if($subscriptions && count($subscriptions) > 0)
            {
                $post = array('status' => TRUE,'data' => $subscriptions);
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

	public function upgradeUserPlan_post()
    {
        try {
                $formData = array(
                    'method' => $this->post('method'),
                    'comment' => $this->post('comment'),
                    'userId' => $this->api_token->ac_userId,
                    'planId' => $this->post('planId'),
					'paymentOption' => $this->post('paymentOption')
                );
                $this->form_validation->set_data($formData);
                $this->form_validation->set_rules('userId', 'User Id', 'required|trim');
                $this->form_validation->set_rules('method', 'method', 'trim');
                $this->form_validation->set_rules('comment', 'comment', 'trim');
                $this->form_validation->set_rules('planId', 'plan Id', 'required|trim');
				$this->form_validation->set_rules('paymentOption', 'payment Option', 'required|trim');
                $this->form_validation->set_error_delimiters('', '<br>');

                if ($this->form_validation->run($this) === FALSE)
                {
                    $post = array('status' => FALSE, 'message' => validation_errors());
                }else{
                    $response = $this->commons->upgrade_user_plan($formData);
                    if($response)
                    {
                        $post = array('status' => TRUE,'message' => 'plan_upgrade_success');
                    }
                    else
                    {
                        $post = array('status' => FALSE,'message' => 'error_something_wrong');
                    }
                }
                $this->response($post, REST_Controller::HTTP_OK);

            } catch(Exception $e) {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
    }

	public function getAllFaq_post()
    {
        try {           
			$lang = $this->post('lang');
            $subscriptions = $this->commons->getAllFaq($lang);

            if($subscriptions && count($subscriptions) > 0)
            {
                $post = array('status' => TRUE,'data' => $subscriptions);
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {

            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Languages
     */
    public function getAllLanguage_get()
    {
        try {           
            $languages = $this->commons->getAllLanguage();
            if($languages && count($languages) > 0)
            {
                $post = array('status' => TRUE,'data' => $languages);
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Languages
     */
    public function getLanguageById_post()
    {
        try {   
            $formData = array(
                'languageId' => $this->post('language'),
            );

            if(!empty($formData['languageId'])){
                $languageId = $formData['languageId'];                
            }else{
                $languageId = 1;
            }

            $languageData = $this->commons->getLanguageById($languageId);
            if($languageData && !empty($languageData))
            {
                $post = array('status' => TRUE,'data' => $languageData);
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Languages
     */
    public function getLanguageBySymbol_post()
    {
        try {   
            $formData = array(
                'languagesymbol' => $this->post('languagesymbol'),
            );

            if(!empty($formData['languagesymbol'])){
                $languagesymbol = $formData['languagesymbol'];                
            }else{
                $languagesymbol = 'en';
            }

            $languageData = $this->commons->getLanguageBySymbol($languagesymbol);
            if($languageData && !empty($languageData))
            {
                $post = array('status' => TRUE,'data' => $languageData);
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops
     */
    public function getAllCrops_post()
    {
        try {   
            $formData = array(
                'languageId' => $this->post('language'),
            );

            if(!empty($formData['languageId'])){
                $languageId = $formData['languageId'];                
            }else{
                $languageId = 1;
            }
            $crop_array=array();

            $languageData = $this->commons->getLanguageById($languageId);

            $formatted_crop_array = array();
            $user_id = $this->api_token->ac_userId;
            $cropsData = $this->commons->getAllCrops($languageData->symbol);

            if(!empty($cropsData)){
                $crop_family_ids = array();
                foreach($cropsData as $key=>$value){
                    $crop = array();
                    if(!in_array($value->crop_family_id, $crop_family_ids)){
                        if(isset($crop_array) && !empty($crop_array)){
                            $crop_family['crops'] =  $crop_array;
                        }

                        if(isset($crop_family) && !empty($crop_family)){
                            array_push($formatted_crop_array, $crop_family);
                        }

                        $crop_family = array();
                        $crop_array = array();
                        $crop_family['crop_family_id'] = $value->crop_family_id;
                        $crop_family['crop_family_name'] = $value->crop_family_name;

                        $crop_family['crops'] = array();
                        array_push($crop_family_ids, $value->crop_family_id);
                    }

                    $crop['crop_id'] = $value->crop_id;
                    $crop['crop_name'] = $value->crop_name;
                    $crop['color'] = $value->color; 

                    array_push($crop_array, $crop);
                }

                if(isset($crop_array) && !empty($crop_array)){
                    $crop_family['crops'] =  $crop_array;
                }

                if(isset($crop_family) && !empty($crop_family)){
                    array_push($formatted_crop_array, $crop_family);
                }
            }

            if($cropsData && count($cropsData) > 0)
            {
                $post = array('status' => TRUE,'data' => $formatted_crop_array);
            }
            else
            {
                $post = array('status' => FALSE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Crops
     */
    public function getAllUserCrops_post()
    {
        try {
            $formData = array(
                'languageId' => $this->post('language'),
            );

            if(!empty($formData['languageId'])){
                $languageId = $formData['languageId'];                
            }else{
                $languageId = 1;
            }
            $crop_array=array();

            $languageData = $this->commons->getLanguageById($languageId);

            $formatted_crop_array = array();
            $user_id = $this->api_token->ac_userId;
            $cropsData = $this->commons->getAlluserCrops($user_id, $languageData->symbol);

            if(!empty($cropsData)){
                $crop_family_ids = array();
                foreach($cropsData as $key=>$value){
                    $crop = array();
                    if(!in_array($value->crop_family_id, $crop_family_ids)){
                        if(isset($crop_array) && !empty($crop_array)){
                            $crop_family['crops'] =  $crop_array;
                        }

                        if(isset($crop_family) && !empty($crop_family)){
                            array_push($formatted_crop_array, $crop_family);
                        }

                        $crop_family = array();
                        $crop_array = array();
                        $crop_family['crop_family_id'] = $value->crop_family_id;
                        $crop_family['crop_family_name'] = $value->crop_family_name;

                        $crop_family['crops'] = array();
                        array_push($crop_family_ids, $value->crop_family_id);
                    }

                    $crop['crop_id'] = $value->crop_id;
                    $crop['crop_name'] = $value->crop_name;
                    $crop['color'] = $value->color; 

                    array_push($crop_array, $crop);
                }

                if(isset($crop_array) && !empty($crop_array)){
                    $crop_family['crops'] =  $crop_array;
                }

                if(isset($crop_family) && !empty($crop_family)){
                    array_push($formatted_crop_array, $crop_family);
                }
            }

            if($cropsData && count($cropsData) > 0)
            {
                $post = array('status' => TRUE,'data' => $formatted_crop_array);
            }
            else
            {
                $post = array('status' => FALSE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get server date
     */
    public function getServerDate_get()
    {
        try {           
            $date = date("m.d.Y");
            if(isset($date) && !empty($date))
            {
                $post = array('status' => TRUE,'data' => $date);
            }
            else
            {
                $post = array('status' => TRUE,'data' => $this->api_token);
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

     /**
     * Update Json File
     */
    public function updateJsonFile_post()
    {
        try {           
            //$file = $this->post('language');
            $update_array = $this->post('update_array');
            //$filename = $file.".json";
            //$url = $_SERVER['DOCUMENT_ROOT']."/assets/i18n/test.json";
            //$url = "./agricontrolApp/src/assets/i18n/test.json";
            
            //$url = SERVER_JSON_FILE_PATH."test.json";
            //$url = LOCAL_JSON_FILE_PATH."test.json";
           
            $newjson = json_encode($update_array);
            $formdata = json_decode($newjson);

            foreach($formdata as $value){
                $symbole = $value->symbol;
                foreach($value as $key => $val) {
                    $jsonNewArr[$symbole][$key] = $val;
                }
            }
            foreach($jsonNewArr as $value){
                $newJson = array();
                foreach($value as $key => $val) {
                    if($key == 'symbol'){
                        $file = $val;
                    }else{
                        $newJson[$key] = $val;
                    }
                }

                $filename = $file.".json";
                $url = LOCAL_JSON_FILE_PATH.$filename;
                $str = file_get_contents($url);//get contents of your json file and store it in a string
                $arr = json_decode($str, true);

                // Push user data to array
                $newArr = array_merge($arr,$newJson); 
                $json = json_encode($newArr,JSON_PRETTY_PRINT);
                file_put_contents($url, $json);
                
            }
            ///$url = LOCAL_JSON_FILE_PATH.$filename;
            /*$jsonNewArr = array();
            foreach($formdata as $value){
                //$newArr = $value;
                foreach($value as $key => $val) {
                    //$newArr = $key . ': ' . $val;
                    $jsonNewArr[$key] = $val;
                }
            }
            $str = file_get_contents($url);//get contents of your json file and store it in a string
            $arr = json_decode($str, true);
            
            // Push user data to array
            $newArr = array_merge($arr,$jsonNewArr); 
            $json = json_encode($newArr,JSON_PRETTY_PRINT);
            file_put_contents($url, $json);*/
            
            $this->set_response([
                'status' => TRUE,
                //'result' => $jsonNewArr,
                //'new' => $newJson,
                'message' => 'Updated Successfully.'
            ], REST_Controller::HTTP_OK);
            
            
        } catch(Exception $e) {
            $this->response([
                'status' => FALSE,
                'message' => 'Something went wrong! Please try again later.'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function getAllLanguageKeyValue_get()
    {
        try {           
            //$languages = $this->commons->getAllLanguage();
            $languages = $this->user_model->getRecord('ac_language',array());
            if($languages && count($languages) > 0)
            {
                //echo "<pre/>";
                //print_r($languages);
                foreach($languages as $lang){
                    $symbol = $lang['symbol'];
                    $filename = $symbol.".json"; 
                    $url = LOCAL_JSON_FILE_PATH.$filename;
                    $str = file_get_contents($url);
                    //$arr1[] = json_decode($str, true);
                    $arr = json_decode($str, true);
                    ///print_r($arr);
                    foreach($arr as $key=>$val){
                        //print_r($key);
                        //print_r($val);
                        //echo $key.':'.$val;
                        ///$newKey = "'".$key."'";
                        $newArr[$key][] = $val;
                        
                    }
                    //print_r($newArr);
                }
                //print_r($newArr);
                //die;
                $post = array('status' => TRUE,'data' => $languages,'keyvalue'=>$newArr);
                //$dataList = $this->user_model->getRecord('ac_machine',array('user_id'=>$user_id));
            }
            else
            {
                $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

}
