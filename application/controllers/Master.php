<?php
/**
 * 
 */
use Restserver\Libraries\REST_Controller;
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Master extends REST_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('user_model');
		$this -> load -> library('session');
		$this->load->library('form_validation');
		$this->load->library('Pdf');
	}
	
    /**
     * add machine function
     */
    public function actionMachine_post()
    {
        try {
            $machine_id = $this->post('machine_id');
            $formData = array(
                'machine_name' => $this->post('machine_name'),
				'user_id' =>  $this->api_token->ac_userId
            );
          
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('machine_name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                if($machine_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_machine',$formData,array('machine_id'=>$machine_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_machine',$formData);
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
				
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
	
    /**
     * Get all machine function
     */
    public function getAllMachine_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $dataList = $this->user_model->getRecord('ac_machine',array('user_id'=>$user_id));
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * delete machine function 
     */
    public function deleteMachine_post()
    {
        try {
            $machine_id = $this->post('machine_id');
            if (!empty($machine_id)) {
                $id = $this->user_model->deleteRecord('ac_machine',array('machine_id' => $machine_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * add soil cultivation function
     */
    public function actionSoil_post()
    {
        try {
            $soil_id = $this->post('soil_id');
            $formData = array(
                'soil_name' => $this->post('soil_name'),
				'user_id' =>  $this->api_token->ac_userId
            );
          
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('soil_name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                if($soil_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_soil_cultivation',$formData,array('soil_id'=>$soil_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_soil_cultivation',$formData);
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
				
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all soil function
     */
    public function getAllSoil_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $dataList = $this->user_model->getRecord('ac_soil_cultivation',array('user_id'=>$user_id,'is_delete'=>0));
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * delete machine function 
     */
    public function deleteSoil_post()
    {
        try {
            $soil_id = $this->post('soil_id');
            if (!empty($soil_id)) {
                $id = $this->user_model->deleteRecord('ac_soil_cultivation',array('soil_id' => $soil_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * add Pests & Disease function
     */
    public function actionPests_post()
    {
        try {
            $pests_id = $this->post('pests_id');
            $formData = array(
                'pests_name' => $this->post('pests_name'),
				'user_id' =>  $this->api_token->ac_userId
            );
          
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('pests_name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                if($pests_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_pests_disease',$formData,array('pests_id'=>$pests_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_pests_disease',$formData);
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Pests function
     */
    public function getAllPests_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $dataList = $this->user_model->getRecord('ac_pests_disease',array('user_id'=>$user_id,'is_delete'=>0));
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * delete Pests function 
     */
    public function deletePests_post()
    {
        try {
            $pests_id = $this->post('pests_id');
            if (!empty($pests_id)) {
                $id = $this->user_model->deleteRecord('ac_pests_disease',array('pests_id' => $pests_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Start Fertilizer */
    
    /* Add/Update fertilizer */
    public function actionFertilizer_post()
    {
        try {
            $fertilizer_id = $this->post('fertilizer_id');
            $formData = array(
                'fertilizer_name' => $this->post('fertilizer_name'),
                'unit' => $this->post('unit'),
                'nitrogen' => $this->post('nitrogen'),
                'phosphorus' => $this->post('phosphorus'),
                'potassium_oxide' => $this->post('potassium_oxide'),
                'magnesium' => $this->post('magnesium'),
				'user_id' =>  $this->api_token->ac_userId
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('fertilizer_name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('unit', 'Unit', 'required|trim');
            $this->form_validation->set_rules('nitrogen', 'N%', 'required|trim');
            $this->form_validation->set_rules('phosphorus', 'P2O5', 'required|trim');
            $this->form_validation->set_rules('potassium_oxide', 'K2O', 'required|trim');
            $this->form_validation->set_rules('magnesium', 'Mg', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                if($fertilizer_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_fertilizer',$formData,array('fertilizer_id'=>$fertilizer_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_fertilizer',$formData);
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Get All fertilizer */
    public function getAllFertilizer_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $dataList = $this->user_model->getRecord('ac_fertilizer',array('user_id'=>$user_id,'is_delete'=>0));
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Delete Fertilizer */
    public function deleteFertilizer_post()
    {
        try {
            $fertilizer_id = $this->post('fertilizer_id');
            if (!empty($fertilizer_id)) {
                $id = $this->user_model->deleteRecord('ac_fertilizer',array('fertilizer_id' => $fertilizer_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
    /* End Fertilizer */

    /* Start Pesticides */

    /* Get All Pestiscide type */
    public function getAllPesticideType_get()
    {
        try {
            $dataList = $this->user_model->getRecord('ac_pesticide_type',array());
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Add/Update Pestiscide */
    public function actionPestiscide_post()
    {
        try {
            $pesticide_id = $this->post('pesticide_id');
            $formData = array(
                'pesticide_name' => $this->post('pesticide_name'),
                'unit' => $this->post('unit'),
                'type_id' => $this->post('type_id'),
                'waiting_days' => $this->post('waiting_days'),
				'user_id' =>  $this->api_token->ac_userId
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('pesticide_name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('unit','Unit','required|trim');
            $this->form_validation->set_rules('type_id', 'Type', 'required|trim');
            $this->form_validation->set_rules('waiting_days', 'Waiting Days', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                if($pesticide_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_pesticides',$formData,array('pesticide_id'=>$pesticide_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_pesticides',$formData);
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Get All Pestiscide  */
    public function getAllPesticide_get()
    {
        try {

            $user_id = $this->api_token->ac_userId;
            ///$dataList = $this->user_model->getRecord('ac_pesticides',array());
            $sql = "SELECT PI.*,TI.type_name FROM `ac_pesticides` PI LEFT JOIN ac_pesticide_type TI ON TI.type_id = PI.type_id WHERE PI.user_id =".$user_id." ";
            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
    
    /* Delete Pestiscide */
    public function deletePestiscide_post()
    {
        try {
            $pesticide_id = $this->post('pesticide_id');
            if (!empty($pesticide_id)) {
                $id = $this->user_model->deleteRecord('ac_pesticides',array('pesticide_id' => $pesticide_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
    /* End Pesticides */

    /* Plant Activity */
    public function plantActivity_post()
    {
        try {
            $plant_protection_id = $this->post('plant_protection_id');
            $formData = array(
                'plant_date' => $this->post('plant_date'),
                
                'pesticide_id' => $this->post('pesticide_id_0'),
                'amount' => $this->checkDecimalnumber($this->post('amount_0')),
                'unit'=>$this->post('unit_0'),
                'waiting_period' => $this->post('waiting_period_0'),
                'total_amount' => $this->checkDecimalnumber($this->post('total_amount_0')),
                'pests_id' => $this->post('pests_id_0'),
                'end_date' => $this->post('end_date_0'),

                'machine_id' => $this->post('machine_id'),
                'person_name' => $this->post('person_name'),
                'comment' => $this->post('comment'),
                'field_id' => $this->post('field_id'),
                'culture_id' => $this->post('culture_id'),
				'user_id' =>  $this->api_token->ac_userId
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('pesticide_id', 'Pesticide', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('plant_date','Date','required|trim');
            $this->form_validation->set_rules('amount' , 'Amount','required|trim');
            $this->form_validation->set_rules('unit' ,'Unit','required|trim');
            $this->form_validation->set_rules('waiting_period', 'Waiting Days', 'required|trim');
            $this->form_validation->set_rules('total_amount','Total amount','required|trim');
            $this->form_validation->set_rules('machine_id', 'Machine', 'required|trim');
            $this->form_validation->set_rules('pests_id','Pests','required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');
            
            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors(),'postdata'=>$formData);
            }
            else
            {
                $formData['plant_date'] = date('Y-m-d',strtotime($this->post('plant_date')));
                $formData['end_date'] = date('Y-m-d',strtotime($this->post('end_date_0')));

                ///$formData['field_id'] = $this->post('field_id');
                ///$formData['culture_id'] = $this->post('culture_id');
                $main_activity_id = '';

                if($plant_protection_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_plant_protection',$formData,array('plant_protection_id'=>$plant_protection_id));
                    $main_activity_id = $plant_protection_id;
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_plant_protection',$formData);
                    $main_activity_id = $id;
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }

                if($main_activity_id != ''){

                    if($plant_protection_id != ''){
                        ////$this->user_model->updateRecord('ac_plant_protection',$subArr,array('plant_protection_id'=>$plant_protection_id));
                        $this->user_model->deleteRecord('ac_plant_protection',array('main_activity_id' => $plant_protection_id));
                    }
                    for($i=1;$i<10;$i++){
                        $pesticide_id = $this->post('pesticide_id_'.$i);
                        $amount = $this->post('amount_'.$i);
                        $unit = $this->post('unit_'.$i);
                        $waiting_period = $this->post('waiting_period_'.$i);
                        $total_amount = $this->post('total_amount_'.$i);
                        $pests_id = $this->post('pests_id_'.$i);
                        $end_date = $this->post('end_date_'.$i);

                        if(!empty($pesticide_id) && !empty($amount) && !empty($unit) && !empty($waiting_period) && !empty($total_amount) && !empty($pests_id) && !empty($end_date) )
                        {
                            $subArr = array(
                                'plant_date' => $this->post('plant_date'),
                    
                                'pesticide_id' => $pesticide_id,
                                'amount' => $this->checkDecimalnumber($amount),
                                'unit' => $unit,
                                'waiting_period' => $waiting_period,
                                'total_amount' => $this->checkDecimalnumber($total_amount),
                                'pests_id' => $pests_id,
                                'end_date' => $end_date,

                                'machine_id' => $this->post('machine_id'),
                                'person_name' => $this->post('person_name'),
                                'comment' => $this->post('comment'),
                                'field_id' => $this->post('field_id'),
                                'culture_id' => $this->post('culture_id'),
                                'user_id' =>  $this->api_token->ac_userId,
                                'activity_type' => 1,
                                'main_activity_id' => $main_activity_id,
                                'created' => date('Y-m-d H:i:s'),
                            );
                            $subArr['plant_date'] = date('Y-m-d',strtotime($this->post('plant_date')));
                            $subArr['end_date'] = date('Y-m-d',strtotime($this->post('end_date_'.$i)));
                            
                            $subArr['created']= date('Y-m-d H:i:s');
                            $this->user_model->saveRecord('ac_plant_protection',$subArr);

                        }
                    }// for loop
                }

                /// Start Other culture ////
                $other_crop_ids = $this->post('other_crop_ids');
                if(!empty($other_crop_ids)){
                    foreach($other_crop_ids as $cultureid){
                        $cultureid;
                        $basicSql = "SELECT CI.`ac_cultureId`,CI.`size`,CI.`end_date`,CI.`harvest_date`,FI.ac_fieldId
                                     FROM `ac_culture` CI 
                                     LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                                     WHERE CI.`ac_cultureId` = '".$cultureid."' ";
                        $basicData = $this->user_model->getRecordSql($basicSql); 
                        if(!empty($basicData)){

                            $pesticide_id_0 = $this->post('pesticide_id_0');
                            $productData = $this->user_model->getRecord('ac_pesticides',array('pesticide_id'=>$pesticide_id_0));

                            $waiting_date = date('Y-m-d', strtotime('+'.$productData[0]['waiting_days'].' days', strtotime($formData['plant_date'])));

                            $basicData[0]['size'];
                            $sizeinhectare = ($basicData[0]['size'] / 100);
                            $total_amount = $this->post('amount_0') * $sizeinhectare;

                            $cultureData = array(
                                'plant_date' => $formData['plant_date'],
                                
                                'pesticide_id' => $pesticide_id_0,
                                'amount' => $this->checkDecimalnumber($this->post('amount_0')),
                                'unit'=>$this->post('unit_0'),
                                'waiting_period' => $productData[0]['waiting_days'].' days',
                                'total_amount' => $this->checkDecimalnumber($total_amount).' '.$this->post('unit_0'),
                                'pests_id' => $this->post('pests_id_0'),
                                'end_date' => $waiting_date,
                
                                'machine_id' => $this->post('machine_id'),
                                'person_name' => $this->post('person_name'),
                                'comment' => $this->post('comment'),
                                'field_id' => $basicData[0]['ac_fieldId'],
                                'culture_id' => $cultureid,
                                'user_id' =>  $this->api_token->ac_userId
                            );
                            $other_activity_id = $this->user_model->saveRecord('ac_plant_protection',$cultureData);

                            if($other_activity_id != ''){
                                for($i=1;$i<10;$i++){

                                    $pesticide = $this->post('pesticide_id_'.$i);
                                    $productData = $this->user_model->getRecord('ac_pesticides',array('pesticide_id'=>$pesticide));
                                    $waiting_date = date('Y-m-d', strtotime('+'.$productData[0]['waiting_days'].' days', strtotime($formData['plant_date'])));
                                    
                                    $amount = $this->post('amount_'.$i);
                                    $unit = $this->post('unit_'.$i);
                                    $waiting_period = $productData[0]['waiting_days'].' days';
                                    ///$total_amount = $this->post('total_amount_'.$i);
                                    $pests_id = $this->post('pests_id_'.$i);
                                    $end_date = $waiting_date;
                                    $total_amount = $amount * $sizeinhectare;
            
                                    if(!empty($pesticide) && !empty($amount) && !empty($unit) && !empty($waiting_period) && !empty($total_amount) && !empty($pests_id) && !empty($end_date) )
                                    {
                                        $subArr = array(
                                            'plant_date' => date('Y-m-d',strtotime($this->post('plant_date'))),
                                
                                            'pesticide_id' => $pesticide,
                                            'amount' => $this->checkDecimalnumber($amount),
                                            'unit' => $unit,
                                            'waiting_period' => $waiting_period,
                                            'total_amount' => $this->checkDecimalnumber($total_amount).' '.$unit,
                                            'pests_id' => $pests_id,
                                            'end_date' => date('Y-m-d',strtotime($this->post('end_date_'.$i))),
            
                                            'machine_id' => $this->post('machine_id'),
                                            'person_name' => $this->post('person_name'),
                                            'comment' => $this->post('comment'),
                                            'field_id' => $basicData[0]['ac_fieldId'],
                                            'culture_id' => $cultureid,
                                            'user_id' =>  $this->api_token->ac_userId,
                                            'activity_type' => 1,
                                            'main_activity_id' => $other_activity_id,
                                            'created' => date('Y-m-d H:i:s'),
                                        );
                                        $this->user_model->saveRecord('ac_plant_protection',$subArr);
            
                                    }
                                }// for loop
                            }
                            
                        }
                    }
                }
                /// End Other culture ////

            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Get All Plant Activity  */
    public function getAllPlantActivity_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');

            $sql = "SELECT PA.*,PI.pesticide_name FROM `ac_plant_protection` PA 
                    LEFT JOIN ac_pesticides PI ON PI.pesticide_id = PA.pesticide_id
                    WHERE PA.user_id = ".$user_id." AND PA.activity_type = 0 
                    AND PA.culture_id = '".$culture_id."' ORDER BY PA.plant_date DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $dataArr = array();
                foreach($dataList as $value){
                    $plant_id = $value['plant_protection_id'];
                    
                    $subArr = '';
                    $query = "SELECT * FROM `ac_plant_protection` WHERE `main_activity_id` = '".$plant_id."' AND `activity_type` = 1 ";
                    $subList = $this->user_model->getRecordSql($query);
                    if(!empty($subList)){
                        $subArr = $subList;
                        $value['pesticide_name'] = "multiple plant protection products";
                    }
                    $value['sub_activity'] = $subArr;
                    $dataArr[] = $value;
                }
                $post = array('status' => TRUE,'data'=>$dataArr ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Delete Plant activity */
    public function deletePlantActivty_post()
    {
        try {
            $plant_protection_id = $this->post('plant_protection_id');
            if (!empty($plant_protection_id)) {
                $id = $this->user_model->deleteRecord('ac_plant_protection',array('plant_protection_id' => $plant_protection_id));
                if($id > 0){
                    $this->user_model->deleteRecord('ac_plant_protection',array('main_activity_id' => $plant_protection_id));
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }
    /* End plan activity */

    /* Soil Activity */
    public function soilActivity_post()
    {
        try {
            $soil_activity_id = $this->post('soil_activity_id');
            $formData = array(
                'soil_date' => $this->post('soil_date'),
                'machine_id' => $this->post('machine_id'),
                'soil_id' => $this->post('soil_id'),
                'field_id' => $this->post('field_id'),
                'culture_id' => $this->post('culture_id'),
                'person_name' => $this->post('person_name'),
                'comment' => $this->post('comment'),
				'user_id' =>  $this->api_token->ac_userId
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('soil_date','Date','required|trim');
             $this->form_validation->set_rules('machine_id', 'Machine', 'required|trim');
            $this->form_validation->set_rules('soil_id','Soil','required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');
            
            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                $formData['soil_date'] = date('Y-m-d',strtotime($this->post('soil_date')));
                if($soil_activity_id != ''){
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_soil_activity',$formData,array('soil_activity_id'=>$soil_activity_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $id = $this->user_model->saveRecord('ac_soil_activity',$formData);
                    if($id > 0){
                        /// Start Other culture ////
                        $other_crop_ids = $this->post('other_crop_ids');
                        if(!empty($other_crop_ids)){
                            foreach($other_crop_ids as $cultureid){
                                $cultureid;
                                $basicSql = "SELECT CI.`ac_cultureId`,CI.`size`,CI.`nitrogen` as basic_nitrogen,CI.`phosphorus` as basic_phosphorus,CI.`potassium_oxide` as basic_potassium,CI.`magnesium` as basic_magnesium,
                                            FI.ac_fieldId,FI.phosphorus,FI.potassium_oxide,FI.magnesium 
                                            FROM `ac_culture` CI 
                                            LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                                            WHERE CI.`ac_cultureId` = '".$cultureid."' ";
                                $basicData = $this->user_model->getRecordSql($basicSql); 
                                if(!empty($basicData)){
                                    $cultureData = array(
                                        'soil_date' => $this->post('soil_date'),
                                        'soil_date' => $formData['soil_date'],
                                        'machine_id' => $this->post('machine_id'),
                                        'soil_id' => $this->post('soil_id'),
                                        'field_id' => $basicData[0]['ac_fieldId'],
                                        'culture_id' => $cultureid,
                                        'person_name' => $this->post('person_name'),
                                        'comment' => $this->post('comment'),
                                        'user_id' =>  $this->api_token->ac_userId,
                                        'created' => date('Y-m-d H:i:s')
                                    );
                                    $other_activity_id = $this->user_model->saveRecord('ac_soil_activity',$cultureData);
                                }
                            }
                        }
                        /// End Other culture ////
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Get All Soil Activity  */
    public function getAllSoilActivity_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');

            $sql = "SELECT SA.*,SI.soil_name FROM `ac_soil_activity` SA 
                    LEFT JOIN ac_soil_cultivation SI ON SI.soil_id = SA.soil_id
                    WHERE SA.user_id = '".$user_id."' AND SA.culture_id = '".$culture_id."' ORDER BY SA.soil_date DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* Delete Soil activity */
    public function deleteSoilActivty_post()
    {
        try {
            $soil_activity_id = $this->post('soil_activity_id');
            if (!empty($soil_activity_id)) {
                $id = $this->user_model->deleteRecord('ac_soil_activity',array('soil_activity_id' => $soil_activity_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getCropMultiSoil_post(){
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');
            $soil_date = $this->post('soil_date');
            $language = $this->post('language');
            
            if (!empty($language)) {
                $languageId = $language;
            } else {
                $languageId = 1;
            }
            $langData = $this->user_model->getRecord('ac_language',array('ac_languageId'=>$languageId));
            $symbol = $langData[0]['symbol'];

            $sql = "SELECT CI.`ac_cultureId`,CI.`size`,CI.`start_date`,CI.`end_date`,CI.`harvest_date`,FI.name as feild_name,
                    CR.name_".$symbol." as crop_name FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    LEFT JOIN ac_crop CR ON CR.ac_cropId = CI.`crop_id`
                    WHERE FI.user_id = '".$user_id."' AND FI.archieve = 0 AND `ac_cultureId` != '".$culture_id."' 
                    AND (CASE WHEN CI.`end_date` IS NULL THEN CI.`harvest_date` ELSE CI.`end_date` END) >= '".$soil_date."' 
                    ORDER BY FI.ac_fieldId DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $dataArr = array(); 
                foreach($dataList as $value){
                    $endDate = date('d.m.Y',strtotime($value['end_date']));
                    if($value['end_date'] == ''){
                        $endDate = date('d.m.Y',strtotime($value['harvest_date']));
                    }
                    $value['crop_detail'] = $value['crop_name'].', '.date('d.m.Y',strtotime($value['start_date'])).'-'.$endDate;
                    $dataArr[] = $value;
                }
                $post = array('status' => TRUE,'data'=>$dataArr,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    } 
    /* End Soil activity */

    /* Fertilizer Activity */
    public function getCropMultiFertilizer_post(){
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');
            $fertilizer_date = $this->post('fertilizer_date');
            $language = $this->post('language');
            
            if (!empty($language)) {
                $languageId = $language;
            } else {
                $languageId = 1;
            }
            $langData = $this->user_model->getRecord('ac_language',array('ac_languageId'=>$languageId));
            $symbol = $langData[0]['symbol'];

            $sql = "SELECT CI.`ac_cultureId`,CI.`size`,CI.`start_date`,CI.`end_date`,CI.`harvest_date`,FI.name as feild_name,
                    CR.name_".$symbol." as crop_name FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    LEFT JOIN ac_crop CR ON CR.ac_cropId = CI.`crop_id`
                    WHERE FI.user_id = '".$user_id."' AND FI.archieve = 0 AND `ac_cultureId` != '".$culture_id."' 
                    AND `start_date` <= '".$fertilizer_date."' AND (CASE WHEN CI.`end_date` IS NULL THEN CI.`harvest_date` ELSE CI.`end_date` END) >= '".$fertilizer_date."' 
                    ORDER BY FI.ac_fieldId DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $dataArr = array(); 
                foreach($dataList as $value){
                    $endDate = date('d.m.Y',strtotime($value['end_date']));
                    if($value['end_date'] == ''){
                        $endDate = date('d.m.Y',strtotime($value['harvest_date']));
                    }
                    $value['crop_detail'] = $value['crop_name'].', '.date('d.m.Y',strtotime($value['start_date'])).'-'.$endDate;
                    $dataArr[] = $value;
                }
                $post = array('status' => TRUE,'data'=>$dataArr,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    } 

    public function fertilizerCultureDetail_post(){
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');

            $sql = "SELECT CI.`nitrogen` as c_nitrogen,CI.`phosphorus` as c_phosphorus, CI.`potassium_oxide` as c_potassium_oxide,
                    CI.`magnesium` as c_magnesium,FI.nitrogen,FI.phosphorus,FI.potassium_oxide,FI.magnesium
                    FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    WHERE CI.`ac_cultureId` = '".$culture_id."' ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $current_date = date('Y-m-d'); $previous = $balanceData = '';
                /*$preSql = "SELECT HI.* FROM `ac_fertilizer_activity` FI 
                           LEFT JOIN ac_fertilizer_history HI ON HI.fertilizer_activity_id = FI.`fertilizer_activity_id`
                           WHERE FI.`culture_id`= '".$culture_id."' AND FI.`fertilizer_date` <= '".$current_date."' 
                           AND HI.type = 4 ORDER BY FI.`fertilizer_date` DESC LIMIT 1 ";
                */   
                /*$dateSql = "SELECT `fertilizer_date` FROM `ac_fertilizer_activity` 
                            WHERE `culture_id` = '".$culture_id."' ORDER BY `fertilizer_activity_id` DESC LIMIT 1";
                $dateInfo = $this->user_model->getRecordSql($dateSql); 
                if(!empty($dateInfo)){
                    $current_date = $dateInfo[0]['fertilizer_date'];
                } */            

                $preSql = "SELECT SUM(FA.`new_nitrogen`) as total_nitrigen,SUM(FA.`new_phosphorus`) as total_phosphorus, 
                            SUM(FA.`new_potassium`) as total_potassium,SUM(FA.`new_magnesium`) as total_magnesium 
                            FROM `ac_fertilizer_activity` FA WHERE FA.`culture_id` = '".$culture_id."' ";
                $preData = $this->user_model->getRecordSql($preSql);  
                if(!empty($preData)){
                    if($preData[0]['total_nitrigen'] != null || $preData[0]['total_phosphorus'] || $preData[0]['total_potassium'] || $preData[0]['total_magnesium'])
                    {
                      $previous = $preData[0];
                    }
                }     
                
                /*$balSql = "SELECT balance_nitrogen,balance_phosphorus,balance_potassium,balance_magnesium
                            FROM `ac_fertilizer_activity` FA WHERE FA.`culture_id` = '".$culture_id."' 
                            ORDER BY `fertilizer_date` DESC LIMIT 1";
                $balData = $this->user_model->getRecordSql($balSql);  
                if(!empty($balData)){
                    $balanceData = $balData[0];
                }*/            
                $post = array('status' => TRUE,'data'=>$dataList[0],'previous'=>$previous,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getFertilizerDetail_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $fertilizer_id = $this->post('fertilizer_id');

            $dataList = $this->user_model->getRecord('ac_fertilizer',array('fertilizer_id'=>$fertilizer_id));
            if(!empty($dataList)){
                $post = array('status' => TRUE,'data'=>$dataList[0] ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function fertilizerActivity_post()
    {
        try {
            $fertilizer_activity_id = $this->post('fertilizer_activity_id');
            $user_id = $this->api_token->ac_userId;
            $formData = array(
                'field_id' => $this->post('field_id'),
                'culture_id' => $this->post('culture_id'),
                'fertilizer_date' => $this->post('fertilizer_date'),
                'fertilizer_id' => $this->post('fertilizer_id'),
                'amount' => $this->checkDecimalnumber($this->post('amount')),
                'unit' => $this->post('unit'),
                'total_amount' => $this->checkDecimalnumber($this->post('total_amount')),
                'machine_id' => $this->post('machine_id'),
                'person_name' => $this->post('person_name'),
                'comment' => $this->post('comment'),
                'user_id' =>  $this->api_token->ac_userId,
                'new_nitrogen' => $this->checkDecimalnumber($this->post('new_nitrogen')),
                'new_phosphorus' => $this->checkDecimalnumber($this->post('new_phosphorus')),
                'new_potassium' => $this->checkDecimalnumber($this->post('new_potassium')),
                'new_magnesium' => $this->checkDecimalnumber($this->post('new_magnesium')),
                'balance_nitrogen' => $this->checkDecimalnumber($this->post('balance_nitrogen')),
                'balance_phosphorus' => $this->checkDecimalnumber($this->post('balance_phosphorus')),
                'balance_potassium' => $this->checkDecimalnumber($this->post('balance_potassium')),
                'balance_magnesium' => $this->checkDecimalnumber($this->post('balance_magnesium')),
            );
            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('fertilizer_date','Date','required|trim');
            $this->form_validation->set_rules('fertilizer_id','Fertilizer','required|trim');
            $this->form_validation->set_rules('amount','Amount','required|trim');
            $this->form_validation->set_rules('unit','Unit','required|trim');
            $this->form_validation->set_rules('total_amount','Total','required|trim');
            $this->form_validation->set_rules('machine_id', 'Machine', 'required|trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            $nitrogenArr = explode('.',$formData['balance_nitrogen']);
            if(count($nitrogenArr)>1){
                $formData['balance_nitrogen']= number_format((float)$formData['balance_nitrogen'], 3, '.', '');
            }

            $phosphorusArr = explode('.',$formData['balance_phosphorus']);
            if(count($phosphorusArr)>1){
                $formData['balance_phosphorus'] = number_format((float)$formData['balance_phosphorus'], 3, '.', '');
            }

            $potassiumArr = explode('.',$formData['balance_potassium']);
            if(count($potassiumArr)>1){
                $formData['balance_potassium'] = number_format((float)$formData['balance_potassium'], 3, '.', '');
            }

            $magnesiumArr = explode('.',$formData['balance_magnesium']);
            if(count($magnesiumArr)>1){
                $formData['balance_magnesium'] = number_format((float)$formData['balance_magnesium'], 3, '.', '');
            }

            if ($this->form_validation->run($this) === FALSE)
            {
                $post = array('status' => FALSE,'message' => validation_errors());
            }
            else
            {
                $formData['amount'] = $this->checkDecimalnumber($this->post('amount'));
                $formData['fertilizer_date'] = date('Y-m-d',strtotime($this->post('fertilizer_date')));
                if($fertilizer_activity_id != ''){
                    
                    $formData['updated']= date('Y-m-d H:i:s');
                    $id = $this->user_model->updateRecord('ac_fertilizer_activity',$formData,array('fertilizer_activity_id'=>$fertilizer_activity_id));
                    if($id > 0){
                        $post = array('status' => TRUE,'message' => 'update_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'update_fail');
                    }   
                    ///$post = array('status' => TRUE);
                }else{
                    $formData['created']= date('Y-m-d H:i:s');
                    $activity_id = $this->user_model->saveRecord('ac_fertilizer_activity',$formData);
                    if($activity_id > 0){
                        
                        $cultureArr = array(
                           'nitrogen' => $this->post('basic_nitrogen'),
                           'phosphorus' => $this->post('basic_phosphorus'),
                           'potassium_oxide' => $this->post('basic_potassium'),
                           'magnesium' => $this->post('basic_magnesium'),
                        );
                        $this->user_model->updateRecord('ac_culture',$cultureArr,array('ac_cultureId'=>$formData['culture_id']));
                        
                        /// Start Other culture ////
                        $other_crop_ids = $this->post('other_crop_ids');
                        if(!empty($other_crop_ids)){
                            foreach($other_crop_ids as $cultureid){
                                $cultureid;
                                $basicSql = "SELECT CI.`ac_cultureId`,CI.`size`,CI.`nitrogen` as basic_nitrogen,CI.`phosphorus` as basic_phosphorus,CI.`potassium_oxide` as basic_potassium,CI.`magnesium` as basic_magnesium,
                                            FI.ac_fieldId,FI.phosphorus,FI.potassium_oxide,FI.magnesium 
                                            FROM `ac_culture` CI 
                                            LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                                            WHERE CI.`ac_cultureId` = '".$cultureid."' ";
                                $basicData = $this->user_model->getRecordSql($basicSql); 
                                if(!empty($basicData)){
                                    $bd_nitrogen = ($basicData[0]['basic_nitrogen'] != '')?$basicData[0]['basic_nitrogen']:$this->post('basic_nitrogen');
                                    $bd_phosphorus = ($basicData[0]['basic_phosphorus']  != '')?$basicData[0]['basic_phosphorus']:$this->post('basic_phosphorus');
                                    $bd_potassium = ($basicData[0]['basic_potassium'] != '')?$basicData[0]['basic_potassium']:$this->post('basic_potassium');
                                    $bd_magnesium = ($basicData[0]['basic_magnesium'] != '')?$basicData[0]['basic_magnesium']:$this->post('basic_magnesium');
                                    
                                    $cf_nitrogen = 1;
                                    $cf_phosphorus = ($basicData[0]['phosphorus']!='')?$basicData[0]['phosphorus']:1;
                                    $cf_potassium_oxide = ($basicData[0]['potassium_oxide'] != '')?$basicData[0]['potassium_oxide']:1;
                                    $cf_magnesium = ($basicData[0]['magnesium'] != '')?$basicData[0]['magnesium']:1;

                                    $cd_nitrogen = $cf_nitrogen*$bd_nitrogen;
                                    $cd_phosphorus = $cf_phosphorus*$bd_phosphorus;
                                    $cd_potassium_oxide = $cf_potassium_oxide*$bd_potassium;
                                    $cd_magnesium = $cf_magnesium*$bd_magnesium;

                                    $basicData[0]['size'];
                                    $sizeinhectare = ($basicData[0]['size'] / 100);
                                    $total_amount = $formData['amount'] * $sizeinhectare;
                                    /// corrected demand ///
                                    /// cd = cf*bd
                                    
                                    $cal_amount = $formData['amount']; 
                                    if($formData['unit'] == 'g'){
                                    /// 1kg=1000g ///
                                    $cal_amount = $formData['amount']/1000;
                                    }else if($formData['unit'] == 'dl'){
                                    /// 1l=10dl ///
                                    $cal_amount = $formData['amount']/10;
                                    }

                                    $ferDetail = $this->user_model->getRecord('ac_fertilizer',array('fertilizer_id'=>$formData['fertilizer_id']));

                                    $new_nitrogen = $ferDetail[0]['nitrogen']*$cal_amount;
                                    $new_phosphorus = $ferDetail[0]['phosphorus']*$cal_amount;
                                    $new_potassium = $ferDetail[0]['potassium_oxide']*$cal_amount;
                                    $new_magnesium = $ferDetail[0]['magnesium']*$cal_amount;

                                    $preSql = "SELECT SUM(FA.`new_nitrogen`) as total_nitrigen,SUM(FA.`new_phosphorus`) as total_phosphorus, 
                                                SUM(FA.`new_potassium`) as total_potassium,SUM(FA.`new_magnesium`) as total_magnesium 
                                                FROM `ac_fertilizer_activity` FA WHERE FA.`culture_id` = '".$cultureid."' ";
                                    $preData = $this->user_model->getRecordSql($preSql);  
                                    if(!empty($preData)){
                                        $pre_nitrogen = $preData[0]['total_nitrigen'];
                                        $pre_phosphorus = $preData[0]['total_phosphorus'];
                                        $pre_potassium = $preData[0]['total_potassium'];
                                        $pre_magnesium = $preData[0]['total_magnesium'];
                                    }     
                                    
                                    $balance_nitrogen = $cd_nitrogen-($pre_nitrogen+$new_nitrogen);
                                    $balance_phosphorus = $cd_phosphorus-($pre_phosphorus+$new_phosphorus);
                                    $balance_potassium = $cd_potassium_oxide-($pre_potassium+$new_potassium);
                                    $balance_magnesium = $cd_magnesium-($pre_magnesium+$new_magnesium);

                                    /*$checkArr = array(
                                        'cd_nitrogen' => $cd_nitrogen,
                                        'cd_phosphorus' => $cd_phosphorus,
                                        'cd_potassium_oxide' => $cd_potassium_oxide,
                                        'cd_magnesium' => $cd_magnesium,
                                        'pre_nitrogen' => $pre_nitrogen,
                                        'pre_phosphorus' => $pre_phosphorus,
                                        'pre_potassium' => $pre_potassium,
                                        'pre_magnesium' => $pre_magnesium,
                                        'balance_nitrogen' => $balance_nitrogen,
                                        'balance_phosphorus' => $balance_phosphorus,
                                        'balance_potassium' => $balance_potassium,
                                        'balance_magnesium' => $balance_magnesium,
                                        'new_nitrogen' => $new_nitrogen,
                                        'new_phosphorus' => $new_phosphorus,
                                        'new_potassium' => $new_potassium,
                                        'new_magnesium' => $new_magnesium
                                    );*/
                                    if(is_float($balance_nitrogen)){
                                        $balance_nitrogen = number_format((float)$balance_nitrogen, 3, '.', '');
                                    }

                                    if(is_float($balance_phosphorus)){
                                        $balance_phosphorus = number_format((float)$balance_phosphorus, 3, '.', '');
                                    }

                                    if(is_float($balance_potassium)){
                                        $balance_potassium = number_format((float)$balance_potassium, 3, '.', '');
                                    }

                                    if(is_float($balance_magnesium)){
                                        $balance_magnesium = number_format((float)$balance_magnesium, 3, '.', '');
                                    }

                                    if($balance_nitrogen > 0){
                                        $new_balance_nitrogen = '-'.$balance_nitrogen;
                                    }else if($balance_nitrogen < 0){
                                        $new_balance_nitrogen = '+'.abs($balance_nitrogen);
                                    }else{
                                        $new_balance_nitrogen = $balance_nitrogen;
                                    }
                            
                                    if($balance_phosphorus > 0){
                                        $new_balance_phosphorus = '-'.$balance_phosphorus;
                                    }else if($balance_phosphorus < 0){
                                        $new_balance_phosphorus = '+'.abs($balance_phosphorus);
                                    }else{
                                        $new_balance_phosphorus = $balance_phosphorus;
                                    }
                            
                                    if($balance_potassium > 0){
                                        $new_balance_potassium = '-'.$balance_potassium;
                                    }else if($balance_potassium < 0){
                                        $new_balance_potassium = '+'.abs($balance_potassium);
                                    }else{
                                        $new_balance_potassium = $balance_potassium;
                                    }
                        
                                    if($balance_magnesium > 0){
                                        $new_balance_magnesium = '-'.$balance_magnesium;
                                    }else if($balance_magnesium < 0){
                                        $new_balance_magnesium = '+'.abs($balance_magnesium);
                                    }else{
                                        $new_balance_magnesium = $balance_magnesium;
                                    }
                                        
                                    $cultureData = array(
                                        'field_id' => $basicData[0]['ac_fieldId'],
                                        'culture_id' => $cultureid,
                                        'fertilizer_date' => $formData['fertilizer_date'],
                                        'fertilizer_id' => $this->post('fertilizer_id'),
                                        'amount' => $this->checkDecimalnumber($this->post('amount')),
                                        'unit' => $formData['unit'],
                                        'total_amount' => $this->checkDecimalnumber($total_amount),
                                        'machine_id' => $this->post('machine_id'),
                                        'person_name' => $this->post('person_name'),
                                        'comment' => $this->post('comment'),
                                        'user_id' =>  $this->api_token->ac_userId,
                                        'new_nitrogen' => $new_nitrogen,
                                        'new_phosphorus' => $new_phosphorus,
                                        'new_potassium' => $new_potassium,
                                        'new_magnesium' => $new_magnesium,
                                        'balance_nitrogen' => $new_balance_nitrogen,
                                        'balance_phosphorus' => $new_balance_phosphorus,
                                        'balance_potassium' => $new_balance_potassium,
                                        'balance_magnesium' => $new_balance_magnesium,
                                        'created' => date('Y-m-d H:i:s')
                                    );
                                    $other_activity_id = $this->user_model->saveRecord('ac_fertilizer_activity',$cultureData);
                                    if($other_activity_id > 0){
                                        if($basicData[0]['basic_nitrogen'] == '' && $basicData[0]['basic_phosphorus'] == '' && $basicData[0]['basic_potassium'] == '' && $basicData[0]['basic_magnesium'] == ''){
                                           $this->user_model->updateRecord('ac_culture',$cultureArr,array('ac_cultureId'=>$cultureid)); 
                                        }
                                    }    
                                }
                            }
                        }
                        /// End Other culture ////
                        $post = array('status' => TRUE,'message' => 'added_success');
                    }else{
                        $post = array('status' => FALSE,'message' => 'added_fail');
                    }
                }
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getAllFertilizerActivity_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $culture_id = $this->post('culture_id');

            $sql = "SELECT FA.*,FI.fertilizer_name FROM `ac_fertilizer_activity` FA 
                    LEFT JOIN ac_fertilizer FI ON FI.fertilizer_id = FA.`fertilizer_id`
                    WHERE FA.`culture_id` = '".$culture_id."'
                    ORDER BY FA.`fertilizer_date` DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                /*foreach($dataList as $val){
                    $activity_id = $val['fertilizer_activity_id'];
                    /*for($i=0;$i<6;$i++){
                        $arr = array();
                        $list = $this->user_model->getRecord('ac_fertilizer_history',array('type'=>$i,'fertilizer_activity_id'=>$activity_id));
                        if(!empty($list)){
                           if($i == 0){
                               $name = 'basic_demand';
                           }elseif($i == 1){
                               $name = "c_factor";
                           }elseif($i == 2){
                               $name = "c_demand";
                           }elseif($i == 3){
                               $name = "previous";
                           }elseif($i == 4){
                               $name = "new";
                           }elseif($i == 5){
                               $name = "balance";
                           }
                           $val[$name]['nitrogen'] = $list[0]['nitrogen'];
                           $val[$name]['phosphorus'] = $list[0]['phosphorus'];
                           $val[$name]['potassium_oxide'] = $list[0]['potassium_oxide'];
                           $val[$name]['magnesium'] = $list[0]['magnesium'];
                           
                          /*$arr = array(
                            'nitrogen' => $list[0]['nitrogen'],
                            'phosphorus' => $list[0]['phosphorus'],
                            'potassium_oxide' => $list[0]['potassium_oxide'],
                            'magnesium' => $list[0]['magnesium']
                          );
                        }
                        //$val[$name] = $arr;
                    }
                    $listArr[] = $val;
                    //$dataList = $val;
                }*/
                $post = array('status' => TRUE,'data'=>$dataList ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getFertilizerActivityDetail_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $activity_id = $this->post('fertilizer_activity_id');

            $sql = "SELECT FA.*,FI.fertilizer_name FROM `ac_fertilizer_activity` FA 
                    LEFT JOIN ac_fertilizer FI ON FI.fertilizer_id = FA.`fertilizer_id`
                    WHERE FA.`fertilizer_activity_id` = '".$activity_id."'
                    ORDER BY FA.`fertilizer_activity_id` DESC ";

            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                
                foreach($dataList as $val){
                    $activity_id = $val['fertilizer_activity_id'];
                    for($i=0;$i<6;$i++){
                        $arr = array();
                        /*$list = $this->user_model->getRecord('ac_fertilizer_history',array('type'=>$i,'fertilizer_activity_id'=>$activity_id));
                        if(!empty($list)){
                           if($i == 0){
                               $name = 'basic_demand';
                           }elseif($i == 1){
                               $name = "c_factor";
                           }elseif($i == 2){
                               $name = "c_demand";
                           }elseif($i == 3){
                               $name = "previous";
                           }elseif($i == 4){
                               $name = "new";
                           }elseif($i == 5){
                               $name = "balance";
                           }
                           $val[$name]['nitrogen'] = $list[0]['nitrogen'];
                           $val[$name]['phosphorus'] = $list[0]['phosphorus'];
                           $val[$name]['potassium_oxide'] = $list[0]['potassium_oxide'];
                           $val[$name]['magnesium'] = $list[0]['magnesium'];
                           */
                          /*$arr = array(
                            'nitrogen' => $list[0]['nitrogen'],
                            'phosphorus' => $list[0]['phosphorus'],
                            'potassium_oxide' => $list[0]['potassium_oxide'],
                            'magnesium' => $list[0]['magnesium']
                          );*/
                        //}
                        //$val[$name] = $arr;
                    }
                    $listArr[] = $val;
                    //$dataList = $val;
                }
                $post = array('status' => TRUE,'data'=>$listArr ,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function deleteFertilizerActivty_post()
    {
        try {
            $activity_id = $this->post('fertilizer_activity_id');
            if (!empty($activity_id)) {
                $id = $this->user_model->deleteRecord('ac_fertilizer_activity',array('fertilizer_activity_id' => $activity_id));
                if($id > 0){
                    $post = array('status' => TRUE,'message' => 'delete_success');
                    ///$this->user_model->deleteRecord('ac_fertilizer_history',array('fertilizer_activity_id' => $activity_id));
                }else{
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
			} else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }	
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /* End fertilizer activity */

    /* Export */
    public function getExportYear_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            ///$dataList = $this->user_model->getRecord('ac_pesticides',array());
            $sql = "SELECT MIN(CI.`start_date`) as min_date FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    WHERE FI.user_id = '".$user_id."' ";
            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                $year = date('Y',strtotime($dataList[0]['min_date']));
                $yearList = array();
                $currentYear = date('Y');
                for($i=$year;$i<=$currentYear;$i++){
                  $yearList[] = $i;
                }
                $post = array('status' => TRUE,'data'=>$yearList,'message' => '');
            }else{
                $post = array('status' => FALSE,'message' => 'no_data_found');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getCultureSheet_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $year = $this->post('year');
            $language = $this->post('language');
            
            if (!empty($language)) {
                $languageId = $language;
            } else {
                $languageId = 1;
            }
            $dataArr = array();
            $langData = $this->user_model->getRecord('ac_language',array('ac_languageId'=>$languageId));
            $symbol = $langData[0]['symbol'];
            /*
            $sql = "SELECT CI.*,FI.name as f_name,FI.size as f_size,CR.name_".$symbol." as crop_name 
                    FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    LEFT JOIN ac_crop CR ON CR.ac_cropId = CI.`crop_id`
                    WHERE FI.user_id = '".$user_id."' AND (YEAR(CI.`start_date`) = '".$year."' 
                    OR YEAR(CI.`end_date`) = '".$year."') order by FI.ac_fieldId DESC ";
            */
            $sql = "SELECT CI.*,FI.name as f_name,FI.size as f_size,FH.field_size,CR.name_".$symbol." as crop_name,
                    (SELECT count(*) FROM `ac_culture` CS WHERE CS.`field_id` = FI.ac_fieldId AND CS.`crop_id` = CI.`crop_id`) as total_serial, 
                    (SELECT count(*) FROM `ac_culture` CS WHERE CS.`field_id` = FI.ac_fieldId AND CS.`crop_id` = CI.`crop_id` AND CS.`start_date` <=  CI.`start_date`) as culture_serial
                    FROM `ac_culture` CI 
                    LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                    LEFT JOIN ac_crop CR ON CR.ac_cropId = CI.`crop_id`
                    LEFT JOIN ac_field_size_history FH ON FH.field_id =  CI.`field_id` AND FH.size_change_year = '".$year."'
                    WHERE FI.user_id = '".$user_id."' AND (YEAR(CI.`start_date`) = '".$year."' 
                    OR YEAR((CASE WHEN CI.`end_date` IS NULL THEN CI.`harvest_date` ELSE CI.`end_date` END)) = '".$year."') order by FI.ac_fieldId DESC ";        
            $dataList = $this->user_model->getRecordSql($sql);

            if (!empty($dataList)) {

                foreach($dataList as $data){
                    $plantArr = $soiltArr = $ferArr = array();
                    $culture_id = $data['ac_cultureId']; 
                    
                    $plantSql = "SELECT PS.pesticide_name,PT.type_name,PD.pests_name,MI.machine_name,PI.* FROM `ac_plant_protection` PI 
                                 LEFT JOIN ac_pesticides PS ON PS.pesticide_id = PI.`pesticide_id`
                                 LEFT JOIN ac_pesticide_type PT ON PT.type_id = PS.`type_id`
                                 LEFT JOIN ac_pests_disease PD ON PD.pests_id = PI.`pests_id`
                                 LEFT JOIN ac_machine MI ON MI.machine_id = PI.`machine_id`
                                 WHERE PI.`culture_id` = '".$culture_id."' ";
                    $plantList = $this->user_model->getRecordSql($plantSql);  
                    if(!empty($plantList)){
                        $plantArr = $plantList;
                    }
                    
                    $soilSql = "SELECT SC.soil_name,MI.machine_name,SI.* FROM `ac_soil_activity` SI 
                                LEFT JOIN ac_soil_cultivation SC ON SC.soil_id = SI.`soil_id`
                                LEFT JOIN ac_machine MI ON MI.machine_id = SI.`machine_id`
                                WHERE SI.`culture_id` = '".$culture_id."' ";
                    $soilList = $this->user_model->getRecordSql($soilSql);  
                    if(!empty($soilList)){
                        $soiltArr = $soilList;
                    }

                    /*$ferSql = "SELECT FI.*,FT.fertilizer_name,MI.machine_name,FH.nitrogen as new_nitrogen,FH.phosphorus as new_phosphorus,
                                FH.potassium_oxide as new_potassium,FH.magnesium as new_magnesium, CI.nitrogen as basic_nitrogen,
                                CI.phosphorus as basic_phosphorus,CI.potassium_oxide as basic_potassium,CI.magnesium as basic_magnesium,
                                FL.nitrogen as corr_nitrogen, FL.phosphorus as corr_phosphorus,FL.potassium_oxide as corr_potassium,
                                FL.magnesium as corr_magnesium,(SELECT `nitrogen` FROM `ac_fertilizer_history` WHERE `fertilizer_activity_id` = FI.`fertilizer_activity_id` AND `type` = 5) as balance_nitrogen,
                                (SELECT `phosphorus` FROM `ac_fertilizer_history` WHERE `fertilizer_activity_id` = FI.`fertilizer_activity_id` AND `type` = 5) as balance_phosphorus,
                                (SELECT `potassium_oxide` FROM `ac_fertilizer_history` WHERE `fertilizer_activity_id` = FI.`fertilizer_activity_id` AND `type` = 5) as balance_potassium,
                                (SELECT `magnesium` FROM `ac_fertilizer_history` WHERE `fertilizer_activity_id` = FI.`fertilizer_activity_id` AND `type` = 5) as balance_magnesium
                                FROM `ac_fertilizer_activity` FI 
                                LEFT JOIN ac_fertilizer FT ON FT.fertilizer_id = FI.`fertilizer_id`
                                LEFT JOIN ac_machine MI ON MI.machine_id = FI.`machine_id`
                                LEFT JOIN ac_fertilizer_history FH ON FH.fertilizer_activity_id = FI.`fertilizer_activity_id`
                                LEFT JOIN ac_culture CI ON CI.ac_cultureId = FI.`culture_id`
                                LEFT JOIN ac_field FL ON FL.ac_fieldId = FI.`field_id`
                                WHERE FI.`culture_id` = '".$culture_id."' AND FH.type = 4 ";
                     */ 
                    $ferSql = "SELECT FI.*,FT.fertilizer_name,MI.machine_name,CI.nitrogen as basic_nitrogen,
                                CI.phosphorus as basic_phosphorus,CI.potassium_oxide as basic_potassium,CI.magnesium as basic_magnesium,
                                FL.nitrogen as corr_nitrogen, FL.phosphorus as corr_phosphorus,FL.potassium_oxide as corr_potassium,
                                FL.magnesium as corr_magnesium
                                FROM `ac_fertilizer_activity` FI 
                                LEFT JOIN ac_fertilizer FT ON FT.fertilizer_id = FI.`fertilizer_id`
                                LEFT JOIN ac_machine MI ON MI.machine_id = FI.`machine_id`
                                LEFT JOIN ac_culture CI ON CI.ac_cultureId = FI.`culture_id`
                                LEFT JOIN ac_field FL ON FL.ac_fieldId = FI.`field_id`
                                WHERE FI.`culture_id` = '".$culture_id."' ORDER BY `fertilizer_date` DESC ";          
                    $ferList = $this->user_model->getRecordSql($ferSql);  
                    if(!empty($ferList)){
                        $ferArr = $ferList;
                    }            

                    $data['plan_activity'] = $plantArr;
                    $data['fertilizer_activity'] = $ferArr;
                    $data['soil_activity'] = $soiltArr;
                    $dataArr[] = $data;
                }
            }
            $file_path = $this->generateTabularPdf($dataArr,$symbol);
            if (!empty($file_path)) {
                $logdata = array(
                    'user_id'     => $user_id,
                    'file'        => $file_path,
                    'export_type' => $year,
                    'sheet_data_type' => 1,
                );
                $logadd = $this->db->insert('ac_export_log', $logdata);
                $post = array('status' => TRUE,'data'=>$file_path,'message' => '');
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            //$post = array('status' => TRUE,'data'=>$file_path,'result'=>$dataArr);
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    function checkDecimalnumber($strt){
         if(is_float($strt)){
            return round($strt,3);
         }else{
            return $strt;
         }
    }

    public function generateTabularPdf($export_data,$symbol)
    {
        $user_data = $this->api_token;
        $user_id = $user_data->ac_userId;

        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Agricontrol');
        $pdf->SetTitle('Export Report');
        $pdf->SetSubject('Agricontrol');
        $pdf->SetKeywords('Agricontrol');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 0, 0), array(255, 255, 255));
        $pdf->footerText = ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(20, 10, 20, true);
        $pdf->SetHeaderMargin(12);
        $pdf->SetFooterMargin(18);
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setCellPadding(0);

        ///$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //$pdf->AddPage();
        //$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
        
        $field = 'Field';
        $size = 'Size';
        $plant_variety = 'Plant Variety';
        $start = 'Start';
        $end = 'End';
        $date = 'Date';
        $plant_protection = 'Plant Protection';
        $product = 'Product';
        $product_type = 'Type';
        $amount = 'Amount';
        $person = 'Person';
        $machine = 'Machine';
        $comment = 'Comment';
        $days = 'Days';
        $pests_diseases = 'Pests/Diseases';
        $waiting_period = 'Waiting Period';
        $fertilizing = 'Fertilizing';
        $fertilizer = 'Fertilizer';
        $total = 'Total';
        $activity = 'Activity';
        $no_data = "No Data Found.";
        $seed_volume = "Seed Volume";
        $harvest_volume = "Harvest Volume";
        $culture_size = "Culture Size";
        $harvest_date = "Harvest Date";
        
        if (!empty($symbol) ) {
			$file = $symbol;
		}else{
            $file = 'en';
        }
        
        $filename = $file.".json";
        $url = LOCAL_JSON_FILE_PATH.$filename;
        $str = file_get_contents($url);//get contents of your json file and store it in a string
        $languageArr = json_decode($str, true);
        if(!empty($languageArr)){
            $field = $languageArr['field_name'];
            $size = $languageArr['field_size'];
            $plant_variety = $languageArr['plant_variety'];
            $start = $languageArr['start_date'];
            $end = $languageArr['end_date'];
            $date = $languageArr['date'];
            $plant_protection = $languageArr['plant_protection'];
            $product = $languageArr['product'];
            $product_type = $languageArr['type'];
            $amount = $languageArr['amount'];
            $person = $languageArr['person'];
            $machine = $languageArr['machine'];
            $comment = $languageArr['comment'];
            $days = $languageArr['Days'];
            $pests_diseases = $languageArr['pests_diaseses'];
            $waiting_period = $languageArr['waiting_period'];
            $fertilizing = $languageArr['fertilizing'];
            $fertilizer = $languageArr['fertilizers'];
            $total = $languageArr['total'];
            $activity = $languageArr['activity'];
            $no_data = $languageArr['no_data_found'];
            $seed_volume = $languageArr['seed_volume'];
            $harvest_volume = $languageArr['harvest_volume'];
            $culture_size = $languageArr['culture_size'];
            $harvest_date = $languageArr['harvest_date'];
        }

        $i = 0;
        foreach ($export_data as $value) {

            if($value['field_size'] != ''){
                $f_size = $value['field_size'];
            }else{
                $f_size =$value['f_size'];
            }
            
            if ($i < (count($export_data)) ) {
                //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->AddPage();
            }
            $html = '';
            if($i == 0){
                $html .= '
                        <img style="" height="50" width="50" src="'.EXPORT_LOGO_PATH.'favicon.png" />
                        <b style="color:#606060;font-size:22px;">Agricontrol </b>';
            }
                    //foreach ($export_data as $value) {
            $i++;    
                $totalNitrogen = $totalPhosphorus = $totalPotassium = $totalMagnesium = 0;
                $serial = ''; $end_date = '';
                if($value['total_serial']>1){ $serial = '('.$value['culture_serial'].')';}
                if($value['end_date'] != '') { $end_date = date('d.m.Y',strtotime($value['end_date'])); }
            $html .= '<style>
                        table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                        border: 1px solid #666666;
                        font-size: 12px;
                        }
                        .trborder{
                            border: 1px solid #666666;
                        }
                        tr.noBorder td {
                        border: 0 !important;
                        font-size: 12px;
                        }
                        .footertext{
                            position: absolute;
                            bottom: 0;
                            left: 0;
                            right: 0;
                            margin: 0 auto;
                        }
                    </style>
                    
                    <br/>
                    <h3 style="padding-left: 12px; font-size: 20px;color:#404040; ">' . ucfirst($value['crop_name']). $serial.  '</h3>
                    &nbsp;

                    <table style="font-size: 12px; font-color: #262626; font-weight: 500;" cellpadding = "2" cellspacing="0">
                        <tr>
                            <td style="font-size: 13px; color:#262626;">'.$field.': '.ucfirst($value['f_name']). '</td>
                            <td style="font-size: 12px; color:#262626;">'.$culture_size.': '.$value['size']. 'a </td>
                            <td style="font-size: 12px; color:#262626;">'.$seed_volume.': '.$value['seed_volume'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; color:#262626;">'.$size.': '.$f_size. 'a </td> 
                            <td style="font-size: 12px; color:#262626;">'.$start.': '.date('d.m.Y',strtotime($value['start_date'])). '</td>
                            <td style="font-size: 12px; color:#262626;">'.$harvest_volume.': '.$value['harvest_volume'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; color:#262626;">'.$plant_variety.': '.$value['plantVariety']. '</td>
                            <td style="font-size: 12px; color:#262626;">'.$harvest_date.': '.date('d.m.Y',strtotime($value['harvest_date'])).'</td>
                            <td style="font-size: 12px; color:#262626;">'.$end.': '.$end_date.'</td>
                            </tr>
                    </table>
                    <br /><br />';

                $html .= '<b style="color:#404040;font-size:14px"> '.$plant_protection.' </b>
                        <br /><br />
                        <table nobr="true" class="fieldTable" cellpadding="2" cellspacing="0" style="" >
                            <tr class="noBorder">
                                <th style="width: 10%; background-color: #66bd66; color: #fff; font-size: 13px;">'.$date.'</th>
                                <th style="width: 10%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$product.'</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$product_type.'</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$amount.'</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">WP*('.$days.')</th>
                                <th style="width: 15%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$pests_diseases.'</th>
                                <th style="width: 10%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$person.'</th>
                                <th style="width: 12%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$machine.'</th>
                                <th style="width: 19%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$comment.'</th>
                            </tr>';

                if(!empty($value['plan_activity'])){          
                    foreach ($value['plan_activity'] as  $plant) {
                    $html .= '<tr style="font-color: #343030;">
                                <td style="width:10%; font-size:11px; color:#333333;">'.date('d.m.Y',strtotime($plant['plant_date'])) . '</td>
                                <td style="width:10%; font-size:11px; color:#333333;">' . $plant["pesticide_name"] . '</td>
                                <td style="width:8%; font-size:11px; color:#333333;">' . $plant["type_name"] . '</td>
                                <td style="width:8%; font-size:11px; color:#333333;text-align: right;">' . $this->checkDecimalnumber($plant["amount"]).' '.$plant['unit'].' </td>
                                <td style="width:7%; font-size:11px; color:#333333;text-align: right;">' . str_replace("days","",$plant["waiting_period"]). ' </td>
                                <td style="width:15%; font-size:11px; color:#333333;">' . $plant["pests_name"] . '</td>
                                <td style="width:8%; font-size:11px; color:#333333;">' . $plant["person_name"] . '</td>
                                <td style="width:14%; font-size:11px; color:#333333;">' . $plant["machine_name"] . '</td>
                                <td style="width:20%; font-size:11px; color:#333333;">' . $plant["comment"] . '</td>
                            </tr>';
                    }
                }else{
                    $html .= '<tr style="font-color: #343030;">
                                <td style="text-align: center; font-size: 12px;" colspan="9">'.$no_data.'</td>
                            </tr>';
                    //break;
                }
                $html .= '</table><span style="color:#808080;font-size:11px;">*WP = '.$waiting_period.'</span><br /><br />';

                $html .= '<b style="color:#404040;font-size:14px"> '.$fertilizing.' </b>
                        <br /><br />
                        <table nobr="true" class="" cellpadding="2" cellspacing="0" style="font-size: 12px;">
                            <tr class="noBorder">
                                <th style="width: 10%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$date.'</th>
                                <th style="width: 9%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$fertilizer.'</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$amount.'</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$total.'</th>
                                <th style="width: 6.5%;background-color: #66bd66; color: #fff; font-size: 13px;">N</th>
                                <th style="width: 6.5%;background-color: #66bd66; color: #fff; font-size: 13px;">P<sub>2</sub>O<sub>5</sub></th>
                                <th style="width: 6.5%;background-color: #66bd66; color: #fff; font-size: 13px;">K<sub>2</sub>O</th>
                                <th style="width: 6.5%;background-color: #66bd66; color: #fff; font-size: 13px;">Mg</th>
                                <th style="width: 8%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$person.'</th>
                                <th style="width: 14%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$machine.'</th>
                                <th style="width: 17%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$comment.'</th>
                            </tr>';

                if(!empty($value['fertilizer_activity'])){   
                    $fertilizVal = $value['fertilizer_activity'];       
                    foreach ($value['fertilizer_activity'] as  $fval) {
                    $html .= '<tr style="font-color: #343030;">
                                <td class="trborder" style="width: 10%;font-size:11px; color:#333333;">'.date('d.m.Y',strtotime($fval['fertilizer_date'])) . '</td>
                                <td class="trborder" style="width: 8%;font-size:11px; color:#333333;">'.$fval["fertilizer_name"] . '</td>
                                <td align="right" class="trborder" style="padding:0px;width: 8%;font-size:11px; color:#333333;">' . $this->checkDecimalnumber($fval["amount"]).' '.$fval['unit'].'/ha</td>
                                <td align="right" class="trborder" style="padding:0px;width: 8%;font-size:11px; color:#333333;">' .$this->checkDecimalnumber($fval["total_amount"]).' '.$fval['unit'].'</td>
                                <td align="right" class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;">' . $this->checkDecimalnumber($fval["new_nitrogen"]) . ' </td>
                                <td align="right" class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;">' . $this->checkDecimalnumber($fval["new_phosphorus"]) . '</td>
                                <td align="right" class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;">' . $this->checkDecimalnumber($fval["new_potassium"]) . '</td>
                                <td align="right" class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;">' . $this->checkDecimalnumber($fval["new_magnesium"]) . '</td>
                                <td class="trborder" style="width: 8%;font-size:11px; color:#333333;">' .$fval["person_name"] . '</td>
                                <td class="trborder" style="width: 14%;font-size:11px; color:#333333;">' .$fval["machine_name"] . '</td>
                                <td class="trborder" style="width: 18%;font-size:11px; color:#333333;">' .$fval["comment"] . '</td>
                            </tr>';
                        $totalNitrogen = $totalNitrogen+$fval["new_nitrogen"];  
                        $totalPhosphorus = $totalPhosphorus+$fval["new_phosphorus"];  
                        $totalPotassium = $totalPotassium+$fval["new_potassium"];
                        $totalMagnesium = $totalMagnesium+$fval["new_magnesium"];
                    }
                    $corrNitrogen = $fertilizVal[0]["basic_nitrogen"]*$fertilizVal[0]["corr_nitrogen"];
                    $corrPhosphorus = $fertilizVal[0]["basic_phosphorus"]*$fertilizVal[0]["corr_phosphorus"];
                    $corrPotassium = $fertilizVal[0]["basic_potassium"]*$fertilizVal[0]["corr_potassium"];
                    $corrMagnesium = $fertilizVal[0]["basic_magnesium"]*$fertilizVal[0]["corr_magnesium"];

                    $balanceNitrogen = 0;
                    if($corrNitrogen > 0){
                        $balanceNitrogen = ($corrNitrogen-$totalNitrogen);
                    }

                    $html .= '<tr>
                                <td class="noneBorder" style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="4">Basic requirement</td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;border-top:2px solid black;">' . $this->checkDecimalnumber($fertilizVal[0]["basic_nitrogen"]) . '</td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;border-top:2px solid black;">' . $this->checkDecimalnumber($fertilizVal[0]["basic_phosphorus"]) . '</td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;border-top:2px solid black;">' . $this->checkDecimalnumber($fertilizVal[0]["basic_potassium"]) . '</td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;border-top:2px solid black;">' . $this->checkDecimalnumber($fertilizVal[0]["basic_magnesium"]) . '</td>
                                <td style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="3"></td>
                            </tr>
                            <tr>
                                <td style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="4">Corrected</td>
                                <td class="trborder" style="padding:0px;text-align: right;width: 6.5%;font-size:11px; color:#333333;">' .$this->checkDecimalnumber($corrNitrogen). '</td>
                                <td class="trborder" style="padding:0px;text-align: right;width: 6.5%;font-size:11px; color:#333333;">' .$this->checkDecimalnumber($corrPhosphorus). '</td>
                                <td class="trborder" style="padding:0px;text-align: right;width: 6.5%;font-size:11px; color:#333333;">' .$this->checkDecimalnumber($corrPotassium). '</td>
                                <td class="trborder" style="padding:0px;text-align: right;width: 6.5%;font-size:11px; color:#333333;">' .$this->checkDecimalnumber($corrMagnesium) . '</td>
                                <td style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="3"></td>
                            </tr>
                            <tr>
                                <td style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="4"><b>Balance</b></td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;"><b>' .$balanceNitrogen. '</b></td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;"><b>' . $this->checkDecimalnumber(($corrPhosphorus-$totalPhosphorus)) . '</b></td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;"><b>' . $this->checkDecimalnumber(($corrPotassium-$totalPotassium)) . '</b></td>
                                <td class="trborder" style="padding:0px;width: 6.5%;font-size:11px; color:#333333;text-align: right;"><b>' . $this->checkDecimalnumber(($corrMagnesium-$totalMagnesium)) . '</b></td>
                                <td style="border: none;text-align: right;font-size:11px; color:#333333;" colspan="3"></td>
                            </tr>';
                }else{
                    $html .= '<tr style="font-color: #343030;">
                                <td style="text-align: center; font-size: 12px;" colspan="11">'.$no_data.'</td>
                            </tr>';
                    //break;
                }
                $html .= '</table><br /><br />';

                $html .= '<b style="color:#404040;font-size:14px"> Soil Cultivation </b>
                        <br /><br />
                        <table nobr="true" class="fieldTable" cellpadding="2" cellspacing="0" style="font-size: 12px;">
                            <tr class="noBorder">
                                <th style="width: 10%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$date.'</th>
                                <th style="width: 15%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$activity.'</th>
                                <th style="width: 15%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$person.'</th>
                                <th style="width: 20%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$machine.'</th>
                                <th style="width: 40%;background-color: #66bd66; color: #fff; font-size: 13px;">'.$comment.'</th>
                            </tr>';
                $j=0;
                if(!empty($value['soil_activity'])){  
                            
                    foreach ($value['soil_activity'] as  $soil) {
                        
                    $html .= '<tr style="font-color: #343030;">
                                <td style="width: 10%;font-size:11px; color:#333333;">'.date('d.m.Y',strtotime($soil['soil_date'])) . '</td>
                                <td style="width: 15%;font-size:11px; color:#333333;">' . $soil["soil_name"] . '</td>
                                <td style="width: 15%;font-size:11px; color:#333333;">' . $soil["person_name"] . '</td>
                                <td style="width: 20%;font-size:11px; color:#333333;">' . $soil["machine_name"] . '</td>
                                <td style="width: 40%;font-size:11px; color:#333333;">' . $soil["comment"] . '</td>
                            </tr>';
                    $j++;
                    }
                }else{
                    $html .= '<tr style="font-color: #343030;">
                                <td style="text-align: center; font-size: 12px;" colspan="5">'.$no_data.'</td>
                            </tr>';
                    //break;
                }
                $html .= '</table>';

                /*if ($value < (count($export_data) - 1)) {
                    $html .= '<br /><br /><br />';
                    $pdf->getAliasNumPage();
                    $pdf->deletePage(6);
                }*/
                /*if ($i < (count($export_data)) ) {
                    $html .= '<br /><br /><br />';
                    $html .= '<br pagebreak="true"/>';
                }*/
            //}
            
            $footerText = '<span class = "footerText">'. ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . '</span>';
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            // $pdf->writeHTMLCell(0, 0, '', '', $footerText, 0, 1, 0, true, 'C', true);
            
        }
        ob_end_clean();
        $current_time_stamp = time();
        $file_name = $current_time_stamp . '' . $user_id . '.pdf';
        $pdf->Output(FCPATH . 'uploads/exports/' . $file_name, 'F');

        //return 'http://localhost/agricontrol/uploads/exports/' . $file_name;
        //return 'https://agricontrol.app/dashboard/uploads/exports/' . $file_name;
        return EXPORT_PATH. $file_name;
    }

    public function getCropByYearExport_post(){
        try {
            $user_id = $this->api_token->ac_userId;
            $year = $this->post('year');
            $language = $this->post('language');
            
            if (!empty($language)) {
                $languageId = $language;
            } else {
                $languageId = 1;
            }

            if (!empty($year)) {
                $current_year_from = date($year . '-01-01');
                $current_year_to = date($year . '-12-31');
            } else {
                $current_year_from = date('Y-01-01');
                $current_year_to = date('Y-12-31');
            }
            
            $dataArr = array();
            $langData = $this->user_model->getRecord('ac_language',array('ac_languageId'=>$languageId));
            $symbol = $langData[0]['symbol'];

            $sql = "select sum(accu.size) as culture_size, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acc.color
            from ac_field acf 
            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId
            Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id   
            where acf.user_id = ".$user_id." AND acf.archieve = 0 AND ((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$current_year_from."' 
            AND (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$current_year_to."') GROUP By acc.ac_cropId order by culture_size DESC";
        
            $dataArr =array();
            $dataList = $this->user_model->getRecordSql($sql);
            if(!empty($dataList)){
                foreach($dataList as $data){
                    $dataArr[] = $data;
                }
            }
            ///$post = array('status' => TRUE,'data'=>$dataList,'message' => '');
            $file_path = $this->generateCropPerYearPdf($dataArr,$symbol,$year);
            if (!empty($file_path)) {
                $logdata = array(
                    'user_id'     => $user_id,
                    'file'        => $file_path,
                    'export_type' => $year,
                    'sheet_data_type' => 2,
                );
                $logadd = $this->db->insert('ac_export_log', $logdata);
                $post = array('status' => TRUE,'data'=>$file_path,'message' => '');
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            //$post = array('status' => TRUE,'data'=>$file_path,'result'=>$dataArr);
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function generateCropPerYearPdf($export_data,$symbol,$export_year)
    {
        $user_data = $this->api_token;
        $user_id = $user_data->ac_userId;

        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Agricontrol');
        $pdf->SetTitle('Export Report');
        $pdf->SetSubject('Agricontrol');
        $pdf->SetKeywords('Agricontrol');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 0, 0), array(255, 255, 255));
        $pdf->footerText = ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        ///$pdf->SetMargins(18, 36, 18, true);
        $pdf->SetMargins(20, 10, 20, true);
        $pdf->SetHeaderMargin(12);
        $pdf->SetFooterMargin(18);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setCellPadding(0);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->AddPage();
        // $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        $title = "Overall Cultivated Crops Per Year";
        $year = 'Year';
        $generated = 'Generated';
        $name = 'Name';
        $address = 'Address';
        $location = 'Location';
        $crop = 'Crop';
        $cultivated_are = 'Cultivated area';
        $no_data = "No Data Found.";
            
        if (!empty($symbol) ) {
			$file = $symbol;
		}else{
            $file = 'en';
        }
        
        $filename = $file.".json";
        $url = LOCAL_JSON_FILE_PATH.$filename;
        $str = file_get_contents($url);//get contents of your json file and store it in a string
        $languageArr = json_decode($str, true);
        if(!empty($languageArr)){
            //foreach($languageArr as $langKey=> $langData){
                $title = $languageArr['overall_crops_per_year'];
                $year = $languageArr['year'];
                $generated = $languageArr['generated'];
                $name = $languageArr['name'];
                $address = $languageArr['address'];
                $location = $languageArr['location'];
                $crop = $languageArr['crop'];
                $cultivated_are = $languageArr['cultivated_are'];
                $no_data = $languageArr['no_data_found'];
            //}
        }
        $html = '<html>
                <head></head>
                <img style="" height="50" width="50" src="'.EXPORT_LOGO_PATH.'favicon.png" />
                        <b style="color:#606060;font-size:22px;">Agricontrol </b>';
        $html .= '<style>
                    table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                    border: 1px solid #666666;
                    font-size: 12px;
                    }
                    .trborder{
                        border: 1px solid #666666;
                    }
                    tr.noBorder td {
                    border: 0 !important;
                    font-size: 12px;
                    }
                    .footertext{
                        position: absolute;
                        bottom: 0;
                        left: 0;
                        right: 0;
                        margin: 0 auto;
                    }
                </style>
                <body>
                <br/>
                <h3 style="padding-left: 10px; font-size: 16px; font-weight: bold;">' . $title . '</h3>
                &nbsp;

                <table style="font-size:12px; font-color:#343030; font-weight:500;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="font-size: 12px;">'.$year.': ' . $export_year . '</td>
                        <td style="font-size: 12px;">'.$name.': ' . ucfirst($user_data->name) . ' </td> 
                    </tr>
                    
                    <tr>
                        <td style="font-size: 12px;">'.$generated.': ' . date("d.m.Y") . '</td>
                        <td style="font-size: 12px;">'.$address.': ' . $user_data->street_name . '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">'.$location.': ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country) . '</td>
                    </tr>
                </table>
                <br /> <br /> <br />';
           
        $html .= '
                    <table nobr="true" class="fieldTable" cellpadding="7" cellspacing="0" style="font-size: 12px;">
                        <tr class="noBorder">
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$crop.'</th>
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$cultivated_are.'</th>
                        </tr>';
            if(!empty($export_data)){    
                        
                foreach ($export_data as $value) {
                    
                $html .= '<tr style="font-color: #343030;">
                            <td style="font-size:11px; color:#333333;">'.$value['crop_name'] . '</td>
                            <td style="font-size:11px; color:#333333;">' . $value["culture_size"] .'a </td>
                        </tr>';
                }
            }else{
                $html .= '<tr style="font-color: #343030;">
                            <td style="text-align: center; font-size: 12px;" colspan="5">'.$no_data.'</td>
                        </tr>';
                //break;
            }
        $html .= '</table>';

        $html .='</body></html>';
        $footerText = '<span class = "footerText">' . ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . '</span>';
        

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->writeHTMLCell(0, 0, '', '', $footerText, 0, 1, 0, true, 'C', true);

        ob_end_clean();
        $current_time_stamp = time();
        $file_name = $current_time_stamp . '' . $user_id . '.pdf';
        $pdf->Output(FCPATH . 'uploads/exports/' . $file_name, 'F');

        //return 'http://localhost/agricontrol/uploads/exports/' . $file_name;
        //return 'https://agricontrol.app/dashboard/uploads/exports/' . $file_name;
        return EXPORT_PATH.$file_name;
    }
    /* End export */

    /**
     * Get Fertilizer Years Data
    */
    public function getFertilizerYears_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            if ($user_id) {
                $sql = "SELECT DISTINCT(YEAR(`fertilizer_date`)) as years FROM `ac_fertilizer_activity` 
                        WHERE `user_id` = '".$user_id."' ORDER BY years DESC; ";
                $dataList = $this->user_model->getRecordSql($sql);      
                if (!empty($dataList)) {
                    $post = array('status' => TRUE,'data' => $dataList,'message' => 'Dashboard Data');
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'Dashboard Data');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function getFertilizerByYearExport_post(){
        try {
            $user_id = $this->api_token->ac_userId;
            $year = $this->post('year');
            $language = $this->post('language');
            
            if (!empty($language)) {
                $languageId = $language;
            } else {
                $languageId = 1;
            }
            
            $dataArr = array();
            $langData = $this->user_model->getRecord('ac_language',array('ac_languageId'=>$languageId));
            $symbol = $langData[0]['symbol'];
            
            //$sql = "SELECT * FROM `ac_fertilizer` WHERE `is_delete` = 0";
            $sql = "SELECT DISTINCT(FA.`fertilizer_id`),FI.* FROM `ac_fertilizer_activity` FA 
            LEFT JOIN ac_fertilizer FI ON FI.fertilizer_id = FA.`fertilizer_id`
            WHERE FA.`user_id` = '".$user_id."' ";
            $dataList = $this->user_model->getRecordSql($sql);

            $dataArr =array();
            if(!empty($dataList)){
                foreach($dataList as $data){
                    $fertilizer_id = $data['fertilizer_id'];

                    $fertilizer_id;
                    $query = "SELECT FI.name,CI.size,FA.`fertilizer_date`,FA.`amount`,FA.`unit`,FA.`total_amount` 
                              FROM `ac_fertilizer_activity` FA 
                              LEFT JOIN ac_field FI ON FI.ac_fieldId = FA.field_id
                              LEFT JOIN ac_culture CI ON CI.ac_cultureId = FA.`culture_id`
                              WHERE FA.`user_id` = '".$user_id."' AND FA.`fertilizer_id` = '".$fertilizer_id."' AND 
                              YEAR(FA.`fertilizer_date`) = '".$year."' ";
                    $result = $this->user_model->getRecordSql($query);          

                    $dataArr[] = array(
                        'fertilizer_id' => $data['fertilizer_id'],
                        'fertilizer_name' => $data['fertilizer_name'],
                        'unit' => $data['unit'],
                        'fertilizing_list' => $result
                    );
                }
            }
            //$post = array('status' => TRUE,'data'=>$dataArr,'message' => '');
            $file_path = $this->generateFertilizerPerYearPdf($dataArr,$symbol,$year);
            if (!empty($file_path)) {
                $logdata = array(
                    'user_id'     => $user_id,
                    'file'        => $file_path,
                    'export_type' => $year,
                    'sheet_data_type' => 3,
                );
                $logadd = $this->db->insert('ac_export_log', $logdata);
                $post = array('status' => TRUE,'data'=>$file_path,'message' => '');
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            //$post = array('status' => TRUE,'data'=>$file_path,'result'=>$dataArr);
            $this->response($post, REST_Controller::HTTP_OK);

        } catch(Exception $e) {
            $this->response(['status' => FALSE, 'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function generateFertilizerPerYearPdf($export_data,$symbol,$export_year)
    {
        $user_data = $this->api_token;
        $user_id = $user_data->ac_userId;

        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Agricontrol');
        $pdf->SetTitle('Export Report');
        $pdf->SetSubject('Agricontrol');
        $pdf->SetKeywords('Agricontrol');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 0, 0), array(255, 255, 255));
        $pdf->footerText = ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        ///$pdf->SetMargins(18, 36, 18, true);
        $pdf->SetMargins(20, 10, 20, true);
        $pdf->SetHeaderMargin(12);
        $pdf->SetFooterMargin(18);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setCellPadding(0);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->AddPage();
        // $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        $title = "Fertilizer Export";
        $year = 'Year';
        $generated = 'Generated';
        $name = 'Name';
        $address = 'Address';
        $location = 'Location';
        
        $field = 'Field';
        $culture = 'Culture';
        $date = 'Date';
        $amount_per_ha = 'Amount/ha';
        $amount_culture = 'Amount Culture';
        $total_fertilizer = 'Total Fertilizer/year';
        $no_data = "No Data Found.";
            
        if (!empty($symbol) ) {
			$file = $symbol;
		}else{
            $file = 'en';
        }
        
        $filename = $file.".json";
        $url = LOCAL_JSON_FILE_PATH.$filename;
        $str = file_get_contents($url);//get contents of your json file and store it in a string
        $languageArr = json_decode($str, true);
        if(!empty($languageArr)){
            $title = $languageArr['fertilizer_export'];
            $year = $languageArr['year'];
            $generated = $languageArr['generated'];
            $name = $languageArr['name'];
            $address = $languageArr['address'];
            $location = $languageArr['location'];
            
            $field = $languageArr['field_name'];
            $culture = $languageArr['culture_size'];
            $date = $languageArr['date'];
            $amount_per_ha = $languageArr['amount_per_ha'];
            $amount_culture = $languageArr['amount_culture'];
            $total_fertilizer = $languageArr['total_fertilizer'].'/'.$year;

            $no_data = $languageArr['no_data_found'];
                
        }
        $html = '<html>
                <head></head>
                <img style="" height="50" width="50" src="'.EXPORT_LOGO_PATH.'favicon.png" />
                        <b style="color:#606060;font-size:22px;">Agricontrol </b>';
        $html .= '<style>
                    table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                    border: 1px solid #666666;
                    font-size: 12px;
                    }
                    .trborder{
                        border: 1px solid #666666;
                    }
                    tr.noBorder td {
                    border: 0 !important;
                    font-size: 12px;
                    }
                    .footertext{
                        position: absolute;
                        bottom: 0;
                        left: 0;
                        right: 0;
                        margin: 0 auto;
                    }
                </style>
                <body>
                <br/>
                <h3 style="padding-left: 10px; font-size: 16px; font-weight: bold;">' . $title . '</h3>
                &nbsp;

                <table style="font-size:12px; font-color:#343030; font-weight:500;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="font-size: 12px;">'.$year.': ' . $export_year . '</td>
                        <td style="font-size: 12px;">'.$name.': ' . ucfirst($user_data->name) . ' </td> 
                    </tr>
                    
                    <tr>
                        <td style="font-size: 12px;">'.$generated.': ' . date("d.m.Y") . '</td>
                        <td style="font-size: 12px;">'.$address.': ' . $user_data->street_name . '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">'.$location.': ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country) . '</td>
                    </tr>
                </table>
                <br /> <br /> <br />';
        
        if(!empty($export_data)){    
                
            foreach ($export_data as $value) {

                $html .= '<div nobr="true"><b style="color:#404040;font-size:14px"> '.$value['fertilizer_name'].' </b>
                    <br /><br />
                    <table nobr="true" class="" cellpadding="5" cellspacing="0" style="font-size: 12px;">
                        <tr class="noBorder">
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$field.'</th>
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$culture.'</th>
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$date.'</th>
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$amount_per_ha.'</th>
                            <th style="background-color: #66bd66; color: #fff; font-size: 13px;">'.$amount_culture.'</th>
                        </tr>';
                    
                    $fertilizing_list = $value['fertilizing_list'];    
                    if(!empty($fertilizing_list)){    
                        $total_unit = 0;        
                        foreach ($fertilizing_list as $f_data) {
                            
                        $html .= '<tr style="font-color: #343030;">
                                    <td class="trborder" style="font-size:11px; color:#333333;">'.$f_data['name'] . '</td>
                                    <td class="trborder" style="font-size:11px; color:#333333;">' . $f_data["size"] .'a </td>
                                    <td class="trborder" style="font-size:11px; color:#333333;">'.date('d.M.Y',strtotime($f_data['fertilizer_date'])). '</td>
                                    <td class="trborder" style="font-size:11px; color:#333333;">' . $f_data["amount"] .'/'.$f_data['unit'].'</td>
                                    <td class="trborder" style="font-size:11px; color:#333333;">'.$f_data['total_amount'] .' '.$f_data['unit']. '</td>
                                </tr>';
                            $total_unit = $f_data['total_amount']+$total_unit;    
                        }
                        $html .= '<tr >
                                    <td style="border:none;text-align: right; font-size: 12px;" colspan="5"><b>'.$total_fertilizer.' : '.$total_unit.' '.$value['unit'].'</b></td>
                                 </tr>';
                        
                    }else{
                        $html .= '<tr style="font-color: #343030;">
                                    <td style="text-align: center; font-size: 12px;" colspan="5">'.$no_data.'</td>
                                 </tr>';
                        //break;
                    }
                $html .= '</table><br/></div>';
            }
        }        

        $html .='</body></html>';
        $footerText = '<span class = "footerText">' . ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . '</span>';

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->writeHTMLCell(0, 0, '', '', $footerText, 0, 1, 0, true, 'C', true);

        ob_end_clean();
        $current_time_stamp = time();
        $file_name = $current_time_stamp . '' . $user_id . '.pdf';
        $pdf->Output(FCPATH . 'uploads/exports/' . $file_name, 'F');

        //return 'http://localhost/agricontrol/uploads/exports/' . $file_name;
        //return 'https://agricontrol.app/dashboard/uploads/exports/' . $file_name;
        return EXPORT_PATH.$file_name;
    }
    /* End export */
    
    
}
?>