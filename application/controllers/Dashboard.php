<?php
ini_set('memory_limit', '1024M');
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Dashboard extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('dashboards');
        $this->load->model('commons');
        $this->load->model('cropmasters');
        $this->load->library('Pdf');
    }

    /**
     * Register field 
     */
    public function addField_post()
    {
        try {
            $formData = array(
                'fieldId' => $this->post('fieldId'),
                'name' => $this->post('name'),
                'size' => $this->post('size'),
                'echo' => $this->post('echo'),
                'echoSize' => $this->post('echoSize'),
                'address' => $this->post('address'),
                'city' => $this->post('city'),
                'zipcode' => $this->post('zipcode'),
                'notes' => $this->post('notes'),
                'nitrogen' => $this->post('nitrogen'),
                'phosphorus' => $this->post('phosphorus'),
                'potassium_oxide' => $this->post('potassium_oxide'),
                'magnesium' => $this->post('magnesium'),
                'ph_value' => $this->post('ph_value'),
                'soil_sample_date' => $this->post('soil_sample_date'),
                'old_field_size' => $this->post('old_field_size'),
                'old_echo_size' => $this->post('old_echo_size'),
                'size_change_year' => $this->post('size_change_year'),
            );

            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('name', 'Field Name', 'required|trim|max_length[50]');
            $this->form_validation->set_rules('size', 'Field Size', 'required|trim|numeric|max_length[10]|greater_than[0]');
            if (!empty($formData['echo'])) {
                $this->form_validation->set_rules('echoSize', 'Eco Size', 'required|trim|numeric|max_length[10]|greater_than[0]');
            }
            $this->form_validation->set_rules('address', 'Address', 'trim|max_length[300]');
            $this->form_validation->set_rules('city', 'City', 'trim|max_length[100]');
            $this->form_validation->set_rules('zipcode', 'Zip Code', 'trim|max_length[10]');
            $this->form_validation->set_rules('notes', 'Notes', 'trim|max_length[500]');
            $this->form_validation->set_rules('nitrogen', 'N2', 'trim|max_length[10]');
            $this->form_validation->set_rules('phosphorus', 'P205', 'trim|max_length[10]');
            $this->form_validation->set_rules('potassium_oxide', 'K2O', 'trim|max_length[10]');
            $this->form_validation->set_rules('magnesium', 'Mg', 'trim|max_length[10]');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => validation_errors());
            }else{

                $lwrStr = strtolower($this->post('name'));
                $user_id = $this->api_token->ac_userId;
                $updateId = $formData['fieldId'];
                $updateSql = '';
                if($updateId != ''){
                    $updateSql = " AND `ac_fieldId` != ".$updateId." ";
                }

                $sql = "SELECT * FROM `ac_field` WHERE `user_id` = '".$user_id."' AND LOWER(`name`) = '".$lwrStr."' ".$updateSql." ";
                $data = $this->dashboards->getRecordSql($sql);
                if(!empty($data)){
                    $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'field_name_exists');
                }else{

                    $formData['soil_sample_date'] = date('Y-m-d',strtotime($this->post('soil_sample_date')));
                    $user_id = $this->api_token->ac_userId;
                    if (!empty($formData['echoSize'])) {
                        if ($formData['echoSize'] > $formData['size']) {
                            $post = array('status' => FALSE,'sizeIssue' => TRUE,'message' => 'echo_size_exceeded');
                        }
                    }

                    if (!empty($formData['fieldId'])) {
                        $fieldID = $formData['fieldId'];

                        $old_field_size = $this->post('old_field_size');
                        $new_size = $this->post('size');
                        $changeDate = $this->post('size_change_year');
                        //$changeDate = date('Y-m-d',strtotime($changeDate));

                        $getMinMax = $this->dashboards->get_field_min_max_culture_Date($fieldID);
                        $min_date = $getMinMax['culture_min'];
                        /*if($changeDate != ''){
                            $min_date = $changeDate;
                        }else{
                            $min_date = $minDate;
                        }*/
                        $culturesInDuration = $this->dashboards->getFieldCulturesInDaterange($fieldID,$min_date,$getMinMax['culture_max']);

                        if (isset($culturesInDuration) && !empty($culturesInDuration)) {
                            $sizeArr = array();
                            $culturesMinMax = $this->dashboards->get_field_min_max_culture_Date($fieldID);
                            $period = new DatePeriod(
                                new DateTime($culturesMinMax['culture_min']),
                                new DateInterval('P1D'),
                                new DateTime($culturesMinMax['culture_max'])
                            );
                            foreach ($period as $pkey => $pvalue) {
                                //if($min_date <= $pvalue->format('Y-m-d')){
                                    $sizeArr[$pvalue->format('Y-m-d')] = 0;
                                //}
                            }
                            foreach ($culturesInDuration as $key => $value) {
                                $period = new DatePeriod(
                                    new DateTime($value->start_date),
                                    new DateInterval('P1D'),
                                    new DateTime($value->end_date)
                                );
                                foreach ($period as $pkey => $pvalue) {
                                    //if($min_date <= $pvalue->format('Y-m-d')){
                                        $sizeArr[$pvalue->format('Y-m-d')] += (int)$value->culture_size;
                                    //}
                                }
                            }
                            $maxSize = max($sizeArr);
                            
                            //$post = array('status' => FALSE,'size_arr'=>$sizeArr,'max_size'=>$maxSize,'sizeIssue' => FALSE,'message' => 'echo_size_exceeded');
                            //$this->response($post, REST_Controller::HTTP_OK);

                            if (!empty($formData['echoSize'])) {
                                if((int)$formData['size'] < (int)$formData['echoSize'] ){
                                    $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'echo_size_exceeded');
                                    $this->response($post, REST_Controller::HTTP_OK);
                                }else if (((int)$formData['size'] - (int)$formData['echoSize']) < ((int)$maxSize)) {
                                    $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'fiels_size_error');
                                    $this->response($post, REST_Controller::HTTP_OK);
                                } else {
                                    $update = $this->dashboards->update_field($formData, $fieldID,$user_id);
                                    if ($update) {
                                        $post = array('status' => TRUE,'sizeIssue' => FALSE,'message' => 'update_success');
                                        $this->response($post, REST_Controller::HTTP_OK);
                                    } else {
                                        $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'Not updated');
                                        $this->response($post, REST_Controller::HTTP_OK);
                                    }
                                }
                            } else {
                                if ((int)$formData['size'] < ((int)$maxSize)) {
                                    $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'fiels_size_error');
                                    $this->response($post, REST_Controller::HTTP_OK);
                                } else {
                                    $update = $this->dashboards->update_field($formData, $fieldID,$user_id);
                                    if ($update) {
                                        $post = array('status' => TRUE,'sizeIssue' => FALSE,'message' => 'update_success');
                                        $this->response($post, REST_Controller::HTTP_OK);
                                    } else {
                                        $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'Not updated');
                                        $this->response($post, REST_Controller::HTTP_OK);
                                    }
                                }
                            }
                        } else {
                            $update = $this->dashboards->update_field($formData, $fieldID,$user_id);
                            if ($update) {
                                $post = array('status' => TRUE,'sizeIssue' => FALSE,'message' => 'update_success');
                                $this->response($post, REST_Controller::HTTP_OK);
                            } else {
                                $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'update_fail');
                                $this->response($post, REST_Controller::HTTP_OK);
                            }
                        }
                    } else {
                        $fieldID = $this->dashboards->add_field($formData, $user_id);
                    }

                    if ($fieldID && $fieldID > 0) {
                        $field = $this->dashboards->get_field($fieldID);
                        if ($field) {
                            $post = array('status' => TRUE,'data' => $field,'sizeIssue' => FALSE,'message' => 'added_success');
                            $this->response($post, REST_Controller::HTTP_OK);
                        } else {
                            $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'error_something_wrong');
                            $this->response($post, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $post = array('status' => FALSE,'sizeIssue' => FALSE,'message' => 'added_fail');
                        $this->response($post, REST_Controller::HTTP_OK);
                    }
                }
                $this->response($post, REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'sizeIssue' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
	*Function for check field name availability
	**/
	public function fieldname_check($str) {
		$lwrStr = strtolower($str);
        $user_id = $this->api_token->ac_userId;
        $sql = "SELECT * FROM `ac_field` WHERE `user_id` = '".$user_id."' AND LOWER(`name`) = '".$lwrStr."' ";
        $data = $this->dashboards->getRecordSql($sql);
        if(!empty($data)){
            $this->form_validation->set_message('field_name_exists');
            return FALSE;
        }else{
            return TRUE;
        }
	}

    /**
     * Register field Details or culture
     */
    public function addFieldCulture_post()
    {
        try {
            $formData = array(
                'fieldId' => $this->post('fieldId'),
                'cultureId' => $this->post('cultureId'),
                'delicate' => $this->post('delicate'),
                'dateFrom' => $this->post('dateFrom'),
                'dateTo' => $this->post('dateTo'),
                'harvest_date' => $this->post('harvest_date'),
                'cultivatedArea' => $this->post('cultivatedArea'),
                'isSwapped' => $this->post('isSwapped'),
                'swappedWith' => $this->post('swappedWith'),
                'rule_breaked' => '0',
                'plantVariety' => $this->post('plantVariety'),
                'notes' => $this->post('notes'),
                'seed_volume' => $this->post('seed_volume'),
                'harvest_volume' => $this->post('harvest_volume'),
            );

            $this->form_validation->set_data($formData);
            $this->form_validation->set_rules('delicate', 'Delicate', 'required|trim');
            $this->form_validation->set_rules('dateFrom', 'Date From', 'required|trim');
            $this->form_validation->set_rules('dateTo', 'Date To', 'trim');
            $this->form_validation->set_rules('harvest_date', 'Harvest Date', 'required|trim|callback_compareDate');
            $this->form_validation->set_rules('cultivatedArea', 'Cultivated Area', 'required|trim|max_length[10]|greater_than[0]');
            if (!empty($formData['isSwapped'])) {
                $this->form_validation->set_rules('swappedWith', 'Swapped With', 'required|trim|max_length[50]');
            }
            $this->form_validation->set_rules('plantVariety', 'Plant Variety', 'trim|max_length[500]');
            $this->form_validation->set_rules('notes', 'Notes', 'trim|max_length[500]');
            $this->form_validation->set_rules('seed_volume', 'Seed volume', 'trim');
            $this->form_validation->set_rules('harvest_volume', 'Harvest Volume', 'trim');
            $this->form_validation->set_error_delimiters('', '<br>');

            $post = array();
            if ($this->form_validation->run($this) === FALSE) {
                $this->response([
                    'status' => FALSE,
                    'cropStatus' => FALSE,
                    'sizeIssue' => FALSE,
                    'message' => validation_errors()
                ], REST_Controller::HTTP_OK);
            }else{
                if (!empty($formData['fieldId'])) {
                    if (!empty($formData['from']) && !empty($formData['to'])) {
                        $formData['from'] = date('Y-m-d', strtotime($formData['from']));
                        $formData['to'] = date('Y-m-d', strtotime($formData['to']));
                    }

                    $fieldId = $formData['fieldId'];
                    $fieldDetails = $this->dashboards->get_field($fieldId);
                    $formEndDate = $formData['dateTo'];
                    if($formData['dateTo'] == ''){
                        $formEndDate =  $formData['harvest_date'];
                    }

                    $culturesInDuration = $this->dashboards->getFieldCulturesInDaterange($fieldId, $formData['dateFrom'], $formEndDate, $formData['cultureId']);

                    if (isset($culturesInDuration) && !empty($culturesInDuration)) {
                        $sizeArr = array();
                        $period = new DatePeriod(
                            new DateTime($formData['dateFrom']),
                            new DateInterval('P1D'),
                            new DateTime($formEndDate)
                        );
                        foreach ($period as $pkey => $pvalue) {
                            $sizeArr[$pvalue->format('Y-m-d')] = 0;
                        }

                        foreach ($culturesInDuration as $key => $value) {
                            if (strtotime($value->start_date) < strtotime($formData['dateFrom'])) {
                                $startDate = $formData['dateFrom'];
                            } else {
                                $startDate = $value->start_date;
                            }

                            if($value->end_date != ''){
                               $fromEndValue = $value->end_date;
                            }else{
                               $fromEndValue = $value->harvest_date; 
                            }
                            if (strtotime($fromEndValue) > strtotime($formEndDate)) {
                                $endDate = $formEndDate;
                            } else {
                                $endDate = $fromEndValue;
                            }

                            $period = new DatePeriod(
                                new DateTime($startDate),
                                new DateInterval('P1D'),
                                new DateTime($endDate)
                            );

                            foreach ($period as $pkey => $pvalue) {
                                $sizeArr[$pvalue->format('Y-m-d')] += (int)$value->culture_size;
                            }
                        }
                        if (empty($sizeArr)) {
                            $maxSize = 1;
                        } else {
                            $maxSize = max($sizeArr);
                        }
                        if (((int)$fieldDetails->size - (int)$fieldDetails->echo_size) >= ((int)$formData['cultivatedArea'] + (int)$maxSize)) {
                            $checkStatus = $this->checkFieldCropRotation($formData);
                            if(!empty($checkStatus)){
                                $this->response(['status' => FALSE,'check'=>$checkStatus,'cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                            }else{
                                $this->addCultureInDb($formData);
                            }
                        } else {
                            $post = array('status'=>FALSE,'cropStatus'=>FALSE,'sizeIssue'=>TRUE,'message'=>'cultivated_size_exceed');
                            $this->response($post, REST_Controller::HTTP_OK);
                        }
                    } else {
                        if (((int)$fieldDetails->size - (int)$fieldDetails->echo_size) >= $formData['cultivatedArea']) {
                            $checkStatus = $this->checkFieldCropRotation($formData);
                            if(!empty($checkStatus)){
                                $this->response(['status' => FALSE,'check'=>$checkStatus,'cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                            }else{
                                $this->addCultureInDb($formData);
                            }
                        } else {
                            $post = array('status'=>FALSE,'cropStatus'=>FALSE,'sizeIssue'=>TRUE,'message'=>'cultivated_size_exceed');
                            $this->response($post, REST_Controller::HTTP_OK);
                        }
                    }
                } else {
                    $post = array('status' => FALSE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'Please Add Field First.');
                }
                //$this->response($post, REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'err'=>$e,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     */
    public function addCultureWithCropRotationWarning_post()
    {
        try {
            $formData = array(
                'fieldId' => $this->post('fieldId'),
                'cultureId' => $this->post('cultureId'),
                'delicate' => $this->post('delicate'),
                'dateFrom' => $this->post('dateFrom'),
                'dateTo' => $this->post('dateTo'),
                'harvest_date' => $this->post('harvest_date'),
                'cultivatedArea' => $this->post('cultivatedArea'),
                'isSwapped' => $this->post('isSwapped'),
                'isSwapped' => $this->post('isSwapped'),
                'swappedWith' => $this->post('swappedWith'),
                'rule_breaked' => '1',
                'plantVariety' => $this->post('plantVariety'),
                'notes' => $this->post('notes'),
                /*'nitrogen' => $this->post('nitrogen'),
                'phosphorus' => $this->post('phosphorus'),
                'potassium_oxide' => $this->post('potassium_oxide'),
                'magnesium' => $this->post('magnesium'),*/
                'seed_volume' => $this->post('seed_volume'),
                'harvest_volume' => $this->post('harvest_volume'),
            );
            $sql = "SELECT `crop_family_id` FROM `ac_crop` WHERE `ac_cropId` = '".$this->post('delicate')."' ";
            $data = $this->dashboards->getRecordSql($sql);
            if(!empty($data)){
                $family_id = $data[0]['crop_family_id'];
                $query="SELECT CL.*,CP.crop_family_id FROM `ac_culture` CL 
                        LEFT JOIN ac_crop CP ON CP.ac_cropId = CL.`crop_id`
                        WHERE CL.`field_id` = '".$this->post('fieldId')."' AND CP.crop_family_id = '".$family_id."' ";
                $allCulture = $this->dashboards->getRecordSql($query);
                if(!empty($allCulture)){
                    foreach($allCulture as $cul){
                        $this->dashboards->updateRecord('ac_culture',array('rule_breaked'=>1),array('ac_cultureId'=>$cul['ac_cultureId']));
                    }
                }
            }

            //$this->dashboards->updateRecord('ac_culture',array('rule_breaked'=>1),array('field_id'=>$this->post('fieldId')));
            $this->addCultureInDb($formData);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    
    public function checkFieldCropRotation($formData)
    {
        $checkEndDate = $formData['dateTo'];
        if($formData['dateTo'] == ''){
            $checkEndDate = $formData['harvest_date'];
        }
        $cultureDuration = $this->getDuration($formData['dateFrom'], $checkEndDate);

        $backtime = strtotime("-6 year", strtotime($formData['dateFrom']));
        //$forwardtime = strtotime("+1 year", strtotime($formData['dateTo']));
        $forwardtime = strtotime($checkEndDate);
        $reportingFromDate = date("Y-m-d", $backtime);
        $reportingToDate = date("Y-m-d", $forwardtime);

        $CropFamilyId = $this->cropmasters->GetCropFamilyIdByCrop($formData['delicate']);
        $CultureCropAndFamilyCount = $this->dashboards->GetCropAndFamilyCount($reportingFromDate, $reportingToDate, $formData, $CropFamilyId->crop_family_id);
        
        /*$checkArr = array(
           'reportingFromDate' => $reportingFromDate,
           'reportingToDate' => $reportingToDate,
           'CultureCropAndFamilyCount' => $CultureCropAndFamilyCount,
        );
        //$this->response(['status' => FALSE,'checkArr'=>$checkArr,'reason'=>'7yearMax','cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
        */
        
        $oneyearBackStartDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($formData['dateFrom'])) . " - 365 day"));
        $oneyearAheadEndDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($checkEndDate)) . " + 365 day"));

        //$oneyearBackStartDate =  $formData['dateFrom'];
        //$oneyearAheadEndDate = $formData['dateTo'];
        $cropFamilyCropInSameYear = $this->dashboards->CheckFamilyCropInSameYear($formData['dateFrom'], $formData['dateTo'], $formData, $CropFamilyId->crop_family_id);
        $cropFamilyShortCropInSameYear = $this->dashboards->CheckFamilyShortCropInYear($formData['dateFrom'], $formData['dateTo'], $formData, $CropFamilyId->crop_family_id);

        $cropFamilyCropInBackSingleYear = $this->dashboards->CheckFamilyCropInYear($oneyearBackStartDate, $oneyearBackStartDate, $formData, $CropFamilyId->crop_family_id);
        $cropFamilyCropInAheadSingleYear = $this->dashboards->CheckFamilyCropInYear($oneyearAheadEndDate, $oneyearAheadEndDate, $formData, $CropFamilyId->crop_family_id);

        $twoyearBackStartDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($formData['dateFrom'])) . " - 730 day"));
        $twoyearAheadEndDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($checkEndDate)) . " + 730 day"));
        
        $cropFamilyCropInBackTwoYear = $this->dashboards->CheckFamilyCropInYear($twoyearBackStartDate, $twoyearBackStartDate, $formData, $CropFamilyId->crop_family_id);
        $cropFamilyCropInAheadTwoYear = $this->dashboards->CheckFamilyCropInYear($twoyearAheadEndDate, $twoyearAheadEndDate, $formData, $CropFamilyId->crop_family_id);

        $threeyearBackStartDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($formData['dateFrom'])) . " - 1095 day"));
        $threeyearAheadEndDate =  date("Y-m-d", strtotime(date("Y-m-d", strtotime($checkEndDate)) . " + 1095 day"));

        $cropFamilyCropInBackThreeYear = $this->dashboards->CheckFamilyCropInYear($threeyearBackStartDate, $threeyearBackStartDate, $formData, $CropFamilyId->crop_family_id);
        $cropFamilyCropInAheadThreeYear = $this->dashboards->CheckFamilyCropInYear($threeyearAheadEndDate, $threeyearAheadEndDate, $formData, $CropFamilyId->crop_family_id);

        $backOneYear = '';
        if(!empty($cropFamilyCropInBackSingleYear)){
           $backOneYear=$cropFamilyCropInBackSingleYear->culture_count;
        }
        $aheadOneYear = '';
        if(!empty($cropFamilyCropInAheadSingleYear)){
            $aheadOneYear = $cropFamilyCropInAheadSingleYear->culture_count;
        }

        $backTwoYear = '';
        if(!empty($cropFamilyCropInBackTwoYear)){
           $backTwoYear=$cropFamilyCropInBackTwoYear->culture_count;
        }
        $aheadTwoYear = '';
        if(!empty($cropFamilyCropInAheadTwoYear)){
            $aheadTwoYear = $cropFamilyCropInAheadTwoYear->culture_count;
        }
        $backThreeYear = '';
        if(!empty($cropFamilyCropInBackThreeYear)){
          $backThreeYear = $cropFamilyCropInBackThreeYear->culture_count;
        }
        $aheadThreeYear = '';
        if(!empty($cropFamilyCropInAheadThreeYear)){
          $aheadThreeYear = $cropFamilyCropInAheadThreeYear->culture_count;
        }
        
        $checkArr = array(
            'reportingFromDate' => $reportingFromDate,
            'reportingToDate' => $reportingToDate,
            'CultureCropAndFamilyCount' => $CultureCropAndFamilyCount,

            'cropFamilyCropInSameYear' => $cropFamilyCropInSameYear,

            'oneyearBackStartDate' => $oneyearBackStartDate,
            'oneyearAheadEndDate' => $oneyearAheadEndDate,
            'cropFamilyCropInBackSingleYear' => $cropFamilyCropInBackSingleYear,
            'cropFamilyCropInAheadSingleYear' => $cropFamilyCropInAheadSingleYear,

            'twoyearBackStartDate' => $twoyearBackStartDate,
            'twoyearAheadEndDate' => $twoyearAheadEndDate,
            'cropFamilyCropInBackTwoYear' => $cropFamilyCropInBackTwoYear,
            'cropFamilyCropInAheadTwoYear' => $cropFamilyCropInAheadTwoYear,

            'threeyearBackStartDate' => $threeyearBackStartDate,
            'threeyearAheadEndDate' => $threeyearAheadEndDate,
            'cropFamilyCropInBackThreeYear' => $cropFamilyCropInBackThreeYear,
            'cropFamilyCropInAheadThreeYear' => $cropFamilyCropInAheadThreeYear,
            'cropFamilyShortCropInSameYear' => $cropFamilyShortCropInSameYear

            // 'fouryearBackStartDate' => $fouryearBackStartDate,
            // 'fouryearAheadEndDate' => $fouryearAheadEndDate,
            // 'cropFamilyCropInBackFourYear' => $cropFamilyCropInBackFourYear,
            // 'cropFamilyCropInAheadFourYear' => $cropFamilyCropInAheadFourYear
        );
        //$status = array('checkArr'=>$checkArr);
        $status = array();

        if (!empty($CultureCropAndFamilyCount->crop_family_count) || !empty($CultureCropAndFamilyCount->crop_limit) ) {
            //$status = array('checkArr'=>$checkArr);
            if ($CultureCropAndFamilyCount->crop_family_limit > ($CultureCropAndFamilyCount->crop_family_count ) && $CultureCropAndFamilyCount->crop_limit > ($CultureCropAndFamilyCount->crop_count ) || ($cultureDuration < 98 && (empty($cropFamilyShortCropInSameYear->culture_count)) ) ) {
                //$status = array('checkArr'=>$checkArr);
                //if((!empty($backOneYear) && !empty($backTwoYear) ) || (!empty($backTwoYear) && !empty($backThreeYear) || (!empty($aheadOneYear) && !empty($aheadTwoYear)) || (!empty($aheadTwoYear) && !empty($aheadThreeYear)) ) || ($cultureDuration >= 98 && (!empty( $cropFamilyShortCropInSameYear->culture_count)))  ){
                if((!empty($backOneYear) && !empty($backTwoYear) ) || (!empty($backTwoYear) && !empty($backThreeYear) || (!empty($aheadOneYear) && !empty($aheadTwoYear)) || (!empty($aheadTwoYear) && !empty($aheadThreeYear)) )  ){

                    //$this->response(['status' => FALSE,'reason'=>'rule6','checkArr'=>$checkArr,'cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                    $status = array('reason'=>'rule6','checkArr'=>$checkArr);
                }else{
                    if (!empty($cropFamilyCropInSameYear->culture_count) ) {
                        //$this->response(['status' => FALSE,'reason'=>'1year','CultureCropAndFamilyCount' =>$CultureCropAndFamilyCount,'cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                        $status = array('reason'=>'1year','checkArr'=>$checkArr);
                    } else {
                        if (!empty($cropFamilyCropInBackTwoYear->culture_count) || !empty($cropFamilyCropInAheadTwoYear->culture_count)) {
                            if (!empty($cropFamilyCropInBackThreeYear->culture_count) || !empty($cropFamilyCropInAheadThreeYear->culture_count)) {
                                //$this->response(['status' => FALSE,'reason'=>'3year','cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                                $status = array('reason'=>'3year');
                            } else {
                                if (!empty($CultureCropAndFamilyCount->crop_family_count) || !empty($CultureCropAndFamilyCount->crop_count)) {
                                    if ($CultureCropAndFamilyCount->crop_family_limit >= ($CultureCropAndFamilyCount->crop_family_count) && $CultureCropAndFamilyCount->crop_limit >= ($CultureCropAndFamilyCount->crop_count) && $cultureDuration < 98) {
                                        //$this->addCultureInDb($formData);
                                    } else {
                                        //$this->response(['status' => FALSE,'reason'=>'7yearMax','cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                                        $status = array('reason'=>'7yearMax');
                                    }
                                } else {
                                    //$this->addCultureInDb($formData);
                                }
                            }
                        } else {
                            if (!empty($CultureCropAndFamilyCount->crop_family_count) || !empty($CultureCropAndFamilyCount->crop_count)) {
                                if ($CultureCropAndFamilyCount->crop_family_limit >= ($CultureCropAndFamilyCount->crop_family_count) && $CultureCropAndFamilyCount->crop_limit >= ($CultureCropAndFamilyCount->crop_count)) {
                                    //$this->addCultureInDb($formData);
                                } else {
                                    //$this->response(['status' => FALSE,'reason'=>'2yearMax','cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                                    $status = array('reason'=>'2yearMax');
                                }
                            } else {
                                //$this->addCultureInDb($formData);
                            }
                        }
                    }
                    ///$status = array('reason'=>'test','checkArr'=>$checkArr);
                }
            } else {
                //$this->response(['status' => FALSE,'CultureCropAndFamilyCount'=>$CultureCropAndFamilyCount,'reason'=>'7yearMax','cropStatus' => TRUE,'sizeIssue' => FALSE,'message' => 'crop_rotation_rule_broken'], REST_Controller::HTTP_OK);
                $status = array('CultureCropAndFamilyCount'=>$CultureCropAndFamilyCount,'reportingFromDate' => $reportingFromDate,
                'reportingToDate' => $reportingToDate,'reason'=>'7yearMax');
            }
        }else{
            $status = array('CultureCropAndFamilyCount'=>$CultureCropAndFamilyCount,'reportingFromDate' => $reportingFromDate,
                'reportingToDate' => $reportingToDate,'reason'=>'7yearMax');
        }
           
        return $status;
    }

    public function addCultureInDb($formData)
    {
        if (!empty($formData['cultureId'])) {
            $update = $this->dashboards->update_culture($formData, $formData['cultureId']);
            if ($update) {
                //$this->set_response(['status' => TRUE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'update_success'], REST_Controller::HTTP_OK);
                
                /*if($formData['rule_breaked'] == 0){
                    $this->dashboards->updateRecord('ac_culture',array('rule_breaked'=>0),array('field_id'=>$this->post('fieldId')));
                }*/
                $sql = "SELECT CP.crop_family_id FROM `ac_culture` CL LEFT JOIN ac_crop CP ON CP.ac_cropId = CL.`crop_id`
                    WHERE CL.`ac_cultureId` = '".$formData['cultureId']."' ";
                $data = $this->dashboards->getRecordSql($sql);
                $family_id = $data[0]['crop_family_id'];

                $allFields = $this->dashboards->getAllCropByFields($formData['fieldId'],$formData['cultureId'],$family_id);
                if ($allFields && count($allFields) > 0) {
                    foreach($allFields as $key=>$value){
                        $updateData = array(
                            'fieldId' => $value->field_id,
                            'cultureId' => $value->ac_cultureId,
                            'delicate' => $value->crop_id,
                            'dateFrom' => $value->start_date,
                            'dateTo' => $value->end_date,
                            'harvest_date' => $value->harvest_date,
                            'cultivatedArea' => $value->size,
                            'isSwapped' => $value->is_swapped,
                            'swappedWith' => $value->swapped_with,
                            'rule_breaked' => $value->rule_breaked,
                            'plantVariety' => $value->plantVariety,
                            'notes' => $value->culture_notes,
                            'seed_volume' => $value->seed_volume,
                            'harvest_volume' => $value->harvest_volume,
                        );
                        $checkStatus = $this->checkFieldCropRotation($updateData); 
                        if(!empty($checkStatus)){
                            $updateData['rule_breaked'] = 1;
                        }else{
                            $updateData['rule_breaked'] = 0;
                        }
                        $this->dashboards->update_culture($updateData, $updateData['cultureId']);
                    }
                }
                $this->set_response(['status' => TRUE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'update_success'], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'update_fail'], REST_Controller::HTTP_OK);
            }
        } else {
            $cultureID = $this->dashboards->add_culture($formData, $formData['fieldId']);
            if ($cultureID && $cultureID > 0) {
                $this->set_response(['status' => TRUE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'added_success'], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'cropStatus' => FALSE,'sizeIssue' => FALSE,'message' => 'added_fail'], REST_Controller::HTTP_OK);
            }
        }
    }

    public function compareDate()
    {
        $startDate = strtotime($this->post('dateFrom'));
        $endDate = strtotime($this->post('dateTo'));
        $harvestDate = strtotime($this->post('harvest_date'));

        //if($harvestDate != NULL){
        if ($harvestDate >= $startDate)
            if($endDate != NULL){
                if ($endDate >= $startDate && $endDate >= $harvestDate)
                    return True;
                else {
                    $this->form_validation->set_message('compareDate', 'Date From and Harvest Date should be less than Date To.');
                    return False;
                }
            }else{
                return True;
            }
        else {
            $this->form_validation->set_message('compareDate', 'Date From should be less than Date To.');
            return False;
        }
        //}
        
        
    }

    /**
     * Get Dashboard Data
     */
    public function getDashboardChartData_post()
    {
        try {
            $inputData = array(
                'min' => $this->post('min'),
                'max' => $this->post('max'),
                'languageId' => $this->post('language'),
            );

            $page_no = $this->post('page_no');
            $limit = $this->post('limit');
            $start = $limit * ($page_no - 1);
            $quryLimit = "LIMIT ".$start.','.$limit; 

            $slider = false;
            if($this->post('service_type')){
                $slider = true;
                $limit_total = $start+$limit;
                $quryLimit = "LIMIT ".$limit_total; 
            }

            if (empty($inputData['min']) || empty($inputData['max'])) {
                $inputData['min'] = date('Y-m-d', strtotime('-2 months'));
                $inputData['max'] = date('Y-m-d', strtotime('+10 months'));
            } else {
                $inputData['min'] = date('Y-m-d', strtotime($inputData['min']));
                $inputData['max'] = date('Y-m-d', strtotime($inputData['max']));
            }

            if (!empty($inputData['languageId'])) {
                $languageId = $inputData['languageId'];
            } else {
                $languageId = 1;
            }
            $culture_array = array();
            $languageData = $this->commons->getLanguageById($languageId);

            $dashboard_data = array();
            $new_array_field_id = array();
            $footerData = new stdClass();
            
            $user_id = $this->api_token->ac_userId;
            
            if ($user_id) {
                $total_count = 0;
                $totalSql = "select DISTINCT(acf.ac_fieldId) as field_id
                            from ac_field acf 
                            Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$inputData['min']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$inputData['min']."') AND (accu.start_date <= '".$inputData['max']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$inputData['max']."') )
                            where acf.user_id = ".$user_id." AND acf.archieve = 0 order by acf.ac_fieldId DESC ";
                $totalData = $this->db->query($totalSql);
                if($totalData->num_rows() > 0)
                {
                  $total_count = $totalData->num_rows();
                  $total_count = ceil($total_count/3);
                }
                $data = $this->dashboards->get_dashboard_chart_data($user_id, $inputData, $languageData->symbol,$quryLimit,$total_count,$page_no);
                //$responseData = array('dashboard_data' => $data,'total_page'=>$total_count, 'field_cultures' => $new_array_field_id, 'footerTextData' => $footerData);
                if (!empty($data)) {
                    $cultureCountArr = array();
                    foreach ($data as $key => $value) {

                        $count_arr = array();
                        $culturesInDuration = $this->dashboards->getFieldCulturesInDaterange($value->field_id, $inputData['min'], $inputData['max'], '');
                        if (isset($culturesInDuration) && !empty($culturesInDuration)) {
                            $culturesMinMax = $this->dashboards->get_field_min_max_culture_Date($value->field_id);
                            $period = new DatePeriod(
                                new DateTime($culturesMinMax['culture_min']),
                                new DateInterval('P1D'),
                                new DateTime($culturesMinMax['culture_max'])
                            );
                            foreach ($period as $pkey => $pvalue) {
                                $count_arr[$pvalue->format('Y-m-d')] = 0;
                            }

                            foreach ($culturesInDuration as $key => $value) {
                                $endValue = $value->end_date;
                                if($value->end_date == ''){
                                    $endValue = $value->harvest_date;
                                }

                                $period = new DatePeriod(
                                    new DateTime($value->start_date),
                                    new DateInterval('P1D'),
                                    new DateTime($endValue)
                                );

                                foreach ($period as $pkey => $pvalue) {
                                    $count_arr[$pvalue->format('Y-m-d')] += 1;
                                }
                            }
                            if (empty($playerlist)) {
                                $maxCount = 1;
                            } else {
                                $maxCount = max($count_arr);
                            }

                            array_push($cultureCountArr, $maxCount);
                        } else {
                            array_push($cultureCountArr, 1);
                        }
                    }

                    $max_culture_count = max($cultureCountArr);

                    foreach ($data as $key => $value) {
                        $row_count = $max_culture_count;
                        $field_ids = array();

                        $before_culture_start_date = $value->before_culture_start_date;
                        $before_culture_end_date = $value->before_culture_end_date;
                        $before_culture_harvest_date = $value->before_culture_harvest_date;
                        $before_culture_size = $value->before_culture_size;
                        $before_culture_crop_name = $value->before_culture_crop_name;

                        $before_culture_start_date_arr = preg_split('@,@', $before_culture_start_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_end_date_arr = preg_split('@,@', $before_culture_end_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_harvest_date_arr = preg_split('@,@', $before_culture_harvest_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_size_arr = preg_split('@,@', $before_culture_size, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_crop_name_arr = preg_split('@,@', $before_culture_crop_name, NULL, PREG_SPLIT_NO_EMPTY);

                        $value->before_culture_start_date = $before_culture_start_date_arr;
                        $value->before_culture_end_date = $before_culture_end_date_arr;
                        $value->before_culture_harvest_date = $before_culture_harvest_date_arr;
                        $value->before_culture_size = $before_culture_size_arr;
                        $value->before_culture_crop_name = $before_culture_crop_name_arr;
                    }
                    $field_count = 0;
                    $result_arr = array();
                    foreach ($data as $key => $value) {
                        $rgba_color = $this->dashboards->hex2rgba($value->color,0.3);
                        $value->rgba_color =$rgba_color;

                        $cultureCount = 0;
                        $culture_ids = array();
                        foreach ($data as $key => $datavalue) {
                            if ($value->field_id == $datavalue->field_id) {
                                if (!in_array($value->culture_id, $culture_ids)) {
                                    array_push($culture_ids, $value->culture_id);
                                }
                            }
                        }
                        $cultureCount = count($culture_ids);
                        if(empty($result_arr)){
                            $result_arr = array_fill(0, 366,  array_fill(0, ($cultureCount+2), 0));
                        }

                        $flag = 0;
                        $i = 0;
                        $start_date_index = $this->getDuration($inputData['min'], $value->start_date) - 1;
                        $end_date_index = $this->getDuration($inputData['min'], $value->end_date) - 1;
                        $harvest_date_index = $this->getDuration($inputData['min'], $value->harvest_date) - 1;

                        if ($field_count == 0) {
                            $temp1 = $value->field_id;
                            $field_count++;
                        }
                        $temp2 = $value->field_id;

                        if ($temp1 != $temp2) {
                            $new_array_field_id[$temp1] = $result_arr;
                            $temp1 = $temp2;
                            $result_arr = array_fill(0, 366,  array_fill(0, ($cultureCount+2), 0));
                        }

                        if ($start_date_index < 0) {
                            $start_date_index = 0;
                        }

                        if ($harvest_date_index > 365) {
                            $harvest_date_index = 365;
                        }

                        if ($end_date_index > 365) {
                            $end_date_index = 365;
                        }

                        if($value->end_date != NULL){
                          $endIndex = $end_date_index;
                        }else{
                          $endIndex = $harvest_date_index;
                        }
                        for ($i = 0; $i <= ($cultureCount+2); $i++) {
                            $flag = 0;
                            if (empty($result_arr[$start_date_index][$i])) {
                                for ($j = $start_date_index; $j <= $endIndex; $j++) {
                                    if (!empty($result_arr[$j][$i])) {
                                        $flag = 1;
                                        break;
                                    }
                                }
                                if ($flag == 0) {
                                    break;
                                }
                            }
                        }

                        if($value->end_date != NULL){
                            for ($j = $start_date_index; $j <= $end_date_index; $j++) {
                                if (strtotime($inputData['min']) > strtotime($value->start_date) && strtotime($value->end_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($inputData['min'], $inputData['max']);
                                } elseif (strtotime($inputData['min']) > strtotime($value->start_date)) {
                                    $days_count = $this->getDuration($inputData['min'], $value->end_date);
                                } elseif (strtotime($value->end_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($value->start_date, $inputData['max']);
                                } else {
                                    $days_count = $this->getDuration($value->start_date, $value->end_date);
                                }
                                $value->duration = $days_count;
                                $value->start_date_index = $start_date_index;
                                $value->end_date_index = $end_date_index;
                                $value->harvest_date_index = $harvest_date_index;
                                
                                
                                $result_arr[$j][$i] = $value;
                            }
                       }else{
                            for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                                if (strtotime($inputData['min']) > strtotime($value->start_date) && strtotime($value->harvest_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($inputData['min'], $inputData['max']);
                                } elseif (strtotime($inputData['min']) > strtotime($value->start_date)) {
                                    $days_count = $this->getDuration($inputData['min'], $value->harvest_date);
                                } elseif (strtotime($value->harvest_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($value->start_date, $inputData['max']);
                                } else {
                                    $days_count = $this->getDuration($value->start_date, $value->harvest_date);
                                }
                                $value->duration = $days_count;
                                $value->start_date_index = $start_date_index;
                                $value->harvest_date_index = $harvest_date_index;
                                $result_arr[$j][$i] = $value;
                            }
                        }
                        

                        if (!in_array($value->field_id, $field_ids)) {
                            array_push($field_ids, $value->field_id);
                        }


                    }
                    
                    $new_array_field_id[$temp1] = $result_arr;
                }
                if (!empty($data)) {
                    $field_ids = array();
                    foreach ($data as $key => $value) {
                        $culture = array();
                        if (!in_array($value->field_id, $field_ids)) {
                            if (isset($culture_array) && !empty($culture_array)) {
                                $field['cultures'] =  $culture_array;
                            }
                            if (isset($field) && !empty($field)) {
                                array_push($dashboard_data, $field);
                            }

                            $field = array();
                            $culture_array = array();
                            
                            $field['field_id'] = $value->field_id;
                            $field['user_id'] = $value->user_id;
                            $field['field_name'] = $value->field_name;
                            $field['field_size'] = $value->field_size;
                            $field['echo'] = $value->echo;
                            $field['echo_size'] = $value->echo_size;
                            $field['address'] = $value->address;
                            $field['city'] = $value->city;
                            $field['zipcode'] = $value->zipcode;
                            $field['archieve'] = $value->archieve;
                            $field['notes'] = $value->notes;
                            $field['nitrogen'] = $value->nitrogen;
                            $field['phosphorus'] = $value->phosphorus;
                            $field['potassium_oxide'] = $value->potassium_oxide;
                            $field['magnesium'] = $value->magnesium;
                            $field['ph_value'] = $value->ph_value;
                            $field['soil_sample_date'] = $value->soil_sample_date;
                            $field['max_culture_count'] = $max_culture_count;
                            $field['cultures'] = array();

                            $sizeArr = array();
                            $sizeData = $this->dashboards->get_field_size_data($value->field_id);
                            $inputData['min']; $inputData['max'];
                            if(!empty($sizeData)){
                                foreach ($sizeData as $sizekey => $sizevalue) {
                                    $sizeArr[] = array(
                                        'field_id' => $sizevalue->field_id,
                                        'size_change_year' => $sizevalue->size_change_year,
                                        'field_size' => $sizevalue->field_size,
                                        'echo' => $sizevalue->echo,
                                        'echo_size' => $sizevalue->echo_size,
                                    );
                                    $currentYear = date('Y');
                                    if($currentYear == $sizevalue->size_change_year){
                                        $field['field_size'] = $sizevalue->field_size;
                                        $field['echo'] = $sizevalue->echo;
                                        $field['echo_size'] = $sizevalue->echo_size;
                                    }
                                }
                            }
                            $field['field_size_data'] = $sizeArr;
                            array_push($field_ids, $value->field_id);
                        }

                        $rgba_color = $this->dashboards->hex2rgba($value->color,0.3);

                        $culture['culture_id'] = $value->culture_id;
                        $culture['start_date'] = $value->start_date;
                        $culture['end_date'] = $value->end_date;
                        $culture['harvest_date'] = $value->harvest_date;
                        $culture['culture_size'] = $value->culture_size;
                        $culture['crop_id'] = $value->crop_id;
                        $culture['crop_name'] = $value->crop_name;
                        $culture['color'] =$value->color;
                        $culture['rgba_color'] = $rgba_color;
                        $culture['crop_family_id'] = $value->crop_family_id;
                        $culture['crop_family_name'] = $value->crop_family_name;
                        $culture['is_swapped'] = $value->is_swapped;
                        $culture['swapped_with'] = $value->swapped_with;
                        $culture['rule_breaked'] = $value->rule_breaked;
                        $culture['before_culture_start_date'] = $value->before_culture_start_date;
                        $culture['before_culture_end_date'] = $value->before_culture_end_date;
                        $culture['before_culture_harvest_date'] = $value->before_culture_harvest_date;
                        $culture['before_culture_size'] = $value->before_culture_size;
                        $culture['before_culture_crop_name'] = $value->before_culture_crop_name;
                        $culture['plantVariety'] = $value->plantVariety;
                        $culture['culture_notes'] = $value->culture_notes;
                        $culture['seed_volume'] = $value->seed_volume;
                        $culture['harvest_volume'] = $value->harvest_volume;

                        $days_count = $this->getDuration($value->start_date, $value->end_date);
                        $culture['duration'] = $days_count;
                        $culture['is_filter'] = '0';

                        array_push($culture_array, $culture);
                    }

                    if (isset($culture_array) && !empty($culture_array)) {
                        $field['cultures'] =  $culture_array;
                    }

                    if (isset($field) && !empty($field)) {
                        array_push($dashboard_data, $field);
                    }
                }
                $responseData = array('dashboard_data' => $dashboard_data,'is_slider'=>$slider,'total_page'=>$total_count,'field_cultures' => $new_array_field_id, 'footerTextData' => $footerData);
                
                $this->response(['status' => TRUE,'data' => $responseData,'message' => 'Dashboard Data'], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Dashboard Single Field Data
     */
    public function getSingleFieldChartData_post()
    {
        try {
            $inputData = array(
                'min' => $this->post('min'),
                'max' => $this->post('max'),
                'fieldId' => $this->post('fieldId'),
                'languageId' => $this->post('language'),
            );

            if (empty($inputData['min']) || empty($inputData['max'])) {
                $inputData['min'] = date('Y-m-d', strtotime('-2 months'));
                $inputData['max'] = date('Y-m-d', strtotime('+10 months'));
            } else {
                $inputData['min'] = date('Y-m-d', strtotime($inputData['min']));
                $inputData['max'] = date('Y-m-d', strtotime($inputData['max']));
            }

            if (!empty($inputData['languageId'])) {
                $languageId = $inputData['languageId'];
            } else {
                $languageId = 1;
            }

            $languageData = $this->commons->getLanguageById($languageId);

            // $twoYearFromMin = date('Y-m-d', strtotime('+2 years', strtotime($inputData['min'])));
            $dataDuration = $this->getDuration($inputData['min'], $inputData['max']);
            // $chartDuration = $this->getDuration($inputData['min'], $twoYearFromMin);

            $dashboard_data = array();

            if ($inputData['fieldId']) {
                $user_id = $this->api_token->ac_userId;
                $data = $this->dashboards->get_single_field_data($user_id, $inputData, $languageData->symbol);
                $result_arr = array();
                if (!empty($data)) {
                    $result_arr = array_fill(0, $dataDuration,  array_fill(0, 10, 0));

                    foreach ($data as $key => $value) {
                        $before_culture_start_date = $value->before_culture_start_date;
                        $before_culture_end_date = $value->before_culture_end_date;
                        $before_culture_harvest_date = $value->before_culture_harvest_date;
                        $before_culture_size = $value->before_culture_size;
                        $before_culture_crop_name = $value->before_culture_crop_name;

                        $before_culture_start_date_arr = preg_split('@,@', $before_culture_start_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_end_date_arr = preg_split('@,@', $before_culture_end_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_harvest_date_arr = preg_split('@,@', $before_culture_harvest_date, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_size_arr = preg_split('@,@', $before_culture_size, NULL, PREG_SPLIT_NO_EMPTY);
                        $before_culture_crop_name_arr = preg_split('@,@', $before_culture_crop_name, NULL, PREG_SPLIT_NO_EMPTY);

                        $value->before_culture_start_date = $before_culture_start_date_arr;
                        $value->before_culture_end_date = $before_culture_end_date_arr;
                        $value->before_culture_harvest_date = $before_culture_harvest_date_arr;
                        $value->before_culture_size = $before_culture_size_arr;
                        $value->before_culture_crop_name = $before_culture_crop_name_arr;
                    }

                    foreach ($data as $key => $value) {
                        $rgba_color = $this->dashboards->hex2rgba($value->color,0.3);
                        $value->rgba_color =$rgba_color;

                        $flag = 0;
                        $i = 0;
                        $start_date_index = $this->getDuration($inputData['min'], $value->start_date) - 1;
                        $end_date_index = $this->getDuration($inputData['min'], $value->end_date) - 1;
                        $harvest_date_index = $this->getDuration($inputData['min'], $value->harvest_date) - 1;

                        if ($start_date_index < 0) {
                            $start_date_index = 0;
                        }

                        if ($harvest_date_index > ($dataDuration - 1)) {
                            $harvest_date_index = $dataDuration - 1;
                        }

                        if ($end_date_index > 365) {
                            $end_date_index = 365;
                        }

                        if($value->end_date != NULL){
                          $endIndex = $end_date_index;
                        }else{
                          $endIndex = $harvest_date_index;
                        }
                        for ($i = 0; $i < 10; $i++) {
                            if (empty($result_arr[$start_date_index][$i])) {
                                for ($j = $start_date_index; $j <= $endIndex; $j++) {
                                    if (!empty($result_arr[$j][$i])) {
                                        $flag = 1;
                                        break;
                                    }
                                }
                                if ($flag == 0) {
                                    break;
                                }
                            }
                        }
                        /*for ($i = 0; $i < 10; $i++) {
                            if (empty($result_arr[$start_date_index][$i])) {
                                for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                                    if (!empty($result_arr[$j][$i])) {
                                        $flag = 1;
                                        break;
                                    }
                                }
                                if ($flag == 0) {
                                    break;
                                }
                            }
                        }*/
                        if($value->end_date != NULL){
                            for ($j = $start_date_index; $j <= $end_date_index; $j++) {
                                if (strtotime($inputData['min']) > strtotime($value->start_date) && strtotime($value->end_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($inputData['min'], $inputData['max']);
                                } elseif (strtotime($inputData['min']) > strtotime($value->start_date)) {
                                    $days_count = $this->getDuration($inputData['min'], $value->end_date);
                                } elseif (strtotime($value->end_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($value->start_date, $inputData['max']);
                                } else {
                                    $days_count = $this->getDuration($value->start_date, $value->end_date);
                                }
                                $value->duration = $days_count;
                                $value->start_date_index = $start_date_index;
                                $value->end_date_index = $end_date_index;
                                $value->harvest_date_index = $harvest_date_index;
                                
                                /*if($j >= $harvest_date_index && $j <= $end_date_index){
                                    $value->color = '#ccc';
                                }*/
                                $result_arr[$j][$i] = $value;
                            }
                        }else{
                            for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                                if (strtotime($inputData['min']) > strtotime($value->start_date) && strtotime($value->harvest_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($inputData['min'], $inputData['max']);
                                } elseif (strtotime($inputData['min']) > strtotime($value->start_date)) {
                                    $days_count = $this->getDuration($inputData['min'], $value->harvest_date);
                                } elseif (strtotime($value->harvest_date) > strtotime($inputData['max'])) {
                                    $days_count = $this->getDuration($value->start_date, $inputData['max']);
                                } else {
                                    $days_count = $this->getDuration($value->start_date, $value->harvest_date);
                                }
                                $value->duration = $days_count;
                                $value->start_date_index = $start_date_index;
                                $value->harvest_date_index = $harvest_date_index;
                                $result_arr[$j][$i] = $value;
                            }
                        }

                        /*for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                            if (strtotime($inputData['min']) > strtotime($value->start_date) && strtotime($value->end_date) > strtotime($inputData['max'])) {
                                $days_count = $this->getDuration($inputData['min'], $inputData['max']);
                            } elseif (strtotime($inputData['min']) > strtotime($value->start_date)) {
                                $days_count = $this->getDuration($inputData['min'], $value->end_date);
                            } elseif (strtotime($value->end_date) > strtotime($inputData['max'])) {
                                $days_count = $this->getDuration($value->start_date, $inputData['max']);
                            } else {
                                $days_count = $this->getDuration($value->start_date, $value->end_date);
                            }
                            $value->duration = $days_count;
                            $value->start_date_index = $start_date_index;
                            $value->harvest_date_index = $harvest_date_index;
                            $result_arr[$j][$i] = $value;
                        }*/
                    }
                }

                $responseData = array('field_cultures' => $result_arr, 'dataDuration' => $dataDuration);
                $this->response(['status' => TRUE,'data' => $responseData,'message' => 'Dashboard Data'], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Days count in between two dates
     */
    public function getDuration($start_date, $end_date)
    {
        if (!empty($start_date) && !empty($end_date)) {
            $startDate = strtotime($start_date);
            $endDate = strtotime($end_date);
            $datediff = $endDate - $startDate;

            $days_count = round($datediff / (60 * 60 * 24)) + 1;

            if (!empty($days_count)) {
                return ( int )$days_count;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Get Most Planted Crops
     */
    public function getUserMostPlantedCrops_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;

            if ($user_id) {
                $inputData = array(
                    'min' => $this->post('min'),
                    'max' => $this->post('max'),
                    'languageId' => $this->post('language'),
                );

                if (empty($inputData['min']) || empty($inputData['max'])) {
                    $inputData['min'] = date('Y-m-d', strtotime('-2 months'));
                    $inputData['max'] = date('Y-m-d', strtotime('+10 months'));
                } else {
                    $inputData['min'] = date('Y-m-d', strtotime($inputData['min']));
                    $inputData['max'] = date('Y-m-d', strtotime($inputData['max']));
                }

                if (!empty($inputData['languageId'])) {
                    $languageId = $inputData['languageId'];
                } else {
                    $languageId = 1;
                }

                $allCropIds = array();
                $MostPlantedCropIds = array();
                $LessPlantedCropIds = array();
                $LessPlantedCrops = array();
                $languageData = $this->commons->getLanguageById($languageId);

                $MostPlantedCrops = $this->dashboards->getUserMostPlantedCrops($user_id, $inputData, $languageData->symbol);
                // $AllCrops = $this->commons->getAlluserCrops($user_id, $languageData->symbol);
                $AllCrops = $this->dashboards->getUserAllPlantedCrops($user_id, $languageData->symbol);

                foreach ($MostPlantedCrops as $key => $value) {
                    array_push($MostPlantedCropIds, $value->crop_id);
                }

                foreach ($AllCrops as $key => $value) {
                    array_push($allCropIds, $value->crop_id);
                }

                $LessPlantedCropIds = array_diff($allCropIds, $MostPlantedCropIds);

                foreach ($AllCrops as $key => $value) {
                    if (in_array($value->crop_id, $LessPlantedCropIds)) {
                        array_push($LessPlantedCrops, $value);
                    }
                }

                $responseArr = array('AllCrops' => $AllCrops, 'MostPlantedCrops' => $MostPlantedCrops, 'LessPlantedCrops' => $LessPlantedCrops, 'MostPlantedCropIds' => $MostPlantedCropIds);

                $this->set_response(['status' => TRUE,'data' => $responseArr], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'data' => array(),'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Field Cultures Min And Max Date
     */
    public function getFieldCultureMinMaxDate_post()
    {
        try {
            $field_id = $this->post('field_id');
            if ($field_id) {
                $min_max_culture_Date = $this->dashboards->get_field_min_max_culture_Date($field_id);
                if (isset($min_max_culture_Date) && !empty($min_max_culture_Date['culture_max']) && !empty($min_max_culture_Date['culture_min'])) {
                    
                    $post = array('status' => TRUE,'data' => $min_max_culture_Date);
                } else {
                    $post = array('status' => FALSE,'data' => array(),'message' => 'no_data_found');
                }
            } else {
                $post = array('status' => FALSE,'data' => array(),'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Field Planned Crops
     */
    public function getUserPlannedFieldCrops_post()
    {
        try {
            $inputData = array(
                'range' => $this->post('range'),
                'field_id' => $this->post('field_id'),
                'languageId' => $this->post('language'),
            );

            if (empty($inputData['range']['min']) || empty($inputData['range']['max'])) {
                $inputData['min'] = date('Y-m-d', strtotime('-2 months'));
                $inputData['max'] = date('Y-m-d', strtotime('+10 months'));
            } else {
                $inputData['min'] = date('Y-m-d', strtotime($inputData['range']['min']));
                $inputData['max'] = date('Y-m-d', strtotime($inputData['range']['max']));
            }

            if (!empty($inputData['languageId'])) {
                $languageId = $inputData['languageId'];
            } else {
                $languageId = 1;
            }

            $languageData = $this->commons->getLanguageById($languageId);

            if ($inputData['field_id']) {
                $user_id = $this->api_token->ac_userId;
                $crops = $this->dashboards->getUserPlannedFieldCrops($user_id, $inputData, $languageData->symbol);

                if (isset($crops) && count($crops) > 0) {
                    $post = array('status' => TRUE,'data' => $crops);
                } else {
                    $post = array('status' => FALSE,'data' => array(),'message' => 'no_data_found');
                }
            } else {
                $post = array('status' => FALSE,'data' => array(),'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field 
     */
    public function archiveField_post()
    {
        try {
            $formData = array(
                'field_id' => $this->post('field_id'),
                'archiveStatus' => $this->post('archiveStatus')
            );

            if (!empty($formData['field_id'])) {
                $fieldID = $formData['field_id'];
                $archiveStatus = $formData['archiveStatus'];

                $archiveStatus = ($archiveStatus == 0) ? 1 : 0;
                $update = $this->dashboards->archieve_field($archiveStatus, $fieldID);

                if ($update) {
                    $post = array('status' => TRUE,'message' => 'archieved_success');
                } else {
                    $post = array('status' => FALSE,'message' => 'archieved_fail');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Delete field Details or culture
     */
    public function deleteField_post()
    {
        try {
            $fieldId = $this->post('fieldId');

            if (!empty($fieldId)) {
                $delete = $this->dashboards->deleteFieldById($fieldId);
                if ($delete) {
                    $post = array('status' => TRUE,'message' => 'delete_success');
                } else {
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Dashboard Data
     */
    public function getDashboardTopBoxesData_post()
    {
        try {
            $inputData = array(
                'languageId' => $this->post('language'),
            );

            $user_id = $this->api_token->ac_userId;

            if ($user_id) {
                if (!empty($inputData['languageId'])) {
                    $languageId = $inputData['languageId'];
                } else {
                    $languageId = 1;
                }
                $languageData = $this->commons->getLanguageById($languageId);

                $today = date('Y-m-d');
                $current_year_from = date('Y-01-01');
                $current_year_to = date('Y-12-31');
                $field_data = $this->dashboards->getFieldAreaAndCount($user_id, $today);
                $SizePerData = $this->dashboards->getSizePerCategoryData($user_id, $current_year_from, $current_year_to, $languageData->symbol);
                $CultureYears = $this->dashboards->getCultureYears($user_id);

                $responseArr = array('field_data' => $field_data, 'SizePerData' => $SizePerData, 'CultureYears' => $CultureYears);
                if (isset($field_data) && !empty($field_data) && isset($SizePerData) && !empty($SizePerData) && isset($CultureYears) && !empty($CultureYears)) {
                    
                    $post = array('status' => TRUE,'data' => $responseArr,'message' => 'Dashboard Data');
                    
                } else {
                    if (empty($field_data)) {
                        $field_data = array('field_count' => 0, 'field_total_size' => 0, 'swapped_field_count' => 0);
                    }
                    if (empty($SizePerData)) {
                        $SizePerData = array();
                    }
                    if (empty($CultureYears)) {
                        $CultureYears = array();
                    }
                    $responseArr = array('field_data' => $field_data, 'SizePerData' => $SizePerData, 'CultureYears' => $CultureYears);
                    $post = array('status' => TRUE,'data' => $responseArr,'message' => 'Dashboard Data');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Culture Years Data
     */
    public function getCultureYears_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            if ($user_id) {
                $CultureYears = $this->dashboards->getCultureYears($user_id);
                if ( !empty($CultureYears)) {
                    $post = array('status' => TRUE,'data' => $CultureYears,'message' => 'Dashboard Data');
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'Dashboard Data');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Dashboard Data
     */
    public function getsizePerCategoryByYear_post()
    {
        try {
            $inputData = array(
                'languageId' => $this->post('language'),
                'year' => $this->post('year'),
            );

            $user_id = $this->api_token->ac_userId;
            if ($user_id) {
                if (!empty($inputData['languageId'])) {
                    $languageId = $inputData['languageId'];
                } else {
                    $languageId = 1;
                }
                $languageData = $this->commons->getLanguageById($languageId);
                if (!empty($inputData['year'])) {
                    $current_year_from = date($inputData['year'] . '-01-01');
                    $current_year_to = date($inputData['year'] . '-12-31');
                } else {
                    $current_year_from = date('Y-01-01');
                    $current_year_to = date('Y-12-31');
                }

                $SizePerData = $this->dashboards->getSizePerCategoryData($user_id, $current_year_from, $current_year_to, $languageData->symbol);
                if (isset($SizePerData) && !empty($SizePerData)) {
                    $post = array('status' => TRUE,'data' => $SizePerData,'message' => 'Dashboard Data');
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'Dashboard Data');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get Dashboard Data
     */
    public function getSingleFieldDataExportByReportingYear_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $inputData = array(
                'min' => $this->post('min'),
                'max' => $this->post('max'),
                'fieldId' => $this->post('fieldId')
            );

            if (empty($inputData['min']) || empty($inputData['max'])) {
                $inputData['min'] = date('Y-m-d', strtotime('-2 months'));
                $inputData['max'] = date('Y-m-d', strtotime('+10 months'));
            } else {
                $inputData['min'] = date('Y-m-d', strtotime($inputData['min']));
                $inputData['max'] = date('Y-m-d', strtotime($inputData['max']));
            }

            if ($inputData['fieldId']) {
                $data = $this->dashboards->get_single_field_data($user_id, $inputData);
                if (!empty($data)) {
                    $post = array('status' => TRUE,'data' => $data,'message' => 'Dashboard Data');
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'Dashboard Data');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post , REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     */
    public function getFieldRemainingSpace_post()
    {
        try {
            $from = $this->post('from');
            $to = $this->post('to');
            $fieldId = $this->post('fieldId');

            if (!empty($fieldId) && !empty($from) && !empty($to)) {
                $from = date('Y-m-d', strtotime($from));
                $to = date('Y-m-d', strtotime($to));

                if ($from <= $to) {
                    $culturesInDuration = $this->dashboards->getFieldCulturesInDaterange($fieldId, $from, $to);
                    $fieldDetails = $this->dashboards->get_field($fieldId);

                    if (isset($culturesInDuration) && !empty($culturesInDuration)) {
                        $sizeArr = array();

                        if ($from == $to) {
                            $sizeArr[$from] = 0;
                            foreach ($culturesInDuration as $key => $value) {
                                $sizeArr[$from] += (int)$value->culture_size;
                            }
                        } else {
                            $period = new DatePeriod(
                                new DateTime($from),
                                new DateInterval('P1D'),
                                new DateTime($to)
                            );

                            foreach ($period as $pkey => $pvalue) {
                                $sizeArr[$pvalue->format('Y-m-d')] = 0;
                            }

                            foreach ($culturesInDuration as $key => $value) {
                                if (strtotime($value->start_date) < strtotime($from)) {
                                    $startDate = $from;
                                } else {
                                    $startDate = $value->start_date;
                                }

                                if (strtotime($value->end_date) > strtotime($to)) {
                                    $endDate = $to;
                                } else {
                                    $endDate = $value->end_date;
                                }

                                $period = new DatePeriod(
                                    new DateTime($startDate),
                                    new DateInterval('P1D'),
                                    new DateTime($endDate)
                                );

                                foreach ($period as $pkey => $pvalue) {
                                    $sizeArr[$pvalue->format('Y-m-d')] += (int)$value->culture_size;
                                }
                            }
                        }
                        $maxSize = max($sizeArr);
                        $remaining_space = ((int)$fieldDetails->size - (int)$fieldDetails->echo_size) - (int)$maxSize;
                    } else {
                        $remaining_space = ((int)$fieldDetails->size - (int)$fieldDetails->echo_size);
                    }
                    $post = array('status' => TRUE,'data' => $remaining_space,'message' => 'remaining_spaced');
                    
                } else {
                    $post = array('status' => FALSE,'message' => 'error_something_wrong');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     */
    public function deleteCulture_post()
    {
        try {
            $cultureId = $this->post('cultureId');
            $fieldId = $this->post('fieldId');
            if (!empty($cultureId)) {
                $sql = "SELECT CP.crop_family_id FROM `ac_culture` CL LEFT JOIN ac_crop CP ON CP.ac_cropId = CL.`crop_id`
                        WHERE CL.`ac_cultureId` = '".$cultureId."' ";
                $data = $this->dashboards->getRecordSql($sql);
                $family_id = $data[0]['crop_family_id'];

                $loginUserID = $this->api_token->ac_userId;
                $sql = "SELECT * FROM ac_field WHERE ac_fieldId =". $fieldId ." ";
                $fieldData = $this->db->query($sql);
                if($fieldData->num_rows() > 0)
                {
                    foreach ($fieldData->result() as $key => $value) {
                        $field_user_id = $value->user_id;
                    }

                    if($field_user_id == 1){
                        /// example field ///
                        $cultureSql = "SELECT * FROM `ac_culture` CI 
                                       LEFT JOIN ac_field FI ON FI.ac_fieldId = CI.`field_id`
                                       WHERE FI.user_id = 1 ORDER BY CI.`ac_cultureId` ";
                        $exampleData = $this->db->query($cultureSql);
                        if($exampleData->num_rows() > 0)
                        {
                            $i = 1;
                            foreach ($exampleData->result() as $key => $val) {
                                $ac_cultureId = $val->ac_cultureId;
                                if($ac_cultureId == $cultureId){
                                    $data = array('example_culture_'.$i => 2);
                                }
                                $i++;
                            }
                            $this->db->where('ac_userId', $loginUserID);
                            $this->db->update('ac_user', $data);
                        }               
                        $post = array('status' => TRUE,'message' => 'delete_success');
                    }else{

                        $delete = $this->dashboards->deleteCultureById($cultureId);
                        if ($delete) {
                            $allFields = $this->dashboards->getAllCropByFields($fieldId,$cultureId,$family_id);
                            if ($allFields && count($allFields) > 0) {
                                foreach($allFields as $key=>$value){
                                    $updateData = array(
                                        'fieldId' => $value->field_id,
                                        'cultureId' => $value->ac_cultureId,
                                        'delicate' => $value->crop_id,
                                        'dateFrom' => $value->start_date,
                                        'dateTo' => $value->end_date,
                                        'harvest_date' => $value->harvest_date,
                                        'cultivatedArea' => $value->size,
                                        'isSwapped' => $value->is_swapped,
                                        'swappedWith' => $value->swapped_with,
                                        'rule_breaked' => $value->rule_breaked,
                                        'plantVariety' => $value->plantVariety,
                                        'notes' => $value->culture_notes,
                                        'seed_volume' => $value->seed_volume,
                                        'harvest_volume' => $value->harvest_volume,
                                    );
                                    $checkStatus = $this->checkFieldCropRotation($updateData); 
                                    if(!empty($checkStatus)){
                                        $updateData['rule_breaked'] = 1;
                                    }else{
                                        $updateData['rule_breaked'] = 0;
                                    }
                                    $this->dashboards->update_culture($updateData, $updateData['cultureId']);
                                }
                            }
                            $post = array('status' => TRUE,'message' => 'delete_success');                    
                        } else {
                            $post = array('status' => FALSE,'message' => 'error_something_wrong');
                        }
                    }
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     PDF in Tabular
     */
    public function tabularExport_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $formData = array(
                'pdfType' => $this->post('pdfType'),
                'fieldIds' => $this->post('fieldIds'),
                'language' => $this->post('language'),
                'startDate' => $this->post('startDate'),
                'endDate' => $this->post('endDate'),
				'languageKey' => $this->post('languageKey')
            );

            $this->form_validation->set_data($formData);

            $this->form_validation->set_rules('fieldIds[]', 'Field to be exported', 'required|trim');
            $this->form_validation->set_rules('pdfType', 'Time period to be exported', 'required|trim');

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'individual') {
                $this->form_validation->set_rules('startDate', 'Start Date', 'required|trim');
                $this->form_validation->set_rules('endDate', 'End Date', 'required|trim|callback_checkExportDate');
            }

            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $this->response(['status' => FALSE,'message' => validation_errors()], REST_Controller::HTTP_OK);
            }

            if (!empty($formData['language'])) {
                $languageId = $formData['language'];
            } else {
                $languageId = 1;
            }
            $culture_array = array();

            $languageData = $this->commons->getLanguageById($languageId);

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'standard') {
                //$start_date = date('Y-01-01', strtotime('-6 years'));
                //$end_date = date("Y-12-31");
                $start_date = date('Y-01-01', strtotime('-7 years'));
                $end_date = date("Y-12-31", strtotime('-1 years'));
                $currentYear = date('Y');
            } else {
                $start_date = $formData['startDate'];
                $end_date = $formData['endDate'];
                $currentYear = date('Y',strtotime($start_date));
            }

            if (!empty($formData['fieldIds'])) {
                $fieldIds = implode("','", $formData['fieldIds']);
            }

            $data = $this->dashboards->get_export_data($user_id, $start_date, $end_date, $fieldIds, $languageData->symbol);

            if (!empty($data)) {
                $export_data = array();
                $field_ids = array();
                foreach ($data as $key => $value) {
                    $culture = array();
                    if (!in_array($value->field_id, $field_ids)) {
                        if (isset($culture_array) && !empty($culture_array)) {
                            $field['cultures'] =  $culture_array;
                        }

                        if (isset($field) && !empty($field)) {
                            array_push($export_data, $field);
                        }

                        $field = array();
                        $culture_array = array();
                        $field['field_id'] = $value->field_id;
                        $field['field_name'] = $value->field_name;
                        $field['field_size'] = $value->field_size;
                        $field['echo'] = $value->echo;
                        $field['echo_size'] = $value->echo_size;
                        $field['address'] = $value->address;
                        $field['city'] = $value->city;
                        $field['zipcode'] = $value->zipcode;
                        $field['archieve'] = $value->archieve;
                        $field['notes'] = $value->notes;

                        $field['cultures'] = array();
                        $sizeArr = array();
                        $sizeData = $this->dashboards->get_field_size_data($value->field_id);
                        if(!empty($sizeData)){
                            foreach ($sizeData as $sizekey => $sizevalue) {
                                $sizeArr[] = array(
                                    'field_id' => $sizevalue->field_id,
                                    'size_change_year' => $sizevalue->size_change_year,
                                    'field_size' => $sizevalue->field_size,
                                    'echo' => $sizevalue->echo,
                                    'echo_size' => $sizevalue->echo_size,
                                );
                                
                                if($currentYear == $sizevalue->size_change_year){
                                    $field['field_size'] = $sizevalue->field_size;
                                    $field['echo'] = $sizevalue->echo;
                                    $field['echo_size'] = $sizevalue->echo_size;
                                }
                            }
                        }
                        $field['field_size_data'] = $sizeArr;

                        array_push($field_ids, $value->field_id);
                    }

                    $culture['culture_id'] = $value->culture_id;
                    $culture['start_date'] = $value->start_date;
                    $culture['end_date'] = $value->end_date;
                    $culture['harvest_date'] = $value->harvest_date;
                    $culture['culture_size'] = $value->culture_size;
                    $culture['crop_id'] = $value->crop_id;
                    $culture['crop_name'] = $value->crop_name;
                    $culture['color'] = $value->color;
                    $culture['crop_family_id'] = $value->crop_family_id;
                    $culture['crop_family_name'] = $value->crop_family_name;
                    $culture['is_swapped'] = $value->is_swapped;
                    $culture['swapped_with'] = $value->swapped_with;
                    $culture['rule_breaked'] = $value->rule_breaked;

                    $days_count = $this->getDuration($value->start_date, $value->end_date);
                    $culture['duration'] = $days_count;
                    $culture['is_filter'] = '0';

                    array_push($culture_array, $culture);
                }

                if (isset($culture_array) && !empty($culture_array)) {
                    $field['cultures'] =  $culture_array;
                }

                if (isset($field) && !empty($field)) {
                    array_push($export_data, $field);
                }
            }

            $file_path = $this->generateTabularPdf($export_data, $formData);

            if (!empty($file_path)) {
                $added = $this->dashboards->add_export_detail($file_path, $user_id, "Tabular");

                $this->response(['status' => TRUE,'data' => $file_path,], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     PDF in graphicalExport
    */
    public function graphicalExport_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $formData = array(
                'pdfType' => $this->post('pdfType'),
                'fieldIds' => $this->post('fieldIds'),
                'language' => $this->post('language'),
                'startDate' => $this->post('startDate'),
                'endDate' => $this->post('endDate'),
				'languageKey' => $this->post('languageKey')
            );

            $this->form_validation->set_data($formData);

            $this->form_validation->set_rules('fieldIds[]', 'Field to be exported', 'required|trim');
            $this->form_validation->set_rules('pdfType', 'Time period to be exported', 'required|trim');

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'individual') {
                $this->form_validation->set_rules('startDate', 'Start Date', 'required|trim');
                $this->form_validation->set_rules('endDate', 'End Date', 'required|trim|callback_checkExportDate');
            }

            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $this->response(['status' => FALSE,'message' => validation_errors()], REST_Controller::HTTP_OK);
            }

            if (!empty($formData['language'])) {
                $languageId = $formData['language'];
            } else {
                $languageId = 1;
            }
            $culture_array = array();
            $languageData = $this->commons->getLanguageById($languageId);

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'standard') {
                //$start_date = date('Y-01-01', strtotime('-6 years'));
                //$end_date = date("Y-12-31");
                $start_date = date('Y-01-01', strtotime('-7 years'));
                $end_date = date("Y-12-31", strtotime('-1 years'));
                $currentYear = date('Y');
            } else {
                $start_date = $formData['startDate'];
                $end_date = $formData['endDate'];
                $currentYear = date('Y',strtotime($start_date));
            }

            if (!empty($formData['fieldIds'])) {
                $fieldIds = implode("','", $formData['fieldIds']);
            }

            $data = $this->dashboards->get_export_data($user_id, $start_date, $end_date, $fieldIds, $languageData->symbol);

            if (!empty($data)) {
                $export_data = array();
                $field_ids = array();
                foreach ($data as $key => $value) {
                    $culture = array();
                    if (!in_array($value->field_id, $field_ids)) {
                        if (isset($culture_array) && !empty($culture_array)) {
                            $field['cultures'] =  $culture_array;
                        }

                        if (isset($field) && !empty($field)) {
                            array_push($export_data, $field);
                        }

                        $field = array();
                        $culture_array = array();
                        $field['field_id'] = $value->field_id;
                        $field['field_name'] = $value->field_name;
                        $field['field_size'] = $value->field_size;
                        $field['echo'] = $value->echo;
                        $field['echo_size'] = $value->echo_size;
                        $field['address'] = $value->address;
                        $field['city'] = $value->city;
                        $field['zipcode'] = $value->zipcode;
                        $field['archieve'] = $value->archieve;
                        $field['notes'] = $value->notes;

                        $field['cultures'] = array();

                        $sizeArr = array();
                        $sizeData = $this->dashboards->get_field_size_data($value->field_id);
                        if(!empty($sizeData)){
                            foreach ($sizeData as $sizekey => $sizevalue) {
                                $sizeArr[] = array(
                                    'field_id' => $sizevalue->field_id,
                                    'size_change_year' => $sizevalue->size_change_year,
                                    'field_size' => $sizevalue->field_size,
                                    'echo' => $sizevalue->echo,
                                    'echo_size' => $sizevalue->echo_size,
                                );
                                //$currentYear = date('Y',strtotime($start_date));
                                if($currentYear == $sizevalue->size_change_year){
                                    $field['field_size'] = $sizevalue->field_size;
                                    $field['echo'] = $sizevalue->echo;
                                    $field['echo_size'] = $sizevalue->echo_size;
                                }
                            }
                        }
                        $field['field_size_data'] = $sizeArr;
                        array_push($field_ids, $value->field_id);
                    }

                    $culture['culture_id'] = $value->culture_id;
                    $culture['start_date'] = $value->start_date;
                    $culture['end_date'] = $value->end_date;
                    $culture['harvest_date'] = $value->harvest_date;
                    $culture['culture_size'] = $value->culture_size;
                    $culture['crop_id'] = $value->crop_id;
                    $culture['crop_name'] = $value->crop_name;
                    $culture['color'] = $value->color;
                    $culture['crop_family_id'] = $value->crop_family_id;
                    $culture['crop_family_name'] = $value->crop_family_name;
                    $culture['is_swapped'] = $value->is_swapped;
                    $culture['swapped_with'] = $value->swapped_with;
                    $culture['rule_breaked'] = $value->rule_breaked;

                    $days_count = $this->getDuration($value->start_date, $value->end_date);
                    $culture['duration'] = $days_count;
                    $culture['is_filter'] = '0';

                    array_push($culture_array, $culture);
                }

                if (isset($culture_array) && !empty($culture_array)) {
                    $field['cultures'] =  $culture_array;
                }

                if (isset($field) && !empty($field)) {
                    array_push($export_data, $field);
                }
            }

            $file_path = $this->generateGraphicalPdf($export_data, $formData);

            if (!empty($file_path)) {
                $added = $this->dashboards->add_export_detail($file_path, $user_id, "Graphical");

                $this->response(['status' => TRUE,'data' => $file_path,], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     */
    public function chartExportCheck_post()
    {
        try {
            $formData = array(
                'pdfType' => $this->post('pdfType'),
                'fieldIds' => $this->post('fieldIds'),
                'language' => $this->post('language'),
                'startDate' => $this->post('startDate'),
                'endDate' => $this->post('endDate')
            );

            $this->form_validation->set_data($formData);

            $this->form_validation->set_rules('fieldIds[]', 'Field to be exported', 'required|trim');
            $this->form_validation->set_rules('pdfType', 'Time period to be exported', 'required|trim');

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'individual') {
                $this->form_validation->set_rules('startDate', 'Start Date', 'required|trim');
                $this->form_validation->set_rules('endDate', 'End Date', 'required|trim|callback_checkExportDate');
            }

            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run($this) === FALSE) {
                $this->response(['status' => FALSE,'message' => validation_errors()], REST_Controller::HTTP_OK);
            } else {
                $this->response(['status' => TRUE,'message' => 'success'], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Register field Details or culture
     */
    public function getchartExportData_post()
    {
        try {
            $user_id = $this->api_token->ac_userId;
            $formData = array(
                'pdfType' => $this->post('pdfType'),
                'fieldIds' => $this->post('fieldIds'),
                'language' => $this->post('language'),
                'startDate' => $this->post('startDate'),
                'endDate' => $this->post('endDate')
            );

            if (!empty($formData['language'])) {
                $languageId = $formData['language'];
            } else {
                $languageId = 1;
            }
            $culture_array = array();
            $languageData = $this->commons->getLanguageById($languageId);

            if (!empty($formData['pdfType']) && $formData['pdfType'] == 'standard') {
                $start_date = date('Y-01-01', strtotime('-6 years'));
                $end_date = date("Y-12-31");
            } else {
                $start_date = $formData['startDate'];
                $end_date = $formData['endDate'];
            }

            if (!empty($formData['fieldIds'])) {
                $fieldIds = implode("','", $formData['fieldIds']);
            }

            $dataDuration = $this->getDuration($start_date, $end_date);

            $data = $this->dashboards->get_export_data($user_id, $start_date, $end_date, $fieldIds, $languageData->symbol);

            if (!empty($data)) {
                $export_field_data = array();
                $field_ids = array();
                foreach ($data as $key => $value) {
                    $culture = array();
                    if (!in_array($value->field_id, $field_ids)) {
                        if (isset($culture_array) && !empty($culture_array)) {
                            $field['cultures'] =  $culture_array;
                        }

                        if (isset($field) && !empty($field)) {
                            array_push($export_field_data, $field);
                        }

                        $field = array();
                        $culture_array = array();
                        $field['field_id'] = $value->field_id;
                        $field['field_name'] = $value->field_name;
                        $field['field_size'] = $value->field_size;
                        $field['echo'] = $value->echo;
                        $field['echo_size'] = $value->echo_size;
                        $field['address'] = $value->address;
                        $field['city'] = $value->city;
                        $field['zipcode'] = $value->zipcode;
                        $field['archieve'] = $value->archieve;
                        $field['notes'] = $value->notes;

                        $field['cultures'] = array();
                        array_push($field_ids, $value->field_id);
                    }

                    $culture['culture_id'] = $value->culture_id;
                    $culture['start_date'] = $value->start_date;
                    $culture['harvest_date'] = $value->harvest_date;
                    $culture['end_date'] = $value->end_date;
                    $culture['culture_size'] = $value->culture_size;
                    $culture['crop_id'] = $value->crop_id;
                    $culture['crop_name'] = $value->crop_name;
                    $culture['color'] = $value->color;
                    $culture['crop_family_id'] = $value->crop_family_id;
                    $culture['crop_family_name'] = $value->crop_family_name;
                    $culture['is_swapped'] = $value->is_swapped;
                    $culture['swapped_with'] = $value->swapped_with;
                    $culture['rule_breaked'] = $value->rule_breaked;

                    $days_count = $this->getDuration($value->start_date, $value->end_date);
                    $culture['duration'] = $days_count;
                    $culture['is_filter'] = '0';

                    array_push($culture_array, $culture);
                }

                if (isset($culture_array) && !empty($culture_array)) {
                    $field['cultures'] =  $culture_array;
                }

                if (isset($field) && !empty($field)) {
                    array_push($export_field_data, $field);
                }
            }

            if (!empty($data)) {
                $export_data = array();
                $row_count = $data[0]->max_culture_count;
                $field_ids = array();

                $result_arr = array_fill(0, $dataDuration,  array_fill(0, $row_count, 0));
                $field_count = 0;
                foreach ($data as $key => $value) {
                    $flag = 0;
                    $i = 0;
                    $start_date_index = $this->getDuration($start_date, $value->start_date) - 1;
                    $harvest_date_index = $this->getDuration($start_date, $value->harvest_date) - 1;

                    if ($field_count == 0) {
                        $temp1 = $value->field_id;
                        $field_count++;
                    }
                    $temp2 = $value->field_id;

                    if ($temp1 != $temp2) {
                        $export_data[$temp1] = $result_arr;
                        $temp1 = $temp2;
                        $result_arr = array_fill(0, $dataDuration,  array_fill(0, $row_count, 0));
                    }

                    if ($start_date_index < 0) {
                        $start_date_index = 0;
                    }

                    if ($harvest_date_index > ($dataDuration - 1)) {
                        $harvest_date_index = $dataDuration - 1;
                    }

                    for ($i = 0; $i < $row_count; $i++) {
                        $flag = 0;
                        if (empty($result_arr[$start_date_index][$i])) {
                            for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                                if (!empty($result_arr[$j][$i])) {
                                    $flag = 1;
                                    break;
                                }
                            }
                            if ($flag == 0) {
                                break;
                            }
                        }
                    }

                    for ($j = $start_date_index; $j <= $harvest_date_index; $j++) {
                        if (strtotime($start_date) > strtotime($value->start_date) && strtotime($value->end_date) > strtotime($end_date)) {
                            $days_count = $this->getDuration($start_date, $end_date);
                        } elseif (strtotime($start_date) > strtotime($value->start_date)) {
                            $days_count = $this->getDuration($start_date, $value->end_date);
                        } elseif (strtotime($value->end_date) > strtotime($end_date)) {
                            $days_count = $this->getDuration($value->start_date, $end_date);
                        } else {
                            $days_count = $this->getDuration($value->start_date, $value->end_date);
                        }
                        $value->duration = $days_count;
                        $value->start_date_index = $start_date_index;
                        $value->harvest_date_index = $harvest_date_index;
                        $result_arr[$j][$i] = $value;
                    }
                    if (!in_array($value->field_id, $field_ids)) {
                        array_push($field_ids, $value->field_id);
                    }
                }
                $export_data[$temp1] = $result_arr;
            }

            if (!empty($export_field_data) && !empty($export_data)) {
                $responseData = array('export_field_data' => $export_field_data, 'export_data' => $export_data);

                $post = array('status' => TRUE,'data' => $responseData,);
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Subscription Plans
     */
    public function getusersAllFields_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;

            if ($user_id) {
                $today = date('Y-m-d');
                $userFields = $this->dashboards->getusersAllFields($user_id, $today);

                if ($userFields && count($userFields) > 0) {
                    $post = array('status' => TRUE,'data' => $userFields);
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get all Subscription Plans
     */
    public function getusersAllExports_get()
    {
        try {
            $user_id = $this->api_token->ac_userId;

            if ($user_id) {
                $userExports = $this->dashboards->getusersAllExports($user_id);
                
                if ($userExports && count($userExports) > 0) {
                    $post = array('status' => TRUE,'data' => $userExports);
                } else {
                    $post = array('status' => TRUE,'data' => array(),'message' => 'no_data_found');
                }
            } else {
                $post = array('status' => FALSE,'message' => 'error_something_wrong');
            }
            $this->response($post, REST_Controller::HTTP_OK);

        } catch (Exception $e) {
            $this->response(['status' => FALSE,'message' => 'error_something_wrong'], REST_Controller::HTTP_OK);
        }
    }

    public function checkExportDate()
    {
        $startDate = strtotime($this->post('startDate'));
        $endDate = strtotime($this->post('endDate'));

        if ($endDate >= $startDate)
            return True;
        else {
            $this->form_validation->set_message('checkExportDate', 'Start Date should be less than End Date.');
            return False;
        }
    }

    public function generateGraphicalPdf($export_data, $formData)
    {
        $user_data = $this->api_token;
        $user_id = $user_data->ac_userId;

        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Agricontrol');
        $pdf->SetTitle('Export Report');
        $pdf->SetSubject('Agricontrol');
        $pdf->SetKeywords('Agricontrol');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 0, 0), array(255, 255, 255));
        $pdf->footerText = ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        ///$pdf->SetMargins(18, 36, 18, true);
        $pdf->SetMargins(20, 10, 20, true);
        $pdf->SetHeaderMargin(12);
        $pdf->SetFooterMargin(18);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setCellPadding(0);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->AddPage();
        // $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        if (!empty($formData['pdfType']) && $formData['pdfType'] == 'standard') {
            $title = "Standard";
            //$start_date = date('01.01.Y', strtotime('-6 years'));
            //$end_date = date("31.12.Y");

            $start_date = date('01.01.Y', strtotime('-7 years'));
            $end_date = date("31.12.Y", strtotime('-1 years'));
            $duration = $start_date. ' - ' . $end_date;
            //$duration = date('d.m.Y', strtotime('-6 years')) . ' - ' . date("d.m.Y");
            $startYear = date('Y', strtotime($start_date));
            $endYear = date("Y", strtotime($end_date));
        } else {
            $title = "Individual";
            $duration = date('d.m.Y', strtotime($formData['startDate'])) . ' - ' . date('d.m.Y', strtotime($formData['endDate']));
            $startYear = date('Y', strtotime($formData['startDate']));
            $endYear = date('Y', strtotime($formData['endDate']));
        }

        $duration_text = 'Duration';
        $generated = 'Generated';
        $name = 'Name';
        $address = 'Address';
        $location = 'Location';
        $field_size = 'Field Size';
        $echo_size = 'Echo Size';
        $crop = 'Crop';
        $culture_size = 'Culture Size';
        $duration_table = 'Duration';
        $no_data = "No Data Found.";
            
        if (!empty($formData['languageKey']) ) {
			$file = $formData['languageKey'];
		}else{
            $file = 'en';
        }
        
        $filename = $file.".json";
        $url = LOCAL_JSON_FILE_PATH.$filename;
        $str = file_get_contents($url);//get contents of your json file and store it in a string
        $languageArr = json_decode($str, true);
        if(!empty($languageArr)){
            //foreach($languageArr as $langKey=> $langData){
                $duration_text = $languageArr['duration'];
                $generated = $languageArr['generated'];
                $name = $languageArr['name'];
                $address = $languageArr['address'];
                $location = $languageArr['location'];
                $echo_size = $languageArr['echo_size'];
                $crop = $languageArr['crop'];
                $culture_size = $languageArr['culture_size'];
                $no_data = $languageArr['no_data_found'];
                $duration_table = $languageArr['duration_table'];
                $field_size = $languageArr['field_size'];
            //}
        }
        $html = '<html>
                <head></head>
                <img style="" height="50" width="50" src="'.EXPORT_LOGO_PATH.'favicon.png" />
                        <b style="color:#606060;font-size:22px;">Agricontrol </b>';
        $html .= '<style>
                    body{
                        font-family: "Source Sans Pro", sans-serif;
                    }
                    table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                      font-size: 12px;
                    }
                    tr.noBorder td {
                      border: 0;
                      font-size: 12px;
                    }
                    .footertext{
                        position: absolute;
                        bottom: 0;
                        left: 0;
                        right: 0;
                        margin: 0 auto;
                    }
                </style>
                <body>
                <br/>
                <h3 style="padding-left: 10px; font-size: 16px; font-weight: bold;">' . $title . '</h3>
                &nbsp;

                <table style="font-size:12px; font-color:#343030; font-weight:500;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="font-size: 12px;">'.$duration_text.': ' . $duration . '</td>
                        <td style="font-size: 12px;">'.$name.': ' . ucfirst($user_data->name) . ' </td> 
                    </tr>
                    
                    <tr>
                        <td style="font-size: 12px;">'.$generated.': ' . date("d.m.Y") . '</td>
                        <td style="font-size: 12px;">'.$address.': ' . $user_data->street_name . '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">'.$location.': ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country) . '</td>
                    </tr>
                </table>
                <br /> <br /> <br />';
        if(!empty($export_data)){        
        foreach ($export_data as $key => $value) {
            $html .= '<div nobr="true" ><b>' . ucfirst($value["field_name"]) . ' </b>' . '<span style="font-weight: normal;">(' . $value["field_size"] . 'a, '.$echo_size.': ' . $value["echo_size"] . 'a)</span> 
                    <br /><br />
                    <table style="font-size: 12px;border-collapse: collapse;width:100%;">
                        <tr  class="noBorder">
                            <th style="width:20%;font-size: 12px;"></th>';
                            $html .='<td style="width:80%;border: none;"><table><tr>';
            for ($x = $startYear; $x <= $endYear; $x++) {
                $html .= '<th style="font-size: 12px;border-left: 1px solid #cdd0d4;border-right: 1px solid #cdd0d4;text-align:center;">' . $x . '</th>';
            }
            $html .='</tr></table></td>';
            $html .= '</tr>';
            if(!empty($value["cultures"])){
            foreach ($value["cultures"] as $ckey => $cvalue) {
                //if (!empty($cvalue["culture_id"])) {
                //if($cvalue["culture_id"]){    
                    $html .= '<br/><tr >
                                <td style="width:20%;font-size: 12px;"> ' . ucfirst($cvalue["crop_name"]) . '</td>';
                    $startDateCulture = date('Y', strtotime($cvalue["start_date"]));
                    $startMonthCulture = date('m', strtotime($cvalue["start_date"]));
                    
                    if($cvalue["end_date"] == null){
                        ///$harvestDateCulture = date('Y', strtotime($cvalue["harvest_date"]));
                        $endYearCulture = date('Y', strtotime($cvalue["harvest_date"]));
                        //$harvestMonthCulture = date('m', strtotime($cvalue["harvest_date"]));
                        $endMonthCulture = date('m', strtotime($cvalue["harvest_date"]));
                    }else{
                        ///$endDateCulture = date('Y', strtotime($cvalue["end_date"]));
                        $endYearCulture = date('Y', strtotime($cvalue["end_date"]));
                        ///$endMonthCulture = date('m', strtotime($cvalue["end_date"]));
                        $endMonthCulture = date('m', strtotime($cvalue["end_date"]));
                    }
                    $diff = (($endYear - $startYear) * 12) + (12 - 1);
                    $html .='<td style="width:80%;"><table><tr>';
                    
                    for ($x = $startYear; $x <= $endYear; $x++) {
                        $html .= '<td style="border-bottom:1px solid #cdd0d4;font-size: 12px;text-align:center;">';
                        if ($x == $startDateCulture) {
                            
                            $html .= '<table width="100%"><tr>';
                            $cropFieldCount = 0;
                            $startCount = $endCount= 0;
                            for($i=1;$i<=12;$i++){

                                $strlength = strlen($cvalue["culture_size"]);

                                if ($endYearCulture > $x && $startMonthCulture <= $i) {
                                    $html .= '<td style="background-color:' . $cvalue["color"] . ';color:#fff;"></td>';
                                    
                                } else if ($startMonthCulture <= $i && $endMonthCulture >= $i) {
                                    $html .= '<td style="background-color:' . $cvalue["color"] . ';color:#fff;"></td>';
                                    $cropFieldCount = $cropFieldCount+1;
                                } else {
                                    $html .= '<td></td>';
                                    if($cropFieldCount == 0){
                                        $startCount = $startCount+1;
                                    }else{
                                        $endCount = $endCount+1;
                                    }
                                }
                            }
                            $showFirstHalf = 0;
                            if($cropFieldCount < 1 && $startDateCulture != $endYearCulture ){
                                $firstYearDiff = (($startDateCulture - $startDateCulture) * 12) + (12 - $startMonthCulture)+1;
                                $secondYearDiff = (($endYearCulture - $endYearCulture) * 12) + ($endMonthCulture - 1)+1;

                                if($firstYearDiff >= $secondYearDiff ){
                                    if($startDateCulture == $x){
                                        $showFirstHalf = 1;
                                        
                                        if($firstYearDiff < 3){
                                            if($startCount > 0){
                                                if($strlength >= 3){
                                                    $startCount = $startCount - 2;
                                                }
                                            }
                                            $firstYearDiff = 4;
                                        }
                                    }
                                }
                            }else{
                                if($diff > 4){
                                    if($cropFieldCount <= 3){
                                        if($strlength >= 3){
                                            $startCount = $startCount - 2;
                                            if($diff > 5){
                                                $startCount = $startCount - 2;
                                            }
                                        }  
                                        $cropFieldCount = 4;
                                    }
                                }
                            }

                            if(($cropFieldCount < 1 || $cropFieldCount == '') && $startDateCulture == $endYear ){
                                if($diff > 4){
                                    if($strlength >= 3){
                                        $startCount = $startCount - 2;
                                        if($diff > 5){
                                            $startCount = $startCount - 2;
                                        }
                                    }  
                                    $cropFieldCount = 4;
                                }
                            }
                            $html .= '</tr><tr>';
                            if($strlength < 5){
                                if($startCount > 0){
                                    $html .= '<td colspan="'.$startCount.'"></td>';
                                }
                                if($cropFieldCount < 1 && $startDateCulture != $endYearCulture){
                                    if($showFirstHalf > 0){
                                        $html .= '<td colspan="'.$firstYearDiff.'" align="right" >'. $cvalue["culture_size"].'a</td>';
                                    }elseif(($cropFieldCount < 1 || $cropFieldCount == '') && $startDateCulture == $endYear ){
                                        
                                        $html .= '<td colspan="'.$cropFieldCount.'" align="right" >'. $cvalue["culture_size"].'a</td>';
                                    }
                                }else{
                                    $html .= '<td colspan="'.$cropFieldCount.'" align="center" >'. $cvalue["culture_size"].'a</td>';
                                }
                                if($endCount > 0){
                                    $html .= '<td colspan="'.$endCount.'"></td>';
                                }
                            }else{
                                if($showFirstHalf > 0){
                                    $html .= '<td colspan="12" align="right" >'. $cvalue["culture_size"].'a </td>';
                                }elseif($cropFieldCount > 0){
                                    $html .= '<td colspan="7" align="left" >'. $cvalue["culture_size"].'a </td>';
                                }else{
                                    $html .= '<td colspan="12" ></td>';
                                }
                            }
                            $html .= '</tr></table>';
                            
                        } else if ($x == $endYearCulture) {
                            $html .= '<table width="100%"><tr>';
                            for($j=1;$j<=12;$j++){
                                if ($endMonthCulture >= $j) {
                                    $html .= '<td style="background-color:' . $cvalue["color"] . ';color:#fff;"></td>';
                                } else {
                                    $html .= '<td></td>';
                                }
                            }

                            $strlength = strlen($cvalue["culture_size"]);
                            $html .= '</tr><tr>';
                            if($startDateCulture != $endYearCulture){
                                $diff = (($endYearCulture - $startDateCulture) * 12) + ($endMonthCulture - $startMonthCulture);
                                $firstYearDiff = (($startDateCulture - $startDateCulture) * 12) + (12 - $startMonthCulture)+1;
                                $secondYearDiff = (($endYearCulture - $endYearCulture) * 12) + ($endMonthCulture - 1)+1;

                                if($firstYearDiff <= $secondYearDiff){
                                    if($endYearCulture == $x){
                                        $html .= '<td colspan="12" align="left" >'. $cvalue["culture_size"].'a </td>';
                                    }else{
                                        $html .= '<td></td>';
                                    }
                                }else{
                                    $html .= '<td></td>';
                                }
                            }
                            $html .= '</tr></table>';
                            
                            
                        }
                        $html .= '</td>';
                    }
                    $html .='</tr></table></td>';
                    
                    $html .= '</tr>';
                    
                // } else {
                //     $html .= '<tr">
                //                 <td style="text-align: center; font-size: 12px;" colspan="8">'.$no_data.'</td>
                //             </tr>';
                //     break;
                // }
                
              }
            }else {
                $html .= '<tr">
                            <td style="text-align: center; font-size: 12px;" colspan="8">'.$no_data.'</td>
                        </tr>';
                break;
            }
            
            $html .= '</table></div>';
            
            if(count($value['field_size_data']) > 1){
            $html .= '<br /><br /><b> Field Size History </b><br />
                    <table  style="font-size: 12px;border-collapse: collapse;width:100%;" >
                        <tr>
                        <th style="font-size: 12px;"></th>';
                        foreach($value['field_size_data'] as $fkey => $fvalue) {
                            $html .= '<th style="font-size: 12px;border-left: 1px solid #cdd0d4;border-right: 1px solid #cdd0d4;text-align:center;">' . $fvalue["size_change_year"] . '</th>';
                        }   
                            
            $html .= '</tr>';
            $html .= '<tr style="font-color: #343030;">
                        <td style="font-size: 12px;">'.$field_size.'</td>';
                        foreach($value['field_size_data'] as $fkey => $fvalue) {
                            $html .= '<td style="border-bottom:1px solid #cdd0d4;font-size: 12px;text-align:center;">' . $fvalue["field_size"] . ' a</td>';
                        } 
            $html .= '</tr>';

            $html .= '<tr style="font-color: #343030;">
                        <td style="font-size: 12px;">'.$echo_size.'</td>';
                        foreach($value['field_size_data'] as $fkey => $fvalue) {
                            $html .= '<td style="border-bottom:1px solid #cdd0d4;font-size: 12px;text-align:center;">' . $fvalue["echo_size"] . ' a</td>';
                        } 
            $html .= '</tr>';
         
            $html .= '</table>';
            }

            if ($key < (count($export_data) - 1)) {
                $html .= '<br /><br /><br />';
            }
            
            
        }
        }  
        $html .='</body></html>';
        $footerText = '<span class = "footerText">' . ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . '</span>';
        
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->writeHTMLCell(0, 0, '', '', $footerText, 0, 1, 0, true, 'C', true);

        ob_end_clean();
        $current_time_stamp = time();
        $file_name = $current_time_stamp . '' . $user_id . '.pdf';
        $pdf->Output(FCPATH . 'uploads/exports/' . $file_name, 'F');

        //return 'http://localhost/agricontrol/uploads/exports/' . $file_name;
        //return 'https://agricontrol.app/dashboard/uploads/exports/' . $file_name;
        return EXPORT_PATH.$file_name;
    }

    public function generateTabularPdf($export_data, $formData)
    {
        $user_data = $this->api_token;
        $user_id = $user_data->ac_userId;

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Agricontrol');
        $pdf->SetTitle('Export Report');
        $pdf->SetSubject('Agricontrol');
        $pdf->SetKeywords('Agricontrol');

        $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 0, 0), array(255, 255, 255));
        $pdf->footerText = ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        ///$pdf->SetMargins(18, 36, 18, true);
        $pdf->SetMargins(20, 10, 20, true);
        $pdf->SetHeaderMargin(12);
        $pdf->SetFooterMargin(18);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->AddPage();
        //$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        if (!empty($formData['pdfType']) && $formData['pdfType'] == 'standard') {
            $title = "Standard";
            //$start_date = date('01.01.Y', strtotime('-6 years'));
            //$end_date = date("31.12.Y");
            $start_date = date('01.01.Y', strtotime('-7 years'));
            $end_date = date("31.12.Y", strtotime('-1 years'));
            $duration = $start_date. ' - ' . $end_date;
            //$duration = date('d.m.Y', strtotime('-6 years')) . ' - ' . date("d.m.Y");
        } else {
            $title = "Individual";
            $duration = date('d.m.Y', strtotime($formData['startDate'])) . ' - ' . date('d.m.Y', strtotime($formData['endDate']));
        }

        $duration_text = 'Duration';
        $generated = 'Generated';
        $name = 'Name';
        $address = 'Address';
        $location = 'Location';
        $echo_size = 'Echo Size';
        $crop = 'Crop';
        $culture_size = 'Culture Size';
        $duration_table = 'Duration';
        $no_data = "No Data Found.";
        $jan_text = 'January';
        $feb_text = 'February';
        $march_text = 'March';
        $april_text = 'April';
        $may_text = 'May';
        $jun_text = 'June';
        $july_text = 'July';
        $aug_text = 'August';
        $sep_text = 'September';
        $oct_text = 'October';
        $nov_text = 'November';
        $dec_text = 'December';
        
        if (!empty($formData['languageKey']) ) {
			$file = $formData['languageKey'];
		}else{
            $file = 'en';
        }
        
        $filename = $file.".json";
        $url = LOCAL_JSON_FILE_PATH.$filename;
        $str = file_get_contents($url);//get contents of your json file and store it in a string
        $languageArr = json_decode($str, true);
        if(!empty($languageArr)){
            //foreach($languageArr as $langKey=> $langData){
                $duration_text = $languageArr['duration'];
                $generated = $languageArr['generated'];
                $name = $languageArr['name'];
                $address = $languageArr['address'];
                $location = $languageArr['location'];
                $echo_size = $languageArr['echo_size'];
                $crop = $languageArr['crop'];
                $culture_size = $languageArr['culture_size'];
                $no_data = $languageArr['no_data_found'];
                $duration_table = $languageArr['duration_table'];
                $field_size = $languageArr['field_size'];
                $jan_text = $languageArr['january'];
                $feb_text = $languageArr['feburary'];
                $march_text = $languageArr['march'];
                $april_text = $languageArr['april'];
                $may_text = $languageArr['may'];
                $jun_text = $languageArr['june'];
                $july_text = $languageArr['july_full'];
                $aug_text = $languageArr['august'];
                $sep_text = $languageArr['september'];
                $oct_text = $languageArr['october'];
                $nov_text = $languageArr['november'];
                $dec_text = $languageArr['december'];
            //}
        }
        $html = '<html>
                <head></head>
                <img style="" height="50" width="50" src="'.EXPORT_LOGO_PATH.'favicon.png" />
                        <b style="color:#606060;font-size:22px;">Agricontrol </b>';

        $html .= '<style>
                    body{
                        font-family: "Source Sans Pro", sans-serif;
                    }
                    table.fieldTable, table.fieldTable > tr, table.fieldTable > tr > td {
                      border: 1px solid #666666;
                      font-size: 12px;
                      
                    }
                    tr.noBorder td {
                      border: 0;
                      font-size: 12px;
                    }
                    .footertext{
                        position: absolute;
                        bottom: 0;
                        left: 0;
                        right: 0;
                        margin: 0 auto;
                    }
                </style>
                <body>
                <br/>
                <h3 style="padding-left: 10px; font-size: 16px; font-weight: bold;">' . $title . '</h3>
                &nbsp;

                <table style="font-size: 12px; font-color: #343030; font-weight: 500;" cellpadding = "5" cellspacing="0">
                    <tr>
                        <td style="font-size: 12px;">'.$duration_text.': ' . $duration . '</td>
                        <td style="font-size: 12px;">'.$name.': ' . ucfirst($user_data->name) . ' </td> 
                    </tr>
                    
                    <tr>
                        <td style="font-size: 12px;">'.$generated.': ' . date("d.m.Y") . '</td>
                        <td style="font-size: 12px;">'.$address.': ' . $user_data->street_name . '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="font-size: 12px;">'.$location.': ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . ', ' . ucfirst($user_data->country) . '</td>
                    </tr>
                </table>
                <br /> <br /> <br />';

        if(!empty($export_data)){
        foreach ($export_data as $key => $value) {
            $html .= '<b>' . ucfirst($value["field_name"]) . ' </b>' . '<span style="font-weight: normal;">(' . $value["field_size"] . 'a, '.$echo_size.': ' . $value["echo_size"] . 'a)</span> 
                    <br /><br />
                    <table nobr="true" class="fieldTable" cellpadding="7" cellspacing="0" style="font-size: 12px;">
                        <tr class="noBorder">
                            <th style="background-color: #66bd66; color: #fff; width: 25%; font-size: 12px;"><b>'.$crop.'</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 25%; font-size: 12px;"><b>'.$culture_size.'</b></th>
                            <th style="background-color: #66bd66; color: #fff; width: 50%; font-size: 12px;"><b>'.$duration_table.'</b></th>
                        </tr>';

            foreach ($value["cultures"] as $ckey => $cvalue) {
                if (!empty($cvalue["culture_id"])) {

                    if($cvalue["end_date"] == null){
                        $cultureEnd = $cvalue["harvest_date"];
                    }else{
                        $cultureEnd = $cvalue["end_date"];
                    }

					$start_date_month = date('F', strtotime($cvalue["start_date"]));
					if($start_date_month == 'January'){
						$start_date_month = $jan_text;
					}
					if($start_date_month == 'February'){
						$start_date_month = $feb_text;
					}
					if($start_date_month == 'March'){
						$start_date_month = $march_text;
					}
					if($start_date_month == 'April'){
						$start_date_month = $april_text;
					}
					if($start_date_month == 'May'){
						$start_date_month = $may_text;
					}
					if($start_date_month == 'June'){
						$start_date_month = $jun_text;
					}
					if($start_date_month == 'July'){
						$start_date_month = $july_text;
					}
					if($start_date_month == 'August'){
						$start_date_month = $aug_text;
					}
					if($start_date_month == 'September'){
						$start_date_month = $sep_text;
					}
					if($start_date_month == 'October'){
						$start_date_month = $oct_text;
					}
					if($start_date_month == 'November'){
						$start_date_month = $nov_text;
					}
					if($start_date_month == 'December'){
						$start_date_month = $dec_text;
					}
					$end_date_month = date('F', strtotime($cultureEnd));
					if($end_date_month == 'January'){
						$end_date_month = $jan_text;
					}
					if($end_date_month == 'February'){
						$end_date_month = $feb_text;
					}
					if($end_date_month == 'March'){
						$end_date_month = $march_text;
					}
					if($end_date_month == 'April'){
						$end_date_month = $april_text;
					}
					if($end_date_month == 'May'){
						$end_date_month = $may_text;
					}
					if($end_date_month == 'June'){
						$end_date_month = $jun_text;
					}
					if($end_date_month == 'July'){
						$end_date_month = $july_text;
					}
					if($end_date_month == 'August'){
						$end_date_month = $aug_text;
					}
					if($end_date_month == 'September'){
						$end_date_month = $sep_text;
					}
					if($end_date_month == 'October'){
						$end_date_month = $oct_text;
					}
					if($end_date_month == 'November'){
						$end_date_month = $nov_text;
					}
					if($end_date_month == 'December'){
						$end_date_month = $dec_text;
					}
                    $html .= '<tr style="font-color: #343030;">
                                            <td style="width: 25%; font-size: 12px;"> ' . ucfirst($cvalue["crop_name"]) . '</td>
                                            <td style="width: 25%; font-size: 12px;">' . $cvalue["culture_size"] . ' a</td>
                                            <td style="width: 50%; font-size: 12px;"> ' . date('d', strtotime($cvalue["start_date"])) .' ' .$start_date_month. ' ' .date('Y', strtotime($cvalue["start_date"])).' – ' . date('d', strtotime($cultureEnd)) .' '.$end_date_month.' '.date('Y', strtotime($cultureEnd)). '</td>
                                        </tr>';
                } else {
                    $html .= '<tr style="font-color: #343030;">
                                                    <td style="text-align: center; font-size: 12px;" colspan="3">'.$no_data.'</td>
                                                </tr>';
                    break;
                }
            }

            $html .= '</table>';
            if(count($value['field_size_data']) > 1){
            $html .= '<br /><br /> Field Size History <br />
                    <table nobr="true" class="fieldTable" cellpadding="7" cellspacing="0" style="font-size: 12px;" >
                        <tr>
                            <th style="width: 25%;">Year</th>
                            <th style="width: 25%;">'.$field_size.'</th>
                            <th style="width: 50%;">'.$echo_size.'</th>
                        </tr>';
            foreach($value['field_size_data'] as $fkey => $fvalue) {
                $html .= '<tr style="font-color: #343030;">
                            <td style="width: 25%; font-size: 12px;"> ' . $fvalue["size_change_year"] . '</td>
                            <td style="width: 25%; font-size: 12px;">' . $fvalue["field_size"] . ' a</td>
                            <td style="width: 50%; font-size: 12px;"> ' . $fvalue["echo_size"]. ' a</td>
                        </tr>';
            }           
            $html .= '</table>';
            }


            if ($key < (count($export_data) - 1)) {
                $html .= '<br /><br /><br />';
            }
            $html .='</body></html>';

            $footerText = '<span class = "footerText">' . ucfirst($user_data->name) . ' | ' . $user_data->street_name . ' | ' . $user_data->postal_code . ' ' . ucfirst($user_data->city) . '</span>';
        }
        }
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->writeHTMLCell(0, 0, '', '', $footerText, 0, 1, 0, true, 'C', true);


        ob_end_clean();
        $current_time_stamp = time();
        $file_name = $current_time_stamp . '' . $user_id . '.pdf';
        $pdf->Output(FCPATH . 'uploads/exports/' . $file_name, 'F');

        //return 'http://localhost/agricontrol/uploads/exports/' . $file_name;
        //return 'https://agricontrol.app/dashboard/uploads/exports/' . $file_name;
        return EXPORT_PATH. $file_name;
    }
    
}
