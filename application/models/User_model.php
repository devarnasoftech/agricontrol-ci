<?php 
 class User_model extends CI_Model {
     
		function __construct() 
		{
			parent::__construct();
			$this->load->database();	
		}
		
		public function saveRecord($table,$data)
		{
			$this -> db -> insert($table,$data);
			$id = $this -> db -> insert_id();
			if ($id == 0) 
			{
				return 0; //if not inserted 
			} 
			else 
			{
				return $id; //if inserted
			}
		}
		public function getRecord($table,$where)
		{
			$query=$this->db->get_where($table,$where);
			$row=$query->result_array();
			if(!$row)
			{
				return 0;	
			}
			else 
			{
				return $row;
			}
		}
		public function getRecordSql($sql)
		{
			$query = $this->db->query($sql);
			$rows = $query->result_array();
			if(!$rows)
			{
				return 0;	
			}
			else 
			{
				return $rows;
			}
		}
		public function deleteRecord($table,$where) 
		{
			$this -> db -> delete($table, $where);
			if ($this -> db -> affected_rows() > 0) 
			{
				return 1;
			} 
			else 
			{
				return 0;
			}
		}
		public function updateRecord($table,$data,$where)
		{
			$this -> db -> where($where);
			$this -> db -> update($table, $data);
			//echo $this->db->last_query();die;
			return $afftectedRows = $this -> db -> affected_rows();
		}
		public function updateRecordSql($sql)
		{
			$query = $this->db->query($sql);
			$rows = $this -> db -> affected_rows();
			if(!$rows)
			{
				return 0;	
			}
			else 
			{
				return $rows;
			}
		}
		public function recordOrder($table,$data,$column,$order)
		{
			$this ->db-> order_by($column,$order);
			$query = $this -> db -> get_where($table,$data);
			$rows = $query -> result_array();
			if(!$rows)
			{
				return 0;
			}
			else 
			{
				return $rows;	
			}
	   }
	   public function addColumnSql($sql)
		{
			$query = $this->db->query($sql);
			//$rows = $query->result_array();
			if(!$query)
			{
				return 0;	
			}
			else 
			{
				return $query;
			}
		}
	   	    
 }
  
?>