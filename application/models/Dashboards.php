<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboards extends CI_Model {

    public function add_field($formData, $user_id)
    {
        $name       = $formData['name'];
        $size       = $formData['size'];
        $echo       = $formData['echo']; 
        $echoSize   = $formData['echoSize'];
        $address    = $formData['address'];
        $city       = $formData['city'];
        $zipcode    = $formData['zipcode'];
        $notes      = $formData['notes'];
        $nitrogen        = $formData['nitrogen'];
        $phosphorus      = $formData['phosphorus'];
        $potassium_oxide = $formData['potassium_oxide'];
        $magnesium       = $formData['magnesium'];
        $ph_value       = $formData['ph_value'];
        $soil_sample_date = $formData['soil_sample_date'];
        
        $data = array(
            'user_id'    => $user_id,
            'name'       => $name,
            'size'       => $size,
            'echo'       => ($echo != '')?$echo:'0',
            'echo_size'  => ($echoSize != '')?$echoSize:'0',
            'address'    => $address,
            'city'       => $city,
            'zipcode'    => $zipcode,
            'notes'      => $notes,
            'nitrogen'        => $nitrogen,
            'phosphorus'      => $phosphorus,
            'potassium_oxide' => $potassium_oxide,
            'magnesium'       => $magnesium,
            'ph_value'  => $ph_value,
            'soil_sample_date' => $soil_sample_date
        );
        
        $fieldAdd = $this->db->insert('ac_field', $data);
        if ($fieldAdd) 
        {
            $fieldID = $this->db->insert_id();
            $historydata = array(
                'user_id'    => $user_id,
                'field_id'   => $fieldID ,
                'size_change_year' => date('Y'),
                'field_size' => $size,
                'echo'       => ($echo != '')?$echo:'0',
                'echo_size'  => ($echoSize != '')?$echoSize:'0',
                'created'    => date('Y-m-d H:i:s')
            );
            $this->db->insert('ac_field_size_history', $historydata);
            return $fieldID;      
        }
        else
        {
            return FALSE;
        }
    }

    public function get_field($ID = '')
    {
        if(isset($ID) && $ID > 0)
        {
            $this->db->where('ac_fieldId', $ID);
            $query = $this->db->get('ac_field');
            $data = $query->row();

            $querystring = "SELECT * FROM `ac_field_size_history` WHERE `field_id` = '".$ID."' AND `size_change_year` = YEAR(CURDATE()) ";
            $sql = $this->db->query($querystring);
            $sizeData = $sql->result();
            if(!empty($sizeData)){
                foreach ($sizeData as $sizekey => $sizevalue) {
                    $data->size =  $sizevalue->field_size;
                    $data->echo =  $sizevalue->echo;
                    $data->echo_size =  $sizevalue->echo_size;
                }
            }
            return $data;
        }
        else
        {
            return FALSE;
        }
    }

    public function getusersAllFields($user_id, $today)
    {
        if(!empty($user_id) && !empty($today))
        {
            $querystring = "select acf.ac_fieldId as field_id, acf.name as field_name, acf.size as field_size, IFNULL(MAX(accu.is_swapped), 0) AS is_swapped
                from ac_field acf 
                Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND (accu.start_date <= '".$today."' AND accu.end_date >= '".$today."')
                where acf.user_id = ".$user_id." Group By acf.ac_fieldId order by acf.ac_fieldId";

            $query = $this->db->query($querystring);
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

    public function getusersAllExports($user_id)
    {
        if(!empty($user_id))
        {
            /*$querystring = "select * from ac_export_log acel 
                where acel.user_id = ".$user_id." order by acel.created_at DESC";
            */
            $querystring = "SELECT `ac_export_logId`,`user_id`,`file`,`export_type`,`sheet_data_type`,
            DATE_FORMAT(`created_at`, '%d.%m.%Y') as `created_at` FROM `ac_export_log` WHERE `user_id` = '".$user_id."' ORDER BY `ac_export_logId` DESC";

            $query = $this->db->query($querystring);
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

    public function update_field($formData, $Id,$user_id)
    {
        if(isset($formData) && !empty($formData) && isset($Id) && $Id > 0)
        {
            $data = array(
                'name'             => $formData['name'],
                //'size'             => $formData['size'],
                //'echo'             => ($formData['echo'] != '')?$formData['echo']:'0',
                //'echo_size'        => ($formData['echoSize'] != '')?$formData['echoSize']:'0',
                'address'          => $formData['address'],
                'city'             => $formData['city'],
                'zipcode'          => $formData['zipcode'],
                'notes'            => $formData['notes'],
                'nitrogen'         => $formData['nitrogen'],
                'phosphorus'       => $formData['phosphorus'],
                'potassium_oxide'  => $formData['potassium_oxide'],
                'magnesium'        => $formData['magnesium'],
                'ph_value'         => $formData['ph_value'],
                'soil_sample_date' => $formData['soil_sample_date'],
            );

            if($formData['old_field_size'] != $formData['size'] || $formData['old_echo_size'] != $formData['echoSize'])
            {
                $deletequery = "DELETE FROM `ac_field_size_history` WHERE `field_id`= '".$Id."' 
                                AND `size_change_year` = '".$formData['size_change_year']."' ";
                $this->db->query($deletequery);

                $historydata = array(
                    'user_id'          => $user_id,
                    'field_id'         => $Id,
                    'size_change_year' => $formData['size_change_year'],
                    'field_size'       => $formData['size'],
                    'echo'             => ($formData['echo'] != '')?$formData['echo']:'0',
                    'echo_size'        => ($formData['echoSize'] != '')?$formData['echoSize']:'0',
                    'created'          => date('Y-m-d H:i:s')
                );
                $this->db->insert('ac_field_size_history', $historydata); 
            }
            $this->db->where('ac_fieldId', $Id);
            return $this->db->update('ac_field', $data);
        }
        else
        {
            return FALSE;
        }
    }

    public function add_culture($formData, $fieldId)
    {
        $delicate        = $formData['delicate'];
        $cultivatedArea  = $formData['cultivatedArea'];
        $dateFrom        = $formData['dateFrom'];
        $dateTo          = $formData['dateTo'];        
        $harvest_date    = $formData['harvest_date'];        
        $isSwapped       = $formData['isSwapped'];        
        $swappedWith     = $formData['swappedWith']; 
        $plantVariety    = $formData['plantVariety']; 
        $notes           = $formData['notes']; 
        $seed_volume     = $formData['seed_volume']; 
        $harvest_volume  = $formData['harvest_volume']; 
        
        $data = array(
            'field_id'      => $fieldId,
            'size'          => $cultivatedArea,
            'crop_id'       => $delicate,
            'start_date'    => $dateFrom,
            'end_date'      => $dateTo,
            'harvest_date'  => $harvest_date,
            'is_swapped'    => ($isSwapped != '')?$isSwapped:'0',
            'swapped_with'  => $swappedWith,
            'rule_breaked'  => $formData['rule_breaked'],
            'plantVariety'  => $plantVariety,
            'culture_notes' => $notes,
            'seed_volume'   => $seed_volume,
            'harvest_volume'=> $harvest_volume
        );
        $this->add_user_crop($fieldId, $delicate);
        $cultureAdd = $this->db->insert('ac_culture', $data);
        if ($cultureAdd) 
        {
            $cultureID = $this->db->insert_id();
            return $cultureID;
        }
        else
        {
            return FALSE;
        }
    }

    public function update_culture($formData, $cultureId)
    {
        if(isset($formData) && !empty($formData) && isset($cultureId) && $cultureId > 0)
        {
            $data = array(
                'size'          => $formData['cultivatedArea'],
                'crop_id'       => $formData['delicate'],
                'start_date'    => $formData['dateFrom'],
                'end_date'      => $formData['dateTo'],
                'harvest_date'  => $formData['harvest_date'],
                'is_swapped'    => ($formData['isSwapped'] != '')?$formData['isSwapped']:'0',
                'swapped_with'  => $formData['swappedWith'],
                'rule_breaked'  => $formData['rule_breaked'],
                'plantVariety'  => $formData['plantVariety'],
                'culture_notes' => $formData['notes'],
                'seed_volume'   => $formData['seed_volume'],
                'harvest_volume'=> $formData['harvest_volume']
            );

			$this->db->select('field_id');
            $this->db->where('ac_cultureId', $cultureId);
            $queryfield= $this->db->get('ac_culture');
            $field = $queryfield->row();
            if(isset($field) && !empty($field)){
				$this->add_user_crop($field->field_id, $formData['delicate']);
				$this->db->where('ac_cultureId', $cultureId);
				return $this->db->update('ac_culture', $data);	
			}
			return FALSE;
        }
        else
        {
            return FALSE;
        }
    }

    public function getAllCropByFields($field_id,$cultureId,$family_id)
    {
        if(!empty($field_id) && !empty($cultureId))
        {
            //$querystring = "SELECT * FROM `ac_culture` WHERE `field_id` = '".$field_id."' AND `ac_cultureId` != '".$cultureId."' ";
            $querystring = "SELECT CL.*,CP.crop_family_id FROM `ac_culture` CL 
                            LEFT JOIN ac_crop CP ON CP.ac_cropId = CL.`crop_id`
                            WHERE CL.`field_id` = '".$field_id."' AND CP.crop_family_id = '".$family_id."' 
                            AND CL.`ac_cultureId` != '".$cultureId."' ";
            $query = $this->db->query($querystring);
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

	public function add_user_crop($fieldId, $crop_id){
		if(isset($fieldId) && !empty($fieldId) && isset($crop_id) && $crop_id > 0)
        {
			$this->db->select('user_id');
            $this->db->where('ac_fieldId', $fieldId);
            $queryfield= $this->db->get('ac_field');
            $field = $queryfield->row();
            if(isset($field) && !empty($field)){
				$this->db->select('ac_user_crop_color_id');
				$this->db->where('user_id',$field->user_id);
				$this->db->where('crop_id',$crop_id);
				$query= $this->db->get('ac_user_crop_color');
				if($query->num_rows() == 0){
					$this->db->select('color');
					$this->db->where('ac_cropId', $crop_id);
					$query2= $this->db->get('ac_crop');
					$crop = $query2->row();
					if(isset($crop) && !empty($crop)){
						$data = array(
							'crop_id' => $crop_id,
							'user_id' => $field->user_id,
							'color'   => $crop->color
						);
						$this->db->insert('ac_user_crop_color', $data);
					}
				}
			}
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

    public function get_dashboard_chart_data($user_id, $dateRange, $symbol,$quryLimit,$total_count,$page_no)
    {
        $fieldSql = $archiveWhere = ''; $str = '';

        $limitSql = "select DISTINCT(acf.ac_fieldId) as field_id
        from ac_field acf 
        Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$dateRange['min']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$dateRange['max']."') )
        where acf.user_id = ".$user_id." AND acf.archieve = 0 order by acf.ac_fieldId DESC ".$quryLimit." ";
        $limitData = $this->db->query($limitSql);
        if($limitData->num_rows() > 0)
        {
           foreach ($limitData->result() as $l_key => $l_value) {
            $str = $l_value->field_id.','.$str;
           }
           $str = trim($str,',');
        }
        if($str != ''){
            $numbersList = explode(',', $str);
            $startNumber = $numbersList[0];
            $lastNumber = end($numbersList);
            /*if($startNumber == $lastNumber || $total_count == $page_no){
                $archiveWhere = " AND (acf.ac_fieldId > ".$lastNumber." ) " ;
            }else{
                $archiveWhere = " AND acf.ac_fieldId BETWEEN ".$startNumber." AND ".$lastNumber." " ;
            }*/
            
            if($page_no == 1){
                if($total_count == $page_no){
                    $archiveWhere = "" ;
                }else{
                    $archiveWhere = " AND ( acf.ac_fieldId > ".$lastNumber." OR acf.ac_fieldId BETWEEN ".$startNumber." AND ".$lastNumber." )" ;
                }
            }elseif($page_no > 1){
                $prev_page_no = $page_no-1;
                $prev_start = 3 * ($prev_page_no - 1);
                $prev_quryLimit = "LIMIT ".$prev_start.',3';

                $PrevSql = "select DISTINCT(acf.ac_fieldId) as field_id from ac_field acf 
                Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$dateRange['min']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$dateRange['max']."') )
                where acf.user_id = ".$user_id." AND acf.archieve = 0 order by acf.ac_fieldId DESC ".$prev_quryLimit." ";
                $PrevData = $this->db->query($PrevSql);
                if($PrevData->num_rows() > 0)
                {
                    foreach ($PrevData->result() as $p_key => $p_value) {
                        $prevLastFieldID = $p_value->field_id;
                    }
                    $prevLastFieldID;
                    if($total_count == $page_no){
                        $archiveWhere = " AND ( acf.ac_fieldId < ".$lastNumber." OR (acf.ac_fieldId BETWEEN ".$lastNumber." AND ".$prevLastFieldID." ) )" ;
                    }else{
                        $archiveWhere = " AND (acf.ac_fieldId BETWEEN ".$lastNumber." AND ".$prevLastFieldID." )" ;
  
                    }
                }
            }

        }
        //return $archiveWhere;
        if($archiveWhere != ''){
            $archiveSql = "select DISTINCT(acf.ac_fieldId) as field_id from ac_field acf 
                           Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$dateRange['min']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$dateRange['max']."') )
                           where acf.user_id = ".$user_id." AND acf.archieve = 1 
                           ".$archiveWhere."  order by acf.ac_fieldId DESC ";
            $archiveData = $this->db->query($archiveSql);

            if($archiveData->num_rows() > 0)
            {
                foreach ($archiveData->result() as $a_key => $a_value) {
                    $str = $a_value->field_id.','.$str;
                }
            }
            $str = trim($str,',');
            $fieldSql = " AND acf.ac_fieldId IN ($str) ";
        }

        $querystring = "select acf.ac_fieldId as field_id,acf.user_id, acf.name as field_name, acf.size as field_size, acf.echo, acf.echo_size, acf.address, acf.city, acf.zipcode, acf.notes, acf.archieve,acf.nitrogen,acf.phosphorus,acf.potassium_oxide,acf.magnesium,
        acf.ph_value,acf.soil_sample_date,accu.nitrogen as c_nitrogen,accu.phosphorus as c_phosphorus,accu.potassium_oxide as c_potassium_oxide,accu.magnesium as c_magnesium,
        accu.start_date, accu.end_date,accu.harvest_date, accu.is_swapped, accu.swapped_with, accu.rule_breaked, accu.ac_cultureId as culture_id, accu.size as culture_size, accu.plantVariety, accu.culture_notes,accu.seed_volume,accu.harvest_volume, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name, 
         (select GROUP_CONCAT(saccu.start_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_start_date,
         (select GROUP_CONCAT(saccu.end_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_end_date,
         (select GROUP_CONCAT(saccu.harvest_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_harvest_date,
         (select GROUP_CONCAT(saccu.size) as culture_size from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_size,
         (select GROUP_CONCAT(sacc.name_".$symbol.") as crop_name from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_crop_name     
            from ac_field acf 
            Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$dateRange['min']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$dateRange['max']."') )
            Left Join ac_crop acc on acc.ac_cropId = accu.crop_id 
            Left Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id  
            Left Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            where acf.user_id = ".$user_id." ".$fieldSql." order by acf.ac_fieldId DESC ";

        $query = $this->db->query($querystring);
        $chart_data = $query->result();

        if($total_count == $page_no || $total_count == 0){
            $userID = $this->api_token->ac_userId;
            $sql = "SELECT * FROM ac_user WHERE ac_userId =". $userID ." ";
            $userData = $this->db->query($sql);
            if($userData->num_rows() > 0)
            {
            $userInfo = $userData->result();
            foreach ($userInfo as $key => $value) {
                    $example_field_status = $value->example_field_status;

                    if($example_field_status == 1 || $example_field_status == 2){

                        $created_at = $value->created_at;
                        $querystring1 = " select acf.ac_fieldId as field_id,acf.user_id, acf.name as field_name, acf.size as field_size, acf.echo, acf.echo_size, acf.address, acf.city, acf.zipcode, acf.notes, acf.archieve,acf.nitrogen,acf.phosphorus,acf.potassium_oxide,acf.magnesium,
                            acf.ph_value,acf.soil_sample_date,accu.nitrogen as c_nitrogen,accu.phosphorus as c_phosphorus,accu.potassium_oxide as c_potassium_oxide,accu.magnesium as c_magnesium,
                            accu.start_date, accu.end_date,accu.harvest_date, accu.is_swapped, accu.swapped_with, accu.rule_breaked, accu.ac_cultureId as culture_id, accu.size as culture_size, accu.plantVariety, accu.culture_notes,accu.seed_volume,accu.harvest_volume, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_en as crop_family_name, 
                            NULL as before_culture_start_date,NULL as before_culture_end_date,NULL as before_culture_harvest_date,
                            NULL as before_culture_size,NULL as before_culture_crop_name     
                            from ac_field acf 
                            Left Join ac_culture accu on accu.field_id = acf.ac_fieldId 
                            Left Join ac_crop acc on acc.ac_cropId = accu.crop_id 
                            Left Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id  
                            Left Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id."
                            where acf.user_id = 1 order by acf.ac_fieldId DESC";
                            $newquery = $this->db->query($querystring1);
                            //$newquery->result();
                            $i=1;
                            $exampleArr = array();
                            foreach ($newquery->result() as $key => $val) {
                                if($i == 1 && $value->example_culture_1 == 1){
                                    $val->soil_sample_date = date('Y-m-d',strtotime($created_at));
                                    $val->start_date = date('Y-m-d',strtotime($created_at));
                                    $val->end_date = date('Y-m-d',strtotime("+3 months",strtotime($created_at)));
                                    $exampleArr[] = $val;
                                }elseif($i == 2 && $value->example_culture_2 == 1){
                                    $val->soil_sample_date = date('Y-m-d',strtotime("+4 months",strtotime($created_at)));
                                    $val->start_date = date('Y-m-d',strtotime("+4 months",strtotime($created_at)));
                                    $val->end_date = date('Y-m-d',strtotime("+7 months",strtotime($created_at)));
                                    $exampleArr[] = $val;
                                }
                                if($example_field_status == 2){
                                    $val->archieve = 1;
                                }
                                $i++;
                                
                            }
                            $chart_data = array_merge($chart_data,$exampleArr);
                    }
                }
            }
        }
        ///$chart_data['total_data'] = $total_count;
        return $chart_data;
    }

    /* Convert hexdec color string to rgb(a) string */

    public function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if(empty($color))
            return $default; 

        //Sanitize $color if "#" is provided 
            if ($color[0] == '#' ) {
                $color = substr( $color, 1 );
            }

            //Check if color has 6 or 3 characters and get values
            if (strlen($color) == 6) {
                    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
            } elseif ( strlen( $color ) == 3 ) {
                    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
            } else {
                    return $default;
            }

            //Convert hexadec to rgb
            $rgb =  array_map('hexdec', $hex);

            //Check if opacity is set(rgba or rgb)
            if($opacity){
                if(abs($opacity) > 1)
                    $opacity = 1.0;
                $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
            } else {
                $output = 'rgb('.implode(",",$rgb).')';
            }

            //Return rgb(a) color string
            return $output;
    }

    public function get_field_size_data($field_id)
    {
        $querystring = "SELECT * FROM `ac_field_size_history` WHERE `field_id` = '".$field_id."' ORDER BY `size_change_year` ASC ";
        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getUserMostPlantedCrops($user_id, $dateRange, $symbol)
    {
        $querystring = "select count(acc.ac_cropId) as crop_count, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name 
            from ac_crop acc
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            Inner Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            Inner Join ac_culture accu on accu.crop_id = acc.ac_cropId
            Inner Join ac_field acf on acf.ac_fieldId = accu.field_id
            where acf.user_id = ".$user_id." AND ( (accu.start_date >= '".$dateRange['min']."' OR accu.end_date >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR accu.end_date <= '".$dateRange['max']."') ) Group By acc.ac_cropId Order By crop_count DESC Limit 0, 5";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getUserAllPlantedCrops($user_id, $symbol)
    {
        $querystring = "select count(acc.ac_cropId) as crop_count, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name 
            from ac_crop acc
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            Inner Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            Inner Join ac_culture accu on accu.crop_id = acc.ac_cropId
            Inner Join ac_field acf on acf.ac_fieldId = accu.field_id
            where acf.user_id = ".$user_id." Group By acc.ac_cropId Order By crop_count DESC";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getCultureYears($user_id)
    {
        $querystring = "select DISTINCT YEAR(accu.harvest_date) as years
            from ac_field acf
            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId
            where acf.user_id = ".$user_id." AND accu.harvest_date != 0 Order By accu.harvest_date DESC ";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getUserPlannedFieldCrops($user_id, $inputData, $symbol)
    {
        $querystring = "select count(acc.ac_cropId) as crop_count, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name 
            from ac_crop acc
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            Inner Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            Inner Join ac_culture accu on accu.crop_id = acc.ac_cropId
            Inner Join ac_field acf on acf.ac_fieldId = accu.field_id
            where acf.ac_fieldId = ".$inputData['field_id']." AND ( (accu.start_date >= '".$inputData['min']."' OR accu.end_date >= '".$inputData['min']."') AND (accu.start_date <= '".$inputData['max']."' OR accu.end_date <= '".$inputData['max']."') ) GROUP By acc.ac_cropId Order By acc.ac_cropId DESC";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getFieldAreaAndCount($user_id, $todayDate)
    {
        $querystring = "select count(acf.ac_fieldId) as field_count, sum(acf.size) as field_total_size, (select count(Distinct ssacf.ac_fieldId) from ac_field ssacf Inner Join ac_culture ssacc ON ssacc.field_id = ssacf.ac_fieldId where ssacf.user_id = ".$user_id." AND ssacc.is_swapped = 1 AND ssacf.archieve = 0 AND (ssacc.start_date <= '".$todayDate."' AND (CASE WHEN ssacc.end_date IS NULL THEN ssacc.harvest_date ELSE ssacc.end_date END) >= '".$todayDate."')) as swapped_field_count
            from ac_field acf
            where acf.user_id = ".$user_id ." AND acf.archieve = 0";

        $query = $this->db->query($querystring);
        return $query->row();
    }

    public function getCropWiseSize($user_id, $dateRange, $symbol)
    {
        $querystring = "select sum(accu.size) as culture_size, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acc.color
            from ac_field acf 
            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId
            Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id   
            where acf.user_id = ".$user_id." AND acf.archieve = 0 AND ( (accu.start_date >= '".$dateRange['min']."' OR accu.end_date >= '".$dateRange['min']."') AND (accu.start_date <= '".$dateRange['max']."' OR accu.end_date <= '".$dateRange['max']."') ) GROUP By acc.ac_cropId";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getSizePerCategoryData($user_id, $from, $to, $symbol)
    {
        $querystring = "select sum(accu.size) as culture_size, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acc.color
            from ac_field acf 
            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId
            Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id   
            where acf.user_id = ".$user_id." AND acf.archieve = 0 AND ((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$from."' AND (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$to."') GROUP By acc.ac_cropId order by culture_size DESC";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function get_field_min_max_culture_Date($field_id)
    {
        $querystring = "select min(start_date) as culture_min, max((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) as culture_max 
            from ac_culture accu
            Inner Join ac_field acf on acf.ac_fieldId = accu.field_id
            where acf.ac_fieldId = ".$field_id;
        $query = $this->db->query($querystring);
        return $query->row_array();
    }

    public function get_single_field_data($user_id, $input, $symbol)
    {
        $querystring = "select acf.ac_fieldId as field_id, acf.name as field_name, acf.size as field_size, acf.echo, acf.echo_size, acf.address, acf.zipcode, acf.archieve, accu.start_date, accu.end_date,accu.harvest_date, accu.is_swapped, accu.swapped_with, accu.rule_breaked, accu.ac_cultureId as culture_id, accu.size as culture_size, accu.plantVariety, accu.culture_notes,accu.seed_volume,accu.harvest_volume, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name,
          (select GROUP_CONCAT(saccu.start_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND acf.ac_fieldId = ".$input['fieldId']." AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_start_date,
         (select GROUP_CONCAT(saccu.end_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND acf.ac_fieldId = ".$input['fieldId']." AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_end_date,
         (select GROUP_CONCAT(saccu.harvest_date) from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND acf.ac_fieldId = ".$input['fieldId']." AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_harvest_date,
         (select GROUP_CONCAT(saccu.size) as culture_size from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND acf.ac_fieldId = ".$input['fieldId']." AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_size,
         (select GROUP_CONCAT(sacc.name_".$symbol.") as crop_name from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." AND saccu.crop_id = accu.crop_id AND acf.ac_fieldId = ".$input['fieldId']." AND sacf.ac_fieldId = acf.ac_fieldId AND saccu.start_date < accu.start_date Group By saccu.crop_id order by saccu.start_date desc ) as before_culture_crop_name
            from ac_field acf 
            Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$input['min']."' OR accu.end_date >= '".$input['min']."') AND (accu.start_date <= '".$input['max']."' OR accu.end_date <= '".$input['max']."') )
            Left Join ac_crop acc on acc.ac_cropId = accu.crop_id 
            Left Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id  
            Left Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            where acf.ac_fieldId = ".$input['fieldId']." order by accu.start_date";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function getFieldCulturesInDaterange($fieldId, $dateFrom = '', $dateTo = '', $cultureId = '')
    {
        $cultureIdWhere = '';
        $dateRangeWhere = '';
        if(!empty($cultureId)){
            $cultureIdWhere = ' AND accu.ac_cultureId != '.$cultureId;
        }
        if(!empty($dateFrom) && !empty($dateTo)){
            $dateRangeWhere = " AND ( (accu.start_date >= '".$dateFrom."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$dateFrom."') 
            AND (accu.start_date <= '".$dateTo."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$dateTo."') )";
        }
        $querystring = "select accu.start_date, accu.end_date,accu.harvest_date, accu.is_swapped, accu.ac_cultureId as culture_id, accu.size as culture_size,
        accu.nitrogen as c_nitrogen,accu.phosphorus as c_phosphorus,accu.potassium_oxide as c_potassium_oxide,accu.magnesium as c_magnesium from ac_field acf 
            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId
            where acf.ac_fieldId = ".$fieldId." ".$dateRangeWhere." ".$cultureIdWhere." order by accu.start_date";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function archieve_field($archiveStatus, $fieldID)
    {
        if(isset($archiveStatus) && isset($fieldID) && $fieldID > 0)
        {
            $loginUserID = $this->api_token->ac_userId;
            
            $sql = "SELECT * FROM ac_field WHERE ac_fieldId =". $fieldID ." ";
            $fieldData = $this->db->query($sql);
            if($fieldData->num_rows() > 0)
            {
                foreach ($fieldData->result() as $key => $value) {
                    $field_user_id = $value->user_id;
                }

                if($field_user_id == 1){
                    /// example field ///
                    $status = 1;
                    if($archiveStatus == 1){
                        $status = 2;
                    }
                    $data = array(
                        'example_field_status' => $status
                    );
                    $this->db->where('ac_userId', $loginUserID);
                    return $this->db->update('ac_user', $data);
                }else{
                    $data = array(
                        'archieve' => $archiveStatus
                    );
                    $this->db->where('ac_fieldId', $fieldID);
                    return $this->db->update('ac_field', $data);
                }
            }

            /*$data = array(
                'archieve' => $archiveStatus
            );
            $this->db->where('ac_fieldId', $fieldID);
            return $this->db->update('ac_field', $data);
            */
        }
        else
        {
            return FALSE;
        }
    }

    public function deleteFieldById($fieldId)
    {
        if(isset($fieldId) && !empty($fieldId))
        {
            $loginUserID = $this->api_token->ac_userId;
            
            $sql = "SELECT * FROM ac_field WHERE ac_fieldId =". $fieldId ." ";
            $fieldData = $this->db->query($sql);
            if($fieldData->num_rows() > 0)
            {
                foreach ($fieldData->result() as $key => $value) {
                    $field_user_id = $value->user_id;
                }

                if($field_user_id == 1){
                    /// example field ///
                    $data = array(
                        'example_field_status' => 3
                    );
                    $this->db->where('ac_userId', $loginUserID);
                    return $this->db->update('ac_user', $data);

                }else{
                    $this->db->where('field_id', $fieldId);
                    $this->db->delete('ac_culture');

                    $this->db->where('ac_fieldId', $fieldId);
                    $this->db->delete('ac_field');

                    $this->db->where('field_id', $fieldId);
                    $this->db->delete('ac_fertilizer_activity');

                    $this->db->where('field_id', $fieldId);
                    $this->db->delete('ac_plant_protection');

                    $this->db->where('field_id', $fieldId);
                    $this->db->delete('ac_soil_activity');
                }
            }
            /*$this->db->where('field_id', $fieldId);
            $this->db->delete('ac_culture');

            $this->db->where('ac_fieldId', $fieldId);
            $this->db->delete('ac_field');

            $this->db->where('field_id', $fieldId);
            $this->db->delete('ac_fertilizer_activity');

            $this->db->where('field_id', $fieldId);
            $this->db->delete('ac_plant_protection');

            $this->db->where('field_id', $fieldId);
            $this->db->delete('ac_soil_activity');
            */

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function GetCropAndFamilyCount($reportingFromDate, $reportingToDate, $formData, $crop_family_id)
    {
        $checkEndDate = $formData['dateTo']; 
        if($formData['dateTo'] == ''){
            $checkEndDate = $formData['harvest_date']; 
            $reportingToDate = $formData['harvest_date'];
        }
        $cultureDuration = $this->getDuration($formData['dateFrom'], $checkEndDate);

        $reportingFromDate = date('Y',strtotime($reportingFromDate));
        $reportingToDate = date('Y',strtotime($reportingToDate));
        $cultureIdWhere = '';
        $dateRangeWhere = '';
        if(!empty($formData['cultureId'])){
            $cultureIdWhere = ' AND accu.ac_cultureId != '.$formData['cultureId'];
        }
        $querystring = "select accf.max_cultivation_limit as crop_family_limit, acc.max_cultivation_limit as crop_limit, 
            (select count(accu.ac_cultureId) from ac_field acf Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (YEAR(accu.start_date) >= '".$reportingFromDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$reportingFromDate."') AND (YEAR(accu.start_date) <= '".$reportingToDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$reportingToDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) >= 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId) as crop_family_count, 
            (select count(accu.ac_cultureId) from ac_field acf Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id where acf.ac_fieldId = ".$formData['fieldId']." AND acc.ac_cropId = ".$formData['delicate']." AND ( (YEAR(accu.start_date) >= '".$reportingFromDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$reportingFromDate."') AND (YEAR(accu.start_date) <= '".$reportingToDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$reportingToDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) >= 98 ".$cultureIdWhere." Group By acc.ac_cropId) as crop_count
            from ac_crop acc 
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            where acc.ac_cropId = ".$formData['delicate']." AND accf.ac_crop_familyId = ".$crop_family_id;

        $query = $this->db->query($querystring);
        $data = $query->row();
        
        $shortculture = "select accf.max_cultivation_limit as crop_family_limit, acc.max_cultivation_limit as crop_limit, 
            (SELECT COUNT(*) FROM (select COUNT(*) as short_count, YEAR(accu.end_date) from ac_field acf Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (YEAR(accu.start_date) >= '".$reportingFromDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$reportingFromDate."') AND (YEAR(accu.start_date) <= '".$reportingToDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$reportingToDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) < 98 ".$cultureIdWhere." AND accf.ac_crop_familyId = 1 GROUP BY YEAR(accu.start_date) HAVING short_count >= 2) as total) as crop_family_count, 
            (select count(accu.ac_cultureId) from ac_field acf Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id where acf.ac_fieldId = ".$formData['fieldId']." AND acc.ac_cropId = ".$formData['delicate']." AND ( (YEAR(accu.start_date) >= '".$reportingFromDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$reportingFromDate."') AND (YEAR(accu.start_date) <= '".$reportingToDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$reportingToDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) < 98 ".$cultureIdWhere." Group By acc.ac_cropId) as crop_count
            from ac_crop acc 
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            where acc.ac_cropId = ".$formData['delicate']." AND accf.ac_crop_familyId = ".$crop_family_id;

        $shortquery = $this->db->query($shortculture);
        $shortdata = $shortquery->row();

        if($shortdata != ''){ 
            $shortFamily = $shortdata->crop_family_count;
            if($data != ''){
            $data->crop_family_count = $data->crop_family_count+$shortFamily;
            }else{
            $data = $shortdata;
            }
            /*if($shortFamily >= 2){
                $count = $shortFamily/2;
                if($data != ''){
                    $count = (int)$count;
                    $data->crop_family_count = $data->crop_family_count+$count; 
                }
            }*/
    
            $shortCrop = $shortdata->crop_count;
            if($data != ''){
                $count = $shortCrop/2;
                if($shortCrop >= 2){
                    $count = (int)$count;
                    $data->crop_count = $data->crop_count+$count; 
                }  
            }
        }
        return $data;
    }

    public function getDuration($start_date, $end_date)
    {
        if (!empty($start_date) && !empty($end_date)) {
            $startDate = strtotime($start_date);
            $endDate = strtotime($end_date);
            $datediff = $endDate - $startDate;

            $days_count = round($datediff / (60 * 60 * 24)) + 1;

            if (!empty($days_count)) {
                return ( int )$days_count;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function CheckFamilyCropInYear($startDate, $endDate, $formData, $crop_family_id)
    {
        if($formData['dateTo'] == ''){
            $endDate = $formData['harvest_date'];
        }
        $cultureDuration = $this->getDuration($formData['dateFrom'], $endDate);
        
        $startDate = date('Y',strtotime($startDate));
        $endDate = date('Y',strtotime($endDate));
        $cultureIdWhere = '';
        $dateRangeWhere = '';
        if(!empty($formData['cultureId'])){
            $cultureIdWhere = ' AND accu.ac_cultureId != '.$formData['cultureId'];
        }
        /*$querystring = "select count(accu.ac_cultureId) as culture_count 
                from ac_field acf 
                Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
                Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
                Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
                where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (accu.start_date >= '".$startDate."' OR accu.end_date >= '".$startDate."') AND (accu.start_date <= '".$endDate."' OR accu.end_date <= '".$endDate."') ) AND datediff(accu.end_date, accu.start_date) >= 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        */ 
        $querystring = "select count(accu.ac_cultureId) as culture_count 
        from ac_field acf 
        Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
        Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
        Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
        where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND 
        ((YEAR(accu.start_date) >= '".$startDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$startDate."') AND (YEAR(accu.start_date) <= '".$endDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$endDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) >= 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        
        $query = $this->db->query($querystring);
        $data = $query->row();
        
        /*$shortculture = "select count(accu.ac_cultureId) as culture_count 
                from ac_field acf 
                Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
                Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
                Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
                where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (accu.start_date >= '".$startDate."' OR accu.end_date >= '".$startDate."') AND (accu.start_date <= '".$endDate."' OR accu.end_date <= '".$endDate."') ) AND datediff(accu.end_date, accu.start_date) < 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        */
        $shortculture = "select count(accu.ac_cultureId) as culture_count 
        from ac_field acf 
        Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
        Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
        Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
        where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (YEAR(accu.start_date) >= '".$startDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$startDate."') 
        AND (YEAR(accu.start_date) <= '".$endDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$endDate."') ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) < 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        
        $shortquery = $this->db->query($shortculture);
        $shortdata = $shortquery->row();

        if($shortdata != ''){
            $culture = $shortdata->culture_count;
            //if($data != ''){
                if($cultureDuration >= 98){
                    if($culture >= 2){ 
                        if($data != ''){
                            $data->culture_count = $data->culture_count+1;
                        }else{
                            $data = $shortdata;
                            $data->culture_count = 1; 
                        } 
                    }
                }
                /*elseif($culture >= 2){
                    $data->culture_count = $data->culture_count+1;   
                }*/
            //}else{
                //$shortdata->culture_count = 1;
                /*$data = $shortdata;
                if($cultureDuration >= 98){
                    $data->culture_count = $culture; 
                }elseif($culture >= 2){
                    $data->culture_count =1;   
                }else{
                    $data->culture_count = '';
                }*/
            //}
            
        }
        return $data;
        
    }

    public function CheckFamilyCropInSameYear($startDate, $endDate, $formData, $crop_family_id)
    {
        if($formData['dateTo'] == ''){
            $endDate = $formData['harvest_date'];
        }
        $cultureDuration = $this->getDuration($formData['dateFrom'], $endDate);
        
        $startDate = date('Y',strtotime($startDate));
        $endDate = date('Y',strtotime($endDate));
        $cultureIdWhere = '';
        $dateRangeWhere = '';
        if(!empty($formData['cultureId'])){
            $cultureIdWhere = ' AND accu.ac_cultureId != '.$formData['cultureId'];
        }
        
        $shortculture = "select count(accu.ac_cultureId) as culture_count 
        from ac_field acf 
        Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
        Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
        Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
        where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (YEAR(accu.start_date) >= '".$startDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$startDate."') 
        AND (YEAR(accu.start_date) <= '".$endDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$endDate."') ) AND (accu.start_date <= '".$formData['dateFrom']."' ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) < 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        
        $shortquery = $this->db->query($shortculture);
        $shortdata = $shortquery->row();

        if($shortdata != ''){
            $culture = $shortdata->culture_count;
            if($cultureDuration <= 98){
                $shortdata->culture_count = '';
            }
        }else{
            $querystring = "select count(accu.ac_cultureId) as culture_count 
                            from ac_field acf 
                            Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
                            Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
                            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
                            where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND (  (YEAR(accu.start_date) >= '".$startDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$startDate."') 
                            AND (YEAR(accu.start_date) <= '".$endDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$endDate."') ) AND (accu.start_date <= '".$formData['dateFrom']."' ) AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) >= 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
            $query = $this->db->query($querystring);
            $data = $query->row();
            if($data != ''){
                $shortdata = $data;
            }
        }
        return $shortdata;
        
    }

    public function CheckFamilyShortCropInYear($startDate, $endDate, $formData, $crop_family_id)
    {
        if($formData['dateTo'] == ''){
            $endDate = $formData['harvest_date'];
        }
        $cultureDuration = $this->getDuration($formData['dateFrom'],$endDate);
        
        $startDate = date('Y',strtotime($startDate));
        $endDate = date('Y',strtotime($endDate));
        $cultureIdWhere = '';
        $dateRangeWhere = '';
        if(!empty($formData['cultureId'])){
            $cultureIdWhere = ' AND accu.ac_cultureId != '.$formData['cultureId'];
        }
        
        /*$shortculture = "select count(accu.ac_cultureId) as culture_count 
                from ac_field acf 
                Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
                Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
                Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
                where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (accu.start_date >= '".$startDate."' OR accu.end_date >= '".$startDate."') AND (accu.start_date <= '".$endDate."' OR accu.end_date <= '".$endDate."') ) AND datediff(accu.end_date, accu.start_date) < 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        */
        $shortculture = "select count(accu.ac_cultureId) as culture_count 
        from ac_field acf 
        Inner Join ac_culture accu on accu.field_id = acf.ac_fieldId 
        Inner Join ac_crop acc on acc.ac_cropId = accu.crop_id 
        Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
        where acf.ac_fieldId = ".$formData['fieldId']." AND accf.ac_crop_familyId = ".$crop_family_id." AND ( (YEAR(accu.start_date) >= '".$startDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) >= '".$startDate."') 
        AND (YEAR(accu.start_date) <= '".$endDate."' OR YEAR((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END)) <= '".$endDate."') ) 
        AND datediff((CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END), accu.start_date) < 98 ".$cultureIdWhere." Group By accf.ac_crop_familyId";
        
        $shortquery = $this->db->query($shortculture);
        $shortdata = $shortquery->row();
        
        return $shortdata;
    }

    public function deleteCultureById($cultureId)
    {
        if(isset($cultureId) && !empty($cultureId))
        {
            $this->db->where('ac_cultureId', $cultureId);
            $this->db->delete('ac_culture');
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function get_export_data($user_id, $start_date, $end_date, $fieldIds, $symbol)
    { 
        $querystring = "select acf.ac_fieldId as field_id, acf.name as field_name, acf.size as field_size, acf.echo, acf.echo_size, acf.address, acf.city, acf.zipcode, acf.archieve, acf.notes, accu.start_date, accu.end_date,accu.harvest_date, accu.is_swapped, accu.swapped_with, accu.rule_breaked, accu.ac_cultureId as culture_id, accu.size as culture_size, acc.ac_cropId as crop_id, acc.name_".$symbol." as crop_name, acucc.color, accf.ac_crop_familyId as crop_family_id, accf.name_".$symbol." as crop_family_name,
            (select count(saccu.ac_cultureId) as culture_count from ac_field sacf Left Join ac_culture saccu on saccu.field_id = sacf.ac_fieldId AND ( (saccu.start_date >= '".$start_date."' OR (CASE WHEN saccu.end_date IS NULL THEN saccu.harvest_date ELSE saccu.end_date END) >= '".$start_date."') AND (saccu.start_date <= '".$end_date."' OR (CASE WHEN saccu.end_date IS NULL THEN saccu.harvest_date ELSE saccu.end_date END) <= '".$end_date."') ) Left Join ac_crop sacc on sacc.ac_cropId = saccu.crop_id where sacf.user_id = ".$user_id." Group By sacf.ac_fieldId ORDER BY culture_count DESC LIMIT 1 ) as max_culture_count
            from ac_field acf 
            Left Join ac_culture accu on accu.field_id = acf.ac_fieldId AND ( (accu.start_date >= '".$start_date."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) >= '".$start_date."') AND (accu.start_date <= '".$end_date."' OR (CASE WHEN accu.end_date IS NULL THEN accu.harvest_date ELSE accu.end_date END) <= '".$end_date."') )
            Left Join ac_crop acc on acc.ac_cropId = accu.crop_id 
            Left Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id  
            Left Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = ".$user_id." 
            where acf.ac_fieldId IN ('".$fieldIds."') AND acf.archieve = 0 order by acf.ac_fieldId, accu.start_date";

        $query = $this->db->query($querystring);
        return $query->result();
    }

    public function add_export_detail($file, $user_id, $export_type)
    {        
        $data = array(
            'user_id'    => $user_id,
            'file'        => $file,
            'export_type' => $export_type,
            'sheet_data_type' => 0,
        );
        $add = $this->db->insert('ac_export_log', $data);
        if ($add) 
        {
            $id = $this->db->insert_id();
            return $id;      
        }
        else
        {
            return FALSE;
        }
    }

    public function updateRecord($table,$data,$where)
    {
        $this -> db -> where($where);
        $this -> db -> update($table, $data);
        //echo $this->db->last_query();die;
        return $afftectedRows = $this -> db -> affected_rows();
    }

    public function getRecordSql($sql)
    {
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        if(!$rows)
        {
            return 0;	
        }
        else 
        {
            return $rows;
        }
    }
        
}

