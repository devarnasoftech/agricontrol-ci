<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cropmasters extends CI_Model {

    public function change_crop_color($user_id, $color, $cropId)
    {
        if(isset($color) && isset($cropId) && $cropId > 0)
        {
            $data = array(
                'color' => $color
            );
            $this->db->where('user_id', $user_id);
            $this->db->where('crop_id', $cropId);
            return $this->db->update('ac_user_crop_color', $data);
        }
        else
        {
            return FALSE;
        }
    }

    public function GetCropFamilyIdByCrop($cropId)
    {
        if(isset($cropId) && $cropId > 0)
        {
            $this->db->select('crop_family_id');
            $this->db->where('ac_cropId', $cropId);
            $query = $this->db->get('ac_crop');
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }
}

