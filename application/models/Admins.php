<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends CI_Model {

    public function getAllUsers()
    {

        $updateRecord = 'update ac_subscription set is_active = 0 WHERE end < NOW()';
        $query = $this->db->query($updateRecord);

        $querystring = "select DISTINCT acu.ac_userId as user_id, acu.name, acu.email, acu.company, 
		acu.profile_picture, acu.phone, acu.street_name, acu.postal_code, acu.city, acu.country, acu.created_at, 
		acs.start as subscription_start, acs.end as subscription_end, acu.is_verified, acp.plan as subscription_name, acs.paymentOption,
		acp.amount as price, acp.currency_symbol, acp.duration, acs.method, acs.is_active, acs.comment, acLang.name as LanguageName,
		acs.ac_subscriptionId as subscription_id FROM ac_user acu 
		Inner JOIN (Select acu.ac_userId, ac_subscriptionId, ac_planId, start, end, method, paymentOption, is_active, comment from ac_subscription acs 
		Inner join (select ac_userId, max(created_date) as maxDate from ac_subscription group by ac_userId) 
		acu ON acu.ac_userId = acs.ac_userId AND acs.created_date = acu.maxDate) 
		acs on acs.ac_userId = acu.ac_userId 
		Inner Join ac_plans acp on acp.ac_planId = acs.ac_planId 
		Inner join ac_language acLang ON acLang.ac_languageId = acu.language
		where acu.user_type = 2 
		AND acu.is_deleted = 0 ORDER BY acu.ac_userId ASC";

        $query = $this->db->query($querystring);

        if($query->num_rows() > 0)
        {
           return $query->result();
        }
        else
        {
           return FALSE;
        }
    }

	public function getAllUsersCSV()
    {

        $updateRecord = 'update ac_subscription set is_active = 0 WHERE end < NOW()';
        $query = $this->db->query($updateRecord);

        $querystring = "select DISTINCT acu.ac_userId as ID, acu.name as Name, acu.email as Email, acu.company as Company, 
		acu.phone as Phone, acu.street_name as Street, acu.city as City, acu.country as Country, acu.postal_code as Postal, 
		acp.plan as Subscription_name, acs.start as subscription_start, acs.end as subscription_end, acs.paymentOption,
		acp.amount as Price, acs.method as Pay_Method, (CASE WHEN acu.is_verified = 1 THEN 'Active' ELSE 'Deactive' END) as Status, acLang.name as Language FROM ac_user acu 
		Inner JOIN (Select acu.ac_userId, ac_subscriptionId, ac_planId, start, end, method, is_active, paymentOption, comment from ac_subscription acs 
		Inner join (select ac_userId, max(created_date) as maxDate from ac_subscription group by ac_userId) 
		acu ON acu.ac_userId = acs.ac_userId AND acs.created_date = acu.maxDate) 
		acs on acs.ac_userId = acu.ac_userId 
		Inner Join ac_plans acp on acp.ac_planId = acs.ac_planId 
		Inner join ac_language acLang ON acLang.ac_languageId = acu.language
		where acu.user_type = 2 
		AND acu.is_deleted = 0 ORDER BY acu.ac_userId ASC";

        $query = $this->db->query($querystring);

        if($query->num_rows() > 0)
        {
           return $query->result();
        }
        else
        {
           return FALSE;
        }
    }

	public function getCSVAllUsers()
    {

        $updateRecord = 'update ac_subscription set is_active = 0 WHERE end < NOW()';
        $query = $this->db->query($updateRecord);

        $querystring = "select DISTINCT acu.ac_userId as user_id, acu.name, acu.email, acu.company, 
		acu.profile_picture, acu.phone, acu.street_name, acu.postal_code, acu.city, acu.country, acu.created_at, 
		acs.start as subscription_start, acs.end as subscription_end, acu.is_verified, acp.plan as subscription_name, 
		acp.amount as price, acp.currency_symbol, acp.duration, acs.method, acs.is_active, acs.comment, acLang.name as LanguageName,
		acs.ac_subscriptionId as subscription_id FROM ac_user acu 
		Inner JOIN (Select acu.ac_userId, ac_subscriptionId, ac_planId, start, end, method, is_active, comment from ac_subscription acs 
		Inner join (select ac_userId, max(created_date) as maxDate from ac_subscription group by ac_userId) 
		acu ON acu.ac_userId = acs.ac_userId AND acs.created_date = acu.maxDate) 
		acs on acs.ac_userId = acu.ac_userId 
		Inner Join ac_plans acp on acp.ac_planId = acs.ac_planId 
		Inner join ac_language acLang ON acLang.ac_languageId = acu.language
		where acu.user_type = 2 
		AND acu.is_deleted = 0 ORDER BY acu.ac_userId ASC";

        $query = $this->db->query($querystring);

        if($query->num_rows() > 0)
        {
           //return $query->result();
		   return $query->result_array();
        }
        else
        {
           return FALSE;
        }
    }

	public function getUserHistory($formData)
    {

        $updateRecord = 'update ac_subscription set is_active = 0 WHERE end < NOW()';
        $query = $this->db->query($updateRecord);

		$user_id = $formData["user_id"];

        $querystring = "select acu.ac_userId as user_id, acu.name, acu.email, acu.company, 
		acu.profile_picture, acu.phone, acu.street_name, acu.postal_code, acu.city, acu.country, 
		acu.created_at, acs.start as subscription_start, acs.end as subscription_end, acu.is_verified, 
		acp.plan as subscription_name, acp.amount as price, acp.currency_symbol, acp.duration, acs.method, 
		acs.is_active, acs.comment, acs.ac_subscriptionId as subscription_id, acs.created_date as action_date
		FROM ac_user acu Inner JOIN ac_subscription acs on acs.ac_userId = acu.ac_userId 
		Inner Join ac_plans acp on acp.ac_planId = acs.ac_planId 
		where acu.ac_userId = ".$user_id." AND acu.is_deleted = 0";

        $query = $this->db->query($querystring);

        if($query->num_rows() > 0)
        {
           return $query->result();
        }
        else
        {
           return FALSE;
        }
    }

    public function check_login_count($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->where('user_type', '1');
            $this->db->where('email', $email);
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password,  $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    return $query->num_rows();
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }   
    }

    public function get_login($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->select('ac_userId, company, profile_picture, language, password');
            $this->db->where('user_type', '1');
            $this->db->where('email', $email);
            $this->db->where('is_verified', 1);
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password, $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    unset($user->password);
                    return $user;
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }       
    }

    public function generate_token()
    {
        // Generate a random salt
        $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

        // If an error occurred, then fall back to the previous method
        if ($salt === FALSE)
        {
            $salt = hash('sha256', time() . mt_rand());
        }

        $new_token = substr($salt, 0, config_item('rest_key_length'));

        if (!empty($new_token)) {
            return $new_token;
        }
        else
        {
            return FALSE;
        }       
    }

    public function add_user_token($userId = '', $token = NULL)
    {
        if(!empty($userId))
        {
            $data = array(
                'user_id'     => $userId,
                'token'     => $token
            ); 
            $userAdd = $this->db->insert('ac_user_token', $data);

            return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }

    public function delete_user_token($userId = '', $token = NULL)
    {
        if(!empty($userId) && !empty($token))
        {
            $this->db->where('user_id', $userId);            
            $this->db->where('token', $token);            
            $this->db->delete('ac_user_token');

            return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }


    public function delete_user($id)
    {
        if($id)
        {
            $data = array(
                'is_deleted'   => 1,
            );

            $this->db->where('ac_userId', $id);
            return $this->db->update('ac_user', $data);
        }
        else
        {
           return FALSE;
        }
    }

	public function userActivation($status, $subscriptionId)
    {
        if($subscriptionId)
        {
			$isActive = $status == 0 ? 1 : 0;
            $data = array(
                'is_active'   => $isActive,
            );

            $this->db->where('ac_subscriptionId', $subscriptionId);
            return $this->db->update('ac_subscription', $data);
        }
        else
        {
           return FALSE;
        }
    }

    public function get_user($ID = '')
    {
        if(isset($ID) && $ID > 0)
        {
            $this->db->where('ac_userID', $ID);
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function check_email_unique($email, $user_id)
    {
        if(!empty($email) && !empty($user_id))
        {
            $this->db->where('email', $email);
            $this->db->where('ac_userId !=', $user_id);
            $query = $this->db->get('ac_user');

            if($query->num_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }        
    }

    public function upgrade_user_plan($formData)
    {
        if(!empty($formData))
        {
            $userId = $formData['userId'];
            $queryString = "select * FROM `ac_subscription` Where ac_userId = {$userId} Order BY ac_planId DESC Limit 1";
            $plansQuery = $this->db->query($queryString);
            $subscription = $plansQuery->row();
            if(isset($subscription) && !empty($subscription)){
                $this->db->select('ac_planId, is_monthly, is_yearly, duration');
                $this->db->where('ac_planId', $formData['planId']);
                $this->db->where('is_active', '1');
                $query2 = $this->db->get('ac_plans');
                $plan = $query2->row();
                if(isset($plan) && !empty($plan)){
                    $subId = $subscription->ac_subscriptionId;
                    $updateRecord = "update ac_subscription set is_active = 0 WHERE ac_subscriptionId = {$subId}";
                    $queryUpdate = $this->db->query($updateRecord);

                    $durationCount = $plan->duration;
                    $start = date('Y-m-d H:i:s');

                    if($plan->is_monthly == 1){
                        $duration = "+".$durationCount." months";
                    }else if($plan->is_yearly == 1){
                        $duration = "+".$durationCount." year";
                    }else{
                        $duration = "+0 months";
                    }
                    $end = date('Y-m-d H:i:s', strtotime("{$duration}", strtotime($start)));

                    $currentSubEnd = date('Y-m-d H:i:s', strtotime($subscription->end));
                    if($currentSubEnd > $start){
                        $startDate = date_create($start);
                        $endDate = date_create($currentSubEnd);
                        $diff=date_diff($startDate,$endDate);
                        $diffDays = $diff->format("%R%a days");
                        $end = date('Y-m-d H:i:s', strtotime("{$diffDays}", strtotime($end)));
                    }
                    $data = array(
                        'ac_userId'     => $userId,
                        'ac_planId'     => $plan->ac_planId,
                        'start'         => $start,
                        'end'           => $end,
                        'is_active'     => "1",
                        'created_by'    => 'admin',
                        'method'        => $formData['method'],
                        'comment'       => $formData['comment']
                    );
                
                    $userAdd = $this->db->insert('ac_subscription', $data);
                    return TRUE;
                }
            }
        }
        else
        {
            return FALSE;
        }       
    }

	public function update_crop_data($formData)
    {
        if(!empty($formData))
        {
			$this->db->select('ac_crop_familyId, max_cultivation_limit');
            $this->db->where('ac_crop_familyId', $formData['crop_family_id']);
            $query2 = $this->db->get('ac_crop_family');
            $plan = $query2->row();
            if(isset($plan) && !empty($plan)){
				/*$data = array(
					'crop_family_id'   => $formData['family'],
					'name_en'   => $formData['name_en'],
					'name_fr'   => $formData['name_fr'],
					'name_it'   => $formData['name_it'],
					'name_de'   => $formData['name_de'],
					'max_cultivation_limit'   => $plan->max_cultivation_limit,
					'color'     => $formData['color']
                );*/
                $formData['max_cultivation_limit'] = $plan->max_cultivation_limit;
				$this->db->where('ac_cropId', $formData['ac_cropId']);
				return $this->db->update('ac_crop', $formData);
			}else{
				return '';
			}
        }
        else
        {
            return FALSE;
        }       
    }

    public function update_user_data($formData)
    {
        if(!empty($formData))
        {
            $data = array(
                'name'   => $formData['name'],
                'email'   => $formData['email'],
                'company'   => $formData['company'],
                'phone'   => $formData['phone'],
                'street_name'   => $formData['streetName'],
                'postal_code'     => $formData['postalCode'],
                'city'     => $formData['city'],
                'country'     => $formData['country']
            );

            $this->db->where('ac_userId', $formData['userId']);
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    public function delete_crop($formData)
    {
        if(!empty($formData))
        {
            $this->db->where('ac_cropId', $formData['cropId']);
            $this->db->delete('ac_crop');

			$this->db->where('crop_id', $formData['cropId']);
            $this->db->delete('ac_user_crop_color');

			return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }

	public function add_crop_data($formData)
    {
        if(!empty($formData))
        {

            /*$data = array(
                'name_en'   => $formData['name_en'],
				'name_de'   => $formData['name_de'],
				'name_fr'   => $formData['name_fr'],
				'name_it'   => $formData['name_it'],
                'crop_family_id'   => $formData['family'],
                'color'   => $formData['color']
            );*/
			$userAdd = $this->db->insert('ac_crop', $formData);
			return TRUE;
        }
        else
        {
            return TRUE;
        }       
    }

    public function update_user_profile($formData, $user_id)
    {
        if(!empty($user_id) && !empty($formData))
        {
            $data = array(
                'name'   => $formData['name'],
                'email'   => $formData['email'],
                'company'   => $formData['company'],
                'language'   => $formData['language'],
                'phone'   => $formData['phone'],
                'street_name'   => $formData['streetName'],
                'postal_code'     => $formData['postalCode'],
                'city'     => $formData['city'],
                'country'     => $formData['country']
            );

            $this->db->where('ac_userId', $user_id);
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    public function update_image_name($img_name, $user_id)
    {
        if(!empty($img_name) && !empty($user_id))
        {           
            $data = array(
                'profile_picture'  => $img_name
            );
            
            $this->db->where('ac_userId', $user_id);          
            $this->db->update('ac_user', $data);

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function check_old_password($user_id, $password)
    {
        if(!empty($user_id) && !empty($password))
        {
            $this->db->where('ac_userId', $user_id);
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password, $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    return $user;
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }       
    }

    public function update_password_by_id($user_id, $password)
    {
        if(!empty($user_id) && !empty($password))
        {           
            $data = array(
                'password'  => password_hash($password, PASSWORD_BCRYPT)
            );
            
            $this->db->where('ac_userId', $user_id);          
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }
    }

    public function get_crop($ID = '', $symbol)
    {
        if(isset($ID) && $ID > 0)
        {
            $querystring = "select acc.ac_cropId as crop_id, acc.name_en, acc.name_fr, acc.name_it, acc.name_de, acc.color, accf.ac_crop_familyId as crop_family_id, 
				accf.name_".$symbol." as crop_family_name 
                FROM `ac_crop` acc
                Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
                where ac_cropId = ".$ID."
                ORDER BY `crop_family_id` ASC";

            $query = $this->db->query($querystring);

            if($query->num_rows() > 0)
            {
               return $query->row();
            }
            else
            {
               return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function generate_user_token($userId)
    {
        // Generate a random salt
        $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

        // If an error occurred, then fall back to the previous method
        if ($salt === FALSE)
        {
            $salt = hash('sha256', time() . mt_rand());
        }

        $new_token = substr($salt, 0, config_item('rest_key_length'));
        if (!empty($new_token)) {

            $data = array(
                'user_id' => $userId,
                'token'   => $new_token
            ); 
            $userAdd = $this->db->insert('ac_user_token', $data);
            return $new_token;
        }
        else
        {
            return FALSE;
        }       
    }

    public function get_language($ID = '')
    {
        if(isset($ID) && $ID > 0)
        {
            $querystring = "select * FROM `ac_language` acc where acc.ac_languageId = ".$ID." ";
            $query = $this->db->query($querystring);
            if($query->num_rows() > 0)
            {
               return $query->row();
            }
            else
            {
               return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function update_language_data($formData)
    {
        if(!empty($formData))
        {
            $data = array(
                'name' => $formData['name'],
            );
            $this->db->where('ac_languageId', $formData['langId']);
            return $this->db->update('ac_language', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    public function add_language_data($formData)
    {
        if(!empty($formData))
        {
            $data = array(
                'name'   => $formData['name'],
				'symbol' => $formData['symbol'],
            );
			$userAdd = $this->db->insert('ac_language', $data);
			return TRUE;
        }
        else
        {
            return TRUE;
        }       
    }

    public function check_language_unique($name, $symbol)
    {
        if(!empty($name) && !empty($symbol))
        {
            
            $querystring = "SELECT * FROM `ac_language` WHERE `name` = '".$name."' OR `symbol` = '".$symbol."' ";
            $query = $this->db->query($querystring);
            if($query->num_rows() > 0)
            {
               return $query->row();
            }
            else
            {
               return FALSE;
            }
            
        }
        else
        {
            return FALSE;
        }        
    }

    public function publish_language($formData)
    {
        if(!empty($formData))
        {
            $data = array(
                'is_publish' => $formData['is_publish'],
            );
            $this->db->where('ac_languageId', $formData['ac_languageId']);
            return $this->db->update('ac_language', $data);
        }
        else
        {
            return FALSE;
        }       
    }

}

