<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auths extends CI_Model {
	
    public function send_team_member_password_email($email, $languageKey)
    {
        if(!empty($email))
        {
            $resetPasswordToken = base64_encode('##'.$email.'##');

            $link = "https://".HOST_NAME."/dashboard/resetPassword/".$resetPasswordToken;
        
            //Email content
            $htmlContent = '<html><link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css"><body>';
            if($languageKey == "de"){
                $subject = "Schliessen Sie die Registrierung bei Agricontrol ab";
                $htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Schliessen Sie die Registrierung bei Agricontrol ab</h1>';
                $htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Bitte klicken Sie auf den folgenden Link, um Ihr Passwort festzulegen: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
            }else if($languageKey == "fr"){
                $subject = "Terminez votre inscription avec Agricontrol";
                $htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Terminez votre inscription avec Agricontrol</h1>';
                $htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Veuillez cliquer sur le lien suivant pour ensemble votre mot de passe: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
            }else{
                $subject = "Complete Signup with Agricontrol";
                $htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Complete Signup with Agricontrol</h1>';
                $htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Please click on the following link to set your password: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
            }
            $htmlContent .= '</body></html>';

            // $headers = "From:info@agricontrol.app\r\n";
            //$headers = 'FROM: Agricontrol <info@agricontrol.app>' . "\r\n";
        //	$headers .= 'MIME-Version: 1.0'. "\r\n";
            //$headers .= 'Content-type: text/html; charset=UTF-8'. "\r\n";
            
            $headers = 'From: Agricontrol <info@agricontrol.app>'. "\r\n";
            $headers .= 'Reply-To: Help Agricontrol <help@agricontrol.app>' . "\r\n" ;
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= "X-Priority: 3" . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

            $newsub = '=?UTF-8?B?'.base64_encode($subject).'?=';
        
            //mail($email, $newsub, $htmlContent, $headers);

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    public function add_team_member_data($formData)
    {
        if(!empty($formData))
        {
			//$languageKey = 3;
			$querystring = "SELECT ac_languageId FROM `ac_language` Where symbol = '".$formData['language']."'";
			$query = $this->db->query($querystring);
			$language = $query->row();
			if(isset($language) && !empty($language)){
				$languageKey = $language->ac_languageId;
			}
            $data = array(
                'name'        => $formData['name'],
				'email'       => $formData['email'] ,
				'is_verified' => 1 ,
				'is_parent'   => $formData['userID'],
				'language'    => $languageKey,
                'company'     => $formData['company']
            );
            $userAdd = $this->db->insert('ac_user', $data); 
			return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }
	public function getAllteam_member()
    {
        $userID = $this->api_token->ac_userId;
        $querystring = "SELECT * FROM ac_user WHERE is_parent =". $userID ." AND is_deleted = 0";
        $query = $this->db->query($querystring);
        if($query->num_rows() > 0)
        {
           return $query->result();
        }
        else
        {
           return FALSE;
        }
    }
    public function delete_user($id)
    {
        if($id)
        {
			$this->db->where('user_id', $id);
            $this->db->delete('ac_user_token');

            $this->db->where('ac_userId', $id);
            return $this->db->delete('ac_user');
        }
        else
        {
           return FALSE;
        }
    } 

    public function generate_token()
    {
        // Generate a random salt
        $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

        // If an error occurred, then fall back to the previous method
        if ($salt === FALSE)
        {
            $salt = hash('sha256', time() . mt_rand());
        }

        $new_token = substr($salt, 0, config_item('rest_key_length'));

        if (!empty($new_token)) {
            return $new_token;
        }
        else
        {
            return FALSE;
        }       
    }

    public function checkSubscription($userID){
        $updateRecord = 'update ac_subscription set is_active = 0 WHERE DATE_ADD(end, INTERVAL 7 DAY) < NOW()';
        $query = $this->db->query($updateRecord);

        $this->db->select('ac_subscriptionId');
        $this->db->where('ac_userId', $userID);
        $this->db->where('is_active', '1');
        $query = $this->db->get('ac_subscription');
        $subs = $query->row();
        if(isset($subs) && !empty($subs)){
            return TRUE;
        }
        return FALSE;
    }

    public function get_login($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->select('ac_userId, company, profile_picture, language, password,is_verified,is_deleted,delete_date', 'is_parent');  //ashish
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_verified', '1');
            ///$this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password, $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    unset($user->password);
                    $this->db->select('symbol');
                    $this->db->where('ac_languageId',$user->language);
                    $query2= $this->db->get('ac_language');
                    $language = $query2->row();
                    if(isset($language) && !empty($language)){
                        $user->languageKey = $language->symbol;
                    }

                    $this->db->select('start, end');
                    $this->db->where('ac_userId',$user->ac_userId);
                    $this->db->where('is_active','1');
                    $query3= $this->db->get('ac_subscription');
                    $subscription = $query3->row();
                    if(isset($subscription) && !empty($subscription)){
                        $user->subscription_start = $subscription->start;
                        $user->subscription_end = $subscription->end;
                        $user->accountActivated = TRUE;
                    }else{
                        $user->accountActivated = FALSE;
                    }

                    return $user;
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }       
    }

    public function check_login_count($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password,  $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    return $query->num_rows();
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }   
    }

    public function check_login_verified($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_verified', 1);
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            return $query->num_rows();
        }
        else
        {
            return FALSE;
        }       
    }

    public function add_user($formData)
    {
        $name        = $formData['name'];
        $email       = $formData['email'];
        $password    = $formData['password'];
        $company     = $formData['company'];
		$languageKey = $formData['languageKey'];

        $password = password_hash($password, PASSWORD_BCRYPT);
        
        $data = array(
            'name'             => $name,
            'email'            => $email,
            'password'         => $password,
            'company'		   => $company,
            'language'		   => $languageKey,
            'example_field_status' => 1,
            'example_culture_1'	   => 1,
            'example_culture_2'	   => 1
        );
        
        $userAdd = $this->db->insert('ac_user', $data);
        if ($userAdd) 
        {
            $userID = $this->db->insert_id();
            $this->db->select('ac_planId, plan, is_monthly, is_yearly, duration');
            $this->db->where('is_active', '1');
            $this->db->where('amount',0);
            $plansQuery = $this->db->get('ac_plans');
            $plans = $plansQuery->row();
            if(isset($plans) && !empty($plans)){
                $duration = "";
                if($plans->is_monthly == 1){
                    $duration = "+".$plans->duration." months";
                }else if($plans->is_yearly == 1){
                    $duration = "+".$plans->duration." years";
                }else{
                    $duration = "+0 months";
                }
                $start = date('Y-m-d H:i:s');

                $data = array(
                    'ac_userId'     => $userID,
                    'ac_planId'     => $plans->ac_planId,
                    'start'         => $start,
                    'end'           => date('Y-m-d H:i:s', strtotime("{$duration}", strtotime($start))),
                    'is_active'     => "1",
                    'created_by'    => 'admin',
                    'method'        => 'Trial',
                    'comment'       => 'trial package activated'
                );
            
                $userAdd = $this->db->insert('ac_subscription', $data);
            }
            //return array('userID' => $userID, 'mail_sent' => TRUE);

            //// start master list entry ///
            $machine = array(
                'user_id'      => $userID,
                'machine_name' => 'Amazone 3000',
                'is_example'   => 1,
            );
            $this->db->insert('ac_machine', $machine);

            $soil = array(
                'user_id'    => $userID,
                'soil_name'  => 'Harvesting',
                'is_example' => 1,
            );
            $this->db->insert('ac_soil_cultivation', $soil);

            $pest = array(
                'user_id'    => $userID,
                'pests_name' => 'Pest',
                'is_example' => 1,
            );
            $this->db->insert('ac_pests_disease', $pest);

            $fertilizer = array(
                'user_id'         => $userID,
                'fertilizer_name' => 'Example Fertilizer',
                'unit'            => 'kg',
                'nitrogen'        => '50',
                'phosphorus'      => '20',
                'potassium_oxide' => '10',
                'magnesium'       => '10',
                'is_example'      => 1,
            );
            $this->db->insert('ac_fertilizer', $fertilizer);

            $pesticides = array(
                'user_id'        => $userID,
                'pesticide_name' => 'Plant Protection Product',
                'unit'           => 'kg',
                'type_id'        => '1',
                'waiting_days'   => '30',
                'is_example'     => 1,
            );
            $this->db->insert('ac_pesticides', $pesticides);

            $color = array(
                'user_id'    => $userID,
                'crop_id' => '1',
                'color' => '#fff200',
            );
            $this->db->insert('ac_user_crop_color', $color);

            $color = array(
                'user_id'    => $userID,
                'crop_id' => '3',
                'color' => '#ffd300',
            );
            $this->db->insert('ac_user_crop_color', $color);
            //// end master list entry ///

            $is_sent = $this->sendConfirmationEmail($email, null);

            if($is_sent){
                 return array('userID' => $userID, 'mail_sent' => TRUE);
            }else{
                return array('userID' => $userID, 'mail_sent' => FALSE);
            }
        }
        else
        {
            return FALSE;
        }
    }

	public function getAllFaq()
    {
        $query = $this->db->get('ac_faq');

        if($query->num_rows() > 0)
        {
           return $query->result();
        }
        else
        {
           return FALSE;
        }
    }

    public function get_user($ID = '')
    {
        if(isset($ID) && $ID > 0)
        {
			$queryString = "select *, acs.start as subscription_start, acs.suggestion_twoweeks, acs.suggestion_fourweeks, acs.trial_alert, ap.plan, acs.end as subscription_end FROM `ac_user` acu Inner join ac_subscription acs ON acs.ac_userId = acu.ac_userID Inner join ac_plans ap ON ap.ac_planId = acs.ac_planId WHERE acu.ac_userID = {$ID} AND acu.is_deleted = 0 Order by acs.ac_subscriptionId DESC LIMIT 1";
            $query = $this->db->query($queryString);
			if(empty($query->result())){
				$queryString = "select acu.name,acu.email,acu.language,acu.is_parent,acp.company, acp.phone, acp.street_name,acp.postal_code,acp.city,acp.country, acs.start as subscription_start, acs.suggestion_twoweeks, acs.suggestion_fourweeks, acs.trial_alert, ap.plan, acs.end as subscription_end FROM `ac_user` acu Inner join ac_user acp ON acp.ac_userId = acu.is_parent Inner join ac_subscription acs ON acs.ac_userId = acu.is_parent Inner join ac_plans ap ON ap.ac_planId = acs.ac_planId WHERE acu.ac_userID = {$ID} AND acu.is_parent != 1 Order by acs.ac_subscriptionId DESC LIMIT 1";
				$query = $this->db->query($queryString);
				return $query->row();
			} else {
				return $query->row();
			}       
		}
        else
        {
            return FALSE;
        }
    }

    public function get_user_by_email($email = '')
    {
        if(isset($email) && $email != '')
        {
			$querystring = "SELECT lang.symbol as languageKey, user.* FROM `ac_user` user INNER JOIN ac_language lang ON lang.ac_languageId = user.language Where user.email = '".$email."' AND is_deleted = 0 AND user_type = 2";
			$query = $this->db->query($querystring);
            //$this->db->select('ac_userId, company, profile_picture, language');
            //$this->db->where('email', $email);
            //$this->db->where('is_deleted', '0');
            //$this->db->where('user_type', '2');
            //$query = $this->db->get('ac_user');
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function check_user_exist($email = '')
    {
        if(!empty($email))
        {
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            return $query->num_rows();
        }
        else
        {
            return FALSE;
        }       
    }

    public function check_user_verified($email = '')
    {
        if(!empty($email))
        {
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_deleted', '0');
            $this->db->where('is_verified', 1);
            $query = $this->db->get('ac_user');
            return $query->num_rows();
        }
        else
        {
            return FALSE;
        }       
    }

    public function verify_user($email = '', $token)
    {
        if(!empty($email))
        {
            $data = array(
                'is_verified' => 1,
            );
            $this->db->where('email', $email);
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    public function add_user_token($userId = '', $token = NULL)
    {
        if(!empty($userId))
        {
            $data = array(
                'user_id'     => $userId,
                'token'     => $token
            );                
            $userAdd = $this->db->insert('ac_user_token', $data);
            return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }

    public function delete_user_token($userId = '', $token = NULL)
    {
        if(!empty($userId) && !empty($token))
        {
            $this->db->where('user_id', $userId);            
            $this->db->where('token', $token);            
            $this->db->delete('ac_user_token');

            return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }

    public function check_user_by_eamilId($email)
    {
        if(!empty($email))
        {
            $this->db->where('email', $email);
            $this->db->where('user_type', '2');
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');

            return $query->num_rows();
        }
        else
        {
            return FALSE;
        }        
    }

    public function check_email_unique($email, $user_id)
    {
        if(!empty($email) && !empty($user_id))
        {
            $this->db->where('email', $email);
            $this->db->where('ac_userId !=', $user_id);
            $query = $this->db->get('ac_user');

            if($query->num_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }        
    }

    public function check_user_by_token($token)
    {
        if(!empty($token))
        {
            $this->db->where('token', $token);
            $query = $this->db->get('ac_user_token');

            return $query->num_rows();
        }
        else
        {
            return FALSE;
        }        
    }

    public function send_reset_password_email($email, $language)
    {
        if(!empty($email))
        {
            $resetPasswordToken = base64_encode('##'.$email.'##');

            $link = "https://".HOST_NAME."/dashboard/resetPassword/".$resetPasswordToken;

            //Email content
            $htmlContent = '<html><link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css"><body>';

			if(!empty($language) && $language == 'de'){
				$subject = 'Passwort zurücksetzen';
				$htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Passwort zur&uuml;cksetzen</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Bitte klicken Sie auf den folgenden Link, um Ihr Passwort zu erneuern: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}else if(!empty($language) && $language == 'fr'){
				$subject = 'Réinitialiser le mot de passe';
				$htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">R&#233;initialiser le mot de passe</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Veuillez cliquer sur le lien suivant pour renouveler votre mot de passe: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}else{
				$subject = 'Reset Password';
				$htmlContent = '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Reset Password</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Please click on the following link to set a new password : <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}
            $htmlContent .= '</body></html>';
           // $headers = "From:info@agricontrol.app\r\n";
            //$headers = 'FROM: Agricontrol <info@agricontrol.app>'. "\r\n";
            //$headers .= 'Content-Type: text/html; charset=UTF-8'. "\r\n";
            //$headers .= 'MIME-Version: 1.0'. "\r\n";

			$headers = 'From: Agricontrol <info@agricontrol.app>'. "\r\n";
            $headers .= 'Reply-To: Help Agricontrol <help@agricontrol.app>' . "\r\n" ;
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= "X-Priority: 3" . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

			$newsub = '=?UTF-8?B?'.base64_encode($subject).'?=';
            
            mail($email, $newsub, $htmlContent, $headers);

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function update_password($email, $password)
    {
        if(!empty($email) && !empty($password))
        {           
            $data = array(
                'password'  => password_hash($password, PASSWORD_BCRYPT)
            );
            
            $this->db->where('email', $email);          
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }
    }

    public function update_password_by_id($user_id, $password)
    {
        if(!empty($user_id) && !empty($password))
        {           
            $data = array(
                'password'  => password_hash($password, PASSWORD_BCRYPT)
            );
            
            $this->db->where('ac_userId', $user_id);          
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }
    }

	public function adminSubscriptionNotification($ID)
    {
        if(!empty($ID))
        {
			//$queryString = "select * FROM `ac_user` WHERE ac_userId = {$ID}"; 
            //$query = $this->db->query($queryString);
			//$user = $query->row();
			$user = $this->get_user($ID);

			if(!empty($user)){
				$htmlContent = '<html><link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css"><body>';
				$htmlContent .= '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">User Upgrade Subscription</h1>';
				$htmlContent .= '<table><tr><td>Name : </td><td><b>'.$user->name.'</b></td></tr><tr><td>Phone : </td><td><b>'.$user->phone.'</b></td></tr><tr><td>Street : </td><td><b>'.$user->street_name.'</b></td></tr><tr><td>Postal Code : </td><td><b>'.$user->postal_code.'</b></td></tr><tr><td>City : </td><td><b>'.$user->city.'</b></td></tr><tr><td>Country : </td><td><b>'.$user->country.'</b></td></tr><tr><td>Email : </td><td><b>'.$user->email.'</b></td></tr><tr><td>Payment Method : </td><td><b>'.$user->paymentOption.'</b></td></tr></table>';
				$htmlContent .= '</body></html>';

				$subject = "Upgrade Subscription";  
				
				$headers = 'From: Agricontrol <info@agricontrol.app>'. "\r\n";
				$headers .= 'Reply-To: Help Agricontrol <help@agricontrol.app>' . "\r\n" ;
				$headers .= 'MIME-Version: 1.0' . "\r\n";
				$headers .= "X-Priority: 3" . "\r\n";
				$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

				if(mail('help@agricontrol.app', $subject, $htmlContent,$headers)) {
					return TRUE;
				} 
				else 
				{
					return FALSE;
				}
			}
        }
        else
        {
            return FALSE;
        }
    }

    public function sendConfirmationEmail($email = '', $languageKey)
    {
        if(!empty($email))
        {
			if($languageKey == null){
				$querystring = "SELECT lang.symbol FROM `ac_user` user INNER JOIN ac_language lang ON lang.ac_languageId = user.language Where user.email = '".$email."'";
				$query = $this->db->query($querystring);
				$language = $query->row();
				if(isset($language) && !empty($language)){
					$languageKey = $language->symbol;
				}
			}

            $confirmEmailToken = base64_encode('##'.$email.'##');
            $link = "https://".HOST_NAME."/dashboard/confirmEmail/".$confirmEmailToken;
            $htmlContent = '<html><link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css"><body>';

			if(!empty($languageKey) && $languageKey == 'de'){
				$subject = "E-Mail-Bestätigung";
				$htmlContent .= '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">E-Mail-Bestätigung</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Bitte klicken Sie auf den folgenden Link, um Ihren Account zu aktivieren und Ihre E-Mail-Adresse zu bestätigen: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}else if(!empty($languageKey) && $languageKey == 'fr'){
				$subject = "confirmation de e-mail";
				$htmlContent .= '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">confirmation de l\'adresse e-mail</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Veuillez cliquer sur le lien suivant pour activer votre compte et confirmer votre adress e-mail <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}else{
				$subject = "Email Confirmation";
				$htmlContent .= '<h1 style="font-family: \'Source Sans Pro\', sans-serif;">Email Confirmation</h1>';
				$htmlContent .= '<p style="font-family: \'Source Sans Pro\', sans-serif;">Please click on the following link to activate your account and confirm your email address: <a href="'.$link.'" target="_blank" >'. $link .'</a></p>';
			}
            
			$htmlContent .= '</body></html>';
			$headers = 'From: Agricontrol <info@agricontrol.app>'. "\r\n";
            $headers .= 'Reply-To: Help Agricontrol <help@agricontrol.app>' . "\r\n" ;
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= "X-Priority: 3" . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";   
			if(mail($email, $subject, $htmlContent,$headers)) {
				return TRUE;
			} 
			else 
			{
				return FALSE;
			}
        }
        else
        {
            return FALSE;
        }
    }

    public function update_user_profile($formData, $user_id)
    {
        if(!empty($user_id) && !empty($formData))
        {
			$company = array(
            'company'   => $formData['company']
			);
			$this->db->where('is_parent', $user_id);
			$this->db->update('ac_user', $company);

            $data = array(
                'name'   => $formData['name'],
                'email'   => $formData['email'],
                'company'   => $formData['company'],
                'language'   => $formData['language'],
                'phone'   => $formData['phone'],
                'street_name' => $formData['streetName'],
                'postal_code' => $formData['postalCode'],
                'city'     => $formData['city'],
                'country'     => $formData['country']
            );
            $this->db->where('ac_userId', $user_id);
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    public function update_image_name($img_name, $user_id)
    {
        if(!empty($img_name) && !empty($user_id))
        {           
            $data = array(
                'profile_picture'  => $img_name
            );
            
            $this->db->where('ac_userId', $user_id);          
            $this->db->update('ac_user', $data);

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function check_old_password($user_id, $password)
    {
        if(!empty($user_id) && !empty($password))
        {
            $this->db->where('ac_userId', $user_id);
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                $isPasswordCorrect = password_verify($password, $user->password);
                if(isset($isPasswordCorrect) && $isPasswordCorrect == 1){
                    return $user;
                }else{
                    return FALSE;    
                }
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }       
    }

    public function check_login($formData)
    {
        $email = $formData['email'];
        $password = $formData['password'];

        if(!empty($email) && !empty($password))
        {
            $this->db->where('email', $email);
            $this->db->where('password', $password);
            $this->db->where('user_type', '2');
            $this->db->where('is_deleted', '0');
            $query = $this->db->get('ac_user');
            $user = $query->row();

            if(isset($user) && !empty($user)){
                return $user;
            }else{
                return FALSE;    
            }            
        }
        else
        {
            return FALSE;
        }   
    }

    public function generate_user_token($userId)
    {
        // Generate a random salt
        $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
        // If an error occurred, then fall back to the previous method
        if ($salt === FALSE)
        {
            $salt = hash('sha256', time() . mt_rand());
        }
        $new_token = substr($salt, 0, config_item('rest_key_length'));
        if (!empty($new_token)) {
            $data = array(
                'user_id' => $userId,
                'token'   => $new_token
            );                
            $userAdd = $this->db->insert('ac_user_token', $data);
            return $new_token;
            //return TRUE;
        }
        else
        {
            return FALSE;
        }       
    }

    public function delete_user_account($user_id,$user_plan)
    {
        if($user_id)
        {
            if($user_plan == 'Trial'){
                /*$this->db->where('user_id', $user_id);
                $this->db->delete('ac_user_token');
    
                $this->db->where('ac_userId', $user_id);
                return $this->db->delete('ac_user');
                */
                $data = array(
                    'is_deleted'  => 1,
                );
                $this->db->where('ac_userId', $user_id);
                return $this->db->update('ac_user', $data);
            }else{
                $data = array(
                    'is_deleted'  => 1,
                    'delete_date' => date('Y-m-d H:i:s')
                );
                $this->db->where('ac_userId', $user_id);
                return $this->db->update('ac_user', $data);
            }
        }
        else
        {
           return FALSE;
        }
    }

    public function restore_account($user_id)
    {
        if(!empty($user_id))
        {
			$data = array(
                'is_deleted'  => 0,
                'delete_date' => ''
            );
            $this->db->where('ac_userId', $user_id);
            return $this->db->update('ac_user', $data);
        }
        else
        {
            return FALSE;
        }       
    }

    // function get_language() {

    //     $this->db->select('language, symbol');
    //     $this->db->from('ac_user');
    //     $this->db->join('ac_language', 'T1.language = T2.ac_languageId'); 
    //     $this->db->where('ac_languageId', $name);
    //     $query = $this->db->get();
    //     return $query->result();

    // }

}

