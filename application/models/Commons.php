<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Commons extends CI_Model
{

    public function getAllPlans()
    {
        $query = $this->db->get('ac_plans');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function cleanDatabase()
    {
        $queryString = "select ac_userId, max(end) as maxDate, method from ac_subscription where method = 'trial' and is_active = 0 AND end <= NOW()-INTERVAL 2 MONTH GROUP by ac_userId";
        $plansQuery = $this->db->query($queryString);
        foreach ($plansQuery->result() as $row) {
            $fields = "select * FROM `ac_field` WHERE `user_id` = {$row->ac_userId}";
            $fields = $this->db->query($fields);
            foreach ($fields->result() as $fieldrow) {
                $this->db->where('field_id', $fieldrow->ac_fieldId);            
                $this->db->delete('ac_culture');
                $this->db->where('ac_fieldId', $fieldrow->ac_fieldId);            
                $this->db->delete('ac_field');
            }
            $this->db->where('user_id', $row->ac_userId);            
            $this->db->delete('ac_user_crop_color');
            $this->db->where('user_id', $row->ac_userId);            
            $this->db->delete('ac_user_token');
            $this->db->where('ac_userId', $row->ac_userId);            
            $this->db->delete('ac_user');
        }

        $queryString = "select ac_userId, max(end) as maxDate, method from ac_subscription where method = 'cash' and is_active = 0 AND end <= NOW()-INTERVAL 6 MONTH GROUP by ac_userId";
        $plansQuery = $this->db->query($queryString);
        foreach ($plansQuery->result() as $row) {
            $fields = "select * FROM `ac_field` WHERE `user_id` = {$row->ac_userId}";
            $fields = $this->db->query($fields);
            foreach ($fields->result() as $fieldrow) {
                $this->db->where('field_id', $fieldrow->ac_fieldId);            
                $this->db->delete('ac_culture');
                $this->db->where('ac_fieldId', $fieldrow->ac_fieldId);            
                $this->db->delete('ac_field');
            }
            $this->db->where('user_id', $row->ac_userId);            
            $this->db->delete('ac_user_crop_color');
            $this->db->where('user_id', $row->ac_userId);            
            $this->db->delete('ac_user_token');
            $this->db->where('ac_userId', $row->ac_userId);            
            $this->db->delete('ac_user');
        }

        return TRUE;
    }

    public function getAllFaq($lang)
    {

		$querystring = "SELECT ac_faq_id, parent_id, route, title_".$lang." as title, content_".$lang." as content, is_active FROM ac_faq";

        $query = $this->db->query($querystring);
        //$query = $this->db->get('ac_faq');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function getAllLanguage()
    {
        $query = $this->db->get('ac_language');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function upgrade_user_plan($formData)
    {
        if (!empty($formData)) {
            $userId = $formData['userId'];
            $queryString = "select * FROM `ac_subscription` Where ac_userId = {$userId} Order BY ac_planId DESC Limit 1";
            $plansQuery = $this->db->query($queryString);
            $subscription = $plansQuery->row();
            if (isset($subscription) && !empty($subscription)) {
                $this->db->select('ac_planId, is_monthly, is_yearly, duration');
                $this->db->where('ac_planId', $formData['planId']);
                $this->db->where('is_active', '1');
                $query2 = $this->db->get('ac_plans');
                $plan = $query2->row();
                if (isset($plan) && !empty($plan)) {
                    $subId = $subscription->ac_subscriptionId;
                    $updateRecord = "update ac_subscription set is_active = 0 WHERE ac_subscriptionId = {$subId}";
                    $queryUpdate = $this->db->query($updateRecord);

                    $durationCount = $plan->duration;
                    $start = date('Y-m-d H:i:s');

                    if ($plan->is_monthly == 1) {
                        $duration = "+" . $durationCount . " months";
                    } else if ($plan->is_yearly == 1) {
                        $duration = "+" . $durationCount . " year";
                    } else {
                        $duration = "+0 months";
                    }
                    $end = date('Y-m-d H:i:s', strtotime("{$duration}", strtotime($start)));

                    $currentSubEnd = date('Y-m-d H:i:s', strtotime($subscription->end));
                    if ($currentSubEnd > $start) {
                        $startDate = date_create($start);
                        $endDate = date_create($currentSubEnd);
                        $diff = date_diff($startDate, $endDate);
                        $diffDays = $diff->format("%R%a days");
                        $end = date('Y-m-d H:i:s', strtotime("{$diffDays}", strtotime($end)));
                    }
                    $data = array(
                        'ac_userId'     => $userId,
                        'ac_planId'     => $plan->ac_planId,
                        'start'         => $start,
                        'end'           => $end,
                        'is_active'     => "1",
                        'created_by'    => 'adddddmin',
                        'method'        => $formData['method'],
                        'comment'       => $formData['comment'],
                        'paymentOption' => $formData['paymentOption']
                    );

                    $userAdd = $this->db->insert('ac_subscription', $data);
                    return TRUE;
                }
            }
        } else {
            return FALSE;
        }
    }

    public function getLanguageById($languageId)
    {
        $this->db->where('ac_languageId', $languageId);
        $query = $this->db->get('ac_language');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function updateSuggestionStatus($subscriptionId, $weeks, $type)
    {
        if (!empty($subscriptionId)) {
            if ($type == "subscription") {
                if ($weeks == 2) {
                    $data = array(
                        'suggestion_twoweeks'   => 1
                    );
                } else {
                    $data = array(
                        'suggestion_twoweeks'   => 1,
                        'suggestion_fourweeks'   => 1
                    );
                }
            } else {
                $data = array(
                    'trial_alert'   => 1
                );
            }

            $this->db->where('ac_subscriptionId', $subscriptionId);
            return $this->db->update('ac_subscription', $data);
        } else {
            return FALSE;
        }
    }

    public function getLanguageBySymbol($languagesymbol)
    {
        $this->db->where('symbol', $languagesymbol);
        $query = $this->db->get('ac_language');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function getAlluserCrops($user_id, $symbol)
    {
        $querystring = "select acc.ac_cropId as crop_id, acc.name_" . $symbol . " as crop_name, (CASE WHEN acucc.color IS NULL THEN acc.color ELSE acucc.color END) as color,
			accf.ac_crop_familyId as crop_family_id, accf.name_" . $symbol . " as crop_family_name 
            FROM `ac_crop` acc
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
            Left Join ac_user_crop_color acucc on acucc.crop_id = acc.ac_cropId AND acucc.user_id = " . $user_id . " 
            ORDER BY `crop_family_id` ASC";

        $query = $this->db->query($querystring);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function getAllCrops($symbol)
    {
        $querystring = "select acc.ac_cropId as crop_id, acc.name_" . $symbol . " as crop_name,  acc.*, accf.ac_crop_familyId as crop_family_id, accf.name_" . $symbol . " as crop_family_name,
		CASE WHEN cu.is_delete = false THEN 'false' ELSE 'true' END AS Is_delete
            FROM `ac_crop` acc
            Inner Join ac_crop_family accf on accf.ac_crop_familyId = acc.crop_family_id 
			LEFT JOIN (SELECT DISTINCT crop_id, false as is_delete FROM `ac_culture`) cu ON acc.ac_cropId = cu.crop_id
            ORDER BY acc.`crop_family_id` ASC";

        $query = $this->db->query($querystring);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function getAllCropFamily($symbol)
    {
        $querystring = "select accf.ac_crop_familyId as crop_family_id, accf.name_" . $symbol . " as crop_family_name 
            FROM `ac_crop_family` accf 
            ORDER BY `crop_family_id` ASC";

        $query = $this->db->query($querystring);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function add_user_crop_color($user_id, $crop)
    {
        $crop_id          = $crop->crop_id;
        $color       = $crop->color;

        $data = array(
            'user_id'             => $user_id,
            'crop_id'            => $crop_id,
            'color'         => $color,
        );

        $colorAdd = $this->db->insert('ac_user_crop_color', $data);
        if ($colorAdd) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
